package br.uff.ic.diversao;

import java.util.List;
import java.util.Queue;

import jason.asSemantics.Event;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.IIntentionBase;
import br.uff.ic.uebdi.MainWithConfigParams;
import br.uff.ic.uebdi.UEBDIImpl;
import br.uff.ic.util.TermHelper;

public class DiversaoUEBDI extends UEBDIImpl {
	/*@Override
	public DriveState updateDrive(Drive drive, BeliefBase beliefBase,
			IEmotionBase emotionBase, IIntentionBase intentionBase) {
		
		if("diversao".equals(drive.getDesired().getFunctor())){
			Literal day =beliefBase.contains(Literal.parseLiteral("day(_)")); 
			if(day != null){
				beliefBase.remove(day);
				drive.inc(+1.0);
			}
		}
		
		return super.updateDrive(drive, beliefBase, emotionBase, intentionBase);
	}*/

	//private static final double DEC_EMO_VALUE = 0.1;
	
	private double decEmotion = -1.0;
	
	

	@Override
	public void firstReviewEmotion(List<Literal> listPerc,
			IEmotionBase emotionBase, IIntentionBase intentionBase) {
		
		if(listPerc == null)
			return;
		
		for(String local: new String[]{"cinema", "park"}){
			Literal assault = TermHelper.contain(listPerc, Literal.parseLiteral(String.format("assault(%s)", local)));
			if(assault!=null){
				emotionBase.incIntensity(1, String.format("fear(%s)", local));
				listPerc.remove(assault);
			}
		
		}
	}

	private int lastDayProcessedByEmotion = -1;
	
	@Override
	public void secondReviewEmotion(BeliefBase beliefBase,
			 Queue<Event> events, IEmotionBase emotionBase,
			IIntentionBase intentionBase) {
		getLogger().fine("firstReviewFunction");
	
		Literal lDay = TermHelper.contain(beliefBase, Literal.parseLiteral("day(_)"));		
		if(lDay !=null ){
			int nDay = TermHelper.getInteger(lDay, 0);
			if(lastDayProcessedByEmotion < nDay){
				decFear("park", emotionBase);
				decFear("cinema", emotionBase);		
				lastDayProcessedByEmotion = nDay;
				getLogger().info(
						String.format(
						"Process emotion to day %d. fear(cinema) = %.3f, fear(park) = %.3f",
						nDay, 
						emotionBase.getIntensity("fear(cinema)"),
						emotionBase.getIntensity("fear(park)")
						)
				);
			}
			
		}
	}

	private void decFear(String local, IEmotionBase eb) {
		String name = String.format("fear(%s)", local);
		
		double value;
		double actualIntensity =eb.getIntensity(name); 
		if(actualIntensity -getDecEmotion() > 0){
			value = -getDecEmotion();
		}else{
			value = -actualIntensity;
		}
		eb.incIntensity(value, name);
	}

	
	public double getDecEmotion() {
		if(decEmotion < 0){
			decEmotion = MainWithConfigParams.queryDouble("decEmotion(_)");
		}
		
		return decEmotion;
	}

	public void setDecEmotion(double decEmotion) {
		this.decEmotion = decEmotion;
	}
}
