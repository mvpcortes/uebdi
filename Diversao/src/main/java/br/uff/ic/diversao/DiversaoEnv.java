// Environment code for project Diversao
package br.uff.ic.diversao;

import jason.asSyntax.*;
import jason.environment.*;

import java.io.Console;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import br.uff.ic.uebdi.MainWithConfigParams;
import br.uff.ic.util.TermHelper;

public class DiversaoEnv extends Environment {

    private static final int MAX_DAY = 1000;

	private Random random = new Random();
    
    private int actualDay = 0;
    
    private double assaultTax = 0.3;
    
    private double satCinema = 0.6;   

	double lastCost = -1;
	
	double lastDrive = -1;
	
    /** Called before the MAS execution with the args informed in .mas2j */
    @Override
    public void init(String[] args) {
        super.init(args);
        
        setAssaultTax(MainWithConfigParams.queryDouble("assaultTax(_)"));
        
        setSatCinema(MainWithConfigParams.queryDouble("satCinema(_)"));
        
        getLogger().info(String.format("init environment with assaultTax=%.3f and litSatCinema=%.3f", getAssaultTax(), getSatCinema()));
        
        //inserindo percepções
        addPercept(ASSyntax.createLiteral("day", new NumberTermImpl(actualDay)));
    }

    @Override
    public boolean executeAction(String agName, Structure action) {
       
    	if("status".equals(action.getFunctor())){
        		clearAllPercepts();
        		
    			double cost = TermHelper.getDouble(action, 0);
    			double fearPark = TermHelper.getDouble(action, 1);
    			double fearCinema = TermHelper.getDouble(action, 2);
    			double drive = TermHelper.getDouble(action, 3);
    			lastCost = cost;
    			lastDrive = drive;
    			//getLogger().info(String.format("day(%d)", actualDay));
        		getLogger().info(String.format("status of agent %s: %d\t%d\t%.3f\t%.3f\t%.3f", agName, actualDay, (int)cost, fearPark, fearCinema, drive) );        		
        		actualDay++;
        		if(actualDay >= MAX_DAY){
        			try {
        				this.getEnvironmentInfraTier().getRuntimeServices().stopMAS();
        			} catch (Exception e) {
        				e.printStackTrace();
        			}
        		}
        		addPercept(ASSyntax.createLiteral("day", new NumberTermImpl(actualDay)));
        		return true;
    	}else if("goto".equals(action.getFunctor())){
    		Atom dest = (Atom)((Structure)action).getTerm(0);
    		if("cinema".equals(dest.getFunctor())){
    			getLogger().info(String.format("%s goes to the cinema.", agName));
    			addPercept(ASSyntax.createLiteral("satisfation", ASSyntax.createNumber(getSatCinema())));
    			testAndSendAssault(agName, dest, -1);
    			//ok
    		}else if("park".equals(dest.getFunctor())){
    			getLogger().info(String.format("%s walk in the park.", agName));
    			addPercept(ASSyntax.createLiteral("satisfation", ASSyntax.createNumber(1.0)));
    			testAndSendAssault(agName, dest, getAssaultTax());
    		}
    		return true;
    	}else {
    		return false;
    	}
    }

    /**
     * Envia, com uma probabilidade prob, uma mensagem dizendo que o agente foi assaltado e envia o custo do agente.
     * @param percent
     */
    private void testAndSendAssault(String agname, Atom local, double prob) {
    	double value = random.nextDouble();
    	
    	if(value <= prob){
    		addPercept(agname, ASSyntax.createLiteral("assault", local));
    		addPercept(agname, ASSyntax.createLiteral("cost", ASSyntax.createNumber(1.0)) );
    	}
	}

	/** Called before the end of MAS execution */
    @Override
    public void stop() {
    	System.out.printf("%.1f\t%.1f\t%.1f\t%.4f\t%.4f%n", this.getAssaultTax(), this.getDecEmotion(), this.getSatCinema(), lastCost, lastDrive);
        super.stop();
    }
    
    private double getDecEmotion() {
		return MainWithConfigParams.queryDouble("decEmotion(_)");		
	}

	private List<Literal> parseProp(String[] args) {
		List<Literal> array = new ArrayList<Literal>(args.length);
		for (String s : args) {
			Literal l = Literal.parseLiteral(s);
			if (l != null) {
				array.add(l);
			}
		}

		return array;
	}

	public double getAssaultTax() {
		return assaultTax;
	}

	public void setAssaultTax(double assaultTax) {
		this.assaultTax = assaultTax;
	}

	public double getSatCinema() {
		return satCinema;
	}

	public void setSatCinema(double satCinema) {
		this.satCinema = satCinema;
	}

}
