import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class ProcessFile {

	private class Tree{
		public String a, b, c;
		public double assault, insatisfation;
		
		public boolean equals(Object o){
			Tree t = (Tree)o;
			return t.a.equals(a) && t.b.equals(b) && t.c.equals(c);
		}

		public String getKeyName() {
			return String.format("%s\t%s\t%s", a, b,c);
		}
	}
	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		File f = new File("./out_exp.txt");
		Scanner scan = new Scanner(f).useDelimiter("\t");
		
		List<Tree> list = new ArrayList<Tree>();
		ProcessFile pf = new ProcessFile();
		while(scan.hasNext()){
			Tree tree = pf.new Tree();
			
			tree.a = scan.next().trim();
			tree.b = scan.next().trim();
			tree.c = scan.next().trim();
			tree.assault = scan.nextDouble();
			tree.insatisfation = scan.nextDouble();
			//System.out.printf("%s\t%s\t%s\t%.3f\t%.3f%n", tree.a, tree.b, tree.c, tree.assault, tree.insatisfation);
			list.add(tree);
		}
		
		List<MetricCalculator> listAssault 	     = new ArrayList<MetricCalculator>();
		List<MetricCalculator> listInsatisfation = new ArrayList<MetricCalculator>();
		
		Iterator<Tree> itActual = list.iterator();
		Tree prevTree = itActual.next();
		MetricCalculator mcA = new MetricCalculator(prevTree.getKeyName(), ""); listAssault.add(mcA);
		MetricCalculator mcI = new MetricCalculator(prevTree.getKeyName(), ""); listInsatisfation.add(mcI);
		mcA.insertValueToMedia(prevTree.assault);
		mcI.insertValueToMedia(prevTree.insatisfation);
		while(itActual.hasNext()){
			Tree actualTree = itActual.next();
			if(!actualTree.equals(prevTree)){
				mcA = new MetricCalculator(actualTree.getKeyName(), ""); listAssault.add(mcA);
				mcI = new MetricCalculator(actualTree.getKeyName(), ""); listInsatisfation.add(mcI);
			}else{
				mcA.insertValueToMedia(actualTree.assault);
				mcI.insertValueToMedia(actualTree.insatisfation);
			}
			prevTree = actualTree;
		}
		
		///faz o dev
		itActual = list.iterator();
		prevTree = itActual.next();
		int pos = 0;
		mcA = listAssault.get(pos);
		mcI = listInsatisfation.get(pos);
		mcA.insertValueToDev(prevTree.assault);
		mcI.insertValueToDev(prevTree.insatisfation);
		while(itActual.hasNext()){
			Tree actualTree = itActual.next();
			if(!actualTree.equals(prevTree)){
				pos++;
				mcA = listAssault.get(pos);
				mcI = listInsatisfation.get(pos);
			}else{
				mcA.insertValueToDev(actualTree.assault);
				mcI.insertValueToDev(actualTree.insatisfation);
			}
			prevTree = actualTree;
		}
		
		for(int i = 0; i < listAssault.size(); i++){
			mcA = listAssault.get(i);
			mcI = listInsatisfation.get(i);
			System.out.printf("%s\t%.3f\t%.3f\t%.3f\t%.3f%n", mcA.getName(), mcA.getMedia(), mcA.getDev(), mcI.getMedia(), mcI.getDev());
		}
	}

}
