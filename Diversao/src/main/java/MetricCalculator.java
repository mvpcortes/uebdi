

/**
 * Usado para clacular valores de média e desvio de métricas calculadas nos dados da simulação.
 * @author Marcos
 *
 */
public class MetricCalculator{
	private String name;
	private String unit;
	private double inseredMediaValue = 0;
	private double inseredDevValue = 0;
	
	private long countMedia = 0;
	private long countDev = 0;
	
	public MetricCalculator(String _name, String _unit){
		name = _name;
		unit = _unit;
	}
	
	public String getName() {
		return name;
	}
	
	public String getUnit() {
		if(unit != null)
			return unit;
		else
			return "";
	}

	public long getInseredValues() {
		return countMedia;
	}
	
	
	public double getMedia(){
		if(countMedia == 0){
			throw new IllegalStateException("Cannot generate Average of a non insered MetricCalculator (count == 0).");
		}
		return inseredMediaValue/countMedia;
	}
	
	public double getDev(){
		if(countDev <= 0.0){
			throw new IllegalStateException("Cannot generate Dev before insert values again (countDev = 0).");
		}
		
		if(countDev != countMedia){
			throw new IllegalStateException("Cannot generate Dev before insert all values insered in media again (countDev != countMedia)");
		}
		
		if(countDev == 1)
			return 0;
		
		return Math.sqrt(inseredDevValue/((double)(countDev-1)));
	}
	
	/**
	 * Insere um valor para calcular a média
	 */
	public void insertValueToMedia(double value){
		
		countMedia++;
		inseredMediaValue += value;
	}
	
	/**
	 * Insere um valor para calcualr o Desvio padrão
	 */
	public void insertValueToDev(double value){		
		if(countDev == countMedia){
			throw new IllegalStateException("Cannot insert more dev values than media values. (countDev == countMed");
		}
		
		inseredDevValue += Math.pow(value - getMedia(), 2);
		countDev++;
	}

	public double getSum() {
		return inseredMediaValue;
	}

}
