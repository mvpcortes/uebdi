// Agent DiversaoUEBDI in project Diversao

/* Initial beliefs and rules */
drive(diversao, -1, 1, 0, 0).//este drive se chama divers�o, tem limiar inferior igual a -1, limiar superior igual a 1, valor de incremento igual a 0 e valor inicial igual a 0.
emotion(fear(park), 0).
emotion(fear(cinema), 0). 
sumCost(0).
/* Initial goals */

/* Plans */
@day[atomic]
+day(DAY):drive(diversao,LS,US,INC,X)<-
	
	-+drive(diversao,LS,US,INC,X+1);
	.log.log(info, "inc drive: ", drive(diversao,LS,US,INC,X+1));
	.
	
@diversao[atomic]
+!diversao:emotion(fear(cinema), FEAR_CINEMA_INT) & emotion(fear(park), FEAR_PARK_INT)<-
	if(FEAR_CINEMA_INT < FEAR_PARK_INT){
		goto(cinema);
		PLACE = cinema;
	}else{
		goto(park);
		PLACE = park;
	}
	
	//trata assalto
	!tratarAssault(PLACE);
	
	//trata custo
	!tratarCost;
	
	.log.log(info, "sumcost");
	if(not sumCost(_)){ .log.log(info, "error sumcost");}
	
	?sumCost(S);
	.log.log(info, "cinema");
	?emotion(fear(cinema), I_CINEMA);
	.log.log(info, "park");
	?emotion(fear(park), I_PARK);
	.log.log(info, "drive");
	?drive(diversao,_,_,_,DRIVE);
	status(S, I_CINEMA, I_PARK, DRIVE);
.

@tratarAssault[atomic]
+!tratarAssault(PLACE):satisfation(SAT) & drive(diversao,LS,US,INC,X)<-
	if(not cost(_)){
		.log.log(info, "place is ", PLACE, "with SAT = ", SAT);
		.log.log(info, prevX(X), newX(X-SAT));
		-+drive(diversao,LS,US,INC,X-SAT);
	}
	-satisfation(SAT);
	.

@tratarCost[atomic]
+!tratarCost<-
	if(cost(X)){
		.log.log(info, "processing cost");
		if(sumCost(FIRST_S)){
			-+sumCost(FIRST_S+X);
		}else{
			+sumCost(X);
		}
		-cost(X);
	}
	.
/**
  houve um assalto
 
@cost[atomic]
+cost(X):true<-
	if(sumCost(S)){
		-+sumCost(S+X);
	}else{
		+sumCost(X);
	}
	. */