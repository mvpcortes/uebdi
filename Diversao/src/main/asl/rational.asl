// Agent rational in project Diversao

/* Initial beliefs and rules */
drive(diversao, -1, 1, 0, 0).//este drive se chama divers�o, tem limiar inferior igual a -1, limiar superior igual a 1, valor de incremento igual a 0 e valor inicial igual a 0.
sumCost(0).
/* Initial goals */

/* Plans */
@day[atomic]
+day(DAY):drive(diversao,LS,US,INC,X)<-
	
	-+drive(diversao,LS,US,INC,X+1);
	.log.log(info, "inc drive: ", drive(diversao,LS,US,INC,X+1));
	.
	
@diversao[atomic]
+!diversao<-
	
	goto(park);
	PLACE = park;
	
	//trata assalto
	!tratarAssault(PLACE);
	
	//trata custo
	!tratarCost;
	
	.log.log(info, "sumcost");
	if(not sumCost(_)){ .log.log(info, "error sumcost");}
	
	?sumCost(S);
	.log.log(info, "cinema");
	.log.log(info, "park");
	.log.log(info, "drive");
	?drive(diversao,_,_,_,DRIVE);
	status(S, -1, -1, DRIVE);
.

@tratarAssault[atomic]
+!tratarAssault(PLACE)<-
	if(not cost(_)){
		.log.log(info, "place is ", PLACE);
		if(PLACE == park){
			?drive(diversao,LS,US,INC,X);
			.log.log(info, prevX(X), newX(X-1.0));
			-+drive(diversao,LS,US,INC,X-1.0);
		}else{
			if(PLACE == cinema){
				?drive(diversao,LS,US,INC,X);
				.log.log(info, prevX(X), newX(X-0.6));
				-+drive(diversao,LS,US,INC,X-0.6);
			}
		}
	}
	.

@tratarCost[atomic]
+!tratarCost<-
	if(cost(X)){
		.log.log(info, "processing cost");
		if(sumCost(FIRST_S)){
			-+sumCost(FIRST_S+X);
		}else{
			+sumCost(X);
		}
		-cost(X);
	}
	.