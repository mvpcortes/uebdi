package tiroteio.exp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Point;
import java.util.List;

import org.junit.Test;


public class ExperimentTest extends Experiment {

	/**
	 * Testa o alg. de gerar pos aleatória para tamanho vazio.
	 */
	@Test
	public void testGenerateRandomPosToAgentsVoid() {
		List<Point> l = SceneFactory.getRandomPosToAgents(0, 0, 0);
		assertNotNull(l);
		assertTrue(l.size() == 0);
	}
	
	/**
	 * Testa o alg. de gerar pos aleatória para somente um agente em mapa 1 por 1;
	 */
	@Test
	public void testGenerateRandomPosToAgentsOneAgent() {
		List<Point> l = SceneFactory.getRandomPosToAgents(1, 1, 1);
		assertNotNull(l);
		assertTrue(l.size() == 1);
	}
	
	/**
	 * Verifica o arcumento inválido count > w*h
	 */
	@Test
	public void testGenerateRandomPosToAgentsInvalidCount()
	{
		try
		{
			SceneFactory.getRandomPosToAgents(4, 1, 1);
			fail();
		}catch(IllegalArgumentException e)
		{
			
		}
	}

	
	/**
	 * Verifica se as posições não são repetidas
	 */
	@Test
	public void testGenerateRandomPosToAgentsPositions()
	{
		List<Point> l = SceneFactory.getRandomPosToAgents(2, 3, 3);
		
		assertNotNull(l);
		final int sizeList = l.size();
		assertEquals(2, sizeList);
		
		for(int i = 0; i < sizeList; i++)
		{
			Point p = l.get(i);
			for(int j = 0; j < i; j++)
			{
				Point pp = l.get(j);
				assertNotSame(p, pp);
			}
			
			for(int j = i+1; j < sizeList; j++)
			{
				Point pp = l.get(j);
				assertNotSame(p, pp);
				
			}
		}
			
			
	}
}
