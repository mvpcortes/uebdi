package tiroteio.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import jason.asSemantics.Agent;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import br.uff.ic.util.TermHelper;

public class getDirTest {

	static getDir instance = null;
	
	TransitionSystem createTS()
	{
		TransitionSystem ts = mock(TransitionSystem.class);
		when(ts.getLogger()).thenReturn(Logger.getLogger("getDirTest:TransitionSystem"));
		Agent a = createAgent();
		when(ts.getAg()).thenReturn(a);
		//when(ts.getAg().getTS()).thenReturn(ts);
		return ts;
	}
	
	Agent createAgent()
	{
		Agent ag = mock(Agent.class);
		Logger l = Logger.getLogger("getDirTest:Agent");
		when(ag.getLogger()).thenReturn(l);
		return ag;
	}
	
	
	@BeforeClass
	static public void beforeClass()
	{
		instance = new getDir();
	}
	/**
	 * Testes:
	 * * Se aceita um vetor vazio. Esperado (0,0).
	 * * Se faz a soma de vetores contrários
	 * @throws Exception 
	 */
	@Test
	public void testExecuteVazio() throws Exception {
			double expectedX = 0;
			double expectedY = 0;
			
			TransitionSystem ts = createTS();
			Unifier un = new Unifier();
			ListTerm list = ListTermImpl.parseList("[]");
			VarTerm varTermX = new VarTerm("X");
			VarTerm varTermY = new VarTerm("Y");
			Object ok = instance.execute(ts, un, new Term[]{list, varTermX, varTermY});
			assertItens(expectedX, expectedY, un, ok);
	}

	private void assertItens(double expectedX, double expectedY,
			Unifier un, Object ok) 
	{
		VarTerm vtX = new VarTerm("X");
		VarTerm vtY = new VarTerm("Y");
		vtX.apply(un);
		vtY.apply(un);
		assertNotNull(ok);
		assertTrue(ok instanceof Boolean);
		assertTrue(((Boolean)ok));			
		assertEquals(expectedX, TermHelper.toDouble(vtX), 1e-10);
		assertEquals(expectedY, TermHelper.toDouble(vtY), 1e-10);
	}

	/*
	 * * Se faz a média de um vetor
	 */
	@Test
	public void testExecuteUnicoVetor() throws Exception {
			double expectedX = 1;
			double expectedY = 1;
			
			TransitionSystem ts = createTS();
			Unifier un = new Unifier();
			ListTerm list = ListTermImpl.parseList("[en(1, 1 , 1)]");
			VarTerm varTermX = new VarTerm("X");
			VarTerm varTermY = new VarTerm("Y");
			Object ok = instance.execute(ts, un, new Term[]{list, varTermX, varTermY});
			assertItens(expectedX, expectedY, un, ok);
	}
	
	/**
	 * Se faz a soma de dois vetores iguais
	 * @throws Exception 
	 */
	@Test
	public void testExecuteDoisVetoresIguais() throws Exception
	{
		double expectedX = -1;
		double expectedY = 2;
		
		TransitionSystem ts = createTS();
		Unifier un = new Unifier();
		ListTerm list = ListTermImpl.parseList("[aa(-1, 2 ,5), aa(-1, 2 ,5)]");
		
		VarTerm varTermX = new VarTerm("X");
		VarTerm varTermY = new VarTerm("Y");
		
		Object ok = instance.execute(ts, un, new Term[]{list, varTermX, varTermY});
		assertItens(expectedX, expectedY, un, ok);
		
		
		
	}
	
	@Test
	 /* * Se faz a soma de três vetores diferentes/
	  */
	public void testExecuteTrêsVetoresDiferentes() throws Exception
	{
		double expectedX = (-1.0*8.0 + -2.0*5.0 + 2.0*10.0)/(8.0+5.0+10.0);
		double expectedY = (-5.0*8.0 +  2.0*5.0 + 2.0*10.0)/(8.0+5.0+10.0);
		
		TransitionSystem ts = createTS();
		Unifier un = new Unifier();
		ListTerm list = ListTermImpl.parseList("[aa(-1, -5, 8), bb(-2, 2 ,5), cc(2, 2, 10)]");
		
		VarTerm varTermX = new VarTerm("X");
		VarTerm varTermY = new VarTerm("Y");
		Object ok = instance.execute(ts, un, new Term[]{list, varTermX, varTermY});
		assertItens(expectedX, expectedY, un, ok);
	}
	
	/**Caso que deu um erro
	 * X	Y	E
	 * 1	3	50
	 * 3	8	100
	 * 2,333333333	6,333333333	
	 * @throws Exception
	 */
	@Test
	public void testExecuteCasoErro() throws Exception
	{
		double expectedX = 2.33333333333333333333333;
		double expectedY = 6.33333333333333333333333;
		
		TransitionSystem ts = createTS();
		Unifier un = new Unifier();
		ListTerm list = ListTermImpl.parseList("[aa(1, 3,50), bb(3, 8 ,100)]");
		
		VarTerm varTermX = new VarTerm("X");
		VarTerm varTermY = new VarTerm("Y");
		Object ok = instance.execute(ts, un, new Term[]{list, varTermX, varTermY});
		assertItens(expectedX, expectedY, un, ok);
	}
}
