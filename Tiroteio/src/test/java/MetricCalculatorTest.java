import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import tiroteio.MetricCalculator;

public class MetricCalculatorTest {

	@Test
	public void testVoidMCMedia() {
		MetricCalculator mc = new MetricCalculator("a", "");

		try {
			mc.getMedia();
			fail();
		} catch (IllegalStateException is) {

		} catch (Throwable t) {
			fail(t.toString());
		}

	}

	@Test
	public void testVoidMCDev() {
		MetricCalculator mc = new MetricCalculator("a", "");

		try {
			mc.getDev();
			fail();
		} catch (IllegalStateException is) {

		} catch (Throwable t) {
			fail(t.toString());
		}

	}

	@Test
	public void testOneMCMedia() {
		MetricCalculator mc = new MetricCalculator("a", "");

		mc.insertValueToMedia(1.0);
		assertEquals(1.0, mc.getMedia(), 1e-7);

	}

	@Test
	public void testOneMCDevFail() {
		MetricCalculator mc = new MetricCalculator("a", "");

		try {
			mc.insertValueToMedia(1.0);
			mc.getDev();
			fail();
		} catch (IllegalStateException is) {

		} catch (Throwable t) {
			fail(t.toString());
		}

	}

	@Test
	public void testOneMCDev() {
		MetricCalculator mc = new MetricCalculator("a", "");

		mc.insertValueToMedia(1.0);
		mc.insertValueToDev(1.0);
		assertEquals(0.0, mc.getDev(), 1e-7);
	}
	
	private static int[] THREE_DATA = new int[] {1, 2, 3};
	
	@Test
	public void testThreeMCMedia() {
		MetricCalculator mc = new MetricCalculator("a", "");

		double accum = 0.0;
		for(int i : THREE_DATA){
			mc.insertValueToMedia(i);
			accum +=i;
		}
		
		assertEquals(accum/((double)THREE_DATA.length), mc.getMedia(), 1e-7);

	}
	
	@Test
	public void testThreeMCDev() {
		MetricCalculator mc = new MetricCalculator("a", "");

		for(int i : THREE_DATA){
			mc.insertValueToMedia(i);
		}
		
		final double media = mc.getMedia();
		
		double accum = 0;
		for(int i : THREE_DATA){
			mc.insertValueToDev(i);
			accum += Math.pow(i-media, 2);
		}
		
		accum = accum / ((double)(THREE_DATA.length-1));
		double dev = Math.sqrt(accum);
		assertEquals(dev, mc.getDev(), 1e-7);
	}


}
