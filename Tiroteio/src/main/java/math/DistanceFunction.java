// Internal action code for project FoodSimulation

package math;

import jason.JasonException;
import jason.asSemantics.DefaultArithFunction;
import jason.asSemantics.TransitionSystem;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

import java.util.logging.Logger;

import br.uff.ic.util.TermHelper;

/** function that computes the distance between two number */
public class DistanceFunction extends DefaultArithFunction {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5364149298884856872L;
	private Logger logger = Logger.getLogger(DistanceFunction.class.getName());

    
    public String getName()
    {
    	return "math.distance";
    }
    
    public boolean checkArity(int a) {
        return a == 2;
    }
    
    @Override
    public double evaluate(TransitionSystem ts, Term[] args) throws Exception {
        
    	if(args[0].isNumeric() && args[1].isNumeric())
    	{
    		logger.info("get distance from two numbers.");
	    	try {
	            int n1 = (int)((NumberTerm)args[0]).solve();
	            int n2 = (int)((NumberTerm)args[1]).solve();
	            return Math.abs(n1 - n2);
	        } catch (Exception e) {
	            logger.warning("Error in function 'math.distance'! "+e);
	        }
	        return 0;
    	}
    	else
	    {
    		logger.info("get distance from two structures.");
	    	Term a = args[0];
	    	Term b = args[1];
	    	if(!(a.isStructure() && b.isStructure()))
	    	{
	    		throw new JasonException("Invalid input term of Arith function distance");
	    	}	
	    	Structure as = (Structure)a;
	    	Structure bs = (Structure)b;
	    	if(as.getArity() != bs.getArity())
	    	{
	    		throw new JasonException("Invalid input structure with different arity of Arith function distance");
	    	}
	    	//double[] vecValues = new double[as.getArity()];
	    	double accum = 0;
	    	for(int i = 0; i < as.getArity(); i++)
	    	{
	    		double dA = TermHelper.getDouble(as, i);
	    	    double dB = TermHelper.getDouble(bs, i);
	    		accum += Math.pow(dA-dB, 2);
	    	}
	    	return Math.sqrt(accum);	    			
	    }
    }
}
