package tiroteio;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import tiroteio.entity.AgentTiroteio;
import br.uff.ic.util.TimeDateHelper;

public class ModelWriter {

	private Writer out;

	private Logger logger = Logger.getLogger(ModelWriter.class.getName());


	public static String getHeadString(MetricByTeam mt) {
		StringBuilder sb = new StringBuilder();
		sb.append("gpid\t");
		sb.append("team\t");
		for (MetricCalculator mc : mt.getMetrics()) {
			sb.append(" \t");
			sb.append(String.format("%s(%s)\t", mc.getName(), mc.getUnit()));
			sb.append(" \t");
		}
		sb.append(String.format("%n"));
		sb.append("---\t");
		sb.append("---\t");
		for (MetricCalculator mc : mt.getMetrics()) {
			sb.append("Total \t");
			sb.append("Media \t");
			sb.append("Dev \t");
		}
		sb.append(String.format("%n"));
		return sb.toString();
	}

	public ModelWriter(Writer fout) {
		out = fout;
	}

	public ModelWriter(OutputStream fout) {
		out = new OutputStreamWriter(fout);
	}

	void write(final TiroteioModel model) {
		PrintWriter pw = new PrintWriter(out);

		String strDate = TimeDateHelper.getStrNow();
		pw.println(strDate);
		pw.printf("agent\tteam\tenergy\tdied\tkill\tfriendKill\tFriendlyFire\tfriendSurvive\tLifeTime%n");
		final long initTime = model.getInitTime();

		ArrayList<AgentTiroteio> list = new ArrayList<AgentTiroteio>();
		Map<String, Integer> mapAgentSurvive = new HashMap<>();
		
		for (AgentTiroteio a : model.getAgents()) {
			list.add(a);
			Integer survived = mapAgentSurvive.get(a.getTeam());
			if(survived == null)
			{
				survived = 0;
				mapAgentSurvive.put(a.getTeam(), survived);
			}
			if(!a.isDied()){
				survived+=1;
				mapAgentSurvive.put(a.getTeam(), survived);
			}
		}

		Collections.sort(list, new Comparator<AgentTiroteio>() {

			@Override
			public int compare(AgentTiroteio o1, AgentTiroteio o2) {
				int comp = o1.getTeam().compareTo(o2.getTeam());
				if (comp == 0) {
					comp = o1.getName().compareTo(o2.getName());
				}
				return comp;
			}
		});

		logger.info(list.toString());

		Map<String, MetricByTeam> map = new HashMap<String, MetricByTeam>();
		for (AgentTiroteio ag : list) {
			
			Integer survived = mapAgentSurvive.get(ag.getTeam());
			
			pw.printf("%s\t", ag.getName());
			pw.printf("%s\t", ag.getTeam());
			pw.printf("%s\t", ag.getStrength());
			pw.printf("%s\t", Boolean.toString(ag.isDied()));
			pw.printf("%d\t", ag.getKill());
			pw.printf("%d\t", ag.getFriendKilled());
			pw.printf("%d\t", ag.getFriendlyFire());
			pw.printf("%d\t", survived);

			double diedTime = calcDiedTime(model, initTime, ag);

			pw.printf("%d\t", (long) (diedTime));
			pw.println();

			MetricByTeam mdt = map.get(ag.getTeam());
			if (mdt == null) {
				mdt = new MetricByTeam(ag.getTeam());
				map.put(ag.getTeam(), mdt);
			}

			mdt.addToMedia(diedTime, ag.getKill(), ag.getFriendKilled(),
					ag.getFriendlyFire(), survived);
		}

		// addDev
		for (AgentTiroteio ag : list) {
			double diedTime = calcDiedTime(model, initTime, ag);

			MetricByTeam mdt = map.get(ag.getTeam());

			Integer survived = mapAgentSurvive.get(ag.getTeam());
			
			mdt.addToDev(diedTime, ag.getKill(), ag.getFriendKilled(),
					ag.getFriendlyFire(), survived);
		}

		pw.println();
		pw.println();

		pw.print(getHeadString(new MetricByTeam("")));

		for (MetricByTeam mdt : map.values()) {
			pw.println(mdt.toString());

		}

		if (model.getTimeOver()) {
			pw.println("timeover");
		}
		pw.println();
		pw.format("timeSimulation\t%d%n", model.getTotalSimulationTime());
		pw.close();
	}

	/**
	 * @param model
	 * @param initTime
	 * @param ag
	 * @return
	 */
	private double calcDiedTime(final TiroteioModel model, final long initTime,
			AgentTiroteio ag) {
		double diedTime;
		if (ag.isDied()) {
			double temp;
			temp = ag.getDiedTime() - initTime;
			temp = temp / ((double) model.getTotalSimulationTime());
			diedTime = temp;
		} else
			diedTime = 1;

		diedTime = diedTime * 100;
		return diedTime;
	}
}