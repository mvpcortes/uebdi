package tiroteio;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.awt.Point;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import tiroteio.entity.AgentTiroteio;
import br.uff.ic.util.StringOutputStream;

public class ModelWriterTest{

	public ModelWriterTest(){
		
	}

	@Test
	public void testTimeLife() throws IOException {
		TiroteioModel model = mock(TiroteioModel.class);
		
		long startTime = System.currentTimeMillis();
		List<AgentTiroteio> agents = new ArrayList<AgentTiroteio>(4);
		agents.add(createAgentDied("1", "a"));
		agents.add(createAgentDied("2", "a"));
		agents.add(createAgentDied("1", "b"));
		agents.add(createAgentDied("2", "b"));
		
		when(model.getAgents()).thenReturn(agents);
		when(model.getTotalSimulationTime()).thenReturn(20l);
		//Writer writer = mock(Writer.class);
		OutputStream out = new StringOutputStream();
		
		ModelWriter mw = new  ModelWriter(out);
		mw.write(model);
		
		String temp = out.toString();
		assertTrue(containMatcher("(grouppoint)(\\s+)(\\w+)(\\s+)(\\d+)(\\s+)(\\d+)(\\s+)(\\d+)", temp, "50", 9));
		
	}

	private boolean containMatcher(String pattern, String strSource, String strMatcher, int group) {

		Pattern regex = Pattern.compile(pattern);
		Matcher m = regex.matcher(strSource);
		if (m.find()) {
			return strMatcher.equals(m.group(group));
		}
		return false;
		
	}

	/**
	 * @param string2 
	 * @param string 
	 * @return
	 */
	private AgentTiroteio createAgentDied(String name, String team) {
		AgentTiroteio a =new AgentTiroteio(String.format("%s_%s", name, team),new Point(1, 0), team);
		a.decStrength(TiroteioModel.INITIAL_STR+1);
		a.setDiedTime(10);
		return a;		
	}

}
