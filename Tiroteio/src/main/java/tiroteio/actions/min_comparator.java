// Internal action code for project jason

package tiroteio.actions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.Term;

import java.util.Iterator;

/**
 * Find min item using comparator
 * @author Marcos
 *
 */
public class min_comparator extends DefaultInternalAction {

	   @Override public int getMinArgs() { return 3; }
	   @Override public int getMaxArgs() { return 3; }

	    @Override protected void checkArguments(Term[] args) throws JasonException {
	        super.checkArguments(args); // check number of arguments
	        if (!args[0].isList())
	            throw JasonException.createWrongArgument(this,"first argument must be a list");
	        
	        if(args[1].isGround())
	        	throw JasonException.createWrongArgument(this,"second argument not must be a ground");
	        
	        if(!(args[2] instanceof LogicalFormula))
	        	throw JasonException.createWrongArgument(this,"third argument must be a LogicalFormula");
	    }

	    @Override
	    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
	        checkArguments(args);

	        ListTerm list = (ListTerm)args[0];
	        if (list.isEmpty()) {
	            return false;               
	        }
	        
	        final Term itemMask = args[1];
	        
	        if(list.size() == 1)
	        {
	        	final Term element = list.get(0);
	        	return un.unifies(element, itemMask);
	        }
	        
	        final Term COMPA = ASSyntax.parseVar("COMP_A");
	        final Term COMPB = ASSyntax.parseVar("COMP_B");
	        
	        
	        final LogicalFormula comparator = (LogicalFormula)args[2];
	        
	        Iterator<Term> i = list.iterator();
	        
	        Term minSource = i.next();
	        
	        
	        while(i.hasNext())
	        {
	        	Term iTerm = i.next();
		        Unifier tempUn = un.clone();
		        
		        Term itCompA = COMPA.clone();
		        Term itCompB = COMPB.clone();
		        tempUn.unifies(itCompA, minSource	);
		        tempUn.unifies(itCompB, iTerm		);
		        Iterator<Unifier> itUn = comparator.logicalConsequence(ts.getAg(), tempUn);
		        if(!itUn.hasNext())
		        {
		        	minSource = iTerm;
		        }
	        }
	        return un.unifies(itemMask, minSource.clone());
	    }
}
