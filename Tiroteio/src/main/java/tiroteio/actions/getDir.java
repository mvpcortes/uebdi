// Internal action code for project Tiroteio

package tiroteio.actions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.util.List;

import br.uff.ic.util.TermHelper;

/**
 * Retorna a direção média de um conjunto de pos(X, Y, E), onde X e Y são as coordenadas e E é o peso
 * @author Marcos
 *
 */
public class getDir extends DefaultInternalAction {

	@Override
	public int getMinArgs() { return 3; }
	@Override
    public int getMaxArgs() { return 3; }
	
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		
		if(!(args[0] instanceof ListTerm))
			throw new JasonException("param 0 is not a ListTerm");
		
		if(	!(args[1].isVar() && !args[1].isGround()))
			throw new JasonException("param 1 should be Var and not ground");
		
		if(	!(args[2].isVar() && !args[2].isGround()))
			throw new JasonException("param 2 should be Var and not ground");
	}
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	List<Term> listTerm = ((ListTerm)args[0]).getAsList();
    	VarTerm X 			= (VarTerm)args[1];
    	VarTerm Y 			= (VarTerm)args[2];
    	
    	if(listTerm.size() == 0)
    	{
    		un.unifies(X, ASSyntax.createNumber(0));
    		un.unifies(Y, ASSyntax.createNumber(0));
    		return true;
    	}
    	
    	double sum = 0;
    	double acumX = 0;
    	double acumY = 0;
    	
    	for(Term t: listTerm)
    	{
    		if(!t.isLiteral())
    		{
    			throw new JasonException("Cannot parse a not literal term " + t.toString());
    		}
    		
    		Literal l = (Literal)t;
    		
    		if(l.getTerms().size() < 3)
    		{
    			throw new JasonException("Cannot parse literal with a less 3 terms " + t.toString());
    		}
    		
    		double a = TermHelper.getDouble(l, 0);
    		double b = TermHelper.getDouble(l, 1);
    		double e = TermHelper.getDouble(l, 2);
    		
    		acumX += a*e;
    		acumY += b*e;
    		sum   += e;
    	}
    	
    	return 		un.unifies(X, ASSyntax.createNumber(acumX/sum)) && 
    				un.unifies(Y, ASSyntax.createNumber(acumY/sum));
    }
}
