// Internal action code for project Tiroteio

package tiroteio.actions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;
import br.uff.ic.uebdi.mapmodel.Direction;
import br.uff.ic.util.TermHelper;

public class vecToDir extends DefaultInternalAction {

	@Override
    public int getMinArgs() { return 3; }
	@Override
    public int getMaxArgs() { return 3; }
    
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		
		if(	!args[0].isNumeric())
			throw new JasonException("The first argument should be numeric");
		
		if(	!args[1].isNumeric())
			throw new JasonException("The second argument should be numeric");
		
			
		if(	!args[2].isVar() && !args[1].isGround())
			throw new JasonException("The third argument should be Var and not ground");
    }
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        double x = TermHelper.toDouble(args[0]);
        double y = TermHelper.toDouble(args[1]);
        VarTerm v= (VarTerm)args[2];
        br.uff.ic.uebdi.mapmodel.Direction dir = Direction.none;
        double temp = Math.acos((x)/(x*x + y*y));
        temp = Math.toDegrees(temp);
        if(y > 0)
        	temp+= 180;
        if(temp <= 45 || temp >= 270)
        {
        	dir = Direction.right;
        }else if(temp > 45 && temp <= 135)
        {
        	dir = Direction.top;
        }else if(temp > 135 && temp <= 225)
        {
        	dir = Direction.left;
        }else if(temp > 225 && temp < 270)
        {
        	dir = Direction.bottom;
        }
        return un.unifies(v, Literal.parseLiteral(dir.name()));
    }
}
