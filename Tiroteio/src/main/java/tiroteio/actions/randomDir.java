// Internal action code for project Tiroteio

package tiroteio.actions;
import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.util.Random;

import br.uff.ic.uebdi.mapmodel.Direction;



public class randomDir extends DefaultInternalAction {

	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
	
			
		if(	!args[1].isVar() && !args[1].isGround())
			throw new JasonException("The first argument should be Var and not ground");
    }
	
	private static Random random = new Random();  
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        VarTerm v= (VarTerm)args[0];
    	// execute the internal action
        ts.getAg().getLogger().fine("executing internal action 'tiroteio.actions.randomDir'");
       
        final int i = random.nextInt(4);
        
        Direction dir;
        switch(i)
        {
	        case 0:
	        	dir = Direction.left;
	        break;
	        case 1:
	        	dir = Direction.right;
	        break;
	        case 2:
	        	dir = Direction.top;
	        break;
	        case 3:
	        default:
	        	dir = Direction.bottom;
	        break;
        }
        
        return un.unifies(v, Literal.parseLiteral(dir.name()));
    }
}
