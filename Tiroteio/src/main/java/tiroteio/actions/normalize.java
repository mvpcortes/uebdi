// Internal action code for project Tiroteio

package tiroteio.actions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;

import java.util.List;

import br.uff.ic.util.TermHelper;

public class normalize extends DefaultInternalAction {

	@Override
	public int getMinArgs() { return 2; }
	@Override
    public int getMaxArgs() { return 2; }
	
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		
		if(!(args[0].isLiteral()))
			throw new JasonException("param 0 is not a Literal");
		
		if(	!(args[1].isVar() && !args[1].isGround()))
			throw new JasonException("param 1 should be Var and not ground");
	}
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        // execute the internal action
        ts.getAg().getLogger().fine("executing internal action 'tiroteio.actions.normalize'");
        
        Literal lit = (Literal)args[0];
        List<Term> listTerm = ((Literal)lit).getTerms();

        Literal newLit = (Literal)lit.clone();
        
        double base = 0;
        for(Term t: listTerm)
        {
        	
        	Double value = TermHelper.toDouble(t);
        	base += value*value;
        }
        
        double sqrt;
        if(base > 0)
        	sqrt = Math.sqrt(base);
        else
        	sqrt = 1;
        
        for(int i = 0; i < newLit.getTerms().size(); i++)
        {
        	double temp = TermHelper.getDouble(newLit, i);
        	newLit.setTerm(i, new NumberTermImpl(temp/sqrt));
        }
        un.unifies(args[1], newLit);
        return true;
    }
}
