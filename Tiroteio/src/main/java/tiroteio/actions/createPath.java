// Internal action code for project FoodSimulation

package tiroteio.actions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import tiroteio.TiroteioModel;
import br.uff.ic.util.TermHelper;

public class createPath extends DefaultInternalAction {
	@Override
	public int getMinArgs() { return 3; }
	@Override
    public int getMaxArgs() { return 3; }
    
	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
       Literal source 	= (Literal)args[0];
       Literal dest		= (Literal)args[1];
       
       VarTerm v 		= (VarTerm)args[2];
       
       try
       {
    	   int i = 0;
	       List<Point> list = createPath(ts, toPoint(source), toPoint(dest));
	       
	       
	       if(list == null)
	    	   return null;
	       
	       ListTerm all = new ListTermImpl();
	    	   
		   ListTerm tail = all;
		   for(Point p: list)
		   {
			   tail = tail.append(Literal.parseLiteral(String.format("pos(%d, %d)", p.x, p.y)));
	       }
	       return un.unifies(v, all);
       }catch(Exception e)
       {
    	   int i = 0;
    	   i ++;
       }
       return false;   
    }
    
	
	/**
	*
	* Retorna os vizinhos da posi��o x, y que s�o v�lidos
	*/
	private List<Point> getNeigbour(TransitionSystem ts, int x, int y, List<Point> list)
	{
		list.clear();
	            
		for(Point p: TiroteioModel.AREA_SEE)
		{
			if(validPos(ts, p.x+x, p.y+y))
			{
				list.add(new Point(p.x+x, p.y+y));
			}
		}
		
		return list;
	}
		 
    private boolean validPos(TransitionSystem ts, int x, int y)
    {
    	
    	Literal lit = Literal.parseLiteral(String.format("believeValidPos(%d, %d)", x, y));
    	boolean test = ts.getAg().believes(lit, new Unifier());
    	return test;
    }
    
	
    private class NodeInformation
    {
    	public Point point;
    	public long gscore = 0;
    	public long hscore = 0;
    	//public long fscore = 0;
    	
    	public Point come_from = null;
    	
    	
    	public boolean closed = false;
    	
    	public long getFScore()
    	{
    		return gscore + hscore;
    	}
    	
    	public NodeInformation(Point p, long g, long h, Point cf)
    	{
    		point = p;
    		gscore = g;
    		hscore = h;
    		come_from = cf;
    	}
    }
    
    private class ComparatorPoints implements Comparator<Point>
    {
		private Map<Point, NodeInformation> mapData;
		
		public ComparatorPoints(Map<Point, NodeInformation> m)
		{
			mapData = m;
		}
		
		@Override
		public int compare(Point a, Point b) {
			
			if(a.equals(b))
				return 0;
			
			NodeInformation nia = mapData.get(a);
			NodeInformation nib = mapData.get(b);
			
			if(nia == null)
			{
				if(nib == null)
					return 0;
				else
					return -1;
			}else
			{
				if(nib == null)
					return +1;
			}
			
			return (int) (nia.getFScore() - nib.getFScore());
			
		}
    	
    }
	List<Point> createPath(TransitionSystem ts, Point source, final Point dest) {
		
		Map<Point, NodeInformation> mapNI= new HashMap<Point, NodeInformation>();
		
		SortedSet<Point> openSet = new TreeSet<Point>(new ComparatorPoints(mapNI));
		//Set<Point> closeSet   = new HashSet<Point>();
		
		openSet.add((Point) source);
		
		long hce =  heuristic_cost_estimate(source, dest);

		NodeInformation niFirst = new NodeInformation(source,  0, hce, null);
		mapNI.put(source, niFirst);
		
		List<Point> listNeir = new ArrayList<Point>();
		while(openSet.size() > 0)
		{
			Point current = openSet.first();
			if(current.equals(dest))
				return reconstructPath(mapNI, dest, new LinkedList<Point>());
			
			openSet.remove(current);
			
			if(openSet.contains(current))
			{
				int i =0;
				i++;
			}
			
			NodeInformation niCurrent = mapNI.get(current); 
			niCurrent.closed = true;
			listNeir = getNeigbour(ts, current.x, current.y, listNeir);
			for(Point neighbor : listNeir)
			{
				{
					NodeInformation niNeighbor = mapNI.get(neighbor);
				
					if(niNeighbor != null && niNeighbor.closed)
						continue;
				}
				
				long tentative_g_score = niCurrent.gscore + distBetween(current ,neighbor);
				boolean tentative_is_better = false;	
				if(!openSet.contains(neighbor))
				{
	                 //openSet.add(neighbor);
	                 mapNI.put(neighbor, 
	                		 new NodeInformation(
	                				 neighbor,
	                				 0, 
	                				 heuristic_cost_estimate(neighbor, dest),
	                				 null)); 
	                 tentative_is_better = true;
				}else{
					NodeInformation ni = mapNI.get(neighbor);
					if(tentative_g_score < ni.gscore)
					{
						openSet.remove(neighbor);//tem de remover pq a � ordenado pelo valor de f. E f ser� alterado
						//caso g seja alterado
						tentative_is_better = true;
					}
					else
					{
						tentative_is_better = false;
					}
					
				}
				
				 if(tentative_is_better == true)
				 {
					 
					 NodeInformation ni = mapNI.get(neighbor);
	                 ni.come_from = current;
	                 ni.gscore = tentative_g_score;
	                 openSet.add(neighbor);
				 }
			}	
		}
		
		return null;
	}
	private long distBetween(Point current, Point neighbor) {
		if(current.x != neighbor.x)
		{
			if(current.y != neighbor.y)
				return 14;
			else
				return 10;
		}else
		{
			if(current.y != neighbor.y)
				return 10;
			else
				return 0;
		}
	}
	

	private long heuristic_cost_estimate(Point source, Point dest) {
		return (long)(source.distance(dest)*10);
	}
	
	private List<Point> reconstructPath(Map<Point, NodeInformation> mapNI, Point dest, List<Point> list) {
		
		NodeInformation ni = mapNI.get(dest);
		if(ni != null)
		{
			Point comeFrom = ni.come_from;
			if(comeFrom != null)
			{
				list = reconstructPath(mapNI, comeFrom, list);
			}			
			
			list.add(ni.point);
			return list;
		}
		return null;
	}
	
	private Point toPoint(Literal lit) {
		int x = TermHelper.toInteger(lit.getTerm(0));
		int y = TermHelper.toInteger(lit.getTerm(1));
		return new Point(x,y); 
	}
	/**
	 * createPath(SOURCE, DEST, PATH);
	 */
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		
		if(	!args[0].isLiteral())
			throw new JasonException("The first argument should be literal");
		if(	!args[1].isLiteral())
			throw new JasonException("The second argument should be literal");
		
		if(	!(args[2].isVar() && !args[2].isGround()))
			throw new JasonException("The third argument should be Var and not ground");
				
		
    }
}
