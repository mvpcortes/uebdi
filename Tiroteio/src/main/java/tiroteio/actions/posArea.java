// Internal action code for project FoodSimulation

package tiroteio.actions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import tiroteio.TiroteioModel;
import br.uff.ic.util.Pair;
import br.uff.ic.util.TermHelper;

public class posArea extends DefaultInternalAction {

	@Override
	public int getMinArgs() { return 5; }
	@Override
    public int getMaxArgs() { return 5; }
	
	/**
	 * createPath(SOURCE, DEST, PATH);
	 */
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		
		if(	!args[0].isNumeric())
			throw new JasonException("The first argument should be numeric");
		
		if(	!args[1].isNumeric())
			throw new JasonException("The secon argument should be numeric");
		
		if(	!args[2].isNumeric())
				throw new JasonException("The third argument should be numeric");
		
		if(	!args[4].isNumeric())
				throw new JasonException("The fourth argument should be numeric");
				
		if(	!args[5].isVar() && !args[5].isGround())
			throw new JasonException("The fifth argument should be Var and not ground");
    }
	
	private static Map<Pair<Integer, Integer>, Point[] > mapBuilt = new HashMap<Pair<Integer, Integer>, Point[]>();
	//foodsimulation.posArea(X, Y, 3, 0, LIST_FELL_T);
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
       super.execute(ts, un, args);
        ts.getAg().getLogger().fine("executing internal action 'foodsimulation.posArea'");
        
        long posX 		= TermHelper.toInteger(args[0]);
        long posY		= TermHelper.toInteger(args[1]);
        int outRound	= TermHelper.toInteger(args[2]);
        int innerRound	= TermHelper.toInteger(args[3]);
        VarTerm v 		= (VarTerm)args[4];
        int width		= Integer.MAX_VALUE;
        int height		= Integer.MAX_VALUE;
        if(args.length >= 6)
        	width		= TermHelper.toInteger(args[5]);
        
        if(args.length >= 7)
        	height		= TermHelper.toInteger(args[6]);

       
        Pair<Integer, Integer> p = new Pair<Integer, Integer>(outRound, innerRound);
        
        Point[] vecPoint = mapBuilt.get(p);
        if(vecPoint == null)
        {
        	vecPoint = TiroteioModel.generateRoundPoints(outRound, innerRound);
            mapBuilt.put(p, vecPoint);
        }
        
        ListTerm all = new ListTermImpl();
 	   
 	   ListTerm tail = all;
 	   for(Point pp: vecPoint)
 	   {
 		  long xReal = posX + pp.x;
 		  long yReal = posY + pp.y;
 		  if( xReal >= 0 && xReal < width && yReal >= 0 && yReal < height)
 		  {
 			  tail = tail.append(Literal.parseLiteral(String.format("pos(%d, %d)", xReal, yReal)));
 		  }
 		  else
 		  {
 			  int i = 0;
 		  }
 		  
       }
        return un.unifies(v, all);
    }
}
