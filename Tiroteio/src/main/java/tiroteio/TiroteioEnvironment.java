package tiroteio;

// Environment code for project FoodSimulation

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.environment.Environment;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import tiroteio.entity.AgentTiroteio;
import br.uff.ic.uebdi.mapmodel.Direction;
import br.uff.ic.uebdi.mapmodel.Viewer;
import br.uff.ic.uebdi.mapmodel.entities.Agent;
import br.uff.ic.uebdi.mapmodel.entities.Entity;
import br.uff.ic.util.TermHelper;

/**
 * Ambiente para o problema de tiroteio
 * 
 * @author mcortes
 * 
 */
public class TiroteioEnvironment extends Environment {

	static final String STR_ENERGY = "energy";
	static final String STR_FUNC_HIT = "hit";
	static final String STR_FUNC_HIT_ME = "hitMe";
	static final String STR_FUNC_MYTEAM = "myTeam";
	static final String STR_FUNC_WALL = "wall";

	public static final String STR_FUNC_DIED = "died";

	public static final String STR_FUNC_AGENT = "agent";

	private static final String STR_LOG = "log";

	public static final String STR_POS_OF_AGENTS = "posOfAgents";
	private static final Object STR_MAX_SIM_TIME = "maxSimulationTime";

	private Logger logger = Logger.getLogger("Tiroteio."
			+ TiroteioEnvironment.class.getName());

	private TiroteioModel model;

	private Viewer view;

	private String strFileOutput = "./out.txt";

	private Map<String, Queue<Point>> mapPosOfTeam = null;

	private Boolean stopping = false;

	private Queue<Point> getQueueOfTeam(String team) {
		if (mapPosOfTeam == null) {
			mapPosOfTeam = new HashMap<String, Queue<Point>>();
		}

		Queue<Point> queue = mapPosOfTeam.get(team);
		if (queue == null) {
			queue = new LinkedList<Point>();
			mapPosOfTeam.put(team, queue);
		}
		return queue;
	}

	private Point popPosOfTeam(String team) {

		Queue<Point> queue = getQueueOfTeam(team);
		if (queue != null)

			return queue.poll();
		else {
			getLogger().warning(
					"problem in get the queue to team " + team + ";");
			return null;
		}
	}

	private void putPosOfTeam(String team, int x, int y) {
		Queue<Point> queue = getQueueOfTeam(team);

		queue.add(new Point(x, y));

	}

	double getStrength(String typeOfAg) {
		double sum = 0;
		double q = 0;
		for (String name : this.getAgNames()) {

			if (name.startsWith(typeOfAg)) {
				Double d = model.getAgStrength(name);
				if (d != null) {
					sum += d;
					q++;
				}

			}
		}
		if (q > 0)
			return sum / q;
		else
			return 0;
	}

	/** Called before the MAS execution with the args informed in .mas2j */
	@Override
	public void init(String[] args) {
		String[] temp = { "2" };
		super.init(temp);
		List<Literal> listProp = parseProp(args);

		int size = 10;
		boolean showGUI = false;
		String strOutput = "./out.txt";
		for (Literal l : listProp) {
			final String functor = l.getFunctor();
			if ("size".equals(functor)) {
				size = TermHelper.getInteger(l, 0);
			} else if ("showGUI".equals(functor)) {
				showGUI = true;
			} else if ("output".equals(functor)) {
				strOutput = TermHelper.getString(l, 0);
			} else if (STR_LOG.equals(functor)) {
				setLoggerOutput(l);
			} else if (STR_POS_OF_AGENTS.equals(functor)) {
				Term content = l.getTerm(0);
				if (content.isString()) {
					String nameFile = TermHelper.toString(content);
					processFileOfTeamPoint(nameFile);
				} else if (content.isStructure()) {
					Collection<Term> listTerm = TermHelper.toCollTerm(content);
					processCollOfTeamPoint(listTerm);
				}
			} else if (STR_MAX_SIM_TIME.equals(functor)) {
				maxSimulationTime = Math
						.round(TermHelper.getDouble(l, 0) * 1000);// em segundos
				getLogger().info(
						"Setting maxSimulationTime = " + maxSimulationTime
								+ "ms");
			}
		}

		model = new TiroteioModel(size);

		envSize = Literal.parseLiteral(String.format("size(%d, %d)",
				model.getWidth(), model.getHeight()));

		// show view
		if (showGUI) {
			view = new Viewer(model, "Viewer", 360);
			view.setVisible(true);
		}

		setFileOutput(strOutput);
		initTimeEnvironment = System.currentTimeMillis();
	}

	private void processCollOfTeamPoint(Collection<Term> listTeamPos) {
		for (Term t : listTeamPos) {
			if (t.isLiteral()) {
				Literal l = (Literal) t;
				processTeamPos(l);
			} else {
				getLogger()
						.severe("Cannot process "
								+ t
								+ " in :processCollOfTeamPoint because it is not a literal");
			}
		}
	}

	private void processFileOfTeamPoint(String nameFile) {
		File f = new File(nameFile);
		FileReader fr;
		try {
			fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				Literal ltemp = Literal.parseLiteral(line);
				processTeamPos(ltemp);
			}

			br.close();
		} catch (IOException e) {
			getLogger().severe(
					"Exception process file teamPos: " + e.toString());
		}
	}

	/**
	 * @param line
	 * @param ltemp
	 */
	private void processTeamPos(Literal ltemp) {
		if (ltemp != null && ltemp.isLiteral()) {
			final Literal teamLiteral = (Literal) ltemp;
			final String team = teamLiteral.getFunctor();
			for (Term posTerm : ltemp.getTerms()) {
				if (posTerm.isStructure()) {
					Structure posStruct = (Structure) posTerm;
					if (posStruct.getTerms().size() == 2) {
						int x = TermHelper.getInteger((Structure) posTerm, 0);
						int y = TermHelper.getInteger((Structure) posTerm, 1);
						this.putPosOfTeam(team, x, y);
					} else {
						getLogger().severe("Invalid pos like Term: " + posTerm);
					}
				} else {
					getLogger().severe(
							"Invalid pos like Term not structure: " + posTerm);
				}
			}
		} else {
			getLogger()
					.severe("Cannot transform string to a literal: " + ltemp);
		}
	}

	/**
	 * This method receive a literal and parse it to set the log output.<br/>
	 * The literal is the form {@value #STR_LOG}(file, limit, count)
	 * 
	 * @param param
	 *            the literal
	 */
	private void setLoggerOutput(Literal param) {
		String fileName = TermHelper.getString(param, 0);

		String strLevel = TermHelper.getString(param, 1);
		strLevel = strLevel.toUpperCase();
		Level level = Level.INFO;
		if (!strLevel.equals("")) {
			try {
				level = Level.parse(strLevel);
			} catch (Exception e) {
				level = Level.INFO;
			}
		}

		int limit = (int) TermHelper.getDouble(param, 2);
		if (limit <= 0)
			limit = 1000000000;

		int count = (int) TermHelper.getDouble(param, 3);
		if (count <= 0)
			count = 1;

		try {
			FileHandler fh = new FileHandler(fileName, limit, count, false);
			fh.setLevel(level);

			// =============================
			Handler[] hs = Logger.getLogger("").getHandlers();
			for (int i = 0; i < hs.length; i++) {
				Logger.getLogger("").removeHandler(hs[i]);
			}
			Logger.getLogger("").addHandler(fh);

		} catch (SecurityException e) {
			getLogger()
					.severe(String
							.format("Cannot add new FileHandler (\"%s\") because the Secutity Exeption (%s)",
									fileName, e.toString()));
		} catch (IOException e) {
			getLogger()
					.severe(String
							.format("Cannot add new FileHandler (\"%s\") because the IOException (%s)",
									fileName, e.toString()));
		}
	}

	private void setFileOutput(String strOutput) {
		strFileOutput = strOutput;
	}

	private List<Literal> parseProp(String[] args) {
		List<Literal> array = new ArrayList<Literal>(args.length);
		for (String s : args) {
			Literal l = Literal.parseLiteral(s);
			if (l != null) {
				array.add(l);
			}
		}

		return array;
	}

	protected Set<String> getAgNames() {
		return getEnvironmentInfraTier().getRuntimeServices().getAgentsNames();
	}

	private static Atom aMyPos = new Atom("my_pos");
	private static Atom aSee = new Atom("see");

	private Literal envSize = null;

	// em milisegundos
	private long initTimeEnvironment = 0;
	// em milisegundos
	private long maxSimulationTime = Long.MAX_VALUE;

	@Override
	public List<Literal> getPercepts(String agName) {
		if (model.getAgStrength(agName) == null) {
			// faznada pq não foi registrado
			return Collections.emptyList();
		}
		return super.getPercepts(agName);
	}

	protected void updatePercepts() {
		synchronized (model) {
			getLogger().fine("init updatePercepts");

			List<Hit> listHit = new LinkedList<Hit>();

			while (model.hasHit()) {
				listHit.add(model.pollHit());
			}

			for (String name : getAgNames()) {

				this.clearPercepts(name);

				Point pos = model.getAgPos(name);
				if (pos != null) {
					addPercept(name, envSize);

					Literal lpos = ASSyntax.createLiteral("pos",
							ASSyntax.createNumber(pos.x),
							ASSyntax.createNumber(pos.y));
					addPercept(name, lpos);

					Literal lstrength = ASSyntax.createLiteral(STR_ENERGY,
							ASSyntax.createNumber(model.getAgStrength(name)));
					addPercept(name, lstrength);

					//
					Iterator<Entity> itMyPos = model.getEntities(pos.x, pos.y);
					while (itMyPos.hasNext()) {
						Entity e = itMyPos.next();
						if (!e.getName().equals(name))
							addPercept(name,
									createPerceptionByEntity(e, aMyPos));
					}

					//
					Iterator<Entity> itSee = model.getAgSee(name);

					while (itSee.hasNext()) {
						Entity e = itSee.next();
						addPercept(name, createPerceptionByEntity(e, aSee));
					}

					for (Hit h : listHit) {
						if (name.equals(h.getDest().getName())) {
							getLogger().info(
									String.format("Agent %s went hit", h
											.getDest().getName()));
							Literal lhit = ASSyntax.createLiteral(
									STR_FUNC_HIT_ME, ASSyntax.createNumber(h
											.getId()), ASSyntax.createAtom(h
											.getSource().getName()));
							addPercept(name, lhit);
						}

						if (model.agentSeeThisPoint(name, h.getX(), h.getY())) {
							// hit(id, shooter, hit, x, y)
							getLogger().info(
									String.format("Agent %s hit %s", h
											.getSource().getName(), h.getDest()
											.getName()));
							Literal lhit = ASSyntax
									.createLiteral(STR_FUNC_HIT, ASSyntax
											.createNumber(h.getId()),
											ASSyntax.createAtom(h.getSource()
													.getName()), ASSyntax
													.createAtom(h.getDest()
															.getName()),
											ASSyntax.createNumber(h.getX()),
											ASSyntax.createNumber(h.getY()));
							addPercept(name, lhit);
						}
					}

				} else {
					getLogger().warning(
							String.format("Agent %s does not found!", name));
				}
			}
		}
	}
	

	/**
	 * Isto n�o est� muito coeso.. mas � melhor do que por dentro da classe
	 * derivada de Entity.
	 * 
	 * @param it
	 */
	private Literal createPerceptionByEntity(Entity e, Atom where) {
		Literal perc = null;
		switch (Type.getType(e)) {
		case agent:
			// agent(name, x, y, energy, team)
			AgentTiroteio a = (AgentTiroteio) e;
			if (!a.isDied()) {
				perc = ASSyntax.createLiteral(
						STR_FUNC_AGENT, // seeing
						ASSyntax.createAtom(e.getName()),
						ASSyntax.createNumber(a.getPoint().x),
						ASSyntax.createNumber(a.getPoint().y),
						ASSyntax.createNumber(a.getStrength()),
						ASSyntax.createLiteral(a.getTeam()));
			} else {
				perc = ASSyntax.createLiteral(STR_FUNC_DIED,
						ASSyntax.createAtom(e.getName()),
						ASSyntax.createNumber(a.getPoint().x),
						ASSyntax.createNumber(a.getPoint().y),
						ASSyntax.createLiteral(a.getTeam()));
			}
			break;
		case wall:
			perc = ASSyntax.createLiteral("wall",
					ASSyntax.createNumber(e.getPoint().x),
					ASSyntax.createNumber(e.getPoint().y));
			break;
		}

		return perc;
	}

	@Override
	public boolean executeAction(String agName, Structure action) {
		// verifica se está parando

		if (stopping) {
			getLogger()
					.info(String
							.format("Cannot execute action %s for agent %s because the MAS is stoping",
									action, agName));
			return false;
		}

		logger.info(String.format("executing: %s to %s", action, agName));
		String actId = action.getFunctor();
		ActionInfo ai = ActionInfo.getActionInfo(actId);
		boolean ok = false;
		try {
			switch (ai) {
			case PAUSE:
				ai.simpleWait(getLogger());
				ok = true;
				break;
			case MOVE: {
				if (action.getTerms().size() > 1) {
					final double mx = TermHelper.getDouble(action, 0);
					final double my = TermHelper.getDouble(action, 1);
					ok = model.move(agName, (int) Math.round(mx),
							(int) Math.round(my));
				} else {
					Direction dir = Direction.fromTerm(action.getTerm(0));
					ok = model.move(agName, dir);
				}
				if (ok) {
					ai.simpleWait(getLogger());
				} else {
					getLogger()
							.warning(
									String.format(
											"the model reject the action %s of agent %s",
											actId, agName));
					ok = false;
				}
			}
				break;
			case REGISTER:
				String team = TermHelper.getString(action, 0);
				Point pos = this.popPosOfTeam(team);
				if (pos == null) {
					model.registerAgentRandomPos(agName, team);
					getLogger().info(
							String.format(
									"Register agent %s in a random position",
									agName));
				} else {
					model.registerAgent(agName, pos.x, pos.y, team);
					getLogger().info(
							String.format(
									"Register agent %s in a pos(%02d, %02d)",
									agName, pos.x, pos.y));
				}

				ok = true;
				break;
			case SHOOT: {
				Term tdir = action.getTerm(0);
				Direction dir = Direction.fromTerm(tdir);
				AtomicReference<Entity> refEntity = new AtomicReference<Entity>();
				ok = model.attack(agName, dir, refEntity);
				ai.simpleWait(getLogger());

				if (refEntity.get() != null
						&& refEntity.get() instanceof Agent) {
					AgentTiroteio refAgent = (AgentTiroteio) refEntity.get();
					if (refAgent.isDied()) {
						testAndStopMasByDied();
					}
				}

			}
				break;

			default:
				getLogger().severe("Invalid action: " + ai.getName());
				ok = false;
			}
		} catch (Exception e) {
			getLogger().severe(
					"unknow exception in TiroteioEnvironment.action: "
							+ e.toString());
			ok = false;
		}

		testAndStopMasByTimeOver();
		updatePercepts();

		updateView();

		return ok;
	}

	private void updateView() {
		if (view != null) {
			view.update();
		}

	}

	private void testAndStopMasByTimeOver() {
		synchronized (stopping) {
			long differenceTime = getMaxSimulationTime() - getTimeCount();

			getLogger()
					.info(String
							.format("rest %.4f seconds to finishSimulation by time over",
									differenceTime / 1000.0));

			if ((getTimeCount() > getMaxSimulationTime())) {
				getLogger().info("exit because time over.");
				model.setTimeOver(true);
				stopMAS();
			}
		}
	}

	private void testAndStopMasByDied() {
		synchronized (stopping) {
			if (model.restATeamAlive()) {
				getLogger().info("Stopping because um time perdeu");
				stopMAS();
			}
		}
	}

	private void stopMAS() {
		synchronized (stopping) {
			try {
				stopping = true;
				model.finishSimulation();
				doOutput();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
			try {
				this.getEnvironmentInfraTier().getRuntimeServices().stopMAS();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private long getMaxSimulationTime() {
		return maxSimulationTime;
	}

	/**
	 * Retorna quanto tempo passou desde o inicio do ambiente
	 * 
	 * @return
	 */
	private long getTimeCount() {
		return System.currentTimeMillis() - initTimeEnvironment;
	}

	/** Called before the end of MAS execution */
	@Override
	public void stop() {
		super.stop();
	}

	private void doOutput() {
		ModelWriter mw;
		try {
			mw = new ModelWriter(new FileWriter(strFileOutput));
			mw.write(this.model);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
