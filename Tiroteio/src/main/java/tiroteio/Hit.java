package tiroteio;

import tiroteio.entity.AgentTiroteio;



public class Hit {
	
	private static int nextId = 0;
	
	private int id = nextId++;
	
	
	private int x, y;
	
	private AgentTiroteio source, dest;
	
	public int getId() {
		return id;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public AgentTiroteio getSource() {
		return source;
	}
	
	public AgentTiroteio getDest() {
		return dest;
	}
	public Hit(int _x, int _y, AgentTiroteio _source, AgentTiroteio _dest)
	{
		x = _x;
		y = _y;
		source = _source;
		dest = _dest;
	}
}
