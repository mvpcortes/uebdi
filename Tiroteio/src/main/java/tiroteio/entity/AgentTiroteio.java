package tiroteio.entity;

import java.awt.Point;

import tiroteio.TiroteioModel;
import br.uff.ic.uebdi.mapmodel.entities.Agent;


/**
 * Agent model class
 * @author mcortes
 *
 */
public class AgentTiroteio extends Agent {
	
//	private State state = State.free;
	
	private int strength = TiroteioModel.INITIAL_STR;
	
	private String team;
	
	/**
	 * Tempo da morte.
	 */
	private long diedTime = -1;
	
	private int killed = 0;
	
	private int friendKilled = 0;
	
	private int friendlyFired = 0;
	
	public AgentTiroteio(String n, Point p, String _team) {
		super(n, p);
		team = _team;
	}

	public int getKill(){
		return killed;
	}
	
	public int getFriendKilled(){
		return friendKilled;
	}
	
	public void incFriendKilled(int value){
		friendKilled+= value;
	}
	
	public int getFriendlyFire(){
		return friendlyFired;
	}
	
	public int getStrength()
	{
		return strength;
	}
	
	
	public void setStrength(int s)
	{
		strength = s;
	}
	


	
	public void decStrength(int n) {
		strength-= n;
		if(isDied())
		{
			diedTime = System.currentTimeMillis();
		}
	}
	
	public long getDiedTime()
	{
		return diedTime;
	}

	public String getTeam() {
		return team;
	}	
	
	public boolean isDied()
	{
		return getStrength() <= 0;
	}

	public void incKill(int i) {
		killed += i;
	}
	
	public void incFriendlyFire(int i){
		friendlyFired+=i;
	}

	public void setDiedTime(long i) {
			diedTime = i;
	}
}
