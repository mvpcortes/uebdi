package tiroteio.exp;

import java.util.ArrayList;
import java.util.List;

import tiroteio.MetricByTeam;

public class Combat {
	
	private Team aTeam;
	private Team bTeam;
	private List<Team> teams;
	
	private MetricByTeam metricTeamA = null;
	
	private MetricByTeam metricTeamB = null;
	
	public Combat(AgentType a, AgentType b, int count) {
	
		this(new AgentTypeCount(a, count), new AgentTypeCount(b, count));
	}
	
	public Combat(AgentTypeCount atcA, AgentTypeCount atcB) {
		aTeam = new Team("a", atcA);
		bTeam = new Team("b", atcB);
		teams = new ArrayList<Team>(2);
		teams.add(aTeam);
		teams.add(bTeam);
	}
	public Combat(AgentType[] atForTeamA, AgentType[] atForTeamB) {
		aTeam = new Team("a", atForTeamA);
		bTeam = new Team("b", atForTeamB);
		teams = new ArrayList<Team>(2);
		teams.add(aTeam);
		teams.add(bTeam);
	}
	
	public Combat(AgentTypeCount[] atForTeamA, AgentTypeCount[] atForTeamB) {
		aTeam = new Team("a", atForTeamA);
		bTeam = new Team("b", atForTeamB);
		teams = new ArrayList<Team>(2);
		teams.add(aTeam);
		teams.add(bTeam);
	}
	
	public String getCombatName()
	{
		return String.format("%sX%s", aTeam.toString(), bTeam.toString());
	}
	
	public List<Team> getTeams()
	{
		return teams;
	}
	
	public MetricByTeam getMetricTeamA() {
		return metricTeamA;
	}
	public void setMetricTeamA(MetricByTeam metricTeamA) {
		this.metricTeamA = metricTeamA;
	}
	public MetricByTeam getMetricTeamB() {
		return metricTeamB;
	}
	public void setMetricTeamB(MetricByTeam metricTeamB) {
		this.metricTeamB = metricTeamB;
	}
	

	@Override
	public String toString(){
		return getCombatName();
	}

	public Team getATeam() {
		return aTeam;
	}
	
	public Team getBTeam() {
		return bTeam;
	}
}
