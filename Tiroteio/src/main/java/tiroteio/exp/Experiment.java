package tiroteio.exp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

import tiroteio.MetricByTeam;
import tiroteio.MetricCalculator;
import tiroteio.MindInformation;
import tiroteio.ModelWriter;
import br.uff.ic.util.FileHelper;
import br.uff.ic.util.NameFileFilter;
import br.uff.ic.util.RegExpNameFileFilter;
import br.uff.ic.util.TimeDateHelper;

public class Experiment {

	final static int SCENE_COUNT = 5;
	final static int AGENTS_BY_TEAM = 10;
	final static int SIZE_OF_ENVIRONMENT = 10;
	
	//Isto � apenas para ajudar, mas somente existir�o 2 times!
	final static int TEAM_COUNT = 2;
	
	private static final String EXT_CONCAT = ".concat";

	private static Logger logger = Logger.getLogger(Experiment.class.getName());

	static Logger getLogger() {
		return logger;
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		Experiment.getLogger().info("creating scenes...");
		List<Scene> listScene = SceneFactory.generateScenes(SCENE_COUNT, SIZE_OF_ENVIRONMENT, Experiment.TEAM_COUNT, Experiment.TEAM_COUNT);
		Experiment.getLogger().info("creating combats...");
		List<Combat> listCombat = ExperimentCombatFactory.generateCombats();

		getLogger().info("create and run instances");

		for (Combat combat : listCombat) {
			
			Collection<File> coll = ExecuteHelper.createExperiment(combat, listScene);
			for(File f: coll){
		
				ExecuteHelper.execute(f);
			
				Experiment.processFileOutput(combat, "concat");
				
				Experiment.processEmotionalLog(combat, "emoPattern");
			}
		}
		
		getLogger().info("finish run");

		getLogger().info("create general metric table");
		printGeneralMetricTable(new File("."), listCombat);
		
		getLogger().info("finish experiment");
	}

	private interface VisitorEmotionColl{
		
		public void visit(MindInformation mi);
		
	}
	static void processEmotionalLog(final Combat combat, String nameFile)
			throws NumberFormatException, IOException {

		final File dirParent= new File(String.format("./%s", combat.getCombatName()));
		
		for(Team team: combat.getTeams()){
	
			Collection<File> collTeamPath = FileHelper.findDirectory(dirParent,
					new ArrayList<File>(),
					new NameFileFilter(team.getTeamName()));
			
			Collection<File> temp = new ArrayList<>();
			List<File> foundFiles = new ArrayList<>();
			
			FileFilter ff = new RegExpNameFileFilter(".*\\.logMS", false);
			for(File dir: collTeamPath){
				temp.clear();
				temp = FileHelper.findFiles(dir, temp, ff);
				foundFiles.addAll(temp);
			}
			File fileOut = (new File(dirParent, nameFile + "_" + team.getTeamName() + EXT_CONCAT));
			
			
			processMentalDataFile(fileOut, foundFiles, dirParent);
			
		}
		
	}

	private static void processMentalDataFile(File file, Collection<File> coll,
			final File dirParent)
			throws FileNotFoundException, IOException {

		final Map<Long, EmotionalAVG> mapEmotionalAVG = new TreeMap<Long, EmotionalAVG>();

		VisitorEmotionColl visitorMedia = new VisitorEmotionColl() {
			
			public void visit(MindInformation mi) {
				long time = mi.getTime();
				EmotionalAVG em = mapEmotionalAVG.get(time);
				if (em == null) 
					em = new EmotionalAVG();
				em.insertMediaMI(mi);
				mapEmotionalAVG.put(em.getTime(), em);
				
			}
		};
		
		VisitorEmotionColl visitorDev = new VisitorEmotionColl() {
			
			@Override
			public void visit(MindInformation mi) {
				EmotionalAVG em = mapEmotionalAVG.get(mi.getTime());
				if (em == null) 
					em = new EmotionalAVG();
				em.insertDevMI(mi);
				mapEmotionalAVG.put(em.getTime(), em);
				
			}
		};
		for (File fileMedia : coll) {
			//processa para a m�dia
			processEmotionalMS(fileMedia, visitorMedia);
		}
		
		for (File fileDev : coll) {
			//processa para a m�dia
			processEmotionalMS(fileDev, visitorDev);
		}
		
		PrintWriter fr = new PrintWriter(file);
		
		if(mapEmotionalAVG.size() == 0){
			getLogger().severe("map is void");
		}
		
		
		EmotionalAVG mediaDaMedia = new EmotionalAVG();
		fr.println("time\tfear(Media)\tfear(Dev)\tangry(Media)\tangry(Dev)\tPercept(Media)\tPercept(Dev)\tDelay(Media)\tDelay(Dev)\tLastInfo(Media)\tLastInfo(Dev)");
		for (EmotionalAVG em : mapEmotionalAVG.values()) {
			
			double fearMedia = em.getMcFear().getSum() == 0? 0: em.getMcFear().getMedia();
			double angryMedia = em.getMcAngry().getSum() == 0? 0: em.getMcAngry().getMedia();
			
			double fearDev = em.getMcFear().getSum() == 0? 0: em.getMcFear().getDev();
			double angryDev = em.getMcAngry().getSum() == 0? 0: em.getMcAngry().getDev();
			fr.format("%d\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f%n",
					em.getTime(), 
					fearMedia, 
					fearDev, 
					angryMedia, 
					angryDev,
					em.getMcPerceptCount().getMedia(), 
					em.getMcPerceptCount().getDev(),
					em.getMcDelayReasoning().getMedia(), 
					em.getMcDelayReasoning().getDev(),
					em.getMcLastInfo().getMedia(),
					em.getMcLastInfo().getDev()
					);
			
			mediaDaMedia.accumMedia(em);
		}
		
		for(EmotionalAVG em: mapEmotionalAVG.values()){
			mediaDaMedia.accumDev(em);
		}

		fr.println();
		fr.println("GeneralMedia: ");
		fr.format("fear: media: %.3f,\tdev: %.3f%n", mediaDaMedia.getMcFear().getMedia(), mediaDaMedia.getMcFear().getDev());
		fr.format("angry: media: %.3f,\tdev: %.3f%n", mediaDaMedia.getMcAngry().getMedia(), mediaDaMedia.getMcAngry().getDev());
		fr.format("Perc: media: %.3f,\tdev: %.3f%n", mediaDaMedia.getMcPerceptCount().getMedia(), mediaDaMedia.getMcPerceptCount().getDev());
		fr.format("Delay: media: %.3f,\tdev: %.3f%n", mediaDaMedia.getMcDelayReasoning().getMedia(), mediaDaMedia.getMcDelayReasoning().getDev());
		fr.format("lastInfo: media: %.3f,\tdev: %.3f%n", mediaDaMedia.getMcLastInfo().getMedia(), mediaDaMedia.getMcLastInfo().getDev());
		fr.close();
	}
	

	private static void processEmotionalMS(File fileLog, VisitorEmotionColl visitor)
			throws FileNotFoundException, IOException {
		FileReader fr = new FileReader(fileLog);
		BufferedReader br = new BufferedReader(fr);
		String line;
		br.readLine();
		while ((line = br.readLine()) != null) {
			MindInformation mi = MindInformation.fromString(line);
			visitor.visit(mi);
		}
		br.close();
	}

	
			
			
	static void processFileOutput(Combat combat, final String name)
			throws FileNotFoundException {

		final File dirParent = new File(new File("."), combat.getCombatName());

		Collection<File> coll = FileHelper.findFiles(dirParent,
				new ArrayList<File>(Experiment.SCENE_COUNT), "out.txt", true);

		// Map<String, MetricByTeam> mapAgData = new TreeMap<String,
		// MetricByTeam>();
		Map<String, List<AgentWithFriendSurvive>> mapAgData = new TreeMap<String, List<AgentWithFriendSurvive>>();
		AtomicReference<Integer> timeOverCount = new AtomicReference<Integer>();
		AtomicReference<Long> totalTimeSimulations = new AtomicReference<Long>();
		for (File f : coll) {
			try {
				getMetricData(f, mapAgData, timeOverCount, totalTimeSimulations);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Set<String> setTeam = mapAgData.keySet();
		List<MetricByTeam> listMetric = new ArrayList<MetricByTeam>();
		listMetric.clear();
		for (String team : setTeam) {
			MetricByTeam mt = new MetricByTeam(team);
			final List<AgentWithFriendSurvive> list = mapAgData.get(team);
			for (AgentWithFriendSurvive a : list) {
				mt.addToMedia(((double)a.getDiedTime() / 1000.0), a.getKill(),
						a.getFriendKilled(), a.getFriendlyFire(), a.getFriendSurvive());
			}

			for (AgentWithFriendSurvive a : list) {
				mt.addToDev(((double)a.getDiedTime() / 1000.0), a.getKill(),
						a.getFriendKilled(), a.getFriendlyFire(), a.getFriendSurvive());
			}

			listMetric.add(mt);
			if("a".equals(team)){
				combat.setMetricTeamA(mt);
			}else{
				combat.setMetricTeamB(mt);
			}
		}

		{
			PrintWriter fr = new PrintWriter(new File(dirParent, name
					+ EXT_CONCAT));

			fr.printf("%s", TimeDateHelper.getStrNow());
			fr.println();

			fr.print(ModelWriter.getHeadString(new MetricByTeam("")));

			for (MetricByTeam mdt : listMetric) {
				fr.println(mdt.toString());

			}

			fr.println();
			fr.format("Time Over Count: %d%n", timeOverCount.get());
			fr.format("Total Time Simulation: %d%n", totalTimeSimulations.get());
			fr.format(
					"avg total Time SImulation: %.3f%n",
					((double) totalTimeSimulations.get())
							/ ((double) coll.size()));
			fr.close();
		}

	}

	private static void getMetricData(File f,
			Map<String, List<AgentWithFriendSurvive>> mapTeamToMetrics,
			AtomicReference<Integer> numberOfTimesOver,
			AtomicReference<Long> refTotalTimeSimulations) throws IOException,
			Exception {

		int toCounter = 0;
		long totalTimeSimulations = 0;

		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		String line;

		for (int i = 0; i < 2; i++) { // lê as 2 primeiras linhas

			if (br.readLine() == null)
				break;
		}

		while ((line = br.readLine()) != null) {

			if (line.trim().equals(""))
				break; // acabou os itens

			try {
				
				//"agent\tteam\tenergy\tdied\tkill\tfriendKill\tFriendlyFire\tfriendSurvive\tLifeTime%n"
				Scanner scanner = new Scanner(line).useDelimiter("\t");

				String name = scanner.next(); // nome do agente;
				String team = scanner.next();
				List<AgentWithFriendSurvive> listAgent = mapTeamToMetrics.get(team);
				if (listAgent == null) {
					listAgent = new LinkedList<AgentWithFriendSurvive>();
					mapTeamToMetrics.put(team, listAgent);
				}

				int energy = scanner.nextInt(); // não preciso da energia;
				boolean died = scanner.nextBoolean();
				int kill = scanner.nextInt();
				int fKilled = scanner.nextInt();
				int fFire = scanner.nextInt();
				int fSurvive = scanner.nextInt();
				double diedTime = scanner.nextDouble();

				AgentWithFriendSurvive mt = new AgentWithFriendSurvive(name, null, team);
				mt.setDiedTime((long) diedTime * 1000);
				mt.incKill(kill);
				mt.incFriendlyFire(fFire);
				mt.incFriendKilled(fKilled);
				mt.incFriendSurvive(fSurvive);
				mt.setStrength(died ? 0 : 100);
				listAgent.add(mt);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		while ((line = br.readLine()) != null) {
			if (line.indexOf("timeover") >= 0) {
				toCounter++;
			} else if (line.indexOf("timeSimulation") >= 0) {
				int indexOfLabel = line.indexOf("timeSimulation");
				String strValue = line.substring(indexOfLabel
						+ "timeSimulation".length());
				totalTimeSimulations += Long.parseLong(strValue.trim());
			}
		}

		br.close();

		numberOfTimesOver.set(toCounter);

		refTotalTimeSimulations.set(totalTimeSimulations);
	}

	private static void printGeneralMetricTable(File dirParent,
			List<Combat> listCombat) {

		Set<String> agNames = new HashSet<String>(4);
		for (Combat combat : listCombat) {
			for(Team t: combat.getTeams()){
				for(AgentTypeCount atc: t.generateAgentTypeCount()){
					agNames.add(atc.getAgentType().getName());
				}
			}
		}

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new File(dirParent, "generalMetric.txt"));

			printMediaGeral(listCombat, agNames, pw, "media");
			
			pw.println("================================================");
			pw.println("================================================");
			pw.println("================================================");
			
			printMediaGeral(listCombat, agNames, pw, "dev");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (pw != null)
				pw.close();
		}

	}

	/**
	 * @param listCombat
	 * @param agNames
	 * @param pw
	 */
	private static void printMediaGeral(List<Combat> listCombat,
			Set<String> agNames, PrintWriter pw, String mode) {
		for (String nameMethod : new String[] { "getMcKilled",
				"getMcLifeTime", "getMcFriendSurviving",
				"getMcFriendlyFire", "getMcFriendKilled" }) {

			pw.println();
			pw.println();
			pw.printf("%s\t", nameMethod);
			for (String agName : agNames) {
				pw.printf("%s\t", agName);
			}
			pw.println();			
			for (String agNameRow : agNames) {
				pw.printf("%s\t", agNameRow);
				for (String agNameCol : agNames) {
					if(agNameRow.equals(agNameCol)){
						pw.print("-\t");
					}else{
						
						MetricByTeam metricRow = getMetricByTeam(listCombat,
								agNameRow, agNameCol);
						if("media".equals(mode))
							pw.printf("%.3f\t", getMediaMc(nameMethod, metricRow));
						else
							pw.printf("%.3f\t", getDevMc(nameMethod, metricRow));
					}

				}
				pw.println();
			}
		}
	}

	private static double getDevMc(String nameMethod, MetricByTeam metricRow) {
		@SuppressWarnings("unchecked")
		Class<MetricByTeam> clazz = (Class<MetricByTeam>) metricRow.getClass();
		
		try {
			Method method = clazz.getMethod(nameMethod);
			MetricCalculator calc = ((MetricCalculator)method.invoke(metricRow));
			return calc.getDev();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {

			e.printStackTrace();
		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		} catch (InvocationTargetException e) {

			e.printStackTrace();
		}
		
		
		return -1;
		
	}
	
	private static double getMediaMc(String nameMethod, MetricByTeam metricRow) {
		@SuppressWarnings("unchecked")
		Class<MetricByTeam> clazz = (Class<MetricByTeam>) metricRow.getClass();
		
		try {
			Method method = clazz.getMethod(nameMethod);
			MetricCalculator calc = ((MetricCalculator)method.invoke(metricRow));
			return calc.getMedia();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {

			e.printStackTrace();
		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		} catch (InvocationTargetException e) {

			e.printStackTrace();
		}
		
		
		return -1;
		
	}

	/**
	 * Lembre-se que o A é quem está na linha e B quem está na coluna
	 * 
	 * @param list
	 * @param nameA
	 * @param nameB
	 * @return sempre retorna o metric do nameA.
	 */
	private static MetricByTeam getMetricByTeam(List<Combat> list,
			String nameA, String nameB) {
		for (Combat combat : list) {
			Team aTeam = combat.getATeam();
			Team bTeam = combat.getBTeam();
			
			//s� funciona para time de um �nico atc.
			AgentTypeCount atcA = aTeam.generateAgentTypeCount().iterator().next();
			AgentTypeCount atcB = bTeam.generateAgentTypeCount().iterator().next();
			AgentType atA = atcA.getAgentType();
			AgentType atB = atcB.getAgentType();
			if (atA.equals(nameA)
					&& atB.equals(nameB)) {
				return combat.getMetricTeamA();
			}
			
			if((atA.equals(nameB)
					&& atB.equals(nameA))) {
				return combat.getMetricTeamB();
			}
		}
		return null;
	}

}
