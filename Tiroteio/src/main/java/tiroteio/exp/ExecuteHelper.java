package tiroteio.exp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import br.uff.ic.util.FileHelper;

public class ExecuteHelper {

	/**
	 * Cria a estrutura do experimento e retorna o mas2j para executar o
	 * experimento
	 * 
	 * @param listScene
	 * @param listCombat
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	static Collection<File> createExperiment(Combat combat, List<Scene> listScene)
			throws IOException, FileNotFoundException {

		final File pathCombat = new File(new File("."), combat.getCombatName());
		
		List<File> list = new ArrayList<>(listScene.size());

		if (pathCombat.exists())
			FileHelper.deleteDirectory(pathCombat);
		else
			pathCombat.mkdir();

		for (Scene scene : listScene) {
			final File pathScene = new File(pathCombat, scene.getName());
			if (!pathScene.exists()) {
				pathScene.mkdir();
			}

			InstanceCombat instance = new InstanceCombat(combat, scene);
			list.add(MAS2JFactory.makeMAS2J(pathScene, instance));
		}

		return list;
	}

	static void execute(File file) {
		System.out.format("executing file: %s\n", file.toString());
		String[] args = new String[] { "java", "-jar", "./Tiroteio.jar",
				file.toString() };
		Runtime runtime = Runtime.getRuntime();

		try {
			Process process = runtime.exec(args);

			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;

			 System.out.printf("Output of running %s is:\n",
			 Arrays.toString(args));

			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
