package tiroteio.exp;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;
import java.util.logging.Logger;

public class SceneFactory {


	
	private static Random random = new Random();
	
	static public List<Point> getRandomPosToAgents(final int count, final int w,
			final int h) {
		if (count > w * h) {
			throw new IllegalArgumentException(
					"count should be greater than w*h");
		}

		List<Point> listVacancy = new ArrayList<Point>(w * h);
		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++) {
				listVacancy.add(new Point(i, j));
			}

		List<Point> listOccuped = new ArrayList<Point>(count);

		for (int k = 0; k < count; k++) {
			final int size = listVacancy.size();
			final int posList = random.nextInt(size);
			listOccuped.add(listVacancy.get(posList));
			listVacancy.set(posList, listVacancy.get(size - 1));
			listVacancy.remove(size - 1);
		}
		return listOccuped;

	}

	private static final char[] CHAR_FOR_TEAM = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'}; 
	
	private static Logger logger =null;
	
	private static String getTeamName(final int id)
	{
		if(id >= CHAR_FOR_TEAM.length)
		{
			return getTeamName(id / CHAR_FOR_TEAM.length) + getTeamName(id % CHAR_FOR_TEAM.length);
		}else
		{
			return Character.toString(CHAR_FOR_TEAM[id]);
		}
	}
	
	private static void printPos(PrintWriter pw, Point p) {
		pw.format("pos(%s, %s)", p.x, p.y);
	}
	
	
	public static Scene createRandomScene(File fileName, final int teamCountA, final int teamCountB) throws IOException 
	{
		List<Point> list = getRandomPosToAgents(teamCountA + teamCountB, Experiment.SIZE_OF_ENVIRONMENT, Experiment.SIZE_OF_ENVIRONMENT);
		
			FileWriter fw;
			fw = new FileWriter(fileName);
			PrintWriter pw = new PrintWriter(fw);
			Iterator<Point> itPoint = list.iterator();

			final String nameTeamA = getTeamName(0);			
			printStringPosOfAgent(pw, itPoint, nameTeamA, teamCountA);
			
			final String nameTeamB = getTeamName(1);
			printStringPosOfAgent(pw, itPoint, nameTeamB, teamCountB);
			pw.close();
			
			return new Scene(fileName);
	}

	private static void printStringPosOfAgent(PrintWriter pw, Iterator<Point> itPoint,
			final String nameTeam, final int teamCount) {
		pw.print(nameTeam);
		pw.print("(");
		// imprime o primeiro
		Point temp = itPoint.next();
		printPos(pw, temp);
		int agentNumber = 1;
		while (agentNumber < teamCount && itPoint.hasNext()) {
			pw.print(", ");
			temp = itPoint.next();
			printPos(pw, temp);
			getLogger().info(String.format("iterator: %s(%d)\n", nameTeam, agentNumber));
			
			agentNumber++;
		}
		pw.println(")");
	}

	
	private static Logger getLogger() {

		if(logger == null)
		{
			logger = Logger.getLogger(SceneFactory.class.getName());
		}
		
		return logger;
	}

	
	/**
	 * Gera cenas com arquivos de posi��es para serem usadas repetidamente em v�rios experimentos
	 * @return
	 */
	static List<Scene> generateScenes(final int count, final int size, final int teamCountA, final int teamCountB) throws IOException {
		File dirParent = new File(".");
		if(count % 2 == 1){
			throw new IllegalArgumentException("Cannot use a odd count");
		}
	
		final int metade = count / 2;
		
		List<Scene> listScene = new ArrayList<Scene>(metade);
		List<Scene> listSceneMirror = new ArrayList<Scene>(metade);
	
		
		for (int i = 0; i < metade; i++) {
			File file = new File(dirParent, String.format(SceneFactory.STR_POS_FILENAME, i));
			Scene scene = createRandomScene(file, teamCountA, teamCountB);
			listScene.add(scene);
			listSceneMirror.add(scene);
			scene.setSizeEnv(size);
		}
	
		int i = metade;
		for (Scene s : listSceneMirror) {
			File newFile = new File(dirParent, String.format(SceneFactory.STR_POS_FILENAME, i));
			Scene sceneMirror = SceneFactory.createMirrorScene(newFile, s);
			sceneMirror.setSizeEnv(size);
			listScene.add(sceneMirror);
			i++;
		}
		return listScene;
	}

	static String STR_POS_FILENAME = "pos_%d.txt";

	public static Scene createMirrorScene(final File newFile,
			final Scene sceneSource) throws IOException {
		final File oldFile = sceneSource.getFile();
	
		FileInputStream fileInput;
		fileInput = new FileInputStream(oldFile);
		BufferedReader bread = new BufferedReader(new InputStreamReader(
				fileInput));
		Stack<String> stackData = new Stack<String>();
		Queue<String> queueHead = new LinkedList<String>();
		String line = null;
		while ((line = bread.readLine()) != null) {
			final int headCount = line.indexOf('(');
			final String head = line.substring(0, headCount);
			final String data = line.substring(headCount);
			stackData.add(data);
			queueHead.add(head);
		}
	
		PrintWriter pw = new PrintWriter(newFile);
		while (stackData.size() > 0) {
			final String head = queueHead.poll();
			final String data = stackData.pop();
			pw.printf("%s%s%n", head, data);
		}
	
		pw.close();
	
		return new Scene(newFile);
	}
}
