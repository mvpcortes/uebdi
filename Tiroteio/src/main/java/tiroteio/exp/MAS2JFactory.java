package tiroteio.exp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import br.uff.ic.util.FileHelper;


public class MAS2JFactory {

	static final String STR_FILE_NAME = "%s.mas2j";

	static Logger logger = null;
	
	static Logger getLogger()
	{
		if(logger == null)
			logger = Logger.getLogger(MAS2JFactory.class.getName());
		
		return logger;
	}
	
	static File makeMAS2J(File pathInstance, InstanceCombat instance) throws IOException {
		getLogger().info("make MAS2J");
		final String name = instance.getName();
		if(!pathInstance.exists()){
			getLogger().info("try create directories");
			pathInstance.mkdirs();
		}
		File file = new File(pathInstance, String.format(STR_FILE_NAME, name));
		getLogger().info("try creating file " + file.toString());
		file.createNewFile();

		PrintWriter pw = new PrintWriter(new FileOutputStream(file));

		pw.printf("MAS %s {\n", name);
		String pathWithoutBarra = FileHelper.replaceOneBarra(pathInstance.toString());
		pw.println("\tinfrastructure: Centralised");
		pw.printf(
				"\tenvironment: tiroteio.TiroteioEnvironment(size(%d), output(\"%s%s%s\"), log(\"%s%s%s\", info), posOfAgents(\"%s\"), maxSimulationTime(60))\n",
				instance.getScene().getSizeEnv(), 
				pathWithoutBarra, 
				File.separator+File.separator, 
				"out.txt",
				pathWithoutBarra, 
				File.separator+File.separator,
				"log.log", 
				FileHelper.replaceOneBarra(instance.getSceneReference())
				);

		pw.println();
		pw.println("\tagents:");
		
		
		for(Team team : instance.getCombat().getTeams())
		{
			for(String s: team.generateStringMAS2J(pathInstance)){
				pw.format("\t%s;%n", s);
			}
		}
		pw.println("aslSourcePath: \"./src/main/asl\";");
		pw.println("}");
		pw.close();
		return file;
	}
}
