package tiroteio.exp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Team {

	private Set<AgentTypeCount> setOfAgentTypes = new TreeSet<AgentTypeCount>();

	private String teamName;

	public Team(String name, AgentType type, int count) {
		teamName = name;

		setOfAgentTypes.add(new AgentTypeCount(type, count));
	}

	public Team(String name, AgentType... types) {
		teamName = name;
		for (AgentType at : types) {
			setOfAgentTypes.add(new AgentTypeCount(at, 1));
		}
	}
	
	public Team(String name, AgentTypeCount... types) {
		teamName = name;
		for (AgentTypeCount at : types) {
			setOfAgentTypes.add(at);
		}
	}

	public int getAgentCount() {
		return setOfAgentTypes.size();
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String toString() {

		Iterator<AgentTypeCount> it = setOfAgentTypes.iterator();
		String strContent = "";
		if (it.hasNext()) {
			AgentTypeCount atc = it.next();
			strContent += String.format("%s%d", atc.getAgentType().getName(),
					atc.getCount());
		}

		while (it.hasNext()) {
			AgentTypeCount atc = it.next();
			strContent += String.format("_%s%d", atc.getAgentType().getName(),
					atc.getCount());
		}

		return strContent;
	}

	public Iterable<AgentTypeCount> generateAgentTypeCount() {
		return setOfAgentTypes;
	}

	public Collection<String> generateStringMAS2J(File pathMAS2J) {
		Collection<String> collString = new ArrayList<>(setOfAgentTypes.size());

		File logMSPath = new File(pathMAS2J, getTeamName());

		if (logMSPath.mkdirs()) {

			for (AgentTypeCount atc : setOfAgentTypes) {
				collString.add(String.format("%s\t#%d%n", 
						atc.getAgentType().getMAS2JString(getTeamName(), logMSPath),
						atc.getCount()));
			}
			return collString;
		} else {

			return Collections.emptyList();
		}
	}
}
