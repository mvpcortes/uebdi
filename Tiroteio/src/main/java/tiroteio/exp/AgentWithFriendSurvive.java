package tiroteio.exp;

import java.awt.Point;

import tiroteio.entity.AgentTiroteio;

class AgentWithFriendSurvive extends AgentTiroteio{
	private int friendSurvive;

	public AgentWithFriendSurvive(String n, Point p, String _team) {
		super(n, p, _team);
	}

	public int getFriendSurvive() {
		return friendSurvive;
	}

	public void setFriendSurvive(int friendSurvive) {
		this.friendSurvive = friendSurvive;
	}

	public void incFriendSurvive(int fSurvive) {
		this.friendSurvive+=fSurvive;
	}
	
	
}