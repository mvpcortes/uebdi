package tiroteio;

import tiroteio.entity.AgentTiroteio;
import br.uff.ic.uebdi.mapmodel.entities.Entity;
import br.uff.ic.uebdi.mapmodel.entities.Wall;

enum Type {
	agent,
	wall,
	entity;
	
	public static Type getType(Entity e){
		if(e instanceof AgentTiroteio){
			return Type.agent;
		}else if (e instanceof Wall){
			return Type.wall;
		}else{
			return entity;
		}
	}
}
