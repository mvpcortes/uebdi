package tiroteio;

import jason.architecture.AgArch;
import jason.asSemantics.Agent;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.IIntentionBase;
import br.uff.ic.uebdi.IntentionBaseException;
import br.uff.ic.uebdi.IntentionBaseSimple;
import br.uff.ic.uebdi.UnifierIterator;
import br.uff.ic.uebdi.WrapperJasonUEBDI;
import br.uff.ic.util.TermHelper;

public class TiroteioAgArch extends AgArch {

	public LogAgentMentalState logMS;

	private Logger logger;

	private boolean isEnvRegistered = false;
	
	private MindInformation mindInformation;

	public static final String LOG_MS_FILE = "logMS";

	public int cycle = 0;

	public long startCycleTime = 0;

	//public long perceiveCount = 0;

	public static InspectAgentInformation inspect = new InspectAgentInformation();

	public TiroteioAgArch() {
		mindInformation = new MindInformation();
	}

	private Logger getLogger() {
		if (logger == null)
			logger = Logger.getLogger(this.getClass().getName() + ":"
					+ getTS().getUserAgArch().getAgName());

		return logger;
	}

	private int getCycle() {
		return cycle;
	}

	@Override
	public void reasoningCycleStarting() {
		cycle++;
		super.reasoningCycleStarting();
		startCycleTime = getUserTime();
		saveMindInformation();

	}

	private static long getUserTime() {
		/*
		 * long nUserTime = 0L; ThreadMXBean bean =
		 * ManagementFactory.getThreadMXBean( ); nUserTime =
		 * bean.isCurrentThreadCpuTimeSupported( ) ?
		 * bean.getCurrentThreadUserTime( ) : 0L;
		 * 
		 * return nUserTime;
		 */
		return System.nanoTime();
	}

	@Override
	public void init() {
		String nameFile = getTS().getSettings().getUserParameter(LOG_MS_FILE);

		if (nameFile == null) {
			nameFile = "";
		}

		nameFile += "\\" + getTS().getUserAgArch().getAgName();
		nameFile += ".logMS";

		File f = new File(nameFile);
		try {
			if (!f.exists()) {

				f.createNewFile();
			}

			getLogger().info("Create logMS to file " + f.toString());
			logMS = new LogAgentMentalState(f);
		}

		catch (FileNotFoundException e) {
			getTS().getLogger().severe(
					"Problem on create file to log mental state. "
							+ e.toString());
		} catch (IOException e) {
			getTS().getLogger().severe(
					"Problem on create file to log mental state. "
							+ e.toString());

		}

		// cria e associa ao inspetor.
		synchronized (inspect) {
			getLogger().info("Creating inspect");
			if (!inspect.isRunning()) {
				inspect.doRun();
			}
		}

		getLogger().info("Try register in inspect.");
		inspect.registerArch(this);

		super.init();
	}

	@Override
	public void stop() {
		logMS.close();
		inspect.unregisterArch(this);
	}

	/**
	 * Sobrecarregado permitir matar o agente quando sua energia chegar a zero.
	 */
	@Override
	public List<Literal> perceive() {

		List<Literal> percepts = super.perceive();

		if (percepts == null)
			return null;

		Iterator<Literal> it = percepts.iterator();
		while (it.hasNext()) {
			Literal lit = it.next();
			if (lit != null) {
				if ("energy".equals(lit.getFunctor())) {
					int life = TermHelper.getInteger(lit, 0);
					if (life <= 0) {
						String name = this.getTS().getUserAgArch().getAgName();
						this.getRuntimeServices().killAgent(name);
					}
				}
			}
		}
		
//		perceiveCount = 0;
//		for (Literal literal : percepts) {
//			if (literal.getFunctor().indexOf("agent") >= 0) {
//				perceiveCount++;
//			}
//		}

//		getLogger().info(String.format("perceiveCount=%d", perceiveCount));
		return percepts;
	}

	private void saveMindInformation() {
		IEmotionBase eb = null;
		IIntentionBase ib = null;

		// ==========================
		int energy = -1;
		int lastInfoCount = -1;
		double fear = -1;
		double angry = -1;
		String intention;
		long cycle = getCycle();
		long prev = startCycleTime;
		long actual = getUserTime();
		long delayReasoning = actual - prev;

		// ==========================
		if (getTS().getAg() instanceof WrapperJasonUEBDI) {
			WrapperJasonUEBDI w = (WrapperJasonUEBDI) this.getTS().getAg();
			ib = w.getIB();
			eb = w.getEB();
		} else {
			ib = new IntentionBaseSimple(this.getTS());
		}

		//
		{
			Literal query = Literal.parseLiteral(TiroteioEnvironment.STR_ENERGY
					+ ("(_)"));
			Iterator<Literal> it;
			final jason.asSemantics.Agent ag = getTS().getAg();
			Iterator<Unifier> itUnifier = query.logicalConsequence(ag,
					new Unifier());
			it = new UnifierIterator(itUnifier, query);

			if (it.hasNext()) {
				energy = TermHelper.getInteger(it.next(), 0);
			}
		}

		//
		{
			// Literal query =
			// Literal.parseLiteral(AgentTiroteio.STR_LAST_INFO_AGENT +
			// ("(_)"));

			lastInfoCount = 0;
			Iterator<Literal> it = getTS().getAg().getBB()
					.getCandidateBeliefs(AgentTiroteio.LAST_INFO_PREDICATE);
			if (it != null)
				while (it.hasNext()) {
					it.next();
					lastInfoCount++;
				}

		}

		// ==============================

		// ===============================
		if (eb != null) {
			fear = TiroteioUEBDI.getFear(eb);
			angry = TiroteioUEBDI.getAngry(eb);
		}

		try {

			intention = "-";
			if (ib.intend(Literal.parseLiteral("hunter")))
				intention = "hunter";
			else if (ib.intend(Literal.parseLiteral("scape")))
				intention = "scape";

		} catch (IntentionBaseException e) {
			intention = "-";
			this.getLogger().warning("Problem in IntentionBase " + e);
		}
		
		Agent a = getTS().getAg();
		
		int perceptCount = -1;
		if(a instanceof WrapperJasonUEBDI){
			WrapperJasonUEBDI wag = (WrapperJasonUEBDI)a;
			
			TiroteioUEBDI uebdi = (TiroteioUEBDI)wag.getUEBDI();
			perceptCount = uebdi.getPerceptCount();
		}else if(a instanceof AgentTiroteio){
			AgentTiroteio at = (AgentTiroteio)a;
			perceptCount = at.getPerceptCount();
		}

		synchronized (mindInformation) {
			getLogger()
					.info(String
							.format("setInformation(%d, %.3f, %.3f, %d, %s, %d, %d, %d)",
									cycle, fear, angry, energy, intention,
									perceptCount, delayReasoning,
									lastInfoCount));
			mindInformation.setInformation(cycle, fear, angry, energy,
					intention, perceptCount, delayReasoning, lastInfoCount);
		}
	}

	public void doLogMS(final long time) {
		
		MindInformation cloneMindInformation;
		synchronized (mindInformation) {
			cloneMindInformation = (MindInformation) mindInformation.clone();
			cloneMindInformation.setTime(time);
		}
		
		logMS.log(time, cloneMindInformation);
	}

	public boolean isEnvRegistered() {
		if (!isEnvRegistered) {
			Literal query = Literal.parseLiteral("registered");
			Iterator<Literal> it;
			final jason.asSemantics.Agent ag = getTS().getAg();
			Iterator<Unifier> itUnifier = query.logicalConsequence(ag,
					new Unifier());
			it = new UnifierIterator(itUnifier, query);

			return isEnvRegistered = it.hasNext();

		}else{
			return true;
		}
	}

}
