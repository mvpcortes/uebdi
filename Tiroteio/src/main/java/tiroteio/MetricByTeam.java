package tiroteio;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class MetricByTeam {
	private String nameTeam = "";
	private MetricCalculator mcKilled;
	private MetricCalculator mcLifeTime;
	private MetricCalculator mcFriendSurviving;
	private MetricCalculator mcFriendlyFire;
	private MetricCalculator mcFriendKilled;

	private List<MetricCalculator> arrayMC;

	public MetricByTeam(String n) {
		nameTeam = n;
		mcKilled = new MetricCalculator("Kill", "");
		mcLifeTime = new MetricCalculator("LifeTime", "%");
		mcFriendSurviving = new MetricCalculator("SuvivingF", "");
		mcFriendlyFire = new MetricCalculator("FriendlyFire", "");
		mcFriendKilled = new MetricCalculator("KilledF", "");

		arrayMC = new ArrayList<MetricCalculator>();
		for (Field f : getClass().getDeclaredFields()) {
			if (f.getType().equals(MetricCalculator.class)) {
				try {
					arrayMC.add((MetricCalculator) f.get(this));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public final List<MetricCalculator> getMetrics(){
		return arrayMC;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof MetricByTeam) {
			return nameTeam.equals(((MetricByTeam) o).nameTeam);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return nameTeam.hashCode();
	}

	public void addToMedia(double _timeLife, int _killedAgents,
			int _friendKilled, int _friendlyFire, int friendSurvive) {
		mcKilled.insertValueToMedia(_killedAgents);
		mcLifeTime.insertValueToMedia(_timeLife);
		mcFriendSurviving.insertValueToMedia(friendSurvive);
		mcFriendlyFire.insertValueToMedia(_friendlyFire);
		mcFriendKilled.insertValueToMedia(_friendKilled);
	}

	public void addToDev(double _timeLife, int _killedAgents,
			int _friendKilled, int _friendlyFire, int friendSurvive) {
		mcKilled.insertValueToDev(_killedAgents);
		mcLifeTime.insertValueToDev(_timeLife);
		mcFriendSurviving.insertValueToDev(friendSurvive);
		mcFriendlyFire.insertValueToDev(_friendlyFire);
		mcFriendKilled.insertValueToDev(_friendKilled);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("grouppoint\t");
		sb.append(this.nameTeam);
		sb.append("\t");

		for (MetricCalculator mc : arrayMC) {
			sb.append(String.format("%.3f\t", mc.getSum()));
			sb.append(String.format("%.3f\t", mc.getMedia()));
			sb.append(String.format("%.3f\t", mc.getDev()));
		}
		return sb.toString();
	}
	
	public MetricCalculator getMcKilled() {
		return mcKilled;
	}

	public MetricCalculator getMcLifeTime() {
		return mcLifeTime;
	}

	public MetricCalculator getMcFriendSurviving() {
		return mcFriendSurviving;
	}

	public MetricCalculator getMcFriendlyFire() {
		return mcFriendlyFire;
	}

	public MetricCalculator getMcFriendKilled() {
		return mcFriendKilled;
	}
}
