// Agent aluno in project ProfessorAluno

/* Initial beliefs and rules */
{include("database.asl")}
selectedDataCount(100).
/* Initial goals */

/* Plans */
@kqmlAskFromProfessor[atomic]
+!kqml_received(SENDER, askOne, ask(ID, CONTENT), IDM): selectedDataCount(COUNT) & br.uff.ic.professoraluno.findSentence(CONTENT, COUNT, LIST)<-
	//.log.log(info, "init process ask: ", ID, " with list: ", LIST);
	+sum(ID, 0);
	for(.member(X, LIST)){
		.getTerm(X, 0, Y);
		?sum(ID, S);
		-+sum(ID, S+Y);
		//.wait(1);
	}
	
	?sum(ID, S);
	status(LENGTH, STRESS);
	.send(SENDER, tell, answer(ID, S), IDM);
	-sum(ID, _);
	.
	
@kqmlAskFromProfessor_fail[atomic]
-!kqml_received(SENDER, askOne, ask(ID, CONTENT), IDM):true<-	
	.log.log(info, "init process ask FAIL");
	-sum(ID, _);
	.

@correct_[atomic]	
+correctness(ID)<-
	.log.log(info, "receive : ", correctness(ID));
	.send(professor, tell, alunoOK(ID));
	-correctness(ID);
	.
	
@tilCorrect_[atomic]	
+timeExpired(ID):decValue(DEC)<-
	.log.log(info, "receive : ", timeExpired(ID));
	.send(professor, tell, alunoOK(ID));
	?selectedDataCount(COUNT);
	-+selectedDataCount(COUNT-DEC);
	-timeExpired(ID);
	.