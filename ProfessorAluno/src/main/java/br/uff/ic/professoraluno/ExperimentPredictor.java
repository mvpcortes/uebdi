package br.uff.ic.professoraluno;

import jason.infra.centralised.RunCentralisedMAS;

import java.io.File;
import java.io.FileInputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class ExperimentPredictor {

	private static final int LIST_SIZE = 100;
	
	private static final String logPropFile = "logging.properties";

	private static Logger logger;

	/**
	 * Modelo algoritmo de como se comporta o experimento. Tentando prever o
	 * resultado
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
	    if (new File(logPropFile).exists()) {
            try {
                LogManager.getLogManager().readConfiguration(new FileInputStream(logPropFile));
            } catch (Exception e) {
                System.err.println("Error setting up logger:" + e);
            }
        } else {
            try {
                LogManager.getLogManager().readConfiguration(RunCentralisedMAS.class.getResource("/templates/" + logPropFile).openStream());
            } catch (Exception e) {
                System.err.println("Error setting up logger:" + e);
                e.printStackTrace();
            }
        }
	    
		logger = Logger.getLogger("simulate");
//		 for(int dec = 20; dec < 21; dec++){
//		 //
//		 //System.out.println("============================================================");
//		 // System.out.printf("init with dec = %d%n", dec );
//		 //System.out.printf("%d\t", dec);
//		 simulateNonEmotional(dec);
//		 //
//		 //System.out.println("============================================================");
//		 }

		for (int dec = 20; dec < 21; dec++) {
			for (int inc = 1; inc < 2; inc++) {
				//logger.info(String.format("%d\t%d\t", dec, inc));
				simulateEmotional(dec, inc);
			}
		}
	}

	private static void simulateNonEmotional(final int dec) {
		double nota = 0; // nota atual
		int L = LIST_SIZE; // valor tamanho da lista
		int delay = 200; // delay do professor

		while (delay > 0 && nota >= 0) {

			if (delay >= L) {
				//nota += (((double) L) / ((double) LIST_SIZE));
				double sum = L*(100.0+(100.0-L+1.0))/2.0;//B6*(100+(100-B6+1))/2//1.0 - Math.min(Math.abs(L-5050)/5050, 1);//=1-M�NIMO(ABS(C6-5050)/5050; 1)
				double tempNota = 1.0 - Math.min(1.0, Math.abs(sum-5050.0)/5050.0);
				nota += tempNota;
			} else {
				L = Math.max(L - dec, 1);
				nota -= 1;
			}

			 System.out.printf("%d\t%.3f\t%d%n", delay, nota, L);
			//
			delay--;
		}
		System.out.printf("%d\t%.3f\t%d%n", delay, nota, L);
	}

	private static void simulateEmotional(final int dec, final int inc) {
		double nota = 0; // nota atual
		int L = LIST_SIZE; // valor tamanho da lista
		int delay = 200; // delay do professor

		while (delay > 0) {

			if (delay >= L) {
				//nota += (((double) L) / ((double) LIST_SIZE));
				double sum = L*(100.0+(100.0-L+1.0))/2.0;//B6*(100+(100-B6+1))/2//1.0 - Math.min(Math.abs(L-5050)/5050, 1);//=1-M�NIMO(ABS(C6-5050)/5050; 1)
				double tempNota = 1.0 - Math.min(1.0, Math.abs(sum-5050.0)/5050.0);
				nota += tempNota;
				L = Math.min(LIST_SIZE, L + inc);
			} else {
				L = Math.max(L - dec, 1);
				nota -= 1;
			}

			// System.out.printf("%d\t%.3f\t%d%n", delay, nota, L);
			//
			System.out.println(String.format("%d\t%d\t%d\t%.3f\t%d", dec, inc, delay, nota, L));
			delay--;
		}
		// System.out.printf("%d\t%.3f\t%d%n", delay, nota, L);
		System.out.println(String.format("%d\t%d\t%d\t%.3f\t%d", dec, inc, delay, nota, L));
	}

}
