package br.uff.ic.professoraluno;

import jason.asSyntax.Literal;
import jason.bb.BeliefBase;
import jason.bb.ChainBBAdapter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import br.uff.ic.util.TermHelper;


/**
 * Classe que ir� melhorar o desempenho na consulta das cren�as para responder o professor
 * @author Marcos
 *
 */
public class SortedBeliefBase extends ChainBBAdapter{

	Map<String, List<Literal>> mapSpeededLiterals = new HashMap<String, List<Literal>>();
	
	 public synchronized boolean add(Literal l) {
		 treatAdd(l);
		 return nextBB.add(l);
     }
	 
	 private void treatAdd(Literal l){
		 if(l.isLiteral()){
			 List<Literal> list = mapSpeededLiterals.get(l.getFunctor());
			 if(list == null){
				 list = new ArrayList<Literal>(50);
				 mapSpeededLiterals.put(l.getFunctor(), list);
			 }
			 list.add(l);
			 Collections.sort(list, new Comparator<Object>(){

				public int compare(Object a, Object b) {
					double valueA = TermHelper.getDouble((Literal)a, 0);
					double valueB = TermHelper.getDouble((Literal)b, 0);
					
					if(Math.abs(valueA - valueB) < 0.0001){
						return 0;
					}else if(valueA < valueB){
						return +1;
					}else{
						return -1;
					}
				}
				 
			 }
			 );
		 }
	 }

    public boolean add(int index, Literal l) {
    	treatAdd(l);
        return nextBB.add(index, l);
    }
    
    public List<Literal> getSortedList(String functor){
    	return mapSpeededLiterals.get(functor);
    }

}
