// Internal action code for project ProfessorAluno

package br.uff.ic.professoraluno;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.WrapperJasonUEBDI;
import br.uff.ic.util.TermHelper;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;
import jason.bb.BeliefBase;

public class findSentence extends DefaultInternalAction {

	
	@Override
	public int getMinArgs() { return 2; }
	
	@Override
    public int getMaxArgs() { return 3; }
	
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
	
		if(!(args[0] instanceof Literal))
			throw new JasonException("param 0 is not a Literal");
	
		if(args.length == 2){
			if(	!(args[1].isVar() && !args[1].isGround()))
				throw new JasonException("param 2 should be Var and not ground");
		}
		if(args.length == 3){
			if(!(args[1].isNumeric()))
				throw new JasonException("param 1 is not a number");
		
			if(	!(args[2].isVar() && !args[2].isGround()))
				throw new JasonException("param 2 should be Var and not ground");
		}
		
	}
	
	/**
	 * Retorna o valor L usado na poda das cren�as
	 * @return
	 */
	private int getL(TransitionSystem ts, int sizeList){
		Agent a = ts.getAg();
		if(a instanceof WrapperJasonUEBDI){
			IEmotionBase eb = ((WrapperJasonUEBDI) a).getEB();
			double intensity = eb.getIntensity("stress");
			//return (int) Math.round(Math.max(1, ((double)sizeList)/Math.pow(2, intensity)));
			return (int)Math.round(Math.max(1, sizeList*(1-(intensity/StressEmotionDyn.maxStress)) ));
		}else{
			return sizeList;
		}
	}
	
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

    	checkArguments(args);
    	
        ts.getAg().getLogger().info("executing internal action 'br.uff.ic.professoraluno.findSentence'");
        
        Literal lit = (Literal)args[0];
        
        BeliefBase bb = ts.getAg().getBB();
        if(bb instanceof SortedBeliefBase){
        	SortedBeliefBase sbb = (SortedBeliefBase)bb;
        	final List<Literal> list = sbb.getSortedList(lit.getFunctor());
        	
        	double percent;
        	if(args.length == 3)
        		percent = TermHelper.toDouble(args[1]);
        	else{
        		percent = getL(ts, list.size());
        	}
        	int L = 0; 
        	
        	if(percent <= 1)
        	{
        		L = Math.round(Math.max(1, Math.round(list.size()*percent)));
        	}else{
        		L = (int) Math.round(Math.max(1, Math.min(list.size(), percent)));
        	}
        	
        	final int size = L;
        	 ts.getAg().getLogger().info(String.format("my L is: %d", L));
             
        	 ListTerm all = new ListTermImpl();
             ListTerm tail = all;

             
             Iterator<Literal> it = list.iterator();
             for(int i = 0; i < size; i++){
            	 Literal litSelected = it.next();
            	 tail = tail.append(litSelected);
             }

             return un.unifies(args[args.length-1], all);
        }else
        	throw new Exception("BB inv�lida");
        
       
    }
}
