package br.uff.ic.professoraluno;

// Environment code for project ProfessorAluno
import jason.asSyntax.*;
import jason.environment.*;

import br.uff.ic.uebdi.MainWithConfigParams;
import br.uff.ic.util.TermHelper;

public class ProfessorAlunoEnv extends Environment {
 
	private double decValue;
    /** Called before the MAS execution with the args informed in .mas2j */
    @Override
    public void init(String[] args) {
        super.init(args);
        decValue = MainWithConfigParams.queryDouble("decValue(_)");
        if(decValue >= 0){
        	addPercept(ASSyntax.createLiteral("decValue", ASSyntax.createNumber(decValue)));
        }else{
        	//trata o caso de existir decEmoValue
        	decValue = MainWithConfigParams.queryDouble("decEmoValue(_)");
        	if(decValue >= 0){
            	addPercept(ASSyntax.createLiteral("decEmoValue", ASSyntax.createNumber(0)));
        	}
        }
    }

    double _nota;
    int _respondida;
    int _nPerguntas;
    int _nDelay;
    int _L;
    double _emo;
    @Override
    public boolean executeAction(String agName, Structure action) {
        if("status".equals(action.getFunctor())){
        	if("aluno".indexOf(agName) >= 0){
        	_L = TermHelper.getInteger(action, 0);
        	_emo = TermHelper.getDouble(action, 1);
        	}else{
        		//status(COUNT, DELAY, NOTA, OK);
	        	_nPerguntas = TermHelper.getInteger(action, 0);
	        	_nDelay = TermHelper.getInteger(action, 1);
	        	_nota = TermHelper.getDouble(action, 2);
	        	_respondida = TermHelper.getInteger(action, 3);
	        	System.out.printf("pergunta:\t%.0f\t%d\t%d\t%.3f\t%s\t%d\t%.3f%n", 
	        			decValue,
	        			_nPerguntas,
	        			_nDelay,
	        			_nota,
	        			_respondida !=0?"true":"false", 
	        			_L,
	        			_emo);
        	}
        	return true;
        }
        
        return false;
    }

    /** Called before the end of MAS execution */
    @Override
    public void stop() {
    	System.out.printf("final:\t%.0f\t%d\t%d\t%.3f\t%s\t%d\t%.3f%n",
    			decValue,
    			_nPerguntas,
    			_nDelay,
    			_nota,
    			_respondida !=0?"true":"false", 
    			_L,
    			_emo);
        super.stop();
    }
}