package br.uff.ic.professoraluno;
import jason.asSemantics.Event;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.IEmotionDyn;
import br.uff.ic.uebdi.IIntentionBase;
import br.uff.ic.uebdi.MainWithConfigParams;
import br.uff.ic.uebdi.SmartBelieves;
import br.uff.ic.util.TermHelper;


public class StressEmotionDyn implements IEmotionDyn {

	private static String strStress = "stress";
	
	static double maxStress = 100;
	
	private static Literal correctness = Literal.parseLiteral("correctness(X)[L]");
	private static Literal timeExpired = Literal.parseLiteral("timeExpired(X)[L]");
	private static String literalDecEmoValue = "decEmoValue(X)";
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	private int lastValueProcessed = -1;
	
	private Double decValue = null;
	public void firstReviewEmotion(List<Literal> listPerc, IEmotionBase eb,
			IIntentionBase intentionBase) {
	}
	

	private double normalize(double prevValue){
		return Math.max(0.0, Math.min(maxStress, prevValue));
	}
	
	public void secondReviewEmotion(BeliefBase bb, Queue<Event> events,
			IEmotionBase eb, IIntentionBase intentionBase) {
		if(bb != null){
			{
				Iterator<Literal> lit = query(timeExpired, bb);
				if(lit != null){
					double value = eb.getIntensity(strStress);
					while(lit.hasNext()){
						Literal literal = lit.next();
						int idQuestion = TermHelper.getInteger(literal, 0);
						if(idQuestion > lastValueProcessed){
							logger.info("dec by: " + literal);
							lastValueProcessed = idQuestion;
							value += getIncStressValue(value);

						}
					}
					
					value = normalize(value);
					eb.changeIntensity(value, strStress);
				}
			}
			
			//faz o decréscimo.
			{
				Iterator<Literal> lit = query(correctness, bb);
				if(lit != null){
					double value = eb.getIntensity(strStress);
					while(lit.hasNext()){
						Literal literal = lit.next();
						int idQuestion = TermHelper.getInteger(literal, 0);
						if(idQuestion > lastValueProcessed){
							logger.info("dec by: " + literal);
							lastValueProcessed = idQuestion;
							value -= getDecStressValue(value);
						}
					}
					value = normalize(value);
					eb.changeIntensity(value, strStress);
				}
				
				
			double actualValue = eb.getIntensity(strStress);
			logger.info(String.format("stress:=%.3f", actualValue));
			}
		}
		
	}

	private Iterator<Literal> query(Literal c, BeliefBase bb) {
		Unifier voidUnifier = new Unifier();
		SmartBelieves sb = (SmartBelieves) bb;
		return sb.believesIt(c, voidUnifier);
	}



	public boolean criticalContext(IEmotionBase eb, BeliefBase bb) {
		return true;
	}
	
	
	public double getDecStressValue(double value){
		if(decValue == null){
			decValue = MainWithConfigParams.queryDouble(literalDecEmoValue);
		}
		
		return decValue;
	}
	public double getIncStressValue(double value){
		return 15;
	}

}
