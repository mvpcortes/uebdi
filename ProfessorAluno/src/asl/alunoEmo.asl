// Agent aluno in project ProfessorAluno

/* Initial beliefs and rules */
{include("database.asl")}
/* Initial goals */

/* Plans */
@kqmlAskFromProfessor[atomic]
+!kqml_received(SENDER, askOne, ask(ID, CONTENT), IDM): br.uff.ic.professoraluno.findSentence(CONTENT, LIST)<-
	.log.log(info, "init process ask: ", ID, " with list: ", LIST);
	+sum(ID, 0);
	for(.member(X, LIST)){
		.getTerm(X, 0, Y);
		?sum(ID, S);
		-+sum(ID, S+Y);
		//.wait(1);
	}
	
	?sum(ID, S);
	status(LENGTH, STRESS);
	.send(SENDER, tell, answer(ID, S), IDM);
	-sum(ID, _);
	.log.log(info, "fim ask");
	.
	
@kqmlAskFromProfessor_fail[atomic]
-!kqml_received(SENDER, askOne, ask(ID, CONTENT), IDM):true<-	
	.log.log(info, "init process ask FAIL");
	-sum(ID, _);
	.

@correct_[atomic]	
+correctness(ID):true<-
	.log.log(info, "receive : ", correctness(ID));
	.send(professor, tell, alunoOK(ID));
	-correctness(ID);
	.
	
@tilCorrect_[atomic]	
+timeExpired(ID):true<-
	.log.log(info, "receive : ", timeExpired(ID));
	.send(professor, tell, alunoOK(ID));
	-timeExpired(ID);
	.