// Agent sample in project ProfessorAluno

/* Initial beliefs and rules */
delay(200).
nota(10).
askCount(0).
/* Initial goals */

!start.

/* Plans */

@startMain[atomic]
+!start : nota(NOTA) & NOTA >= 0 & delay(DELAY) & DELAY > 0 & askCount(COUNT)<-
	!generateAsk(ASK, CORRECT_ANSWER);
	.cputime(PREV_TIME);
	.send(aluno, askOne, ask(COUNT, ASK), REAL_ANSWER);
	.cputime(ACTUAL_TIME);
	DELTA = (ACTUAL_TIME - PREV_TIME)*(1/1000000);
	if(DELTA > DELAY){
		.log.log(info, "timeout");
		NEW_NOTA = NOTA-1;
		.send(aluno, tell, timeExpired(COUNT));
		.log.log(info, timeExpired(COUNT));
		+answerResult(COUNT, 0);
	}
	else
	{
		.log.log(info, "oka");
		answer(ID, ANSWER) = REAL_ANSWER;		
		.log.log(info, "ok");
		INC_NOTA = 1-math.min(1, math.abs(ANSWER - CORRECT_ANSWER)/CORRECT_ANSWER);
		.log.log(info, ANSWER, " : ", CORRECT_ANSWER, " : ", math.abs(ANSWER - CORRECT_ANSWER), " : ",  math.min(1, math.abs(ANSWER - CORRECT_ANSWER)/CORRECT_ANSWER));
		NEW_NOTA = INC_NOTA + NOTA; 
		.send(aluno, tell, correctness(COUNT));
		.log.log(info, correctness(COUNT));
		+answerResult(COUNT, 1);
	}
	
	
	-+nota(NEW_NOTA);
	-+delay(DELAY-1);
	-+askCount(COUNT+1);
	.log.log(info, "fimStart");
	.

@trateAlunoOK[atomic]
+alunoOK(COUNT_PREV)[L]: true<-
	-alunoOK(COUNT_PREV)[L];
	?nota(NOTA);
	?delay(DELAY);
	?askCount(COUNT);
	?answerResult(COUNT_PREV, OK);
	-answerResult(COUNT_PREV, OK);
	status(COUNT, DELAY, NOTA, OK);
	!!start;
	.

@startFinish[atomic]	
+!start:true<- .stopMAS.

@startFail[atomic]
-!start:true<- .stopMAS.

@generateAsk[atomic]
+!generateAsk(ASK, ANSWER):true<-
	ASK = a(Y);
	ANSWER = 5050;
	.