require 'FileUtils'
require 'set'


class AgentConfig
#	attr_reader name, hashAttr
#	attr_writer name, hashAttr
	
	
	def initialize(*args)

		@name = "noname"
		@agentClass = nil
		
		if(args.size >= 1)
			@name = args[0]
		end
		
		@hashAttr = Hash.new
		@hashAttr["verbose"] = "0"
		
		if(args.size >=2)
			args[1].each{|key, value| @hashAttr[key] = value};
		end
		
		if(args.size >=3)
			@agentClass = args[2]
		end
	end
	
	def to_s (id)
	
		if(id > 0) then
			strOut = @name + "#{id} #{@name}.asl\ ["
		else
			strOut = @name + " ["
		end
		
		listAttr = @hashAttr.to_a
		for i in (0 .. listAttr.size-2)
		#	puts(@name, ": enter in #{i}\n");
			strOut += "#{listAttr[i][0].to_s}=#{listAttr[i][1].to_s},"
		end
		#puts(@name, ": finish loop\n");
		strOut += "#{listAttr[listAttr.size-1][0].to_s}=#{listAttr[listAttr.size-1][1].to_s}]"
		
		if(@agentClass != nil)
			strOut += " agentClass #{@agentClass}"
		end
		return strOut
	end
	
	
	def getName()
		return @name
	end
end

class Experiment

	STR_DIR_OUT = "OUT";
	STR_DIR_MAS2J = "MAS2J";
	STR_DIR_LOG = "log";
	STR_DIR_ASL = "asl";
	STR_DIR_ASL_SOURCE = "../src/main/asl/.";
	def main(params)
		agent_list = [
			#AgentConfig.new("random", Hash["verbose"=>"2"]),
			AgentConfig.new("random"),
			AgentConfig.new("playerTFT"),
			AgentConfig.new("playerAllC"),
			AgentConfig.new("playerAllD"),
			AgentConfig.new("pavlov"),
			AgentConfig.new("jiang", Hash["emotionalDyn"=>"\"br.uff.ic.ipd.EmotionDynJiang\"", "verbose"=>"0"], "br.uff.ic.uebdi.WrapperJasonUEBDI")
			];
		
		
		puts "Deleting prev data"
		freeDirectories()
		puts "creating output and mas2j directories"
		createDirectories()
		versusExperiment(agent_list);
		joinXML();
	end
	
	def joinXML()
		#D:\UFF-Mestrado\project\workspace\Ipd\experiment>java -cp saxon.jar net.sf.saxon.Query query.xq -o:"m.xml"
		puts("make the join.xml");
		runApp("java -cp saxon.jar net.sf.saxon.Query query.xq -o:\"join.xml\"");
		
	end
	def freeDirectories()
		FileUtils.rm_rf STR_DIR_OUT
		FileUtils.rm_rf STR_DIR_MAS2J
		FileUtils.rm_rf STR_DIR_LOG
		FileUtils.rm_rf STR_DIR_ASL
		end
	
	def createDirectories()
		if !FileTest::directory?(STR_DIR_OUT)
			Dir::mkdir(STR_DIR_OUT)
		end
		
		if(!FileTest::directory?(STR_DIR_MAS2J))
			Dir::mkdir(STR_DIR_MAS2J)
		end
		
		if(!FileTest::directory?(STR_DIR_LOG))
			Dir::mkdir(STR_DIR_LOG)
		end
		
		if(!FileTest::directory?(STR_DIR_ASL))
			Dir::mkdir(STR_DIR_ASL);
			FileUtils.cp_r(STR_DIR_ASL_SOURCE, STR_DIR_ASL);
		end
	end
	
	
	
	def versusExperiment(agent_list)
	
		for i in (0 .. agent_list.size-1)
			for j in (i .. agent_list.size-1)
				#puts("combat(#{i}=>#{agent_list[i]}, #{j}=>#{agent_list[j]})");
				namefile = createMAS2J([agent_list[i],agent_list[j]]);
				runApp("java -jar ipd.jar #{namefile}");
			end
		end
	end
	
	def communityExperiment(agent_list)
		namefile = createMAS2J(agent_list);
		runApp("java -jar ipd.jar #{namefile}");
	end
	
	def runApp(strApp)
		IO.popen(strApp){|out| puts(out);}
	end
	
	def generatePrefix(listAc)
	
		if(listAc.size == 0)
			return "";
		else
			out = "";
			for i in (0 .. listAc.size-2)
				out += listAc[i].getName() + "_"
			end
			
			out = out + listAc[listAc.size-1].getName()
			return out;
		end
		
		
=begin
if(collAc.size() == 0)
	return "";
else if(collAc.size() == 1)
	return collAc.iterator().next().getName();
else
{
	StringBuilder sb = new StringBuilder();
	Iterator<AgentConfig> it = collAc.iterator();
	Iterator<AgentConfig> itNext = collAc.iterator();
	itNext.next();
	
	while(itNext.hasNext())
	{
		AgentConfig ac = it.next();
		itNext.next();
		sb.append(ac.getName());
		sb.append("_");
	}
	
	AgentConfig lastAc = it.next();
	sb.append(lastAc.getName());			
	return sb.toString();
}
=end
	end
	def createMAS2J(listAc)
	
		prefix = generatePrefix(listAc);
		filenameMAS = ".\\#{STR_DIR_MAS2J}\\#{prefix}.mas2j";
		filenameOUT = ".\\\\#{STR_DIR_OUT}\\\\#{prefix}OUT.xml";
		
		f = File.new(filenameMAS, "w")
		f.puts(
			"MAS ipd {\n",
			"infrastructure: Centralised\n",
			"environment: br.uff.ic.ipd.Ipd(\"#{filenameOUT}\")\n",
			"agents:\n",
			"arbitrer [verbose=0];\n");
		
		count = 0;
		setNames = Set.new [''];
		listAc.each{|ac|
			if(setNames.include? ac.getName) then
				count+=1
				f.puts(ac.to_s(count) + ";\n")
			else
				f.puts(ac.to_s(0) + ";\n");
				setNames.add? ac.getName
			end
			 #f.puts(ac.to_s + ";\n")}
			 puts(" #{ac.to_s(count)}, #{count}, #{setNames}")
		}
		f.puts("aslSourcePath: \"asl\";\n","}")
		
		f.close
		return filenameMAS
	end
end

Experiment.new().main([])
