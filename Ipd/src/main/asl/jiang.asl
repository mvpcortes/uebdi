/*** I'm a emotional player... ***/

{include("player.asl")}

   hate(X):- (X <=-2).
dislike(X):- (X <=-1) & (X > -2).
unhappy(X):- (X <= 0) & (X > -1).
  happy(X):- (X <= 1) & (X >  0).
   like(X):- (X <= 2) & (X >  1).
   love(X):- (X >  2).

//emotions
//emotion(source, hate | unlike | neutral | like | love)
//behavior
//if hate other: 	deny
//if dislike other:	pavlov
//if unhappy | happy other: tip to tat
//if like other: tit to tat
//if like: complain


+arrested(T,O)[source(arbitrer)]
  :  emotion(O,INT) & hate(INT)
  <-
  		//.print("I hate ", O);
  		!deny(T, O);
    .
    
 +arrested(T,O)[source(arbitrer)]
  :  emotion(O,INT) & dislike(INT)
  <-
  		//.print("I dislike ", O);
  		!pavlov(T, O);
    .
    
        
 +arrested(T,O)[source(arbitrer)]
  :  emotion(O,INT) & unhappy(INT)
  <-
  		//.print("I unhappy ", O);
  		!ttt(T, O);
    .
    
 +arrested(T,O)[source(arbitrer)]
  :  emotion(O,INT) & happy(INT)
  <-
  		//.print("I happy ", O);
  		!ttt(T, O);
    .
    
  +arrested(T,O)[source(arbitrer)]
  :  emotion(O,INT) & like(INT)
  <-
  		//.print("I like ", O);
  		!complain(T, O);
    .
  
  +arrested(T,O)[source(arbitrer)]
  :  emotion(O, INT) & love(INT)
  <-
  		//.print("I love ", O);
  		!complain(T, O);
    .
  

  +arrested(T,O)[source(arbitrer)]
  : not emotion(O, _)
 <-
  		//.print("I do not know ", O);
  		!complain(T, O);
    .
    
//+emotion(O)[emotionIntensity(INTENSITY)]: true
//<- //.print("emotion come from ", O, "with intensity", INTENSITY);
//.
    
    
//////////////////////////
    
//Pavlov repeat
+!pavlov(T,O)
  : lastPenality(Penality) & Penality <=1 & lastMyAction(O, Action) 
  <-
  		//.print("pavlov politics -action:", Action, "- to ", O);
  		//.print("I say ", Action, " to ", O);
    	.send(arbitrer, tell, play(T, Action));
    .
//Pavlov invert c
+!pavlov(T,O)
  :  lastPenality(Penality) & Penality > 1 & lastMyAction(O, c) 
  <-
  		//.print("pavlov politics -d- to ", O);
  		-lastMyAction(O, c);
  		+lastMyAction(O, d);
  		//.print("I say ", d, " to ", O);
    	.send(arbitrer, tell, play(T, d));
    .
    
//Pavlov invert d
+!pavlov(T,O)
  :  lastPenality(Penality) & Penality > 1 & lastMyAction(O,d) 
  <-
  		//.print("pavlov politics -c- to ", O);
  		+lastMyAction(O, c);
  		-lastMyAction(O, d);
  		//.print("I say ", c, " to ", O);
    	.send(arbitrer, tell, play(T, c));
    .
    
//Pavlov i dont know
+!pavlov(T,O)
  : true
  <-
  		if(lastMyAction(O,d))
  		{
  			-lastMyAction(O,d);
  		}	
  		+lastMyAction(O,c);
  		//.print("pavlov politics init-c- to ", O);
  		//.print("I say ", c, " to ", O);
    	.send(arbitrer, tell, play(T, c));
    .


+!complain(T,O)
  :  true
  <-	
  		//.print("complain politics to ", O);
		//.print("I say c to ", O);
    	.send(arbitrer, tell, play(T, c));
    .
+!deny(T, O):true
<-		
		//.print("deny politics to ", O);
		//.print("I say d to ", O);
    	.send(arbitrer, tell, play(T, d));
    .
    
+!ttt(T,O):lastOtherAction(O,Action)
<-
	//.print("ttt politics -Action:", Action, "-to ", O);
	//.print("I say ", Action, " to ", O);
	.send(arbitrer, tell, play(T, Action));
	.
	
+!ttt(T,O):true
<-
	+lastOtherAction(O, c);
	//.print("ttt politics -c-to ", O);
	//.print("I say c to ", O);
	.send(arbitrer, tell, play(T, c));
	.
	
+!take_records(T,O,M):true
<-
	-lastOtherAction(O, _);
	+lastOtherAction(O, M);
	.     