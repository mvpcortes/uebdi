// Agent random in project Ipd


{ include("player.asl") }
/*** ... and my strategy is ... ***/

// AllD:
// I always play d
@records[atomic]
+arrested(T,O)[source(arbitrer)]
  :  br.uff.ic.ipd.random(RESULT,2)
  <- 
		//.print("My random is", RESULT);
  		!playerRandom(RESULT, T, O).
  		
+!playerRandom(R, T, O):R == 0
<-
	.send(arbitrer, tell, play(T,c));
    //.print("I random played c, as always, against ",O," at time ",T).
	.
    
    
+!playerRandom(R, T, O):R == 1
<-
	.send(arbitrer, tell, play(T,d));
    //.print("I random played d, as always, against ",O," at time ",T);
	.

 

// I don't need records
+!take_records(T,O,M).
