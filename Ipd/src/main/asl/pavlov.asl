/*** I'm a emotional player... ***/

{include("player.asl")}

//pavlov
//@records[atomic]
+arrested(T,O)[source(arbitrer)]
:true
<-!pavlov(T,O).

//Pavlov repeat
+!pavlov(T,O)
  : lastPenality(Penality) & Penality >=3 & lastMyAction(O, Action) 
  <-
  		//.print("pavlov politics -action:", Action, "- to ", O);
  		//.print("I say ", Action, " to ", O);
    	.send(arbitrer, tell, play(T, Action));
    .
//Pavlov invert c
+!pavlov(T,O)
  :  lastPenality(Penality) & Penality < 3 & lastMyAction(O, c) 
  <-
  		//.print("pavlov politics -d- to ", O);
  		-lastMyAction(O, c);
  		+lastMyAction(O, d);
  		//.print("I say ", d, " to ", O);
    	.send(arbitrer, tell, play(T, d));
    .
    
//Pavlov invert d
+!pavlov(T,O)
  :  lastPenality(Penality) & Penality < 3 & lastMyAction(O,d) 
  <-
  	//	.print("pavlov politics -c- to ", O);
  		+lastMyAction(O, c);
  		-lastMyAction(O, d);
  	//	.print("I say ", c, " to ", O);
    	.send(arbitrer, tell, play(T, c));
    .
    
//Pavlov i dont know
+!pavlov(T,O)
  : true
  <-
  		if(lascAction(O,d))
  		{
  			-lastMyAction(O,d);
  		}	
  		+lastMyAction(O,c);
  		//.print("pavlov politics init-c- to ", O);
  		//.print("I say ", c, " to ", O);
    	.send(arbitrer, tell, play(T, c));
    .
    
+!take_records(T,O,M):true
<-
	-lastOtherAction(O, _);
	+lastOtherAction(O, M);
	.     