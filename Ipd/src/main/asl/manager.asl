// Agent manager in project Ipd

/* Initial beliefs and rules */

/* Initial goals */
/*
experiment(1, 1, 
			[
				agent(jiang1, jiang, "br.uff.ic.uebdi.WrapperJasonUEBDI", "br.uff.ic.ipd.Jiang"),
			 	agent(r2, random, agent, none)
			 ]),

listExperiment([
		experiment(1, 1, [agent(r1, random, agent, none), agent(r2, random, agent, none)]),
		experiment(2, 1, [agent(r3, random, agent, none), agent(p1, playerTFT, agent, none)])
		]).*/

listExperiment([
	experiment(1, 1, 
			[
				//agent(jiang1, jiang, "br.uff.ic.uebdi.WrapperJasonUEBDI", "br.uff.ic.ipd.JiangAgent"),
				agent(r2, random, agent, none),
				agent(r2, random, agent, none)
			]),
	experiment(2, 1, 
			[
				agent(r2, random, agent, none),
				//agent(jiang2, jiang, "br.uff.ic.uebdi.WrapperJasonUEBDI", "br.uff.ic.ipd.JiangAgent"),
				agent(pavlov, pavlov, agent, none)
			]),
			
	experiment(3, 1, 
			[
				agent(r2, random, agent, none),
				//agent(jiang3, jiang, "br.uff.ic.uebdi.WrapperJasonUEBDI", "br.uff.ic.ipd.JiangAgent"),
				agent(playerAllC, playerAllC, agent, none)
			]),
	experiment(4, 1, 
			[
				agent(r2, random, agent, none),
				//agent(jiang4, jiang, "br.uff.ic.uebdi.WrapperJasonUEBDI", "br.uff.ic.ipd.JiangAgent"),
				agent(playerAllD, playerAllD, agent, none)
			]),
	experiment(5, 1, 
			[
				agent(r2, random, agent, none),
				//agent(jiang5, jiang, "br.uff.ic.uebdi.WrapperJasonUEBDI", "br.uff.ic.ipd.JiangAgent"),
				agent(playerTFT, playerTFT, agent, none)
			])
]).

!loop.
/* Plans */

+!loop:listExperiment(LIST)<-
	.print("comešando um loop.");
	for(.member(X, LIST)){
		.print("Loop of ", X);
		!X;		
	};
	.print("final do loop.");
	.stopMAS.
	
/*
	.print("loop normal");
	if(.list(HLIST)){
		.print("is a list :/", HLIST);
		HLIST = [UNILIST|ZEROLIST];
		!UNILIST;		
	}else{
		.print("is not a list :/", HLIST);
		!HLIST;
	}
	.print("tentando remover elemento da lista e manter o resto");
	-+listExperiment([TLIST]);
	.print("fazendo loooopppp");	
	!!loop.
	*/

+!loop:listExperiment([])
<-
	.print("loop sair");
	.stopMAS.
	
//@experiment[atomic]
+!experiment(ID, ROUND, LIST_AGENT): true
<-
	for(.range(N_ROUND,0,ROUND-1)){
		.concat("out_", ID, "_", N_ROUND, ".xml", STR_FILE);
		createMatch(STR_FILE); 
		!start_experiment(LIST_AGENT);
		.wait({+finish});
		!free_experiment(LIST_AGENT);
		closeMatch;
	};
.


+!start_experiment(LIST_AGENT) : true 
<- 
	.print("start experiment");
	.create_agent(arbitrer, "./src/main/asl/arbitrer.asl", []);
	for( .member( agent(NICK, NAME, AGENT_CLASS, EMOTION_MODEL), LIST_AGENT)){
		.print("loop of startexperiment");
		.concat("./src/main/asl/", NAME, ".asl", FILE_AGENT);
		if(AGENT_CLASS == agent){
			.print("AGENT_CLASS == AGENT");
			.create_agent(NICK, FILE_AGENT , [verbose(0)]);
		}else{
			.print("AGENT_CLASS \\== AGENT");
			.create_agent(NICK, FILE_AGENT , [agentClass(AGENT_CLASS), uebdi(EMOTION_MODEL), verbose(1)]);
		}
	};
	.



+!free_experiment(LIST_AGENT):finish
<-
	.print("kill arbitrer >:P");
	.kill_agent(arbitrer);
	for(.member(agent(NICK, NAME, STR_, Y), LIST_AGENT)){
		.print("kill ", NICK, " >:P");
		.kill_agent(NICK);
	};
	.abolish(finish);
	?not finish;
	.