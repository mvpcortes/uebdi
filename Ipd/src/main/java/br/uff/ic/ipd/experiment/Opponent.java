package br.uff.ic.ipd.experiment;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/*
 * Represents a tag opponent
 */
public class Opponent {
	private String name;
	private long penality;
	private String action;
	private long delay;
	
	public Opponent()
	{
		clear();
	}
	public Opponent(String _name, int _penality, String _action, int _delay)
	{
		name = _name;
		penality = _penality;
		action = _action;
		delay = _delay;
	}
	
	public void clear() {
		setName("noname");
		setPenality(-1);
		setAction("none");
		setDelay(-1);		
	}
	public String getAction() {
		return action;
	}
	public long getDelay() {
		return delay;
	}
	public String getName() {
		return name;
	}
	public long getPenality() {
		return penality;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public void setDelay(long delay) {
		this.delay = delay;
	}
	public void setName(String name) {
		this.name = name;
	}

	public void setPenality(long l) {
		this.penality = l;
	}
	public void write(XMLStreamWriter xmlWriter)throws XMLStreamException {
		//
		xmlWriter.writeStartElement("opponent");
		xmlWriter.writeAttribute("name", getName());
		xmlWriter.writeAttribute("point", String.format("%d", getPenality()));
		xmlWriter.writeAttribute("action", getAction());
		xmlWriter.writeAttribute("delay", String.format("%d", getDelay()));
		xmlWriter.writeEndElement();
		
	
	}
}
