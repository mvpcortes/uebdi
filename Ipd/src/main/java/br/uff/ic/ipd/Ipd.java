package br.uff.ic.ipd;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import br.uff.ic.ipd.experiment.ExperimentOutput;
import br.uff.ic.ipd.experiment.Opponent;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.environment.Environment;


// Environment code for project ipd

public class Ipd extends Environment {

 
    TreeMap<String, Long> map = new TreeMap<String, Long>();
    
    private String fileOut = "";
    private ExperimentOutput exOut = null;

    static ArrayList<Opponent> staticVecOpn = new ArrayList<Opponent>();
    
    private void createExOut(String name)
    {
    	try {
    		OutputStream os = new FileOutputStream(name);
			exOut = new ExperimentOutput(os);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
    
    @Override
    public boolean executeAction(String agName, Structure action) {
        //logger.info("executing: "+action+", but not implemented!");
 		if(action.getFunctor().equals("createMatch"))
 		{
 			String str= null;
 			str = fileOut;			
 			createExOut(str);
 			
 			return true;
 		}
 		else if(action.getFunctor().equals("writeMatch"))
		{
			try
			{
    		   List<Term> terms = action.getTerms();
    		   if(terms.size() < 3)
    			   throw new JasonException("there should be 3 parameters");
    		   
    		   //pega o time
    		   long time = 0;
    		   {
    			   NumberTerm nt = (NumberTerm)terms.get(0);
    			   time = Math.round(nt.solve());
    		   }
    		   
    		  //mant�m o tamanho do vetor statico igual ao do outro
    		  synchronized(staticVecOpn)
    		  {
    			  final int realSize = (terms.size()-1);
	    		  if(staticVecOpn.size() != realSize)
	    		  {
	    			  staticVecOpn.clear();
	    			  staticVecOpn.ensureCapacity(realSize);
	    			  for(int i = 0; i < realSize; i++)
	    				  staticVecOpn.add(new Opponent());  
	    		  }
	    		  for(int i = 0; i < staticVecOpn.size(); i++)
	    		  {
	    			  Opponent opn = staticVecOpn.get(i);
	    			  Term term = terms.get(i+1);
	    			  getOpn((Literal)term, opn);
	    			  
	    			  
	    			  {//salva a soma
	    				  Long sum = opn.getPenality();
		    			  if(map.containsKey(opn.getName()))
		    			  {
		    				  sum += map.get(opn.getName());
		    			  }
		    			  
		    			  map.put(opn.getName(), sum);
	    			  }
	    		  }
	    		  getExOut().write(time, staticVecOpn);
    		  }
    		  return true;
	        }catch(Exception e)
	        {
	        	return false;
	        }
		}else if ( action.getFunctor().equals("closeMatch"))
		{
			getExOut().close();
			return true;
		}
	
    	return false;  
  
    }
    
    private ExperimentOutput getExOut()
	{
		if(exOut == null)
		{
			try {
				exOut = new ExperimentOutput(new FileOutputStream("experimentOut.xml"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		
		return exOut;
	}
	 
    
    private Opponent getOpn(Literal t, Opponent opn) {
    	
    	opn.clear();
    	
    	List<Term> list = t.getTerms();
    	if(list.size() > 0)
    	{
    		Term tName = list.get(0);
    		opn.setName(tName.toString());    		
    	}
    	
    	if(list.size() > 1)
    	{
    		Term tAction = list.get(1);
    		opn.setAction(tAction.toString());
    	}
    	
    	if(list.size() > 2)
    	{
    		Term tAction = list.get(2);
    		NumberTerm nt = (NumberTerm)tAction;
    		opn.setPenality(Math.round(nt.solve()));
    	}
    	
    	if(list.size() > 3)
    	{
    		Term tDelay = list.get(3);
    		NumberTerm nt = (NumberTerm)tDelay;
    		opn.setDelay(Math.round(nt.solve()));
    	}    	
    	return opn;
	}

    /** Called before the MAS execution with the args informed in .mas2j */
    @Override
    public void init(String[] args) {
        super.init(args);
        if(args.length > 0)
        	fileOut = args[0];
      // addPercept(Literal.parseLiteral("percept(demo)"));
    }

	/** Called before the end of MAS execution */
    @Override
    public void stop() {
        super.stop();
        getExOut().writeMediaMatch(0, map);
        getExOut().close();
    }
}
