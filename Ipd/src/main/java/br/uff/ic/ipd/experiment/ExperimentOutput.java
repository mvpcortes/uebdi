package br.uff.ic.ipd.experiment;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import br.uff.ic.ipd.experiment.Opponent;

public class ExperimentOutput {
	
	private XMLStreamWriter xmlWriter;
	
	public ExperimentOutput(OutputStream _o)
	{
		try {
			xmlWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(
			        new OutputStreamWriter(_o, "utf-8"));
			xmlWriter.writeStartDocument();
		} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		}
		
		try {
			xmlWriter.writeProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"../tabela_tempo.xsl\"");
			xmlWriter.writeStartElement("experiment");
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}
	
	public void close()
	{
		if(xmlWriter == null) return;
		
		try {
			xmlWriter.writeEndElement();
			xmlWriter.writeEndDocument();
			xmlWriter.close();
			xmlWriter = null;
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	public void write(long time, Collection<Opponent> vecOpn)
	{
		
		try {
			xmlWriter.writeStartElement("combat");
	
			xmlWriter.writeAttribute("id", String.format("%d", time));

			for(Opponent opn: vecOpn)
				opn.write(xmlWriter);
			
			xmlWriter.writeEndElement();
			xmlWriter.flush();
				
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	public void writeMediaMatch(int time, Map<String, Long> map) {	
		try {
			xmlWriter.writeStartElement("media_combat");
			xmlWriter.writeAttribute("lastId", String.format("%d", time));
			
			for(Entry<String, Long> entry: map.entrySet())
			{
				//
				xmlWriter.writeStartElement("opponent");
				xmlWriter.writeAttribute("name", entry.getKey());
				
				xmlWriter.writeAttribute("point", entry.getValue().toString());
				
				xmlWriter.writeEndElement();
			}
				//
			xmlWriter.writeEndElement();
				
		} catch (XMLStreamException e) {
				e.printStackTrace();
		}
	}
}
