package br.uff.ic.ipd;

import jason.asSemantics.Event;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Trigger;
import jason.bb.BeliefBase;

import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.IEmotionDyn;
import br.uff.ic.uebdi.IIntentionBase;

public class EmotionDynJiang implements IEmotionDyn {

	private Logger logger = Logger.getLogger(this.getClass().getName());
	
	private Logger getLogger()
	{
		return logger;
	}
	private void generateDebugInfo(IEmotionBase eb, BeliefBase bb) {
		
		getLogger().info(eb.toString());
	}


	@Override
	public void secondReviewEmotion(BeliefBase bb, Queue<Event> events,
			IEmotionBase eb, IIntentionBase intentionBase) {

		getLogger().info("EmotionDynJiang:secondReviewEmotion (init)");

		if (events != null && events.size() > 0) 
		{
			getLogger().fine("EmotionDynJiang:secondReviewEmotion events= "	+ events.toString());

			for (Event e : events)
			{
			    getLogger().fine("Event " + e.toString());

			    Trigger trigger = e.getTrigger();
			    Literal triggerLiteral = trigger.getLiteral();
			    ListTerm listProcessed = triggerLiteral.getAnnots("emotionProcessed");
			    if (listProcessed.size() == 0) 
			    {
                		getLogger().fine("EmotionDynJiang:secondReviewEmotion: listProcessed.size() == 0");
                		
                		if (trigger.getOperator().equals(Trigger.TEOperator.add))
                		{
                		    if (triggerLiteral != null) 
                		    {
                			getLogger().info("EmotionDynJiang:secondReviewEmotion: equals(Trigger.TEOperator.add): " + triggerLiteral.getFunctor());
                					if (triggerLiteral.getFunctor().indexOf("take_records") >= 0) 
                					{
                						getLogger().info("EmotionDynJiang:secondReviewEmotion: triggerLiteral.getFunctor().indexOf(\"take_records\") >=0");
                						
                						@SuppressWarnings("unused")
                						long nTime;
                						String strOther = "";
                						String strOtherPlay = "";
                						boolean ok = true;
                						try {
                							NumberTerm ntTime = (NumberTerm) triggerLiteral
                									.getTerm(0);
                							Literal stOther = (Literal) triggerLiteral
                									.getTerm(1);
                							Literal stOtherPlay = (Literal) triggerLiteral
                									.getTerm(2);// qual foi a jogada do
                												// outro carinha
                							nTime = (long) ntTime.solve();
                							strOther = stOther.toString();
                							strOtherPlay = stOtherPlay.toString();
                						} catch (Exception e2) {
                							ok = false;
                						}
                
                						if (ok) {
                
                							double intensity = eb.getIntensity(strOther);
                							getLogger().info(String.format("old emotion = %.3f", intensity));
                							if (strOtherPlay.equals("c")) 
                							{
                								intensity += +1;
                								if (intensity >= 3)
                									intensity = 3;
                							} else if (strOtherPlay.equals("d")) {
                								intensity += -1;
                								if (intensity <= -2)
                									intensity = -2;
                							} else {
                								getLogger().warning(
                										"Cannot interpret answer "
                												+ strOtherPlay);
                							}
                
                							eb.changeIntensity(intensity, strOther);
                
                							triggerLiteral.addAnnot(Literal
                									.parseLiteral("emotionProcessed"));
                						}
                					}
                				}
                			}
                		}
			}

			generateDebugInfo(eb, bb);
		}
	}
	@Override
	public void firstReviewEmotion(List<Literal> listPerc, IEmotionBase eb,
			IIntentionBase intentionBase) {
		
	}
	@Override
	public boolean criticalContext(IEmotionBase eb, BeliefBase bb) {
		return true;
	}
}
