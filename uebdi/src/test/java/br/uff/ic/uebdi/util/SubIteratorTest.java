/**
 * 
 */
package br.uff.ic.uebdi.util;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import br.uff.ic.util.SubIterator;
import br.uff.ic.util.SubIteratorCondition;

/**
 * @author Marcos
 *
 */
public class SubIteratorTest {

    static List<String> listTemp = new LinkedList<String>();
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
	listTemp.add("a");
	listTemp.add("b");
	listTemp.add("c");
	listTemp.add("d");
	listTemp.add("e");
	listTemp.add("f");
	listTemp.add("g");
	listTemp.add("h");
	listTemp.add("i");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    
    /**
     * Test Iterating all list.
     */
    
    @Test
    public void testCase1()
    {
	SubIterator<String> si = new SubIterator<String>(listTemp.iterator(), 
		new SubIteratorCondition<String>() {

		    @Override
		    public boolean isIt(String e) {
			return true;
		    }
		});
	Iterator<String> sit = listTemp.iterator();
	compareIt(si, sit);
    }

    private void compareIt(Iterator<String> itA, Iterator<String> itB) { 
	while(itA.hasNext() && itB.hasNext())
	{
	    String a = itA.next();
	    String b = itB.next();
	    assertTrue("The values not equals!", a.equals(b));
	}
	
	assertFalse("Has item in itA!", itA.hasNext());
	assertFalse("Has item in itB!", itB.hasNext());
    }
    
    
    /**
     * Iterators it a only element
     */
    @Test
    public void testCase2()
    {
	for(final String s: listTemp)
	{
	    SubIterator<String> si = new SubIterator<String>(listTemp.iterator(), 
    		new SubIteratorCondition<String>() {
    
    		    @Override
    		    public boolean isIt(String e) {
    			return s.equals(e);
    		    }
    		});
	    
	    assertTrue("Should exist a element.", si.hasNext());
	    String element = si.next();
	    assertTrue("Should be equal: ", element.equals(s));
	    
	    assertFalse("Should not exist a element", si.hasNext());
	}	
    }
    
    /**
     * Iterator random elements
     */
    @Test
    public void testCase3()
    {
	for(int i = 0; i < 100; i++)
	{
        	final Collection<String> coll = createSubList(listTemp);
        
        	SubIterator<String> si = new SubIterator<String>(listTemp.iterator(), 
        		new SubIteratorCondition<String>() {
        
        		    @Override
        		    public boolean isIt(String e) {
        			return coll.contains(e);
        		    }
        		});
        	
        	compareIt(si, coll.iterator());
	}
    }

    private static Collection<String> createSubList(List<String> list) {
	Collection<String> coll = new LinkedList<String>();
	for(String ele: list)
	{
	    if(Math.random() > 0.5)
	    {
		coll.add(ele);
	    }
	}
	
	return coll;
    }

}
