package br.uff.ic.uebdi.internalaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.LogExpr;
import jason.asSyntax.Term;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import br.uff.ic.uebdi.internalaction.min_comparator;

public class min_comparatorTest {

	private static min_comparator instance;
	
	private TransitionSystem ts;
	
	@BeforeClass
	public static void beforeClass()
	{
		instance = new min_comparator();
	}
	
	@Before
	public void setUp()
	{
		ts = Mockito.mock(TransitionSystem.class);
	}
	
	@Test
	public void testDifferentMaskElements()
	{
		Term list = ListTermImpl.parseList("[a(u, 2), a(uu, 1), a(uuu, 3), a(uuuu, 0)]");
		Term rule = Literal.parseLiteral("a(U1, U2)");
		Term formula = LogExpr.parseExpr("a(X1, X2) = COMP_A & a(Y1, Y2) = COMP_B & X2 < Y2");
		Term expected = Literal.parseLiteral("a(uuuu, 0)");
		utilTestElements(list, rule, formula, expected);
	}
	
	@Test
	public void testTwoInvertedElements()
	{
		Term list = ListTermImpl.parseList("[a(2), a(1)]");
		Term rule = Literal.parseLiteral("a(X)");
		Term formula = LogExpr.parseExpr("a(X) = COMP_A & a(Y) = COMP_B & X < Y");
		Term expected = Literal.parseLiteral("a(1)");
		utilTestElements(list, rule, formula, expected);
	}
	@Test
	public void testTwoElements()
	{
		Term list = ListTermImpl.parseList("[a(1), a(2)]");
		Term rule = Literal.parseLiteral("a(X)");
		Term formula = LogExpr.parseExpr("a(X) = COMP_A & a(Y) = COMP_B & X < Y");
		Term expected = Literal.parseLiteral("a(1)");
		utilTestElements(list, rule, formula, expected);
	}
	
	public void utilTestElements(Term list, Term rule, Term formula, Term expected)
	{
		Unifier un = new Unifier();
		
		Object o = true;
		try {
			o = instance.execute(ts, un, new Term[]{list, rule, formula});
		} catch (Exception e) {
			fail("Exception unexpected: " + e.toString());
		}
		
		assertNotNull(o);
		assertTrue((Boolean)o);
		//
		
		Term query = rule.clone();
		query.apply(un);
		assertTrue(query.isGround());
		
		assertEquals(query, expected);
	}
	
	@Test
	public void testOneElement(){
		Term list = ListTermImpl.parseList("[a(1)]");
		Term rule = Literal.parseLiteral("a(X)");
		Term formula = LogExpr.parseExpr("a(X) = COMP_A & a(Y) = COMP_B & X < Y");
		Term expected = Literal.parseLiteral("a(1)");
		utilTestElements(list, rule, formula, expected);		
	}
	
	@Test
	public void testVoidList() {
		
		Term list = ListTermImpl.parseList("[]");
		Term rule = Literal.parseLiteral("a(X)");
		Term formula = LogExpr.parseExpr("a(X) = COMP_A & a(Y) = COMP_B & X < Y");
		Unifier un = new Unifier();
		
		Object o = true;
		try {
			o = instance.execute(ts, un, new Term[]{list, rule, formula});
		} catch (Exception e) {
			fail("Exception unexpected: " + e.toString());
		}
		
		assertTrue(o != null || (o instanceof Boolean && ((Boolean)o).booleanValue()==false));
		
	}

}
