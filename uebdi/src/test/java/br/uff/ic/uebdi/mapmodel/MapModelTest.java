package br.uff.ic.uebdi.mapmodel;

import java.awt.Point;
import java.util.Arrays;

import org.junit.Test;

public class MapModelTest {

	@Test
	public void testGenerateRoundPoints() {
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < i; j++) {
				Point[] pNovo = MapModel.generateRoundPoints(i, j);
				Point[] pVelho = MapModel.minusPoints(MapModel.generateRoundPoints(i),
						MapModel.generateRoundPoints(j));
				if (!Arrays.equals(pNovo, pVelho)) {
					assert false;
				}
			}
	}

}
