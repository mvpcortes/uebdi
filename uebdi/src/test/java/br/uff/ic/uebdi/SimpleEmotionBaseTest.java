/**
 * 
 */
package br.uff.ic.uebdi;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.bb.DefaultBeliefBase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import br.uff.ic.util.RandomStringUtils;

/**
 * @author Marcos
 *
 */
@Ignore
public class SimpleEmotionBaseTest {

	static private WrapperJasonUEBDI agent = new WrapperJasonUEBDI();
	static private LiteralEmotionBase eb = null;
	
	static private LiteralEmotionBase getEB(){return eb;}
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		agent.setBB(new DefaultBeliefBase() );
		agent.setUEBDI(new UEBDIImpl());
		agent.initAg();
		eb = (LiteralEmotionBase)agent.getEB();
	}

	public static void assertDoubleEqual(double valuea, double valueb)
	{
		assertTrue(Math.abs(valuea-valueb) < 0.000001 );
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testAdd1() {
		for(int i = 1; i < 11; i++) {
			getEB().add(i, new String[]{"lala"});
			assertDoubleEqual(getEB().getIntensity(Literal.parseLiteral("emotion(lala, 0)")),i);	
		}
		
		checkUniqueOfEmotions();
		
		getEB().delete("lala");
		assertFalse(getEB().getAll("lala").hasNext());
	}
	
	@Test
	public void testRandomInsertRemove()
	{
		Set<String> set = new HashSet<String>();
		for(int i = 0; i < 50; i++)
		{
			 String idOne = RandomStringUtils.random(10,"qwertyuiopasdfghjklzxcvbnm");
			 getEB().add(1, idOne);
			 set.add(idOne);
			 if(Math.random() < 0.5 && set.size() > 0)
			 {
				 String other = set.iterator().next();
				 getEB().delete(other.toString());
				 set.remove(other);
			 }
		}
		
		getEB().resetEmotion();
		assertTrue(!getEB().getAllEmotions().hasNext());
		//assertDoubleEqual(getEB().getIntensity(), 0);
	}
	
	private void checkUniqueOfEmotions() {
		Set<Term> set = new HashSet<Term>();
		Iterator<Literal> it = getEB().getAllEmotions();
		if(it == null) return;
		
		while(it.hasNext())
		{
			Literal lit = it.next();
			Term term = lit.getTerm(0);
			assertFalse(set.contains(term));
			set.add(term);
		}		
	}
	
	@Test
	public void testAdd2()
	{
		getEB().resetEmotion();
		for(int i = 1; i < 11; i++)
		{
			String name = String.format("lala%d", i);
			getEB().add(i, name);
			assertDoubleEqual(getEB().getIntensity(Literal.parseLiteral("emotion("+name+")")), i);
			getEB().delete(name);
		}
		Iterator<Literal> it = getEB().getAllEmotions();
		assertTrue(it == null || it.hasNext() == false);
		
		////////
	}
	
	@Test
	public void testIncIntensity()
	{
		getEB().add(1,"lala");
		
		for(int i = 2; i <= 100; i++)
		{
			getEB().incIntensity(1, "lala");
			double intensity = getEB().getIntensity(Literal.parseLiteral("emotion(lala)"));
			assertTrue(i == (int)intensity);
		}
		
		for(int i = 100; i > 0; i--)
		{
			double intensity = getEB().getIntensity(Literal.parseLiteral("emotion(lala)"));
			assertTrue(i == (int)intensity);
			getEB().incIntensity(-1, "lala");
		}
		
		assertTrue(getEB().getIntensity(Literal.parseLiteral("emotion(lala)")) <= 0);
	}
}
