package br.uff.ic.util;

import java.util.Iterator;

import javax.management.RuntimeErrorException;

public class IteratorCannotRemove<E> implements Iterator<E> {

	Iterator<E> internalIterator;
	
	public IteratorCannotRemove(Iterator<E> it)
	{
		internalIterator = it;
	}
	@Override
	public boolean hasNext() {
		if(internalIterator == null)
			return false;
		else
			return internalIterator.hasNext();
	}

	@Override
	public E next() {
		if(internalIterator == null)
			return null;
		else
			return internalIterator.next();
	}

	@Override
	public void remove() {
		throw new RuntimeErrorException(null, "Cannot remove with IteratorCannotRemove");
	}

}
