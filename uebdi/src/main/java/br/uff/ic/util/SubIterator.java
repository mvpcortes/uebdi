package br.uff.ic.util;


import java.util.Iterator;

public class SubIterator<E> implements Iterator<E> {

	private Iterator<E> it;
	private SubIteratorCondition<E> sic;
	
	private E current;
	
	public SubIterator(Iterator<E> _it, SubIteratorCondition<E> _sic)
	{
		it = _it;
		sic = _sic;
		updateCurrent();
	}
	private void updateCurrent()
	{
		current = null;
		while(it.hasNext())
		{
			E temp = it.next();
			if(sic.isIt(temp))
			{
				current = temp;
				break;
			}
		}
	}
	
	@Override
	public boolean hasNext() {
		return current!= null;
	}

	@Override
	public E next() {
		E temp = current;
		updateCurrent();
		return temp;
	}

	@Override
	public void remove() {
	}

}
