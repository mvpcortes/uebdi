package br.uff.ic.util;


public class Pair <X extends Comparable<X>, Y extends Comparable<Y>> implements Comparable<Pair<X, Y>>
{
	private X x;
	private Y y;
	
	public Pair(X a, Y b)
	{
		x 	= a;
		y 	= b;
	}
	
	public X getX(){return x;}
	
	public Y getY(){return y;}

	@Override
	public int compareTo(Pair<X, Y> other) {
		float cX = getX().compareTo(other.getX());
		float cY = getY().compareTo(other.getY());
		
		if(cX < 0)
			return -1;
		if(cX > 0)
			return 1;
		if(cY < 0)
			return -1;
		if(cY > 0)
			return 1;
		
		return 0;
		
	}
	
	public boolean equals(Object p)
	{
		if(p != null && p instanceof Pair< ?, ?>)
		{
			@SuppressWarnings("unchecked")
			Pair<X, Y> other = (Pair<X,Y>)p;
			return x.equals(other.x) && y.equals(other.y);
		}
		return false;
	}
	@Override
	public String toString()
	{
		return String.format("(%s, %s)", x.toString(), y.toString());
	}
	
	@Override
	public int hashCode()
	{
		return (x!=null?x.hashCode():0) ^ (y!=null?y.hashCode():0);
	}
	
	
}
