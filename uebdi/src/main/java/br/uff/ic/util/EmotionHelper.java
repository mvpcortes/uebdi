package br.uff.ic.util;

import java.util.logging.Logger;

import jason.asSyntax.Literal;
import jason.asSyntax.LiteralImpl;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

public class EmotionHelper {
	
	public static String FUNCTOR = "emotion";

	private static Logger logger = null;

	public static Logger getLogger()
	{
		if(logger == null)
		{
			logger = Logger.getLogger(EmotionHelper.class.getName());
		}
		return logger;
	}
	
	public static boolean validEmotion(Literal l)
	{
		if(!l.canBeAddedInBB())
			return false;
		
		if(!FUNCTOR.equals(l.getFunctor()))
			return false;
		
		if(l.getTerms().size() == 0)
			return false;
		
		Term t = l.getTerm(l.getTerms().size()-1);
		
		if(!t.isNumeric())
			return false;
		
		
		return true;
		
	}
	
	public static Literal generateLiteral(String... query) throws Exception {
	    return generateLiteral(0.0, query);
	}
	/**
	 * @param query
	 * @return
	 * @throws Exception 
	 */
	public static Literal generateLiteral(double intensity, String... query) throws Exception {
		try
		{
			Literal litQuery = new LiteralImpl(true, new Structure(FUNCTOR));
			
			for(String s: query)
			{
				Literal add = null;
				if(s == null || s.equals("_"))
				{
					add = Literal.parseLiteral("_");
				}else
				{
					add = Literal.parseLiteral(s);
				}
				if(add==null)
					return null;
				
				litQuery.addTerm(add);
					
			}
			
			litQuery.addTerm(new NumberTermImpl(intensity));
			return litQuery;
		}catch(Exception e)
		{
			getLogger().severe("Problem in generate Literal: "+ e.toString());
			throw e;
		}
	}
	public static Literal[] toTerms(String[] strs) {
		Literal terms[] = new Literal[strs.length];
		
		for(int i = 0; i < strs.length; i++)
		{
			String s = strs[i];
			if("_".equals(s) | " ".equals(s) | "".equals(s))
			{
				terms[i] = null;
			}else
			{
				terms[i] = Literal.parseLiteral(s);
			}
		}
		return terms;
	}
	
	public static double getIntensity(Literal l)
	{
		int size = l.getTerms().size();
		if(size > 0)
			return TermHelper.getDouble(l, size-1);
		
		return -1;
		
	}

	public static void setIntensity(Literal l, double intensity) {
		int size =  l.getTerms().size();
		if(size > 0)
		{
			l.setTerm(size-1, new NumberTermImpl(intensity));
		}
	}
	
	public static void incIntensity(Literal l, double intensity) {
		int size =  l.getTerms().size();
		if(size > 0)
		{
			Term t = l.getTerm(size-1);
			double oldValue = 0;
			if(t.isNumeric())
			{
				oldValue = ((NumberTerm)t).solve();
			}
				
			l.setTerm(size-1, new NumberTermImpl(oldValue+intensity));
		}
	}

	/**
	 * Compara ignorando a intensidade
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean equalEmotionTerm(Literal a, Literal b) 
	{
		int sizeA = a.getTerms().size();
		int sizeB = b.getTerms().size();
		
		Term lasta = a.getTerm(sizeA-1);
		Term lastb = b.getTerm(sizeA-1);
		
		if(!lasta.isNumeric() || !lastb.isNumeric())
		{
			return false;
		}
		
		if(sizeA != sizeB && sizeA <= 0)
			return false;
		
		for(int i = 0; i < sizeA-1; i++)
		{
			Term ta = a.getTerm(i);
			Term tb = b.getTerm(i);
			if(!ta.equals(tb))
				return false;
		}
		
		return true;
	}

}
