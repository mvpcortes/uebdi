package br.uff.ic.util;

public class Matrix <E>{
	private int w, h;
	
	private Object[][] data = null;
	public Matrix(int _w, int _h)
	{
		w = _w;
		h = _h;
		data = new Object[w][h];
		zerofy();
	}
	
	
	public Matrix(int _w, int _h, IDataCreator<? extends E> dc)
	{
		w = _w;
		h = _h;
		data = new Object[w][h];
		fill(dc);
	}
	
	
	public void zerofy()
	{
		for(int i = 0; i < w; i++)
		{
			for(int j = 0; j < h; j++)
			{
				data[i][j] = null;
			}
		}
	}
	
	public interface IDataCreator<E> {
		
		E create(int i, int h);
	}
	
	public void fill(IDataCreator<? extends E> dc)
	{
		for(int i = 0; i < w; i++)
		{
			for(int j = 0; j < h; j++)
			{
				data[i][j] = dc.create(i,j);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public E get(int i, int j)
	{
		return (E)data[i][j];
	}
	
	public void set(int i, int j, E e)
	{
		data[i][j] = e;
	}
}
