package br.uff.ic.util;

import java.util.Comparator;

public class InvertComparator<T> implements Comparator<T> {

	private Comparator<T> comparatorSource;
	
	public InvertComparator(Comparator<T> comp){
		comparatorSource  =comp;
	}
	
	@Override
	public int compare(T o1, T o2) {
		final int delta = comparatorSource.compare(o1, o2);
		
		if(delta == 0)
			return 0;
		else if (delta < 0)
			return +1;
		else
			return -1;
	}

}
