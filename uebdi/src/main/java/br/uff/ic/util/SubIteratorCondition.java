package br.uff.ic.util;

public interface SubIteratorCondition <E>{

	public boolean isIt(E e);
}
