package br.uff.ic.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeDateHelper {
	public static String getStrNow() {
		SimpleDateFormat s = new SimpleDateFormat("HH:mm:ss - dd/MM/yyyy");
		return s.format(new Date());
	}
}
