package br.uff.ic.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MultiMatrix <E>{

	private int w, h;
	private Object[][] data;
	
	
	public int getWidth()
	{
		return w;
	}
	
	public int getHeight()
	{
		return h;
	}
	
	public MultiMatrix(int _w, int _h)
	{
		w = _w;
		h = _h;
		data = new Object[w][h];
		for(int i = 0; i < w; i++)
			for(int j = 0; j < h; j++)
			{
				data[i][j] = new LinkedList<E>();
			}
	}
	
	public boolean isValidPos(int i, int j)
	{
		return i >= 0 && i < w && j >= 0 && j < h;
	}
	
	public Iterator<E> getContents(int i , int j)
	{
		@SuppressWarnings("unchecked")
		List<E> list = (List<E>)data[i][j];
		return list.iterator();
	}
	

	public Iterator<E> find(int i , int j, SubIteratorCondition<E> cond)
	{
		return new SubIterator<E>(getContents(i,j), 
				cond
		);
	}
	
	public boolean add(int i , int j, E e)
	{
		@SuppressWarnings("unchecked")
		List<E> list = (List<E>)data[i][j];
		if(!list.contains(e))
		{
			list.add(e);
			return true;
		}else
		{
			return false;
		}
	}
	
	public boolean remove(int i, int j, E e)
	{
		@SuppressWarnings("unchecked")
		List<E> list = (List<E>)data[i][j];
		return list.remove(e);
	}
	
	public boolean contain(int i, int j, E e)
	{
		@SuppressWarnings("unchecked")
		List<E> list = (List<E>)data[i][j];
		return list.contains(e);
	}
}
