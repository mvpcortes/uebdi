package br.uff.ic.util;

import java.io.IOException;
import java.io.OutputStream;

public class StringOutputStream extends OutputStream {

	StringBuilder sb = new StringBuilder();
	@Override
	public void write(int b) throws IOException {
		sb.append((char)b);
	}
	
	public String toString()
	{
		return sb.toString();
	}
	
	public void clear()
	{
		sb.delete(0, sb.length());
	}

}
