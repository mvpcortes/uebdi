package br.uff.ic.uebdi.mapmodel;

import jason.asSyntax.Term;

import java.awt.Point;

import br.uff.ic.util.TermHelper;

public enum Direction
{
	none	(new Point( 0, 0)	),
	top		(new Point( 0,-1)	),
	bottom	(new Point( 0,+1)	),
	left	(new Point(-1, 0)	),
	right	(new Point(+1, 0)	);
	
	
	private final Point relPoint; 
	Direction(Point p)
	{
		relPoint = p;
	}
	public final Point getRelativePos()
	{
		return relPoint;
	}
	public static Direction fromTerm(Term tdir) {
		String s = TermHelper.toString(tdir);
		s = s.toLowerCase();
		for(Direction d: Direction.values())
		{
			if(s.equals(d.name()))
				return d;
		}
		return Direction.none;
	}
}