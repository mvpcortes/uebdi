package br.uff.ic.uebdi;

import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import jason.asSemantics.Agent;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.PredicateIndicator;
import jason.bb.BeliefBase;

/**
 * Esta classe encapsula a BeliefBase para ser usada na UEBDi e permitir usar a {@link SmartBelieves}
 * @author Marcos
 *
 */
public class WrapperBeliefBase implements BeliefBase, SmartBelieves {

	
	public Agent agent;
	
	public void setAgent(Agent a)
	{
		agent = a;
	}
	
	public WrapperBeliefBase(Agent a)
	{
		agent = a;
	}
	
	@Override
	public boolean believes(LogicalFormula bel, Unifier un) {
		return agent.believes(bel, un);
	}

	@Override
	public void init(Agent ag, String[] args) {
		agent.getBB().init(ag, args);
	}

	@Override
	public void stop() {
		agent.getBB().stop();
	}

	@Override
	public boolean add(Literal l) {
		return agent.getBB().add(l);
	}

	@Override
	public boolean add(int index, Literal l) {
		
		return agent.getBB().add(index, l);
	}

	@Override
	public Iterator<Literal> iterator() {
		return agent.getBB().iterator();
	}

	@Override
	public Iterator<Literal> getAll() {
		return agent.getBB().iterator();
	}

	@Override
	public Iterator<Literal> getCandidateBeliefs(PredicateIndicator pi) {
		return agent.getBB().getCandidateBeliefs(pi);
	}

	@Override
	public Iterator<Literal> getCandidateBeliefs(Literal l, Unifier u) {
		return agent.getBB().getCandidateBeliefs(l, u);
	}

	@SuppressWarnings("deprecation")
	@Override
	public Iterator<Literal> getRelevant(Literal l) {
		return agent.getBB().getRelevant(l);
	}

	@Override
	public Literal contains(Literal l) {
		return agent.getBB().contains(l);
	}

	@Override
	public int size() {
		return agent.getBB().size();
	}

	@Override
	public Iterator<Literal> getPercepts() {
		return agent.getBB().getPercepts();
	}

	@Override
	public boolean remove(Literal l) {
		return agent.getBB().remove(l);
	}

	@Override
	public boolean abolish(PredicateIndicator pi) {
		return agent.getBB().abolish(pi);
	}

	@Override
	public Element getAsDOM(Document document) {
		return agent.getBB().getAsDOM(document);
	}
	
	@Override 
	public BeliefBase clone()
	{
		throw new UnsupportedOperationException("Method cannot be implemented to the class WrapperBeliefBase");		
	}

	@Override
	public Iterator<Literal> believesIt(Literal bel, Unifier un) {
		
		Iterator<Unifier> it = bel.logicalConsequence(this.agent, un);
		return new UnifierIterator(it, bel);
	}

}
