package br.uff.ic.uebdi;

import jason.asSyntax.Literal;

/**
 * Listener of operations Emotional Base
 * @author Marcos
 *
 */
public interface IEmotionBaseListener {
	
	void fireAdded(Literal _l);
	
	void fireRemoved(Literal _l);
}
