package br.uff.ic.uebdi;


import jason.JasonException;
import jason.RevisionFailedException;
import jason.architecture.AgArch;
import jason.asSemantics.Agent;
import jason.asSemantics.Event;
import jason.asSemantics.Intention;
import jason.asSemantics.Message;
import jason.asSemantics.Option;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.runtime.Settings;
import jason.stdlib.desire;
import jason.stdlib.drop_desire;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import br.uff.ic.uebdi.exceptions.EmotionalDynException;
import br.uff.ic.uebdi.exceptions.UEBDIException;

/**
 * Wapper of UEBDI agent in the Jason architecture.
 * @author Marcos
 *
 */
public final class WrapperJasonUEBDI extends jason.asSemantics.Agent
{
	private static final String STR_EMOTIONAL_DYN = "emotionalDyn";

	private static final String STR_EMOTIONAL_BASE = "emotionalBase";

	private static final String STR_UEBDI = "uebdi";

	/**
	 * Default class of UEBDI
	 */
	private static final Class<UEBDIImpl> defaultUEBDI = UEBDIImpl.class;
	
	/**
	 * Default instance of emotion base
	 */
	private static final Class<LiteralEmotionBase> defaultEmotionBase = LiteralEmotionBase.class;
	
	/**
	 * Instance of UEBDI agent
	 */
	private IUEBDI uebdi = null;


	private IEmotionBase eb = null;

	/**
	 * Instance of a wrapper to the beliefbase. Isto permitirá executar operações não definidas na BeliefBase de forma "fácil".
	 */
	private WrapperBeliefBase wrapperBB = null;
	
	/**
	 * Return the UEBDI Architecture
	 * @return
	 */
	public IUEBDI getUEBDI(){return uebdi;}
	
	public void setUEBDI(IUEBDI _uebdi){uebdi = _uebdi;}

	/**
	 * Return the Emotional Base
	 * @return
	 */
	public IEmotionBase getEB()
	{
		return eb;
	}
	
	public WrapperBeliefBase getWrapperBB()
	{
		if(wrapperBB == null)
		{
			wrapperBB = new WrapperBeliefBase(this);
		}
		
		return wrapperBB;
	}
	
	@Override
	public void initAg()
	{
		getLogger().entering("UEBDIAgent", "initAg");
		super.initAg();
		Settings set = getTS().getSettings();
		String strUEBDI 	= set.getUserParameter(STR_UEBDI);
		String strEmoBase 	= set.getUserParameter(STR_EMOTIONAL_BASE);
		String strEmoDyn 	= set.getUserParameter(STR_EMOTIONAL_DYN);
		IUEBDI prevUEBDI = getUEBDI();
		
		//get uebdi
		uebdi = createInstance(strUEBDI);
		if(uebdi == null)
		{
			try {
				uebdi = (IUEBDI) defaultUEBDI.newInstance();
			} catch (InstantiationException e) {
				getLogger().severe("problem in the initiation of UEBDI. RollBack: " + e.toString());
				uebdi = prevUEBDI; 
			} catch (IllegalAccessException e) {
				getLogger().severe("problem in the initiation of UEBDI. RollBack: " + e.toString());
				uebdi = prevUEBDI; 
			}
		}
		
		//get EmotionBase
		IEmotionBase prevEB = eb;
		eb = createInstance(strEmoBase);
		if(eb == null)
		{
			try {
				eb  = (br.uff.ic.uebdi.IEmotionBase) defaultEmotionBase.newInstance();
			} catch (InstantiationException e) {
				getLogger().severe("problem in the initiation of IEmotionBase. RollBack: " + e.toString());
				eb = prevEB;
			} catch (IllegalAccessException e) {
				getLogger().severe("problem in the initiation of IEmotionBase. RollBack: " + e.toString());
				eb = prevEB; 
			}
		}
		
		eb.initBase(this);

		if(uebdi instanceof IAccopledEmotionDyn)
		{
			IEmotionDyn ed = createInstance(strEmoDyn);
			if(ed != null)
			{
	
				IAccopledEmotionDyn iac = (IAccopledEmotionDyn)uebdi;
				iac.setEmotionDyn(ed);				
				
				if(ed instanceof ISetAgent)
				{
					ISetAgent isa = (ISetAgent)ed;
					isa.setAgent(this);
				}
			}
		}
		
		//verifica quest�o do verbose
		if (this.getTS().getSettings().verbose() >= 0)
		{
	          uebdi.setLoggerData(this.getTS().getUserAgArch().getAgName(), this.getTS().getSettings().logLevel());
		}
		
	
	}
	
	/**
	 * Create a instance of a class
	 * @param str
	 * @return
	 */
	private  <T> T createInstance(String str) {
		if(str == null || str.equals(""))
			return null;
		
		T t = null;
		
		try
		{
			@SuppressWarnings("unchecked")
			Class<T> c= (Class<T>) Class.forName(str);
			
			t = c.newInstance();
			
		} catch (ClassNotFoundException e) {
			getLogger().warning("Cannot create instance of " + str + " because ClassNotFoundException");
			t = null;
		} catch (InstantiationException e) {
			getLogger().warning("Cannot create instance of " + str + " because InstantiationException");
			t = null;
		} catch (IllegalAccessException e) {
			getLogger().warning("Cannot create instance of " + str + " because IllegalAccessException");
			t = null;
		}		
		return t;
	}

	@Override
	public void initAg(String asSrc) throws JasonException
	{
		getLogger().entering("UEBDIAgent", "initAg", asSrc);
		super.initAg(asSrc);
	}

	
	@Override
	public Agent clone(AgArch arch)
	{
		getLogger().entering("UEBDIAgent", "clone", arch);
		WrapperJasonUEBDI a = (WrapperJasonUEBDI) super.clone(arch);     
		a.eb = this.eb.clone(this);
		a.uebdi = uebdi.clone(); 
		return a;
	}
	
	
	/**
	 * Return the name of agent
	 * @return
	 */
	String getName()
	{		
		try
		{
			getLogger().entering("UEBDIAgent", "getName");
			TransitionSystem t = getTS();
	    	AgArch agArch = t.getUserAgArch();
	    	String agName = agArch.getAgName();
	    	logger.info("getName: "+agName);
			return agName;
		}catch(Throwable e)
		{
			logger.severe("getName(): problem in agArch request: " + e.toString());
			return "";
		}
	}
	
	private IIntentionBase instanceIB = null;
	public IIntentionBase getIB()
	{
		if(instanceIB == null)
		{
			instanceIB = new IntentionBaseSimple(this.getTS());
		}
		return instanceIB;
		
	}
	

	/**
	 * this is a temporary list to filter perceptions
	 */
	private List<Literal> listPercDest = new LinkedList<Literal>();
	
	@Override
	public void buf(List<Literal> percepts) 
	{
		
		getWrapperBB().setAgent(this);
		
		//faz a percepção
		getUEBDI().perception(percepts, getEB(), getBB(), getIB());
		
		try{
			getUEBDI().firstReviewEmotion(percepts, getEB(), getIB());
		}catch(EmotionalDynException e){
			getLogger().severe("Cannot process emotion flow because: " + e.toString() + "exception");
		}
		
		getUEBDI().updateBeliefs(percepts, getWrapperBB(), getEB(), getIB());
		
		super.buf(percepts);
	}
		
	@Override
	public Message selectMessage(Queue<Message> queueMsg)
	{
		return super.selectMessage(queueMsg);
	}
	
	
	@Override
	public boolean socAcc(Message m)
	{
		return super.socAcc(m);
	}
	
	@Override
	public List<Literal>[] brf(Literal beliefToAdd, Literal beliefToDel,  Intention i) throws RevisionFailedException 
    {
		getLogger().entering("UEBDIAgent", "brf");
		List<Literal>[] list = super.brf(beliefToAdd, beliefToDel, i);
		
		if(list != null)
		{
			List<Literal> listAdd = list[0];
			if(listAdd != null )
			{
				for(Literal l: listAdd)
				{
					try {
						getUEBDI().taggingBelief(l, getEB());
					} catch (UEBDIException e) {
						throw new RevisionFailedException("Cannot tag the belief " + l.toString(), e);
					}
				}
			}		
		}
       	return list;
    }
	
	
	@Override
	public Event selectEvent(Queue<Event> events)
	{
		List<Literal> listForget = null;
		/*for(Literal l: getBB())
		{
			getLogger().fine("selectEvent");
			if(getUEBDI().forgetBelief(l))
			{
				if(listForget == null)
					listForget = new LinkedList<Literal>();
				listForget.add(l);
			}else
			{
				Drive drive = Drive.parseLiteral(l);
				if(drive != null)
				{
					getLogger().info("found a drive: " + drive.toString());
					//primeiro, v� se j� deseja ou n�o este drive (tanto na base de eventos quanto na intent.
				
					desire checkDesire = new desire();
					boolean alreadyDesired = false;
					try {
						@SuppressWarnings("unchecked")
						Iterator<Unifier> it = (Iterator<Unifier>) checkDesire.execute(this.getTS(), new Unifier(), new Term[]{drive.generateDesire()});
						if(it.hasNext())
							alreadyDesired = true;
						
					} catch (Exception e1) {
						alreadyDesired = false;
					}
					
					//v� o que se deve fazer com o drive
					DriveState state = getUEBDI().updateDrive(drive, getWrapperBB(), getEB(), getIB());
					switch(state)
					{
						case activeByUpper:
						case activeByLower:
						case activeByOther:
						
							if(!alreadyDesired)//inclui
							{
								getLogger().info("add drive: " + drive.toString());
								Literal desire = drive.generateDesire();
								getTS().getC().addAchvGoal(desire,Intention.EmptyInt);            
							}
						break;
						case inactive:
							if(alreadyDesired)
							{
								getLogger().info("remove drive: " + drive.toString());
								Literal desire = drive.generateDesire();
								drop_desire drop = new drop_desire();
								drop.dropEvt(getTS().getC(), desire, new Unifier());
							}
						break;
						default:
					}
					
					//atualiza o belief do drive
					drive.updateSource();
				}
			}
		}
		if(listForget != null)
		{
			for(Literal forgetIt : listForget)
			{
				getWrapperBB().remove(forgetIt);
			}
		}
		*/
		try{
			if(getUEBDI().criticalContext(getEB(), getWrapperBB()))
			{
				getUEBDI().secondReviewEmotion(getWrapperBB(),events,  getEB(), getIB());
			}
			
		}catch(EmotionalDynException e){
			getLogger().severe("Cannot process emotion flow because: " + e.toString() + "exception");
		}
	
		if(events.isEmpty())
			return null;
		
		
		//ordena os desejos em ordem de prioridade
		Event e= getUEBDI().priorityDesire(events, getWrapperBB(), getEB(), getIB());
		return e;
		 
		/*return super.selectEvent(events);*/
	}
	
	@Override
	public Option selectOption(List<Option> listOption)
	{
		return getUEBDI().selectPlan(listOption, getWrapperBB(), getEB());
	}
	
	@Override
	public Intention selectIntention(Queue<Intention> queueIntention)
	{
		getLogger().entering("UEBDIAgent", "selectIntention");
		return getUEBDI().selectIntention(queueIntention, getEB(), getWrapperBB());
		
	}
	
	
}