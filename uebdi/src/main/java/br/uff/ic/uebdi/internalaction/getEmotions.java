package br.uff.ic.uebdi.internalaction;

import java.util.Iterator;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.WrapperJasonUEBDI;



import jason.JasonException;
import jason.asSemantics.Agent;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;

/**
 * This internal action return all emotions of a agent. it work similar to .findall(emotions(X, Y, Z), emotions(X, Y, Z), LIST);
 * @author Marcos
 *
 */
public class getEmotions extends DefaultInternalAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6235769891738764206L;


	@Override
	    public int getMinArgs(){return 1;}
	    
	    @Override
	    public int getMaxArgs(){return getMinArgs();}
	    
	    @Override
	    public void checkArguments(Term[] terms)throws JasonException 
	    {
	    	Term tUnion= terms[0]; 
	    	if(!tUnion.isVar())
	    	{
	    		throw JasonException.createWrongArgument(this,"the term is not a var");
	    	}
	    }
	    
	    
	    @Override
	    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception 
	    {
	        // execute the internal action
	        ts.getAg().getLogger().fine("executing internal action 'br.uff.ic.uebdi.getEmotions'");
	        
	        Agent a = ts.getAg();
	        if(!(a instanceof WrapperJasonUEBDI))
	        {
	        	ts.getAg().getLogger().severe(String.format("Cannot use %s in a agent not is WrapperJasonUEBDI", getClass().getName()));
	        	return false;
	        }else
	        {
	        	WrapperJasonUEBDI w = (WrapperJasonUEBDI)a;
	        	IEmotionBase eb = w.getEB();
	        	Iterator<Literal> it = eb.getAllEmotions();
	        	
				ListTerm all = new ListTermImpl();
				ListTerm tail = all;
				while(it.hasNext()) 
				{
					Literal l = it.next();
					tail = tail.append(l.clone());        		
				}
	        	return un.unifies(args[0], all);
	        }
	    }
	}
