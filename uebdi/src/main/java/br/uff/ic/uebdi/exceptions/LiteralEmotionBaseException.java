package br.uff.ic.uebdi.exceptions;

public class LiteralEmotionBaseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LiteralEmotionBaseException(String message, Exception other)
	{
		super(message, other);
	}
}
