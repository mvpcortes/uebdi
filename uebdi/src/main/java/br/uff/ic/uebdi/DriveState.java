package br.uff.ic.uebdi;

/**
 * Determine the  next operation after update a drive 
 * @author Marcos
 *
 */
public enum DriveState {
	inactive(), // the drive is active
	activeByUpper(), //the drive is active because a upper threshold
	activeByLower(), //the drive is active because a lower threshold
	activeByOther()  //the drive is active because another cause.
}
