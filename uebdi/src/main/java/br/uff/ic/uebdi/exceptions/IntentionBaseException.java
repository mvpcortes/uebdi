package br.uff.ic.uebdi.exceptions;


public class IntentionBaseException extends UEBDIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1429638503235263854L;

	public IntentionBaseException(String msg, Exception e) {
		super(msg, e);
	}

}
