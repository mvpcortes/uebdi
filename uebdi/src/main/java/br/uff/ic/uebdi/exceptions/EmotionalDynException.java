package br.uff.ic.uebdi.exceptions;

public class EmotionalDynException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 13423426L;

	public EmotionalDynException(String msg, Exception e) {
		super(msg, e);
	}
}
