package br.uff.ic.uebdi;

import java.util.logging.Logger;

import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;

/**
 * TODO document it!
 * This class represent a Desire of type Drive. 
 * TODO I need to do it.
 * @author Marcos
 *
 */
public class Drive {

	private static final String STR_DRIVE = "drive";
	
	//drive(nome, limiteSup, limiteInf, incremento, valor) 
	private static final int POS_NAME = 0;
	private static final int POS_UPPERTH = 1;
	private static final int POS_LOWERTH = 2;
	private static final int POS_INC 	 = 3;
	private static final int POS_VALUE   = 4;
	private static final int POS_QTD     = 5;
	
	private Literal desired  = Literal.parseLiteral("driveDesire"); 
	
	private static Logger logger = Logger.getLogger(Drive.class.getName());
	
	private static Logger getLogger()
	{
		return logger;
	}
	
	
	private double intensity = 0;
	
	private double incValue = 1;
	
	private double upperTh = 100;
	
	private double lowerTh = -100;
	
	private Literal source = null;
	
	public Literal getDesired() {
		return desired;
	}
	
	public Literal generateDesire()
	{
		Literal temp = (Literal) getDesired().clone();
		//temp.addAnnot(this.source);
		return temp;
	}

	public boolean isMyDesire(Literal possible)
	{
		if(getDesired().equals(possible))
		{
			Literal l = (Literal) possible.getAnnots(STR_DRIVE);
			return this.source.equals(l);
		}
		return false;
	}
	public double getIntensity() {
		return intensity;
	}

	public double getIncValue() {
		return incValue;
	}

	public double getUpperTh() {
		return upperTh;
	}
	
	public double getLowerTh()
	{
		return lowerTh;
	}
	
	
	
	private Drive()
	{
		desired		 	= null;
		intensity		= 0;
		incValue 		= 0;
		upperTh 		= 0;
		lowerTh			= 0;
	}
		
	
	public boolean actived() {
	//	return getIntensity() > getUpperTh() || getIntensity() < getLowerTh();
		return activedByUpper() || activedByLower();
	}
	
	public boolean activedByUpper()
	{
		return getIntensity() > getUpperTh();
	}
	
	public boolean activedByLower()
	{
		return getIntensity() < getLowerTh();
	}
	
	public void naturalInc(double alfaValue)
	{
		intensity += incValue*alfaValue;
	}
	
	public void inc(double value)
	{
		intensity += value;
	}
	
	public void dec(double value)
	{
		intensity = intensity-value;
	}
	
	static public Drive parseLiteral(Literal l)
	{
		return parseLiteral (l, new Drive());
	}
	//drive(nome, valor, limiteSup, limiteInf, incremento) 
	static public Drive parseLiteral(Literal l, Drive dest)
	{
		if(!l.getFunctor().equals(STR_DRIVE))
			return null;
		try
		{
			if(l.getTerms().size() != POS_QTD)
				return null;
			
			Literal lName 		= (Literal)l.getTerm(POS_NAME);
			NumberTerm ntValue 	= (NumberTerm)l.getTerm(POS_VALUE);
			NumberTerm ntUT		= (NumberTerm)l.getTerm(POS_UPPERTH);
			NumberTerm ntLT		= (NumberTerm)l.getTerm(POS_LOWERTH);
			NumberTerm ntInc	= (NumberTerm)l.getTerm(POS_INC);
			//Drive d 			= new Drive(lName, ntUT.solve(), ntLT.solve(),ntInc.solve(), ntValue.solve());
			dest.desired 	= lName;
			dest.intensity 	= ntValue.solve();
			dest.upperTh   	= ntUT.solve();
			dest.lowerTh   	= ntLT.solve();
			dest.incValue	= ntInc.solve();
			dest.source		= l;
			return dest;
		}catch(Exception e)
		{
			getLogger().severe(String.format("problem in parse to Drive the literal: %s", l));
			return null;
		}
	}
	
	//drive(nome, limiteSup, limiteInf, incremento, value) 
	@Override
	public String toString()
	{
		return String.format("drive(%s, %f, %f, %f, %f)", getDesired(), getUpperTh(), getLowerTh(), getIncValue(), getIntensity());
	}

	public void updateSource() {
		if(source == null)
		{
			getLogger().severe("Tentando atualizar um drive sem source");
		}else
		{
			source.setTerm(POS_VALUE, new NumberTermImpl(this.getIntensity()));
		}		
	}
	

}
