package br.uff.ic.uebdi.mapmodel.entities;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.stdlib.literal;

import java.awt.Color;
import java.awt.Point;

public abstract class Entity {

	
	private String name;
	
	private Point pos;
	
	private Color color;
	
	private String text;
	
	public Entity(String n, Point p)
	{
		name = n;
		pos = p;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Point getPoint()
	{
		return pos;
	}
	
	public void setPoint(Point p){
		pos = p;
	}
	
	public void setPos(int x, int y) {
		pos = new Point(x, y);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public boolean visible(){
		return false;
	}
	
	public boolean isColised(){
		return false;
	}
	
	public boolean showText(){
		return true;
	}
	
	
}
