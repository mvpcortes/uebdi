package br.uff.ic.uebdi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Logger;

import br.uff.ic.uebdi.exceptions.IntentionBaseException;

import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.parser.ParseException;



/**
 * A simple implementation of {@link IIintentionBase}
 * @author Marcos
 *
 */
public class IntentionBaseSimple implements IIntentionBase 
{
	final TransitionSystem ts;
	
	private Logger logger = null;
	
	private Logger getLogger()
	{
		if(logger == null)
		{
			logger = Logger.getLogger(getClass().getName());
		}
		return logger;
	}
	/**
	 * usando uma instância estática pq este objeto não mantém estado.
	 */
	private static jason.stdlib.intend intendAction;
	private static Unifier voidUnifier;
	
	public IntentionBaseSimple(TransitionSystem _ts) {
		ts = _ts;
	}

	private static jason.stdlib.intend getIntendAction()
	{
		if(intendAction == null)
		{
			intendAction = new jason.stdlib.intend();
		}
		return intendAction;
	}
	
	private static Unifier getVoidUnifier()
	{
		if(voidUnifier == null)
		{
			voidUnifier = new Unifier();
		}
		return voidUnifier;
	}
	@Override
	public boolean intend(Literal query) throws IntentionBaseException 
	{
		try {
			@SuppressWarnings("unchecked")
			Iterator<Unifier> it = (Iterator<Unifier>) (getIntendAction().execute(ts, getVoidUnifier(), new Term[]{query}));
			return it!= null && it.hasNext() == true;
		} catch (Exception e) {
			throw new IntentionBaseException("Error in intend method", e);
		} 
				
	}
	
	@Override
	public Collection<Literal> getIntentions(Literal query) throws IntentionBaseException
	{
		return getIntentions(query, new LinkedList<Literal>());
	}
	
	@Override
	public
	Collection<Literal> getIntentions(Literal query, Collection<Literal> coll) throws IntentionBaseException
	{
		try {
			Object o = getIntendAction().execute(ts, getVoidUnifier(), new Term[]{query});
			if(o instanceof Iterator)
			{
				@SuppressWarnings({ "unchecked", "rawtypes" })
				Iterator it = (Iterator<Unifier>)o;
				
				while(it.hasNext())
				{
					Object item = it.next();
					if(item instanceof Unifier)
					{
						Unifier u = (Unifier)item;
						Literal newLit = (Literal) query.clone();
						newLit.apply(u);
						coll.add(newLit);
					}else
					{
						getLogger().warning("Unexpected value in return of .intend action");
					}
								
				}
				
				return coll;
			}else
			{
				getLogger().warning("Unexpected value in return of .intend action");
			}
			return Collections.emptyList();
		}catch(ClassCastException e)
		{
			throw new IntentionBaseException("Error in cast item in get Itentions", e);
		}catch (Exception e) {
		
			throw new IntentionBaseException("Error in intend method", e);
		} 
	}
	
	private static Collection<Literal> collUsedInToString = new ArrayList<Literal>();
	private static Term queryX = null;
	public String toString()
	{
		if(queryX == null)
		{
			try {
				queryX = ASSyntax.parseVar("X");
			} catch (ParseException e) {
				return e.toString();
			}
		}
		
		synchronized(collUsedInToString)
		{
			collUsedInToString.clear();
			try {
				this.getIntentions((Literal)queryX, collUsedInToString);
			} catch (IntentionBaseException e) {
				return e.toString();
			}
			
			return collUsedInToString.toString();
		}
	}
	
}
