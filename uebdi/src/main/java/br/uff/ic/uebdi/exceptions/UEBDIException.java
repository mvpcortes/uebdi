package br.uff.ic.uebdi.exceptions;

public class UEBDIException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UEBDIException(String msg, Exception e)
	{
		super(msg, e);
	}
}
