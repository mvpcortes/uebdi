package br.uff.ic.uebdi;

import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;

import java.util.Iterator;


public class UnifierIterator implements Iterator<Literal> {

	private Literal applicableLiteral;
	
	private Iterator<Unifier> itUnifier;
	
	public UnifierIterator(Iterator<Unifier> it, Literal app)

	{
		applicableLiteral = app;
		itUnifier = it;
	}
	
	@Override
	public boolean hasNext() {
		return itUnifier.hasNext();
	}

	@Override
	public Literal next() {
		Unifier u = itUnifier.next();
		Literal l = (Literal) applicableLiteral.clone();
		l.apply(u);
		return l;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot remove a item from the UnifierIterator");
	}

}
