package br.uff.ic.uebdi.mapmodel.entities;

import java.awt.Color;
import java.awt.Point;

public class Exit extends Entity {

	public Exit(Point point) {
		super("exit", point);
		setColor(Color.green);
	}
	
	@Override
	public boolean visible(){
		return true;
	}

	@Override
	public boolean isColised(){
		return false;
	}
}
