package br.uff.ic.uebdi.mapmodel.entities;

import java.awt.Point;

public class Wall extends Entity {

	public Wall(Point p) {
		super("wall", p);
	}

	
	public boolean equals(Object o)
	{
		if(o instanceof Wall)
		{
			Wall other = (Wall)o;
			return other.getPoint() != null && getPoint() != null && other.getPoint().equals(getPoint());
		}
		
		return false;
	}
	
	public boolean isColised(){
		return true;
	}

}
