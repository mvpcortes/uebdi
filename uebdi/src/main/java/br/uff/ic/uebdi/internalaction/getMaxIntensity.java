// Internal action code for project UEBDI

package br.uff.ic.uebdi.internalaction;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.WrapperJasonUEBDI;
import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

/**
 * Return the max intensity associate to an emotion.
 * @author Marcos
 *
 */
public class getMaxIntensity extends DefaultInternalAction {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 8705937964492126698L;


	@Override
    public int getMinArgs(){return 1;}
    
    @Override
    public int getMaxArgs(){return getMinArgs();}
    
    @Override
    public void checkArguments(Term[] terms)throws JasonException 
    {
    	Term tUnion= terms[0]; 
    	if(!tUnion.isVar())
    	{
    		throw JasonException.createWrongArgument(this,"the term is not a var");
    	}
    }
    
    
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        // execute the internal action
        ts.getAg().getLogger().fine("executing internal action 'br.uff.ic.uebdi.getMaxIntensity'");
        
        Agent a = ts.getAg();
        if(!(a instanceof WrapperJasonUEBDI))
        {
        	ts.getAg().getLogger().severe(String.format("Cannot use %s in a agent not is WrapperJasonUEBDI", getClass().getName()));
        	return false;
        }else
        {
        	WrapperJasonUEBDI w = (WrapperJasonUEBDI)a;
        	IEmotionBase eb = w.getEB();
        	double maxInt = eb.getMaxIntensity();
        	return un.unifies(args[0], new NumberTermImpl(maxInt));
        }
    }
}
