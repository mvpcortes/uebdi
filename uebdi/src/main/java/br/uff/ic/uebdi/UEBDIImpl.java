package br.uff.ic.uebdi;

import jason.asSemantics.Event;
import jason.asSemantics.Intention;
import jason.asSemantics.Option;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.Term;
import jason.asSyntax.Trigger;
import jason.bb.BeliefBase;

import java.util.Calendar;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.exceptions.EmotionalDynException;
import br.uff.ic.uebdi.exceptions.UEBDIException;

/**
 * Implementa��o da UEBDI
 * @author Marcos
 *
 */
public class UEBDIImpl implements IUEBDI, IEmotionDyn, IAccopledEmotionDyn{

	private static final double DEFAULT_PRIOR_DESIRE = 0.5;

	private static final String STR_PRIORITY = "priority";

	/**
	 * Used to know where forget a belief
	 */
	private static final int FORGET_FACTOR = 10;
	
	/**
	 * Logger
	 */
	private Logger logger = null;
	
	
	/**
	 * reference of emotion dyn 
	 */
	private IEmotionDyn ed = null;
	
	public UEBDIImpl()
	{
		logger = Logger.getLogger(getClass().getName());
		logger.setLevel(Level.OFF);
	}
	
	
	protected Logger getLogger()
	{
		return logger;
	}
	
	@Override
	public void setLoggerData(String name, Level level)
	{
		logger = Logger.getLogger(String.format("%s.%s", getClass().getName(), name));
		logger.setLevel(level);
	}
	
	@Override
	public void firstReviewEmotion(List<Literal> listPerc,
			IEmotionBase emotionBase, IIntentionBase intentionBase)  throws EmotionalDynException{
		
		getLogger().fine("firstReviewFunction");
		
		if(ed != null)
			ed.firstReviewEmotion(listPerc, emotionBase, intentionBase);
	
		
	}

	@Override
	public void secondReviewEmotion(BeliefBase beliefBase,
			 Queue<Event> events, IEmotionBase emotionBase,
			IIntentionBase intentionBase)  throws EmotionalDynException {
		
		getLogger().fine("secondReviewFunction");
		
		if(ed != null)
			ed.secondReviewEmotion(beliefBase, events, emotionBase, intentionBase);
		
	}

	@Override
	public void taggingBelief(Literal beliefAdd, IEmotionBase emotionBase) throws UEBDIException
	{
	}


	static Calendar cal = Calendar.getInstance();
	private long getActualTime() {
		return cal.getTimeInMillis();
	}
	
	@Override
	public
	void updateBeliefs(List<Literal> percepts, BeliefBase bb, IEmotionBase eb,
			IIntentionBase ib)
	{
		
	}

	@Override
	public boolean forgetBelief(Literal belief) {
		getLogger().entering("UEBDIImpl", "fogettingBelief");
		Literal lit = (Literal) belief.getAnnots("tagEmotion");
		Literal litForget = (Literal)belief.getAnnots("forget");//tag para indicar que a cren�a � esquec�vel
		
		if(lit == null  || !lit.isList() || ((ListTerm)lit).isEmpty()){
			return false;
		}
		if(litForget == null  || !litForget.isList() || ((ListTerm)litForget).isEmpty())
			return false;
		
		try
		{
			NumberTerm ntIntensity = (NumberTerm) lit.getTerm(0);
			NumberTerm ntMiliTime = (NumberTerm) lit.getTerm(1);
			double nIntensity = ntIntensity.solve();
			double nMiliTime = ntMiliTime.solve();
			
			double alfa = -1/(FORGET_FACTOR*nIntensity);
			
			double atual = alfa*(getActualTime() - nMiliTime) + 1;
			return atual <= 0.0;
		}catch(Exception e)
		{
			return false;
		}
	}



	@Override
	public void perception(List<Literal> listSensing, IEmotionBase emotionBase,
			BeliefBase beliefBase, IIntentionBase intentionBase) {
		
		if(listSensing != null)
		{
			getLogger().fine("run UEBDI.perception: " + listSensing.toString());
		}else
		{
			getLogger().fine("run UEBDI.perception: null");
		}
		
		if(listSensing == null || listSensing.size() <= 0)
		{
			getLogger().fine("finish UEBDI.perception with list void");
		}else
		{
			perceptionResourceFilter	(beliefBase, listSensing);
			perceptionRulesFilter		(beliefBase, listSensing);
			perceptionEmotionFilter		(emotionBase, listSensing);
		}
		
			
			
		getLogger().fine("finish UEBDI.perception");
	}

	@Override
	public void perceptionResourceFilter(BeliefBase beliefBase,
			List<Literal> listSensing) {
	}

	@Override
	public void perceptionRulesFilter(BeliefBase beliefBase, List<Literal> listSensing) {
	}

	@Override
	public void perceptionEmotionFilter(IEmotionBase emotionBase, List<Literal> listSensingg) {
	}

	@Override
	public DriveState updateDrive(Drive drive, BeliefBase beliefBase,
			IEmotionBase emotionBase, IIntentionBase intentionBase) {

		//increment drive value.
		
		drive.naturalInc(1);
		{
			getLogger().info("updateDrive");
			if(drive.activedByUpper())
			{
				return DriveState.activeByUpper;
			}else if(drive.activedByLower())
			{
				return DriveState.activeByLower;
			}else
				return DriveState.inactive;
		}
	}

	@Override
	public int compareGoals(Event a, Event b, BeliefBase beliefBase,
			IEmotionBase emotionBase, IIntentionBase intentionBase) {
		
		getLogger().entering("UEBDIImpl", "compareGoals", String.format("%s==%s?", a.toString(), b.toString()));
		double pA = getPriority(a);
		double pB = getPriority(b);
		
		int test = (int) (pA - pB);
		if(test != 0)
			return test;
		else
			return pA > pB? 1:-1;
			
	}

	private double getPriority(Event ev)
	{
		Trigger t = ev.getTrigger();
		if(t == null)
			return DEFAULT_PRIOR_DESIRE;
		else
		{
			Literal l = t.getLiteral();
			return getPriority(l);
		}
	}
	
	/**
	 * return the priority of a desire.
	 * it is specified by the annotation priority(VALUE). VALUE = the priority between [0,1].
	 * The default value is 0.5
	 * @param ev
	 * @return
	 */
	private double getPriority(Literal ev)
	{
		Literal temp = null;
		try
		{
			
			ListTerm lt = ev.getAnnots(STR_PRIORITY);
			if(lt != null && lt.size() > 0)
			{
				Term t = lt.get(0);
				if(t.isLiteral())
				{
					temp = (Literal)t;
				}
				
			}
		}catch(Exception e)
		{
			getLogger().severe("Problem in get the priority annotation of a event");
		}
		
		if(temp != null)
		{
			NumberTerm nterm = null;
			try
			{
				nterm = (NumberTerm) temp.getTerm(0);
			}catch(Exception e)
			{
				nterm = null;
			}
			
			if(nterm != null)
			{
				double d = nterm.solve();
				return d;
			}		
		}
		
		return DEFAULT_PRIOR_DESIRE;
	}
	

	@Override
	public Event priorityDesire(Queue<Event> listEvent,
			BeliefBase beliefBase, IEmotionBase emotionBase,
			IIntentionBase intentionBase) {
		
		getLogger().fine("priorityDesire");
	
		if(listEvent.size() == 0)
			return null;
		
		Event maxEvent = listEvent.iterator().next();
		
		for(Event ev: listEvent)
		{
			int i = compareGoals(ev, maxEvent, beliefBase, emotionBase, intentionBase);
			if(i > 0)
				maxEvent = ev;	
		}
		
		listEvent.remove(maxEvent);	
		getLogger().fine("Selected Event: " + maxEvent.toString());
		return maxEvent;
	}
	
	@Override
	public Option selectPlan(List<Option> listOp, BeliefBase beliefBase,
			IEmotionBase emotionBase) {
		getLogger().entering("UEBDIImpl", "selectPlan");
		if(listOp != null && listOp.size() > 0)
			return listOp.remove(0);
		else
			return null;
	}
	
	@Override
	public Intention selectIntention(Queue<Intention> listIntention, IEmotionBase eb, BeliefBase bb)
	{
		return listIntention.poll();
	}
	
	
	@Override
	public UEBDIImpl clone()
	{
		getLogger().entering("UEBDIImpl", "clone");
		UEBDIImpl other = new UEBDIImpl();
		return other;
	}


	@Override
	public boolean criticalContext(IEmotionBase eb, BeliefBase bb) throws EmotionalDynException
	{
		if(ed != null)
		{
			return ed.criticalContext(eb, bb) ;
		}else
		{
			return true;
		}
	}



	@Override
	public void setEmotionDyn(IEmotionDyn _ed) {
		ed = _ed;
	}


	@Override
	public void initCycle(int nCycle, Object... params) {}


	@Override
	public void endCycle(int nCycle) {}

}
