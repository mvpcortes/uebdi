package br.uff.ic.uebdi;

/**
 * Indica que esta inst�ncia de IUEBDI permite a adi��o de um EmotionDyn
 * @author Marcos
 *
 */
public interface IAccopledEmotionDyn {
	
	/**
	 * Set method to set the emotional Dynamic.
	 * @param ed
	 */
	public void setEmotionDyn(IEmotionDyn ed);
}
