package br.uff.ic.uebdi;

import java.util.Iterator;
import java.util.logging.Logger;


import jason.asSyntax.Literal;
import jason.asSyntax.LiteralImpl;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.PredicateIndicator;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.bb.BeliefBase;
import br.uff.ic.util.EmptyIterator;
import br.uff.ic.util.SubIterator;
import br.uff.ic.util.SubIteratorCondition;
import br.uff.ic.util.TermHelper;

/**
 * emo��es da forma emotion(ID_0, ID_1,..., ID_N, intensidade). Baseada na base de cren�as.
 * @author Marcos
 */
class LiteralEmotionBase implements IEmotionBase{

	private class SICEqual implements SubIteratorCondition<Literal>
	{
		final IEmotionBase base;
		final Literal it;
		public SICEqual(final Literal _emotion, final IEmotionBase _base)
		{
			if(_emotion == null)
				throw new IllegalArgumentException("Cannot use a null _emotion argument.");
			it = _emotion;
			
			if(_base == null)
				throw new IllegalArgumentException("Cannot use a null _base argument.");
			base = _base;
		}

		@Override
		public boolean isIt(Literal e) {
			return base.emotionEquals(it, e);
		}
	}
	
	//EmotionBaseIntensityBounders ebib = new EmotionBaseIntensityBounders();
	
	private void fireAdded(Literal l)
	{
		//ebib.fireAdded(l);
	}
	
	private void fireRemoved(Literal l)
	{
		//ebib.fireRemoved(l);
	}

	
	Logger logger = null;
	
	private BeliefBase bb;
	
	
	private static String FUNCTOR = "emotion";
	
	public LiteralEmotionBase()
	{
		bb = null;
		logger = Logger.getLogger(this.getClass().getName());
	}
	
	@Override
	public boolean add(double intensity, String... params) 
	{
		Structure addTerm = new LiteralImpl(FUNCTOR);
		for(String s: params)
		{
			Literal l = Literal.parseLiteral(s);
			if(l.canBeAddedInBB())
				addTerm.addTerm(l);
			else
				return false;
		}
		
		addTerm.addTerm(new NumberTermImpl(intensity));
		
		if(getBB().add(addTerm))
		{
			fireAdded(addTerm);
			return true;
		}else
		{
			return false;
		}
	}
	
	@Override
	public boolean add(Literal emotion) {
		if(!isValidEmotion(emotion))
		{
			if(emotion.isList())
			{
				for(Term t: emotion.getTerms())
				{
					if(t.isLiteral())
					{
						Literal ll = (Literal)t;
						if(isValidEmotion(ll))
							if(!getBB().add(ll))
							{
								getLogger().warning("Cannot add a term in EB! " + ll.toString());
							}else
							{
								fireAdded(ll);
							}
						else
							getLogger().warning("Cannot add a list term in EB! " + ll.toString());
					}
				}
				return true;
			}
		}
		if(getBB().add(emotion))
		{
			fireAdded(emotion);
			return true;
		}else
		{
			return false;
		}
		
	}

	@Override
	public double changeIntensity(double newIntensity, String... params) {
		Iterator<Literal> it = getAll(params);
		
		if(!it.hasNext())//faz inserir um conforme especificado
		{
		    Literal l = this.generateLiteral(newIntensity, params);
		    if(getBB().add(l))
		    {
		    	fireAdded(l);
		    }
		}else
		{
		    
        		while(it.hasNext())
        		{
        			Literal l = it.next();
        			if(getBB().remove(l))
        			{
        				fireRemoved(l);
        			}
        			int pos = l.getTerms().size()-1;
        			if(pos<0)
        			{
        				getLogger().severe("the literal" + l.toString() + " in bb is not a valid emotion!");
        			}else
        			{
        			
        				//double value = TermHelper.getDouble(l, pos);
        				l.setTerm(pos, new NumberTermImpl(newIntensity));
        			}
        			if(getBB().add(l))
        			{
        				fireAdded(l);
        			}
        		}
		}
		
		return newIntensity;
	}
	
	
	@Override
	public IEmotionBase clone(WrapperJasonUEBDI newWrapperJasonUEBDI) {
		LiteralEmotionBase eb = new LiteralEmotionBase();
		eb.bb = newWrapperJasonUEBDI.getBB();
		return eb;
	}

	public int delete(Literal emo)
	{
		int count = 0;
		Iterator<Literal>lit = getAll(emo);
		while(lit.hasNext())
		{
			Literal l = lit.next();
			if(getBB().remove(l))
			{
				fireRemoved(l);
				count++;
			}
		}
		
		return count;
	}

	public int delete(String... params)
	{
		int count = 0;
		Iterator<Literal>lit = getAll(params);
		while(lit.hasNext())
		{
			Literal l = lit.next();
			if(getBB().remove(l))
			{
				fireRemoved(l);
				count++;
			}
		}
		
		return count;
	}
	/**
	 * @param query
	 * @return
	 */
	private Literal generateLiteral(double intensity, String... query) {
		Literal litQuery = new LiteralImpl(true, new Structure(FUNCTOR));
		
		for(String s: query)
		{
			Literal add = null;
			if(s == null || s.equals("_"))
			{
				add = Literal.parseLiteral("_");
			}else
			{
				add = Literal.parseLiteral(s);
			}
			litQuery.addTerm(add);
		}
		
		litQuery.addTerm(new NumberTermImpl(intensity));
		return litQuery;
	}

	private Literal generateLiteral(String... query) {
	    return generateLiteral(0.0, query);
	}

	@Override
	public Literal get(Literal query) {
		Iterator<Literal> it =  getAll(query);
		if(it == null || !it.hasNext())
		    return null;
		else
		    return it.next();
	}
	
	@Override
	public Iterator<Literal> getAll( Literal query) 
	{
		if(!query.isLiteral() || !FUNCTOR.equals(query.getFunctor()))
		{
			return new EmptyIterator<Literal>(); 
		}
		
		if(query.isList())
		{
			Literal new_query = new Structure(FUNCTOR);
			new_query.addTerms(query.getTerms());
			query = new_query;			
		}
		
		final int size = query.getTerms().size();
		
		final PredicateIndicator pi = new PredicateIndicator(FUNCTOR, size);
		
		Iterator<Literal> it = getBB().getCandidateBeliefs(pi);
		
		if(it == null || !it.hasNext())
		{
		    return new EmptyIterator<Literal>();
		}
		//faz o condition para apenaas retornar os v�lidos
		SubIteratorCondition<Literal> sic = new SICEqual(query, this);
		
		return new SubIterator<Literal>(it, sic);
	}
	
	@Override
	public Iterator<Literal> getAll(String... query) {
		
		if(query == null || query.length == 0)
			return new EmptyIterator<Literal>();
		
		
		Literal litQuery = generateLiteral(query);
		
		return getAll(litQuery);
		
	}

	public Iterator<Literal> getAllEmotions()
	{
		SubIteratorCondition<Literal> sic = new SubIteratorCondition<Literal>() {
			
			@Override
			public boolean isIt(Literal e) {
				return isValidEmotion(e);
			}
		};
		
		return new SubIterator<Literal>(getBB().iterator(), sic);
	}

	private BeliefBase getBB()
	{
		return bb;
		
	}
	
	@Override
	public double extractIntensity(Literal emotion) {
		if(isValidEmotion(emotion))
		{
			int size = emotion.getTerms().size();
				return TermHelper.getDouble(emotion, size-1); 
		}
		
		throw new IllegalArgumentException("Invalid emotion format.");
		
	}

	@Override
	public double getIntensity(Literal emotion) 
	{
		if(emotion == null)
		{
			throw new IllegalArgumentException("emotion should not be null.");
		}
	    Literal l = get(emotion);
	    if(l==null)
	    {
	    	return 0;
	    }else
	    {
	    	return extractIntensity(l);
	    }
	}

	@Override
	/**
	 * return the intensity of a emotion
	 * @param query A array of string to query
	 * @return the intensity of emotion 
	 */
	public double getIntensity(String... query)
	{
		Literal q = generateLiteral(query);
		return getIntensity(q);
	}

	private Logger getLogger()
	{
		return logger;
	}
	
	public double incIntensity(double incValue, double startValue, String...params)
	{
		return incIntensityInternal(incValue, startValue, params);
	}
	
	@Override
	public double incIntensity(double incValue, String... params) {
		
		
		return incIntensityInternal(incValue, incValue, params);
	}
	
	
	
	private double incIntensityInternal(double incValue, double startValue, String[] params)
	{
		Iterator<Literal> it = getAll(params);
		double value = 0;
		if(!it.hasNext())//faz inserir um conforme especificado
		{
		    Literal l = this.generateLiteral(startValue, params);
		    if(getBB().add(l))
		    {
		    	fireAdded(l);
		    }
		    value = startValue;
		}else
		{
        		while(it.hasNext())
        		{
        			Literal l = it.next();
        			if(getBB().remove(l))
        			{
        				fireRemoved(l);
        			}
        			
        			int pos = l.getTerms().size()-1;
        			if(pos<0)
        			{
        				getLogger().severe("the literal" + l.toString() + " in bb is not a valid emotion!");
        			}else
        			{
        			
        				value = TermHelper.getDouble(l, pos) + incValue;
        				l.setTerm(pos, new NumberTermImpl(value));
        			}
        			getBB().add(l);
        		}
		}
		
		return value;
	}
	

	@Override
	public void initBase(WrapperJasonUEBDI w)
	{
		bb = w.getBB();
	}

	@Override
	public void resetEmotion()
	{
		Iterator<Literal> it = getBB().iterator();
		int minA = Integer.MAX_VALUE;
		int maxA = Integer.MIN_VALUE;
		while(it.hasNext())
		{
			Literal lit = it.next();
			if(isValidEmotion(lit))
			{
				minA = Math.min(minA, lit.getArity());
				maxA = Math.max(maxA, lit.getArity());
			}
		}
		
		for(int i = minA; i<= maxA; i++)
		{
			PredicateIndicator pi = new PredicateIndicator(FUNCTOR, i);
			getBB().abolish(pi);
		}
	}
	
	
	@Override
	public String toString()
	{
		Iterator<Literal> it = this.getAllEmotions();
		
		if(it == null || it.hasNext() == false)
		{
			return "[]";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("[ ");
		sb.append(it.next());
		while(it.hasNext())
		{
			Literal lit = it.next();
			sb.append(", ");
			sb.append(lit.toString());
		}
		
		sb.append("]");
		return sb.toString();		
	}
	
	
	
	
	public double getMaxIntensity()
	{
		Iterator<Literal> it = getAllEmotions();
		double max = -1;
		while(it.hasNext())
		{
			Literal l = it.next();
			double in = extractIntensity(l);
			if(in > max)
			{
				max = in;
			}
		}
		
		return max;
	}

	@Override
	public boolean isValidEmotion(Literal emotion) {
		if(emotion == null)
			return false;
		
		if(!emotion.canBeAddedInBB())
			return false;
		
		if(!FUNCTOR.equals(emotion.getFunctor()))
			return false;
		
		final int size = emotion.getTerms().size() ;
		if(size > 0)
		{
			Term last = emotion.getTerm(size-1);
			
			if(last.isNumeric())
				return true;
			
			if((last.isVar() || last.isUnnamedVar()) & !last.isGround())
				return true;
		}
		return false;
	}

	@Override
	public boolean emotionEquals(Literal a, Literal b) {
		
		if(!this.isValidEmotion(a) || !this.isValidEmotion(b))
			return false;
		
		int sizeA = a.getTerms().size();
		int sizeB = b.getTerms().size();
		
		if(sizeA != sizeB)
			return false;
		
		for(int i = 0; i < sizeA-1; i++)
		{
			Term ta = a.getTerm(i);
			Term tb = b.getTerm(i);
			if(!ta.equals(tb))
				return false;
		}
		
		return true;
	}

	

}
