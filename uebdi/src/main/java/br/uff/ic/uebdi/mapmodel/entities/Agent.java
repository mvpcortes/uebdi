package br.uff.ic.uebdi.mapmodel.entities;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;

import java.awt.Point;

//Entidade que � tratada como um agente
public class Agent extends Entity {

	private boolean died = false;

	private boolean colised = true;
	
	public Agent(String n, Point p) {
		super(n, p);
		// TODO Auto-generated constructor stub
	}
	
	public Agent(String n, Point p, boolean colised) {
		super(n, p);
		this.colised = colised;
	}

	public boolean isDied() {
		return died;
	}

	public void setDied(boolean yes) {
		died = yes;
	}

	public boolean visible() {
		return true;
	}
	
	public boolean showText(){
		return true;
	}
	
	public boolean isColised(){
		return colised; 
	}

	public Literal toLiteralPerception() {
		return ASSyntax.createLiteral("agent",
					ASSyntax.createAtom(getName()),
					ASSyntax.createNumber(getPoint().x),
					ASSyntax.createNumber(getPoint().y),
					ASSyntax.createAtom(isDied()?"died":"alive")
				);
	}

}
