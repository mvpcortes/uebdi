package br.uff.ic.uebdi;

import jason.asSemantics.Event;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

import java.util.List;
import java.util.Queue;

import br.uff.ic.uebdi.IIntentionBase;
import br.uff.ic.uebdi.exceptions.EmotionalDynException;

/**
 * Emotional Dynamic Interface
 * @author Marcos
 *
 */
public interface IEmotionDyn extends Cloneable{
	
	/**
	 * The first review emotional function
	 * @param eb
	 * @param listPerc
	 * @param intentionBase
	 */
	public void firstReviewEmotion(List<Literal> listPerc, IEmotionBase eb,
			IIntentionBase intentionBase) throws EmotionalDynException;

	/**
	 * The second review emotional function
	 * @param eb
	 * @param listPerc
	 * @param intentionBase
	 * @throws EmotionalDynException 
	 */
	public void secondReviewEmotion(BeliefBase bb, Queue<Event> events,
			IEmotionBase eb, IIntentionBase intentionBase) throws EmotionalDynException;
	
	/**
	 * The critical context function
	 * @param eb
	 * @param bb
	 * @return
	 */
	boolean criticalContext(IEmotionBase eb, BeliefBase bb)  throws EmotionalDynException;
}
