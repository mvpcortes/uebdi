package br.uff.ic.uebdi;

import java.util.Collection;

import br.uff.ic.uebdi.exceptions.IntentionBaseException;

import jason.asSyntax.Literal;


/**
 * Pemits fast and robust acess to intentions of a agent
 * TODO I need look it. I do not know I need it.
 * @author Marcos
 *
 */
public interface IIntentionBase extends Cloneable
{
	public boolean intend(Literal query)  throws IntentionBaseException ;

	Collection<Literal> getIntentions(Literal query) throws IntentionBaseException;
	
	Collection<Literal> getIntentions(Literal query, Collection<Literal> coll) throws IntentionBaseException;
}
