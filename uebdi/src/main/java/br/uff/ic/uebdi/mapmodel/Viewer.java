package br.uff.ic.uebdi.mapmodel;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Iterator;

import javax.swing.JFrame;

import br.uff.ic.uebdi.mapmodel.entities.Entity;

public class Viewer extends JFrame {
	private static final long serialVersionUID = 1L;

	protected int cellSizeW = 0;
	protected int cellSizeH = 0;

	protected GridCanvas drawArea;
	protected MapModel model;

	protected Font defaultFont = new Font("Arial", Font.BOLD, 10);

	public Viewer(MapModel model, String title, int windowSize) {
		super(title);
		this.model = model;
		initComponents(windowSize);

	}

	/** sets the size of the frame and adds the components */
	public void initComponents(int width) {
		setSize(width, width);
		getContentPane().setLayout(new BorderLayout());
		drawArea = new GridCanvas();
		getContentPane().add(BorderLayout.CENTER, drawArea);
	}

	@Override
	public void repaint() {
		cellSizeW = drawArea.getWidth() / model.getWidth();
		cellSizeH = drawArea.getHeight() / model.getHeight();
		drawArea.repaint();
	}

	/** updates all the frame */
	public void update() {
		repaint();
	}

	public void drawObstacle(Graphics g, int x, int y) {
		g.setColor(Color.darkGray);
		g.fillRect(x * cellSizeW + 1, y * cellSizeH + 1, cellSizeW - 1,
				cellSizeH - 1);
		g.setColor(Color.black);
		g.drawRect(x * cellSizeW + 2, y * cellSizeH + 2, cellSizeW - 4,
				cellSizeH - 4);
	}

	public void drawString(Graphics g, int x, int y, Font f, String s) {
		g.setFont(f);
		FontMetrics metrics = g.getFontMetrics();
		int width = metrics.stringWidth(s);
		int height = metrics.getHeight();
		g.drawString(s, x * cellSizeW + (cellSizeW / 2 - width / 2), y
				* cellSizeH + (cellSizeH / 2 + height / 2));
	}

	public void drawEmpty(Graphics g, int x, int y) {
		g.setColor(Color.green);
		g.fillRect(x * cellSizeW + 1, y * cellSizeH + 1, cellSizeW - 1,
				cellSizeH - 1);
		g.setColor(Color.green);
		g.drawRect(x * cellSizeW, y * cellSizeH, cellSizeW, cellSizeH);
	}

	private void draw(Graphics g, int x, int y) {
		Iterator<Entity> it = model.getEntities(x, y);
		while (it.hasNext()) {
			Entity e = it.next();
			if (e.visible()) {
				String text = null;
				if (e.showText() && e.getName() != null
						&& !e.getName().equals(""))
					text = e.getText();

				drawEntity(g, x, y, e.getColor(), e.getName(), text);
			}
		}
	}

	private void drawEntity(Graphics graphics, int x, int y, Color color,
			String name, String text) {
		final int margem = 0;
		graphics.setColor(color);
		//graphics.fillOval(x * cellSizeW + margem, y * cellSizeH + margem,
		//		cellSizeW - margem * 2, cellSizeH - margem * 2);
		graphics.fillRect(x * cellSizeW + margem, y * cellSizeH + margem,
				cellSizeW - margem * 2, cellSizeH - margem * 2);
		if (text != null) {
			graphics.setColor(Color.black);
			if (!text.trim().equals("")) {
				drawString(graphics, x, y, defaultFont,
						String.format("%s(%s)", name, text));
			} else {
				drawString(graphics, x, y, defaultFont,
						String.format("%s", name));
			}
		}

	}

	public Canvas getCanvas() {
		return drawArea;
	}

	class GridCanvas extends Canvas {

		private static final long serialVersionUID = 1L;

		Image backbuffer;

		int lastWidth, lastHeight;
		Graphics backg;

		public GridCanvas() {
			lastHeight = -1;
			lastWidth = -1;
		}

		private void updateBuffer() {
			cellSizeW = drawArea.getWidth() / model.getWidth();
			cellSizeH = drawArea.getHeight() / model.getHeight();
			if (lastWidth != this.getWidth() || lastHeight != this.getHeight()) {
				lastWidth = this.getWidth();
				lastHeight = this.getHeight();
				backbuffer = createImage(lastWidth, lastHeight);
			}
			backg = backbuffer.getGraphics();
			backg.setColor(Color.white);
			int mwidth = model.getWidth();
			int mheight = model.getHeight();

			backg.setColor(Color.lightGray);
			// for (int l = 1; l <= mheight; l++) {
			// backg.drawLine(0, l * cellSizeH, mwidth * cellSizeW, l *
			// cellSizeH);
			// }
			// for (int c = 1; c <= mwidth; c++) {
			// backg.drawLine(c * cellSizeW, 0, c * cellSizeW, mheight *
			// cellSizeH);
			// }

			for (int x = 0; x < mwidth; x++) {
				for (int y = 0; y < mheight; y++) {
					try {
						draw(backg, x, y);
					} catch (Exception e) {
						int i = 0;
						i++;
					}
				}
			}

		}

		public void paint(Graphics g) {
			super.paint(g);
			updateBuffer();
			g.drawImage(backbuffer, 0, 0, this);
		}
	}

}
