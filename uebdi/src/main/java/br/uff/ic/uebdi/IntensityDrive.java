package br.uff.ic.uebdi;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.SourceInfo;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

public class IntensityDrive implements NumberTerm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Drive drive;
	public IntensityDrive(Drive d)
	{
		drive = d;
	}
	
	@Override public boolean isVar()            { return false; }
	@Override public boolean isUnnamedVar()     { return false; }
    @Override public boolean isLiteral()        { return false; }
    @Override public boolean isRule()           { return false; }
    @Override public boolean isList()           { return false; }
    @Override public boolean isString()         { return false; }
    @Override public boolean isInternalAction() { return false; }
    @Override public boolean isArithExpr()      { return false; }
    @Override public boolean isNumeric()        { return true; }
    @Override public boolean isPred()           { return false; }
    @Override public boolean isStructure()      { return false; }
    @Override public boolean isAtom()           { return false; }
    @Override public boolean isPlanBody()       { return false; }
    @Override public boolean isGround()         { return true; }
    @Override public boolean hasVar(VarTerm t)  { return false; }
    
	
	@Override
	public void countVars(Map<VarTerm, Integer> c) {
		
	}

	@Override
	public boolean apply(Unifier u) {
		return false;
	}

	@Override
	public void setSrcInfo(SourceInfo s) {
	}

	@Override
	public SourceInfo getSrcInfo() {
		return null;
	}

	  @Override
	    public boolean equals(Object o) {
	        if (o == this) return true;

	        if (o != null && o instanceof Term && ((Term)o).isNumeric() && !((Term)o).isArithExpr()) {
	            NumberTerm st = (NumberTerm)o;
	            try {
	                return solve() == st.solve();
	            } catch (Exception e) { }
	        } 
	        return false;
	    }

	   
	    
	    @Override
	    public int compareTo(Term o) {
	        if (o instanceof VarTerm) {
	            return o.compareTo(this) * -1;
	        }
	        
	        if (o instanceof NumberTerm) {
	            NumberTerm st = (NumberTerm)o;
	            if (solve() > st.solve()) return 1;
	            if (solve() < st.solve()) return -1;
	            return 0;
	        }
	        return -1;
	    }

	    @Override
	    public String toString() {
	    	double temp = solve();
	        long r = Math.round(temp);
	        if (temp == (double)r) {
	            return String.valueOf(r);
	        } else {
	            return String.valueOf(temp);
	        }
	    }
	    
	    /** get as XML */
	    public Element getAsDOM(Document document) {
	        Element u = (Element) document.createElement("number-term");
	        u.appendChild(document.createTextNode(toString()));
	        return u;
	    } 
	

	@Override
	public double solve() {
		return drive.getIntensity();
	}
	
	@Override
	public NumberTerm clone()
	{
		return new IntensityDrive(drive);
	}

}
