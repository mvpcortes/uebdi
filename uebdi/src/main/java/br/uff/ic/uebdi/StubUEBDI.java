package br.uff.ic.uebdi;

import jason.asSemantics.Event;
import jason.asSemantics.Intention;
import jason.asSemantics.Option;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

import java.util.List;
import java.util.Queue;
import java.util.logging.Level;


/**
 * A fake UEBDI agent to test
 * @author Marcos
 *
 */
public class StubUEBDI implements IUEBDI {

	
	private StubUEBDI()
	{
		
	}
	
	private static IUEBDI instance = null;

	static
	{
		instance = new StubUEBDI();
	}
	
	public static IUEBDI getInstance()
	{
		return instance;
	}


	@Override
	public void taggingBelief(Literal beliefAdd, IEmotionBase emotionBase) {
	
	}




	@Override
	public void firstReviewEmotion(List<Literal> listPerc,
			IEmotionBase emotionBase, IIntentionBase intentionBase) {		
	}


	@Override
	public void secondReviewEmotion(BeliefBase beliefBase,
			Queue<Event> events, IEmotionBase emotionBase, 
			IIntentionBase intentionBase) {
		
	}


	
	@Override
	public IUEBDI clone()
	{
		return StubUEBDI.getInstance();
	}


	@Override
	public void perception(List<Literal> listSensing, IEmotionBase emotionBase,
			BeliefBase beliefBase, IIntentionBase intentionBase) {
		//faz nada
		
	}






	@Override
	public DriveState updateDrive(Drive drive, BeliefBase beliefFase,
			IEmotionBase emotionBase, IIntentionBase intentionBase) {
		return DriveState.inactive;
	}


	@Override
	public int compareGoals(Event a, Event b, BeliefBase beliefBase,
			IEmotionBase emotionBase, IIntentionBase intentionBase) {
		return 0;
	}




	@Override
	public Event priorityDesire(Queue<Event> listEvent,
			BeliefBase beliefFase, IEmotionBase emotionBase,
			IIntentionBase intentionBase) {
		return listEvent.poll();
	}


	@Override
	public Option selectPlan(List<Option> listOp, BeliefBase beliefBase,
			IEmotionBase emotionBase) {
		  if (listOp != null && !listOp.isEmpty()) {
		      return listOp.remove(0);
		  } else {
		      return null;
		  }
	}




	@Override
	public boolean forgetBelief(Literal belief) {
		return false;
	}


	@Override
	public Intention selectIntention(Queue<Intention> queueIntention,
			IEmotionBase eb, BeliefBase bb) {
		return queueIntention.poll();
	}



	@Override
	public void setLoggerData(String name, Level level) {
	
	}


	@Override
	public boolean criticalContext(IEmotionBase eb, BeliefBase bb) {
		return true;
	}


	@Override
	public void updateBeliefs(List<Literal> percepts, BeliefBase bb,
			IEmotionBase eb, IIntentionBase ib) {
		
	}


	@Override
	public void perceptionResourceFilter(BeliefBase beliefBase,
			List<Literal> listSensing) {
		
	}


	@Override
	public void perceptionRulesFilter(BeliefBase beliefBase,
			List<Literal> listSensing) {
		
	}


	@Override
	public void perceptionEmotionFilter(IEmotionBase emotionBase,
			List<Literal> listSensing) {
		
	}


	@Override
	public void initCycle(int nCycle, Object... params) {
		
	}


	@Override
	public void endCycle(int nCycle) {
		
	}

}
