package br.uff.ic.uebdi;

import java.util.Iterator;

import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.LogicalFormula;
import jason.bb.BeliefBase;

/**
 * Esta interface permite fazer consulta a uma base usando uma fórmula lógica.
 * Isto é uma "marretada que vou dar para facilitar minhas consultas na UEBDI.
 * @author Marcos
 *
 */
public interface SmartBelieves extends BeliefBase {
	
	public boolean believes(LogicalFormula bel, Unifier un);
	
	public Iterator<Literal> believesIt(Literal bel, Unifier un);
}
