package br.uff.ic.uebdi;

import jason.asSemantics.Agent;

/**
 * object need reference to agent
 * @author Marcos
 *
 */
public interface ISetAgent {
	
	void setAgent(Agent ag);

}
