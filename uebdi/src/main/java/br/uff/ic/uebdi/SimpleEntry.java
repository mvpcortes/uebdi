package br.uff.ic.uebdi;

import java.util.Map.Entry;

public final class SimpleEntry<T, E> implements Entry<T, E> {

	private T t;
	private E e;
	@Override
	public T getKey() {
		return t;
	}

	@Override
	public E getValue() {
		return e;
	}

	@Override
	public E setValue(E arg0) {
		E temp = e;
		e = arg0;
		return temp;
	}	
	
	public SimpleEntry(T _t, E _e){
		t = _t;
		e = _e;
	}

}
