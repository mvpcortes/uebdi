package br.uff.ic.uebdi;


import jason.asSemantics.Event;
import jason.asSemantics.Intention;
import jason.asSemantics.Option;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

import java.util.List;
import java.util.Queue;
import java.util.logging.Level;

import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.exceptions.UEBDIException;
/**
 * UEBDI Agent Interface
 * It defines the basic methods of architecture.
 * @author Marcos
 *
 */
public interface IUEBDI extends Cloneable, IEmotionDyn
{	
	/**
	 * Informa início de novo ciclo
	 */
	public void initCycle(int nCycle, Object... params);
	
	
	/**
	 * Informa fim do ciclo
	 * @param nCycle
	 */
	public void endCycle(int nCycle);
	
	/**
	 * The Perception Function
	 * @param listSensing List of sensing itens. This list must be updated.
	 * @param emotionBase Emotion Base of agent
	 * @param beliefBase The belief base to get the rules of Rules Filter
	 * @param intentionBase Intention Base of Agent
	 */
	void perception(List<Literal> listSensing, 
			IEmotionBase emotionBase, 
			BeliefBase beliefBase, 
			IIntentionBase intentionBase);
	
	/**
	 * Filter of sensing items by resources
	 * @param beliefBase the Belief Base
	 * @param listSensing list of Sensing
	 */
	void perceptionResourceFilter(BeliefBase beliefBase, List<Literal> listSensing);
	
	/**
	 * Filter of sensing items by Rules
	 * @param beliefBase the Belief Base
	 * @param listSensing list of Sensing
	 */
	void perceptionRulesFilter(BeliefBase beliefBase, List<Literal> listSensing);
	
	/**
	 * Filter of sensing items by Emotion
	 * @param emotionBae the Emotion Base
	 * @param listSensing list of Sensing
	 */
	void perceptionEmotionFilter(IEmotionBase emotionBase, List<Literal> listSensing);
	
	
	
	/////Belief Review Function
	/////
	/////
	
	
	
	/**
	 * It tags inserted beliefs.
	 * Consider that the tag is a annotation of belief.
	 * @param beliefAdd the inserted belief.
	 * @param emotionBase the Emotion Base
	 * @throws UEBDIException 
	 */
	void taggingBelief(Literal beliefAdd, IEmotionBase emotionBase) throws UEBDIException;
	
	/**
	 * Do fogetting belief operation
	 * Execute in abstract brf 
	 * @param belief
	 * @return true if should remove belief
	 */
	boolean forgetBelief(Literal belief);
	
	////Option Function
	/**
	 * It Updates drive values
	 * Obs. isto precisaria tamb�m acesso a base de drives. Mas como drives tamb�m s�o cren�as desconsiderei
	 * @param drive the drive to update
	 * @param beliefBase the belief base usage
	 * @param emotionBase the emotion base usage
	 */
	DriveState updateDrive(Drive drive, BeliefBase beliefBase, IEmotionBase emotionBase, IIntentionBase intentionBase);
	
	/**
	 * Priority desires with its importance relative to emotional state.<br/>
	 * A ordem de prioridade desta fun��o �:
	 * 1� processar eventos de percep��o (ainda vendo como fazer isto
	 * 2� processar eventos de inclus�o e exclus�o de emo��es
	 * 3� processar teste de desejos
	 * 4� processar desejos de acordo com as emo��es, inten��es e cren�as
	 * 
	 * @param listEvent the list of events in the system
	 * @param beliefFase the belief base
	 * @param emotionBase the emotion base
	 * @param intentionBase the intention base
	 * @return the more priority event.
	*/
	Event priorityDesire(Queue<Event> listEvent,
			BeliefBase beliefFase,
			IEmotionBase emotionBase,
			IIntentionBase intentionBase);
	 
	
	/** This method compare two events/goals about its priorities
	 * @param a the first event
	 * @param b the second event
	 * @return the diferrence of two items
	 * @deprecated isto deveria ser dependente de dom�nio e estar em UEBDIImpl
	 */
	@Deprecated
	int compareGoals(Event a, Event b, BeliefBase beliefBase, IEmotionBase emotionBase, IIntentionBase intentionBase);
	
	////Filter Function
	////
	////	
	/**
	 * Selects an option to a desire
	 * @param listOp list of options
	 * @param smBase base of SomaticMark 
	 */
	Option selectPlan(List<Option> listOp, BeliefBase beliefBase, IEmotionBase emotionBase);

	
	/**
	 * Select de intention of the cicle
	 *
	 * @return
	 */
	Intention selectIntention(Queue<Intention> queueIntention, IEmotionBase eb, BeliefBase bb);
	
	//// Other methods
	////
	////
	IUEBDI clone();
	
	/**
	 * 
	 */
	void setLoggerData(String name, Level level);

	/**
	 * Update the beliefs with perception data, intentions and emotions.
	 * @param percepts
	 * @param bb
	 * @param eb
	 * @param ib
	 */
	void updateBeliefs(List<Literal> percepts, BeliefBase bb, IEmotionBase eb,
			IIntentionBase ib);


	
}
