package br.uff.ic.uebdi;
import jason.asSyntax.Literal;
import java.util.Iterator;
import br.uff.ic.uebdi.WrapperJasonUEBDI;


/**
 * Interface of Emotion Base
 * @author Marcos
 *
 */
public interface IEmotionBase {
	
	
	/**
	 * A emotion is base-dependent. Because it may be the base the validator of emotions
	 * @param emotion
	 * @return
	 */
	public boolean isValidEmotion(Literal emotion);
	/**
	 * Get All emotions that match with query 
	 * @param query A literal to match. Ex. emotion(x)
	 * @return a iterator to a sequence of literals.
	 * @see {@link IEmotionBase#get(String...)}, {@link IEmotionBase#getAll(Literal)} 
	 */
	public Iterator<Literal> getAll(Literal query);
	
	/**
	 * Get All emotions that match with query 
	 * @param query a array of string parameters that identify the emotion
	 * @return a iterator to a sequence of literals.
	 * @seealso {@link IEmotionBase#get(Literal)}, {@link IEmotionBase#getAll(Literal)}
	 */
	public Iterator<Literal> getAll(String... query);
	
	/**
	 * Get All emotions in the EmotionBase
	 * @return all emotions in the EmotionBase
	 */
	public Iterator<Literal> getAllEmotions();
	
	/**
	 * return the first emotion what match with query
	 * @param query
	 * @return
	 * * @seealso {@link IEmotionBase#get(Literal)}, {@link IEmotionBase#getAll(Literal)}
	 */
	public Literal get(Literal query);
	
	
	/**
	 * return the intensity of a emotion. It will query the base.
	 * @param query
	 * @return the intensity of emotion 
	 */
	public double getIntensity(Literal query);

	/**
	 * return the intensity of a emotion. It will query the base.
	 * @param query A array of string to query
	 * @return the intensity of emotion 
	 */
	public double getIntensity(String... query);
	
	/**
	 * Extract the intensity from the literal emotion. It will not query the base.
	 */
	public double extractIntensity(Literal emotion);
	/**
	 * Add a new emotion
	 * @param emotion The emotion identifier.
	 * @return true if could add the new emotion
	 */
	public boolean add(Literal emotion);
	
	/**
	 * Add a new emotion
	 * @param intensity the intensity of this emotion
	 * @param params the params of this emotion
	 * @return true if could add the new emotion
	 */
	public boolean add(double intensity, String... params);
	
	/**
	 * change the intensity of a emotion
	 * @param newIntensity the new intensity
	 * @param params the indentifier of emotion
	 * @return the new value of intensity
	 * @see {@link IEmotionBase#incIntensity(double, String...)}
	 */
	public double changeIntensity(double newIntensity, String...params);
	
	/**
	 * 
	 * @param incValue the value to be added
	 * @param params params the indentifier of emotion
	 * @return the new value of intensity
	 * @see {@link IEmotionBase#incIntensity(double, String...)}
	 */
	public double incIntensity(double incValue, String...params);
	
	/**
	 * If the Intensity there is, inc the intensity using incValue. Else, create a new emotion with startValue.  
	 * @param incValue the value to be added
	 * @param startValue the value to start emotion
	 * @param params params the indentifier of emotion
	 * @return the new value of intensity
	 * @see {@link IEmotionBase#incIntensity(double, String...)}
	 */
	public double incIntensity(double incValue, double startValue, String...params);
	
	/**
	 * retorna a intensidade m�xima relacionada a uma emo��o 
	 * @return >0 se existe pelo menos umaa emo��o e -1 caso contr�rio
	 */
	public double getMaxIntensity();
	
	/**
	 * delete the emotion with the pattern emo
	 * @param emo
	 * @return the number of removed emotions
	 */
	public int delete(Literal emo);
	
	/**
	 * delete the emotion with the params
	 * @param params
	 * @return the number of removed emotions
	 */
	public int delete(String... params);
	
	/**
	 * Turn to the initial emotionstate
	 */	
	public void resetEmotion();

	/**
	 * Init the base with the agent information
	 * @param w
	 */
	void initBase(WrapperJasonUEBDI w);

	/**
	 * Clone the base
	 * @param wrapperJasonUEBDI
	 * @return
	 */
	public IEmotionBase clone(WrapperJasonUEBDI wrapperJasonUEBDI);

	public boolean emotionEquals(Literal a, Literal b);
}
