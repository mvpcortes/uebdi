package br.uff.ic.uebdi;

import java.util.Collection;
import java.util.Iterator;

import br.uff.ic.util.SubIterator;
import br.uff.ic.util.SubIteratorCondition;

import jason.asSemantics.Event;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.Trigger;
import jason.bb.BeliefBase;

/**
 * Util functions to manipulate data in Emotional Dynamic.
 * @author Marcos
 *
 */
public final class DynHelper {
    
	/**
	 * Does a query in a collection of events look up to a specific operation and functor.<br/>
	 * It helps do infer desires 
	 * @param collEvent
	 * @param op
	 * @param strFunctor
	 * @return
	 */
    public static Literal queryEvent(Collection<Event> collEvent, Trigger.TEOperator op, String strFunctor)
    {
		for(Event e: collEvent)
		{
		    if(op.equals(e.getTrigger().getOperator()))
		    {
			if(strFunctor.equals(e.getTrigger().getFunctor()))
			{
			    return e.getTrigger();
			}
		    }
		}
		return null;
    }
    
    /**
	 * Return a interator with all events with the {@link #op} operator and {@link #strFunc} functor.
	 * It helps do infer desires 
	 * @param collEvent
	 * @param op
	 * @param strFunctor
	 * @return
	 */
    public static Iterator<Event> getItEventQuery(Iterator<Event> itEvent, final Trigger.TEOperator op, final String strFunctor)
    {
    	SubIteratorCondition<Event> sic = new SubIteratorCondition<Event>() 
    			{
    				final String targetStr = strFunctor;
					@Override
					public boolean isIt(Event e) {
						if(op.equals(e.getTrigger().getOperator()))
						{
							String other = e.getTrigger().getLiteral().getFunctor();
							if(targetStr.equals(other))
							{
								Term al = e.getTrigger().getAnnots(MARK_EVENT);
							    if(!MARK_EVENT.equals(al.toString()))//evita marca��o
							    {
							    	return true;
							    }
							}
						}
						return false;
					}
    			};
    	return new SubIterator<Event>(itEvent, sic);
    }
    
    
    /**
     * identify the functor of anotation used to mark has processed events. 
     */
    private static String MARK_EVENT = String.format("mark_event");
    
    /**
     * Mark a event
     * @param e
     */
    public static void markEvent(Event e)
    {
    	e.getTrigger().addAnnot(Literal.parseLiteral(MARK_EVENT));
    }

    /**
     * create a string representing a Belief Base.
     * @param bb
     * @return
     */
	public static String bbToString(BeliefBase bb) {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		for(Literal l : bb)
		{
			sb.append("\t");
			sb.append(l.toString());
			sb.append(",\n ");
		}
		sb.append("}");
		return sb.toString();
	}
}
