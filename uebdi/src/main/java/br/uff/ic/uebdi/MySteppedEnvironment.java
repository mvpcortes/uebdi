package br.uff.ic.uebdi;

import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.environment.Environment;

public class MySteppedEnvironment extends Environment {

	private Queue<Entry<String, Literal>> listWaitingPercepts = new LinkedList<Entry<String, Literal>>();
	private Queue<String> listWaitingKillAgents = new LinkedList<String>();
	
	private static String FUNCTOR_STEP = "step";

	private Integer qtdAgentWaiting = 0; // USado tamb�m como se��o cr�tica

	private int actualStep = 0;

	public int getActualStep() {
		return actualStep;
	}

	public void setActualStep(int actualStep) {
		this.actualStep = actualStep;
	}

	public void incActualStep() {
		actualStep++;
	}

	public void addWaitingPercept(Literal l) {
		listWaitingPercepts.add(new SimpleEntry<String, Literal>(null, l));
	}

	public void addWaitingPercept(String agName, Literal l) {
		listWaitingPercepts.add(new SimpleEntry<String, Literal>(agName, l));
	}

	public void addAgentToKill(String agName) {
		listWaitingKillAgents.add(agName);
	}

	@Override
	public void scheduleAction(String agName, Structure action, Object infraData) {
		if (!isRunning())
			return;

		synchronized (qtdAgentWaiting) {
			if (qtdAgentWaiting == getAgentCount() - 1) {
				doNextStep();
			} else {
				try {
					qtdAgentWaiting++;
					super.scheduleAction(agName, action, infraData);
					qtdAgentWaiting.wait();
				} catch (InterruptedException e) {
					getLogger().severe("Problem in waiting mechanism.");
					doNextStep();
				}
			}
		}
	}

	private void doNextStep() {
		finishStep(getActualStep());
		qtdAgentWaiting = 0;
		qtdAgentWaiting.notifyAll();
		incActualStep();
		updateWatingPerceptions();
		doKillAgents();
		startStep(getActualStep());
	}

	private int getAgentCount() {
		return getEnvironmentInfraTier().getRuntimeServices().getAgentsQty();
	}

	protected void updateWatingPerceptions() {
		super.clearAllPercepts();
		
		//adiciona step
		Literal litStep = ASSyntax.createLiteral(FUNCTOR_STEP, ASSyntax.createNumber(getActualStep()));
		addPercept(litStep);
		synchronized (qtdAgentWaiting) {
			Entry<String, Literal> headEntry = null;
			while ((headEntry = listWaitingPercepts.poll()) != null) {
				if (headEntry.getKey() == null) {
					addPercept(headEntry.getValue());
				} else {
					addPercept(headEntry.getKey(), headEntry.getValue());
				}
			}
		}
	}

	private void doKillAgents() {

		synchronized (qtdAgentWaiting) {

			String agName = null;
			while ((agName = listWaitingKillAgents.poll()) != null) {
				getEnvironmentInfraTier().getRuntimeServices()
						.killAgent(agName);
			}
		}
	}
	
	protected void startStep(final int step){
		
	}
	
	protected void finishStep(final int step){
		
	}
	
	
}
