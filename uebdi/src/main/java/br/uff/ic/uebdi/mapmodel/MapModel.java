package br.uff.ic.uebdi.mapmodel;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import br.uff.ic.uebdi.mapmodel.entities.Agent;
import br.uff.ic.uebdi.mapmodel.entities.Entity;
import br.uff.ic.uebdi.mapmodel.entities.Wall;
import br.uff.ic.util.EmptyIterator;
import br.uff.ic.util.MultiMatrix;
import br.uff.ic.util.SubIteratorCondition;


public class MapModel {
	public static final int RANGE_SEE = 6;
	public static Point[] AREA_SEE = minusPoints(
			generateRoundPoints(RANGE_SEE), generateRoundPoints(0));

	public static final int INITIAL_STR = 100;
	public static final int MOVING_COST = 1;

	private static Logger logger;

	// ======================================================================
	private Map<String, Agent> mapAgent = new HashMap<String, Agent>();

	private MultiMatrix<Entity> mmEntity;

	private boolean init = false;
	
	private Random random = new Random();

	/**
	 * Tempo inicial. Em milisegundos.
	 */
	private long initTime;

	private long finishTime;
	
	private boolean timeOver = false; 

	private static Logger getLogger() {
		return logger;
	}

	public void setTimeOver(boolean ok){
		timeOver = ok;
	}
	
	public boolean getTimeOver(){
		return timeOver;
	}
	
	public MapModel(int size){
		super();
		logger = Logger.getLogger(getClass().getName());
		mmEntity = new MultiMatrix<Entity>(size, size);
		initTime = System.currentTimeMillis();

	}
	
	
	/**
	 * generate a vector of relative points around a point
	 * 
	 * @param range
	 * @return
	 */
	public static Point[] generateRoundPoints(int range) {

		if (range < 0)
			return new Point[0];

		if (range == 0) {
			return new Point[] { new Point(0, 0) };
		}

		List<Point> list = new LinkedList<Point>();
		for (int x = -range; x <= +range; x++) {
			for (int y = -range; y <= +range; y++) {
				if (x * x + y * y - range * range <= 0)
					list.add(new Point(x, y));
			}
		}
		return list.toArray(new Point[list.size()]);
	}
	

	/**
	 * generate a vector of relative points around a point
	 * 
	 * @param range
	 * @return
	 */
	public static Point[] generateRoundPoints(int outRange, int innerRange) {

		if ((outRange - innerRange < 0) || (outRange < 1)) {
			return new Point[0];

		}

		if (outRange - innerRange == 0) {
			return new Point[] { new Point(0, 0) };
		}

		List<Point> list = new LinkedList<Point>();
		for (int x = -outRange; x <= +outRange; x++) {
			for (int y = -outRange; y <= +outRange; y++) {
				if ((x * x + y * y - outRange * outRange <= 0)
						&& (x * x + y * y - innerRange * innerRange > 0))
					list.add(new Point(x, y));
			}
		}
		return list.toArray(new Point[list.size()]);
	}

	
	public static Point[] minusPoints(Point[] all, Point[] minus) {
		Set<Point> setMinus = new HashSet<Point>();
		for (Point p : minus) {
			setMinus.add(p);
		}

		List<Point> l = new LinkedList<Point>();
		for (Point p : all) {
			if (!setMinus.contains(p)) {
				l.add(p);
			}
		}
		return l.toArray(new Point[l.size()]);
	}



	public synchronized Agent getAgent(int x, int y) {
		Iterator<Entity> it = mmEntity.find(x, y,
				new SubIteratorCondition<Entity>() {
					@Override
					public boolean isIt(Entity e) {
						return e instanceof Agent;

					}
				});
		
		if (it.hasNext())
			return (Agent) it.next();
		else
			return null;
	}

	/**
	 * 
	 * @param negCond
	 *            condicao de nega��o. Isto �, se para algum elemento ela
	 *            retornar true, n�o se deve selecionar um c�lula randomica.
	 * @return
	 */
	private synchronized Point getRandomPos(SubIteratorCondition<Entity> negCond) {
		int x;
		int y;
		final int max = mmEntity.getWidth() * mmEntity.getHeight() * 5;
		int loop = 0;
		do {
			x = random.nextInt(mmEntity.getWidth());
			y = random.nextInt(mmEntity.getHeight());
			Iterator<Entity> it = mmEntity.find(x, y, negCond);
			if (!it.hasNext())
				break;
			
			loop++;
		} while (loop < max);

		if (loop < max) {
			getLogger()
					.fine(String.format("generateRandomPos: (%d, %d)", x, y));
			return new Point(x, y);
		} else {
			for (int i = 0; i < mmEntity.getWidth(); i++)
				for (int j = 0; j < mmEntity.getHeight(); j++) {
					Iterator<Entity> it = mmEntity.find(i, j, negCond);
					if (!it.hasNext()) {
						getLogger().fine(
								String.format("generateRandomPos: (%d, %d)", x,
										y));
						return new Point(i, j);
					}
				}
		}

		getLogger().warning("Cannot generate random pos");
		return null;
	}

	final static SubIteratorCondition<Entity> collisionSIC = new SubIteratorCondition<Entity>() {
		public boolean isIt(Entity e) {
			return e.isColised();
		}
	};

	public synchronized boolean move(String strAg, Direction dir) {
		Agent ag = mapAgent.get(strAg);
		if (ag == null) {
			getLogger().warning(
					String.format("Cannot move because cannot found agent %s",
							strAg));
			return false;
		}

		final Point oldPos = ag.getPoint();

		int nx = oldPos.x;
		int ny = oldPos.y;
		final Point relP = dir.getRelativePos();
		nx += relP.x;
		ny += relP.y;

		if (!validPos(nx, ny)) {
			getLogger()
					.warning(
							String.format(
									"Agent %s cannot move to (%s, %s) because is an invalid position.",
									strAg, nx, ny));
			return false;
		}

		Iterator<Entity> itE = collideIterator(nx, ny);
		if (!itE.hasNext()) {
			mmEntity.remove(oldPos.x, oldPos.y, ag);
			mmEntity.add(nx, ny, ag);
			ag.setPos(nx, ny);
			getLogger().fine(
					String.format("Agent %s moved to %d, %d", strAg, nx, ny));
			return true;
		} else {
			Entity e = itE.next();
			getLogger()
					.info(String
							.format("Agent %s cannot move because there is a entity %s in pos(%d,%d)",
									strAg, e.toString(), nx, ny));
			return false;

		}
	}

	private boolean validPos(int nx, int ny) {
		return !(nx < 0 || ny < 0 || nx >= mmEntity.getWidth() || ny >= mmEntity
				.getHeight());

	}

	public boolean collideTo(final int x, final int y){
		Iterator<Entity> it = collideIterator(x, y);
		return it.hasNext();
	}
	
	public Iterator<Entity> collideIterator(final int x, final int y) {
		if (mmEntity.isValidPos(x, y)) {
			Iterator<Entity> it = mmEntity.find(x, y, collisionSIC);
			return it;
		} else {
			return new Iterator<Entity>() {
				private boolean found = false;

				@Override
				public boolean hasNext() {
					return !found;
				}

				@Override
				public Entity next() {
					found = true;
					return new Wall(new Point(x, y));
				}

				@Override
				public void remove() {
				}
			};
		}
	}

	public synchronized boolean move(String strAg, int x, int y) {
		Agent ag = mapAgent.get(strAg);
		if (ag == null) {
			getLogger().warning(
					String.format("Cannot move because cannot found agent %s",
							strAg));
			return false;
		}

		Point oldPos = ag.getPoint();

		// tra�a
		double vecX = x - oldPos.x;
		double vecY = y - oldPos.y;

		// normaliza
		double size = Math.sqrt(Math.pow(vecX, 2) + Math.pow(vecY, 2));
		vecX = vecX / size;
		vecY = vecY / size;
		int dirX = (int) Math.round(vecX);
		int dirY = (int) Math.round(vecY);

		Direction[] dirPossible = new Direction[4];
		// tra�ar plano
		if (dirX < 0) {
			if (dirY < 0) {
				dirPossible[0] = Direction.left;
				dirPossible[1] = Direction.top;
				dirPossible[2] = Direction.right;
				dirPossible[3] = Direction.bottom;
			} else if (dirY == 0) {
				dirPossible[0] = Direction.left;
				dirPossible[1] = Direction.top;
				dirPossible[2] = Direction.bottom;
				dirPossible[3] = Direction.right;

			} else {
				dirPossible[0] = Direction.left;
				dirPossible[1] = Direction.bottom;
				dirPossible[2] = Direction.right;
				dirPossible[3] = Direction.top;
			}
		} else if (dirX == 0) {
			if (dirY < 0) {
				dirPossible[0] = Direction.top;
				dirPossible[1] = Direction.left;
				dirPossible[2] = Direction.right;
				dirPossible[3] = Direction.bottom;
			} else if (dirY == 0) {
				dirPossible[0] = Direction.none;
				dirPossible[1] = Direction.none;
				dirPossible[2] = Direction.none;
				dirPossible[3] = Direction.none;
			} else {
				dirPossible[0] = Direction.bottom;
				dirPossible[1] = Direction.left;
				dirPossible[2] = Direction.right;
				dirPossible[3] = Direction.top;
			}
		} else {
			if (dirY < 0) {
				dirPossible[0] = Direction.right;
				dirPossible[1] = Direction.top;
				dirPossible[2] = Direction.left;
				dirPossible[3] = Direction.bottom;

			} else if (dirY == 0) {
				dirPossible[0] = Direction.right;
				dirPossible[1] = Direction.top;
				dirPossible[2] = Direction.bottom;
				dirPossible[3] = Direction.left;
			} else {
				dirPossible[0] = Direction.right;
				dirPossible[1] = Direction.bottom;
				dirPossible[2] = Direction.left;
				dirPossible[3] = Direction.top;
			}
		}

		if (dirPossible[0] == Direction.none) {
			getLogger().warning(
					String.format("Agent %s cannot move to the same position",
							strAg));
			return false;
		}
		boolean moved = false;
		for (Direction dir : dirPossible) {
			if (move(strAg, dir)) {
				moved = true;
				break;
			}
		}

		if (!moved) {
			getLogger().warning(String.format("Agent %s cannot move to dir pos(%d, %d)", strAg, dirX, dirY));
		}
		return moved;
	}

	/*
	 * public synchronized boolean randomMove(String strag) { Agent ag =
	 * mapAgent.get(strag); if(ag==null) { getLogger().warning(String.format(
	 * "Cannot randomMove because cannot found agent %s", strag)); return false;
	 * }
	 * 
	 * Point pos = ag.getPoint(); List<Point> possibleList = new
	 * ArrayList<Point>(AREA_SEE.length); for(Point p: AREA_SEE) { int newx =
	 * p.x + pos.x; int newy = p.y + pos.y; if(mmEntity.isValidPos(newx, newy)
	 * && getAgent(newx, newy)==null) { possibleList.add(p); } }
	 * if(possibleList.size() == 0) return false; else { Point p =
	 * possibleList.get(random.nextInt(possibleList.size())); int newx = p.x +
	 * pos.x; int newy = p.y + pos.y; ag.setPos(newx, newy);
	 * mmEntity.remove(pos.x, pos.y, ag); mmEntity.add(newx, newy, ag);
	 * getLogger().fine(String.format("randomMove agent %s", strag)); return
	 * true; } }
	 */

	public synchronized Iterator<Entity> getAgSee(String strag) {
		return getAgArea(strag, AREA_SEE);
	}

	private synchronized Iterator<Entity> getAgArea(String strag, Point[] area) {
		final Agent ag = mapAgent.get(strag);
		if (ag == null) {
			return new EmptyIterator<Entity>();
		}

		final int sX = ag.getPoint().x;
		final int sY = ag.getPoint().y;

		ArrayList<Entity> al = new ArrayList<Entity>(area.length);
		for (Point p : area) {
			final int x = sX + p.x;
			final int y = sY + p.y;
			if (mmEntity.isValidPos(x, y)) {
				Iterator<Entity> it = mmEntity.getContents(x, y);
				while (it.hasNext()) {
					Entity e = it.next();
					if (!e.equals(ag))
						al.add(e);
				}
			} else {
				al.add(getRoundWall(x, y));
			}
		}
		return al.iterator();
	}

	private Map<Point, Wall> mapRoundWall = new HashMap<Point, Wall>();

	private Entity getRoundWall(int x, int y) {

		Point p = new Point(x, y);
		Wall w = mapRoundWall.get(p);
		if (w == null) {
			w = new Wall(p);
			mapRoundWall.put(p, w);
		}
		return w;
	}

	public synchronized boolean registerAgent(Agent a) {
		
		Point p = getAgPos(a.getName());
		if (p != null) {
			getLogger()
					.warning(
							String.format(
									"Cannot register agent %s because agent already exist",
									a.getName()));
			return false;
		}
		
		final int x = a.getPoint().x;
		final int y = a.getPoint().y;
		if (a.isColised() && collideTo(x, y)) {
			getLogger()
					.warning(
							String.format(
									"Cannot register agent %s in pos(%d, %d) because already a agente here",
									a.getName(), x, y));
			return false;
		}
		
		mapAgent.put(a.getName(), a);
		mmEntity.add(x, y, a);
		getLogger().fine(
				String.format("register agent %s in pos(%d, %d)", a.getName(), x, y));
		return true;
	}

	

	public synchronized Point registerAgentRandomPos(Agent a) {
		Point p = getAgPos(a.getName());
		if (p != null) {
			getLogger()
					.warning(
							String.format(
									"Cannot register agent %s in randompos because agent already exist",
									a.getName()));
			return null;
		}

		if(a.isColised()){
			p = getRandomPos(new SubIteratorCondition<Entity>() {
	
				@Override
				public boolean isIt(Entity e) {
					return e.isColised();
				}
			});
		}else{
			p = getRandomPos(new SubIteratorCondition<Entity>() {
				
				@Override
				public boolean isIt(Entity e) {
					return false;
				}
			});
		}
		if (p != null) {
			a.setPoint(p);
			if (registerAgent(a))
				return p;

			getLogger()
					.fine(String
							.format("register agent %s in pos(%d, %d) with registerAgentRandomPos",
									a.getName(), p.x, p.y));
		}

		getLogger()
				.severe(String
						.format("cannot register Agent %s because cannot found empty cell",
								a.getName()));
		return null;
	}

	public synchronized Agent getAgent(String name){
		Agent ag = mapAgent.get(name);
		return ag;
	}
	
	public synchronized Point getAgPos(String name) {
		Agent ag = mapAgent.get(name);
		if (ag != null) {
			return (Point) ag.getPoint().clone();
		}

		getLogger().warning(
				String.format("agent %s does not registed: call in %s", name,
						"getAgPos"));
		return null;
	}

	public synchronized Iterator<Entity> getEntities(int x, int y) {
		return mmEntity.getContents(x, y);
	}
	
	public Entity getEntity(int x, int y, Class clazz) {
		
		Iterator<Entity> it = getEntities(x, y);
		while(it.hasNext()){
			Entity e = it.next();
			if(clazz.isInstance(e)){
				return e;
			}
		}
		
		return null;
	}

	public synchronized int getWidth() {
		return mmEntity.getWidth();
	}

	public synchronized int getHeight() {
		return mmEntity.getHeight();
	}



	public boolean agentSeeThisPoint(String name, int x, int y) {
		Agent a = (Agent) mapAgent.get(name);
		if (a == null) {
			getLogger()
					.severe(String.format("Cannot found the agent %s", name));
			return false;
		}
		double dist = Math.sqrt(Math.pow(x - a.getPoint().x, 2)
				+ Math.pow(y - a.getPoint().y, 2));

		return dist < RANGE_SEE;

	}

	public boolean isDied(String name) {
		Agent a = mapAgent.get(name);
		if (a != null)
			return a.isDied();
		else
			return false;
	}

	public Iterable<Agent> getAgents() {
		return mapAgent.values();
	}

	
	public long getInitTime() {
		return initTime;
	}

	public void finishSimulation() {
		finishTime = System.currentTimeMillis();
	}

	public long getTotalSimulationTime() {
		return finishTime - initTime;
	}

	public Iterator<Entity> getEntities() {
		
		final MultiMatrix<Entity> multiMatrixEntities = mmEntity;
		if(multiMatrixEntities.getHeight() == 0 || multiMatrixEntities.getWidth() == 0){
			return new Iterator<Entity>() {
				@Override public boolean hasNext() {return false;}

				@Override public Entity next() {return null;}

				@Override public void remove() {}
			};
		}
		return new Iterator<Entity>(){

			int x=0, y=0;
			Iterator<Entity> itCurrentCell = multiMatrixEntities.getContents(0, 0);
			@Override
			public boolean hasNext() {
				return y < multiMatrixEntities.getHeight();
			}

			@Override
			public Entity next() {
				if(itCurrentCell.hasNext()){
					return itCurrentCell.next();
				}else if(x < multiMatrixEntities.getWidth()-1){
					x++;
					itCurrentCell = multiMatrixEntities.getContents(x, y);
					return next();
				}else if(y < multiMatrixEntities.getHeight()-1){
					y++;
					x=0;
					itCurrentCell = multiMatrixEntities.getContents(x,y);
					return next();
				}else{
					return null;
				}
			}

			@Override
			public void remove() {
			}
		};
		
	}

	protected void addEntity(Entity e, int x, int y){
		mmEntity.add(x, y, e);
	}
	
	public boolean isValidPos(int x, int y){
		return mmEntity.isValidPos(x, y);
	}

	public int getRegisteredAgentCount() {
		return mapAgent.size();
	}

	public double getDiagonalSize() {
		//define a diagonal
		return Math.sqrt(this.getWidth()*this.getWidth()+ this.getHeight()*this.getHeight());
	}
	
	
	
	public boolean isInit() {
		return init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}

}
