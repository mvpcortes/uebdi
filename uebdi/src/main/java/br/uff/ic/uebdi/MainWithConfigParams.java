package br.uff.ic.uebdi;

import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import br.uff.ic.util.TermHelper;
import jason.JasonException;
import jason.asSyntax.Literal;
import jason.asSyntax.parser.as2j;
import jason.infra.centralised.RunCentralisedMAS;

/**
 * Arquivo main guarda informa��es de par�metros de entrada.
 * Isto servir� para me facilitar nos testes
 * @author Marcos
 *
 */
public class MainWithConfigParams {
	private static RunCentralisedMAS runner;
	
	//private static BeliefBase paramBB;
	private static Set<Literal> setParams = new HashSet<Literal>();
	
	public static void main(String[] args) throws JasonException{
		parseParams(args);
		runner = new RunCentralisedMAS();
        runner.init(args);
        runner.create();
        runner.start();
        runner.waitEnd();
        runner.finish();
	}
	
	private static Literal parseLiteral(String s){
		try {
            //as2j parser = new as2j(new StringReader(s));
            //return parser.literal();
			//System.out.printf("try parse \"%s\" %n", s);
			return Literal.parseLiteral(s);
        } catch (Exception e) {
            return null;
        }
	}

	protected static void parseParams(String[] args) {
		for(String s: args){
				s = removerAspas(s);
				Literal lit = parseLiteral(s);
				if(lit != null){
					setParams.add(lit);
				}
			
		}
	}
	
	private static String removerAspas(String s) {
		if(s.charAt(0) == '\"' && s.charAt(s.length()-1) == '\"'){
			return s.substring(1, s.length()-2);
		}
		return s;
	}

	public static Literal query(Literal query){
		Literal found = TermHelper.contain(setParams, query);
		return found;
	}
	
	public static Literal query(String strQuery){
		Literal query = Literal.parseLiteral(strQuery);
		return query(query);
	}
	
	public static double queryDouble(String strQuery){
		Literal lit = query(strQuery);
		
		if(lit != null)
			return TermHelper.getDouble(lit, 0);
		return -1.0;
	}
}
