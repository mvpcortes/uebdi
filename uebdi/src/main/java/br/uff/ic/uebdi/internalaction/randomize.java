package br.uff.ic.uebdi.internalaction;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Term;

public class randomize extends DefaultInternalAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6235769891738764206L;


	@Override
	    public int getMinArgs(){return 2;}
	    
	    @Override
	    public int getMaxArgs(){return getMinArgs();}
	    
	    @Override
	    public void checkArguments(Term[] terms)throws JasonException 
	    {
	    	Term tList= terms[0]; 
	    	if(!tList.isList())
	    	{
	    		throw JasonException.createWrongArgument(this,"the term is not a list");
	    	}
	    	 
	    	if(!terms[1].isVar())
	    	{
	    		throw JasonException.createWrongArgument(this,"the term is not a var");
	    	}
	    }
	    
	    
	    @Override
	    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception 
	    {
	        // execute the internal action
	        ts.getAg().getLogger().fine("executing internal action 'br.uff.ic.uebdi.randomize'");
	        
	        ListTerm lt = (ListTerm)args[0];
	        
	        int size = lt.size();
	        Term[] terms = new Term[size];
	        terms = lt.toArray(terms);
	      
	        suffle(terms);
	        ListTerm all = new ListTermImpl();
			ListTerm tail = all;
			for(int i = 0; i < terms.length; i++) 
			{
				tail = tail.append(terms[i].clone());        		
			}
	        return un.unifies(args[1], all);
	    }
	    
	    private static void suffle(Object[] array)
	    {
	    	int end = array.length-1;
	    	for(int i = end; i >=0; i--)
	    	{
	    		int pos = (int) Math.floor(Math.random()*i);
	    		if(pos != i)
	    		{
	    			Object o = array[pos];
	    			array[pos] = array[i];
	    			array[i] = o; 
	    		}
	    	}
	    }
}
