// Agent with ask for its neighbor.

/* Initial beliefs and rules */

/* Initial goals */
!generateA.
/* Plans */

+!generateA:.my_name(M)<-
	+a(M);
	.wait(100);
	!sendIt.

+!sendIt:.all_names(AGENTS)<-
	.send(AGENTS, askOne, a(_), LIST);
	.print("List is: ", LIST);
	+list(LIST);
.