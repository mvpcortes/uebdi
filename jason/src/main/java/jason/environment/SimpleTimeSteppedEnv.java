package jason.environment;

import jason.asSyntax.Literal;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Structure;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SimpleTimeSteppedEnv extends Environment{
	
	private int step = 0;   // step counter
	
	private long startStepTime = 0;
	
	
	private Map<String,ActRequest> requests; // actions to be executed
	   
	public SimpleTimeSteppedEnv() {
	    super(1);
	}

	public int getStep()
	{
		return step;
	}
	
	public long getInitTimeStep()
	{
		return startStepTime;
	}
	
	public void incStep()
	{
		startStepTime = System.currentTimeMillis();
		step++;
		//atualiza literal
		literalStep.setTerm(0, new NumberTermImpl(getStep()));
	}
	
	protected int getNbAgents()
	{
		return this.getEnvironmentInfraTier().getRuntimeServices().getAgentsQty();
	}
	
	private static Literal literalStep = Literal.parseLiteral("step(0)");  
		
	protected static Object getCriticalSectionObject()
	{
		return literalStep;
	}
	public Literal getStepLiteral()
	{
		return (Literal)literalStep.clone();
	}
	
	@Override
	public void init(String[] args) {
		super.init(args);      
        
	    // reset everything
	    requests = new HashMap<String,ActRequest>();
	    step = 0;

	    updateAgsPercept();
	    stepStarted(step);
	}
	
	 /** This method is called after the execution of the action and before to send 'continue' to the agents */ 
    protected void updateAgsPercept() {
    	synchronized(literalStep)
    	{
	    	//limpa percep��o dos agentes
	    	clearAllPercepts();
	    	/*for(String s: getEnvironmentInfraTier().getRuntimeServices().getAgentsNames())
	    	{
	    		clearPercepts(s);
	    		addPercept(s, getStepLiteral());
	    	}*/
	    	addPercept(getStepLiteral());
    	}
    }
    
    @Override
    public List<Literal> getPercepts(String agName) {
    	synchronized(literalStep)
    	{
    		return super.getPercepts(agName);
    	}
    }
	
	 /** to be overridden by the user class */
    protected void stepStarted(int step) {
    }

    /** to be overridden by the user class */
    protected void stepFinished(int step, long elapsedTime, boolean byTimeout) {     
    }
    
    protected int requiredStepsForAction(String agName, Structure action) {
        return 1;
    }
    
    @Override
    public void scheduleAction(String agName, Structure action, Object infraData) {
        if (!isRunning()) return;
        
        getLogger().info(String.format("agent %s schedule action %s", agName, action));
        
        ActRequest newRequest = new ActRequest(agName, action, requiredStepsForAction(agName, action), infraData);

        boolean startNew = false;
        
        synchronized (requests) { // lock access to requests
            // if the agent already has an action scheduled, fail the first
            ActRequest inSchedule = requests.get(agName);
            if (inSchedule != null) {
            	getLogger().info(String.format("The agent %s already act in this turn.", agName));
                //for�a falha
            	getEnvironmentInfraTier().actionExecuted(agName, action, false, infraData);
                 
            } else {
                // store the action request         
                requests.put(agName, newRequest);
        
                // test if all agents have sent their actions
                while(testEndCycle()) {
                	runActions();
                	endStep();
                	startNewStep();
                }
            }
        }
    }
    
    private void runActions()
    {
    	if(!testEndCycle())
    	{
    		throw new IllegalStateException("Cannot run a action in a not end cycle state");
    	}
    	
    	Iterator<ActRequest> it = requests.values().iterator();
    	while(it.hasNext())
    	{
    		ActRequest ar = it.next();
    		ar.decRemainSteps();
    		if(ar.notHasRemainStep())
    		{
    			// calls the user implementation of the action
                ar.setSuccess(executeAction(ar.getAgName(), ar.getAction()));
    		}
    	}
    }
    
    private void endStep()
    {
    	// notify the agents about the result of the execution
        Iterator<ActRequest> i = requests.values().iterator();
        while (i.hasNext()) {
            ActRequest a = i.next();
            if (a.notHasRemainStep()) {
                getEnvironmentInfraTier().actionExecuted(a.getAgName(), a.getAction(), a.getSuccess(), a.getInfraData());
                i.remove();
            }
        }
        long current = System.currentTimeMillis();
        stepFinished(getStep(), current - getInitTimeStep(), false);
    }
    private void startNewStep()
    {
	  if (!isRunning())
      {
      	getLogger().warning("Cannot start a new step if the environment is not running");
      	return;
      }
	  incStep();
	  updateAgsPercept();
	  stepStarted(getStep());
    }
    /** 
     * Returns true when a new cycle can start, it normally 
     * holds when all agents are in the finishedAgs set.
     *  
     * @param finishedAgs the set of agents' name that already finished the current cycle
     */ 
    protected boolean testEndCycle() {
        return requests.size() >= getNbAgents();
    }
    
    public Structure getActionInSchedule(String agName) {
        ActRequest inSchedule = requests.get(agName);
        if (inSchedule != null) {
            return inSchedule.getAction();
        }
        return null;
    }
}
