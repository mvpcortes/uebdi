package jason.environment;

import jason.asSyntax.Structure;

class ActRequest {
    private String agName;
    private Structure action;
    private Object infraData;
    private Boolean success = null; 
    private int remainSteps; // the number os steps this action have to wait to be executed
   
    public ActRequest(String ag, Structure act, int rs, Object data) {
        agName = ag;
        action = act;
        infraData = data;
        remainSteps = rs;
    }
    
    public boolean equals(Object obj) {
        return agName.equals(obj);
    }
    
    public int hashCode() {
        return agName.hashCode();
    }
    
    public String toString() {
        return "["+agName+","+action+"]";
    }

	public Structure getAction() {
		return action;
	}

	public void decRemainSteps() {
		if(remainSteps <= 0)
		{
			throw new IllegalStateException("Cannot dec a actionRequest with remainSteps == 0");
		}
		remainSteps--;
	}

	/**
	 * Acabaram os steps (remainSteps == 0) 
	 * @return
	 */
	public boolean notHasRemainStep() {
		return remainSteps == 0;
	}

	public String getAgName() {
		return agName;
	}

	public void setSuccess(boolean s) {
		if(success != null)
		{
			throw new IllegalStateException("this ActRequest already finished");
		}
		success = s;
	}

	public boolean getSuccess() {
		return success != null && success;
	}

	public Object getInfraData() {
		return infraData;
	}
}