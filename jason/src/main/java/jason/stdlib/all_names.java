//----------------------------------------------------------------------------
// Copyright (C) 2003  Rafael H. Bordini, Jomi F. Hubner, et al.
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// To contact the authors:
// http://www.inf.ufrgs.br/~bordini
// http://www.das.ufsc.br/~jomi
//
//----------------------------------------------------------------------------

package jason.stdlib;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Atom;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Term;
import jason.runtime.RuntimeServicesInfraTier;

/**
  <p>Internal action: <b><code>.all_names</code></b>.
  
  <p>Description: get the names of all agents in the system.
  This identification is given by the runtime
  infrastructure of the system (centralised, saci, jade, ...)
  
  <p>Parameters:<ul>  
  <li>+/- names (list).</li>
  <li>+   <i>percent</i>: (number [optional]) a number between 0 to +inf to specify a number of agents to return randomly. If it is between [0 1), return the ratio of all agents 0.7 of 10 agents, it return random 7 agents. </li>
  </ul>
  
  <p>Examples:</br>
  (consider the agents a1, a2, ... a10).  	
  <ul> 
  <li> <code>.all_names(L)</code>: unifies with L a list of all agents in the system [a1, a2, ..., a10].</li>
  <li> <code>.all_names(L, 0.7)</code>: unifies with L a list of 0.7 percent of agents, (ex. [a2, a5, a6, a7, a8, a9, a10]).</li>
  <li> <code>.all_names(L, "exp")</code>: unifies with L a list of all agents witch true the expression "exp" (java regular expression).</li>
  </ul>

  @see jason.stdlib.my_name
  @see jason.runtime.RuntimeServicesInfraTier
*/
public class all_names extends DefaultInternalAction {

    @Override public int getMinArgs() { return 1; }
    @Override public int getMaxArgs() { return 2; }

    @Override public void checkArguments(Term[] args)  throws JasonException 
    {
    	super.checkArguments(args);
    	
    	
    	if(args.length > 1)
    	{
        	Term term1 = args[1];
    		if(!term1.isNumeric() && !term1.isString())
    			throw JasonException.createWrongArgument(this, "invalid argument in 1 pos");
    		
    	}
    }
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        checkArguments(args);
        RuntimeServicesInfraTier rs = ts.getUserAgArch().getArchInfraTier().getRuntimeServices();
       
        Collection<String> coll = null;
        if(args.length > 1)        	
        {
        	if(args[1].isNumeric())
        	{
        		coll = getAgentsByNumber((NumberTerm)args[1], new LinkedList<String>(), ts);
        	}else if(args[1].isString())
        	{
        		coll = getAgentsByExp((StringTerm)args[1], new LinkedList<String>(), ts);
        	}
        }else
        {
       		coll = rs.getAgentsNames();	
        }
        
        ListTerm ln = new ListTermImpl();
        ListTerm tail = ln;
        for (String a: coll) {
            tail = tail.append(new Atom(a));
        }
        return un.unifies(args[0], ln);
    }
    
    private Collection<String> getAgentsByExp(StringTerm st, Collection<String> coll, TransitionSystem ts)
    {
	    coll.clear();
	    Set<String> setAg = ts.getUserAgArch().getArchInfraTier().getRuntimeServices().getAgentsNames();
	    String exp =  st.getString();
	    for(String temp: setAg)
	    {
	    	if(temp.matches(exp))
	    	{
	    		coll.add(temp);
	    	}
	   	}
	    return coll;    
    }
    
    private Collection<String> getAgentsByNumber(NumberTerm nt, Collection<String> coll, TransitionSystem ts)
    {
    	coll.clear();
    	
    	Set<String> setAg = ts.getUserAgArch().getArchInfraTier().getRuntimeServices().getAgentsNames();
      	double value = nt.solve();
		value = Math.abs(value);
		int count = 1;
		if(value > 1)//envia usando o n�mero de itens especificado
		{
			count = (int) Math.round(value);
		}else //envia para % especificada entre 0 e 1.
		{
			count = (int)Math.round(value*setAg.size());
		}
		
		count = Math.min( setAg.size(), Math.max(count,1));
		
		Iterator<String> it = setAg.iterator();
		List<String> tempList = new LinkedList<String>();
		while(it.hasNext())
		{
			String tempAg = it.next();
			tempList.add(tempAg);
		}
		
		while(tempList.size() > 0 && coll.size()<count)
		{
			int l = (int) Math.ceil((Math.random()*(tempList.size()-1)));
			coll.add(tempList.get(l));
			tempList.remove(l);
		}
		
	    return coll;
    }
}
