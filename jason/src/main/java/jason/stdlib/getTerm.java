// Internal action code for project jason

package jason.stdlib;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class getTerm extends DefaultInternalAction {

	@Override public int getMinArgs() { return 3; }
    @Override public int getMaxArgs() { return 3; }
    
	@Override
	protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		if( !args[0].isLiteral())
			throw new JasonException("The first argument should be literal");
		if( !args[1].isNumeric())
			throw new JasonException("The second argument should be numeric");
		
		if( !(args[2].isVar() && !args[2].isGround()))
			throw new JasonException("The fifth argument should be Var and not ground");
	}
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	checkArguments(args);
        // execute the internal action
        ts.getAg().getLogger().fine("executing internal action 'jason.stdlib.getTerm'");
        
        Literal l = (Literal)args[0];
        
        NumberTerm n = (NumberTerm)args[1];
        int pos = (int)n.solve();
        Literal v = (VarTerm)args[2];
        
        Term termFound = l.getTerm(pos);
        
        return un.unifies(v, termFound);
    }
}
