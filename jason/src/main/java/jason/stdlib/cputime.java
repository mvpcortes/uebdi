package jason.stdlib;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.Calendar;
import java.util.GregorianCalendar;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.InternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;

/**
 * return the sys time in nanoseconds
 * @author Marcos
 *
 */
public class cputime extends DefaultInternalAction {

	    @Override public int getMinArgs() { return 1; }
	    @Override public int getMaxArgs() { return 1; }

	    @Override
	    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
	        checkArguments(args);
	        
	      long nCpuTime = System.nanoTime();
	      /*  long nSystemTime = 0L;
	        ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
	        nSystemTime = bean.isCurrentThreadCpuTimeSupported() ?
	            bean.getCurrentThreadCpuTime( ) : 0L;*/
	        return un.unifies(args[0], new NumberTermImpl(nCpuTime));         
	    }
}
