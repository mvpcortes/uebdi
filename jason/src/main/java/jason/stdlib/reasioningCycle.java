// Internal action code for project jason

package jason.stdlib;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

/**
 * Retorna o número do ciclo de raciocínio BDI.
 * @author Marcos
 *
 */
public class reasioningCycle extends DefaultInternalAction {

	 @Override public int getMinArgs() { return 1; }
	 @Override public int getMaxArgs() { return 1; }

	    @Override public void checkArguments(Term[] args)  throws JasonException 
	    {
	    	super.checkArguments(args);
	    	
	    	
	    	if(args.length != 1)
	    	{ 			
	    		throw JasonException.createWrongArgument(this, "invalid argument number.");
	    	}
	    	
	    	if(!(args[0].isVar() && !args[0].isGround()))
	    	{
	    		throw JasonException.createWrongArgument(this, "invalid argument type.");
	    	}
	    }
	    
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	checkArguments(args);
    	
        ts.getAg().getLogger().fine("executing internal action 'jason.stdlib.reasioningCycle'");
        
        VarTerm vt = (VarTerm)args[0];
        
        int cycle = ts.getUserAgArch().getCycleNumber();
        
        NumberTerm nt = new NumberTermImpl(cycle);
        
        return un.unifies(vt, nt);
    }
}
