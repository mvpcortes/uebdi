package jason.stdlib;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;

/**
 * return the time of the agent thread (usertime) in nanoseconds
 * @author Marcos
 *
 */
public class usertime extends DefaultInternalAction {

	    @Override public int getMinArgs() { return 1; }
	    @Override public int getMaxArgs() { return 1; }

	    @Override
	    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
	        checkArguments(args);
	        
	        long nUserTime = 0L;
	        ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
	        nUserTime = bean.isCurrentThreadCpuTimeSupported( ) ?
	            bean.getCurrentThreadUserTime( ) : 0L;
	            
	        return un.unifies(args[0], new NumberTermImpl((double)nUserTime));          
	    }
}
