package jason.stdlib.log;

import java.util.logging.Level;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.InternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Term;

public class log extends DefaultInternalAction {

	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private static InternalAction singleton = null;
	    public static InternalAction create() {
	        if (singleton == null) 
	            singleton = new log();
	        return singleton;
	    }
	    
	    protected String getNewLine() {
	        return "\n";
	    }
	    
	    @Override
	    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
	        String sout = argsToString(args);
	        
	      //  Logger.Level = parseType(args[0]);
	        final Level level = parseType(args[0]);
	        if(level == null)
	        {
	        	throw new  IllegalArgumentException("The level " + level + "not valid");
	        }
	        
	        

	        if (ts != null) {
	            //ts.getLogger().info(sout.toString());
	        	ts.getLogger().log(level, sout.toString());
	        } else {
	        	throw new JasonException("The ts is null");
	        }
	        
	        return true;
	    }

	    private final Level parseType(Term term) {
	    	String t = "";
	    	if(term.isString())
	    	{
	    		t = ((StringTerm)term).getString();
	    	}else
	    	{
	    		t = term.toString();
	    	}
	    	t = t.toUpperCase();
	    	
	    	Level l = null;
	    	try
	    	{
	    		l = Level.parse(t);
	    	}catch(Exception e)
	    	{
	    	}
	    	return l;
	    	
		}

		protected String argsToString(Term[] args) {
	        StringBuilder sout = new StringBuilder();    
	        for (int i = 1; i < args.length; i++) {//ignora o level
	            if (args[i].isString()) {
	                StringTerm st = (StringTerm)args[i];
	                sout.append(st.getString());
	            } else {
	                Term t = args[i];
	                if (! t.isVar()) {
	                    sout.append(t);
	                } else {
	                    sout.append(t+"<no-value>");
	                }
	            }
	        }
	        return sout.toString();
	    }
}
