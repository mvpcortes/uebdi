// Internal action code for project FoodSimulation

package jason.functions;

import jason.JasonException;
import jason.asSemantics.DefaultArithFunction;
import jason.asSemantics.TransitionSystem;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.StringTerm;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.util.logging.Logger;


/** function that computes the distance between two number */
public class DistanceFunction extends DefaultArithFunction {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5364149298884856872L;
	private Logger logger = Logger.getLogger(DistanceFunction.class.getName());

    
    public String getName()
    {
    	return "math.distance";
    }
    
    public boolean checkArity(int a) {
        return a == 2;
    }
    
    @Override
    public double evaluate(TransitionSystem ts, Term[] args) throws Exception {
        
    	if(args[0].isNumeric() && args[1].isNumeric())
    	{
    		logger.info("get distance from two numbers.");
	    	try {
	            int n1 = (int)((NumberTerm)args[0]).solve();
	            int n2 = (int)((NumberTerm)args[1]).solve();
	            return Math.abs(n1 - n2);
	        } catch (Exception e) {
	            logger.warning("Error in function 'math.distance'! "+e);
	        }
	        return 0;
    	}
    	else
	    {
    		logger.info("get distance from two structures.");
	    	Term a = args[0];
	    	Term b = args[1];
	    	if(!(a.isStructure() && b.isStructure()))
	    	{
	    		throw new JasonException("Invalid input term of Arith function distance");
	    	}	
	    	Structure as = (Structure)a;
	    	Structure bs = (Structure)b;
	    	if(as.getArity() != bs.getArity())
	    	{
	    		throw new JasonException("Invalid input structure with different arity of Arith function distance");
	    	}
	    	//double[] vecValues = new double[as.getArity()];
	    	double accum = 0;
	    	for(int i = 0; i < as.getArity(); i++)
	    	{
	    		double dA = getDouble(as, i);
	    	    double dB = getDouble(bs, i);
	    		accum += Math.pow(dA-dB, 2);
	    	}
	    	return Math.sqrt(accum);	    			
	    }
    }
    
    public static double getDouble(Literal lit, int pos)
	{
		if(pos < lit.getTerms().size())
		{
			return toDouble(lit.getTerm(pos));
		}else
		{
			return 0.0;
		}
	}
	public static double toDouble(Term term)
	{
		if(term.isNumeric())
		{
			NumberTerm nt = (NumberTerm)term;
			return nt.solve();
		}else if(term.isString())
		{
			String t = ((StringTerm)term).getString();
			try
			{
				return Double.parseDouble(t);
			}catch(Exception e)
			{
				return 0;
			}
		}else if(term.isVar())
		{
			VarTerm vt = (VarTerm) term;
			if(vt.getValue()!= null && vt.getValue().isNumeric())
			{
				NumberTerm nt = (NumberTerm)vt.getValue();
				return nt.solve();
			}
		}
		return 0;
	}
}
