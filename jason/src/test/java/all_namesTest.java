import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jason.JasonException;
import jason.architecture.AgArch;
import jason.architecture.AgArchInfraTier;
import jason.asSemantics.ActionExec;
import jason.asSemantics.Agent;
import jason.asSemantics.Circumstance;
import jason.asSemantics.Message;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTerm;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.StringTerm;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;
import jason.mas2j.ClassParameters;
import jason.runtime.RuntimeServicesInfraTier;
import jason.runtime.Settings;

import jason.stdlib.all_names;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class all_namesTest {

	private static String[] ARRAY_NAME_AG = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
	private static TransitionSystem ts;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AgArch agArch = new AgArch()
		{
			@Override public AgArchInfraTier getArchInfraTier() {
			{
				return new AgArchInfraTier() {
					
					@Override
					public void wake() {
						
						
					}
					
					@Override
					public void sleep() {
						
						
					}
					
					@Override
					public void sendMsg(Message m) throws Exception {
						
						
					}
					
					@Override
					public List<Literal> perceive() {
						
						return null;
					}
					
					@Override
					public boolean isRunning() {
						
						return false;
					}
					
					@Override
					public RuntimeServicesInfraTier getRuntimeServices() {
						return new  RuntimeServicesInfraTier()
						{

							@Override
							public String createAgent(String agName,
									String agSource, String agClass,
									List<String> archClasses,
									ClassParameters bbPars, Settings stts)
									throws Exception {
								
								return null;
							}

							@Override
							public void startAgent(String agName) {
								
								
							}

							@Override
							public AgArch clone(Agent source,
									List<String> archClasses, String agName)
									throws JasonException {
								
								return null;
							}

							@Override
							public boolean killAgent(String agName) {
								
								return false;
							}

							@Override
							public Set<String> getAgentsNames() {
								Set<String> set = new HashSet();
								for(String s: ARRAY_NAME_AG)
								{
									set.add(s);
								}
								return set;
							}

							@Override
							public int getAgentsQty() {
								
								return 0;
							}

							@Override
							public void stopMAS() throws Exception {
								
								
							}
							
						};
					}
					
					@Override
					public String getAgName() {
						
						return null;
					}
					
					@Override
					public void checkMail() {
						
						
					}
					
					@Override
					public boolean canSleep() {
						
						return false;
					}
					
					@Override
					public void broadcast(Message m) throws Exception {
						
						
					}
					
					@Override
					public void act(ActionExec action, List<ActionExec> feedback) {
						
						
					}
				};
			}
			}
		};
		
		ts = new TransitionSystem(
				new Agent(), 
				new Circumstance(), 
				new Settings(),
				agArch			
			);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testExecute() {
		all_names an = new all_names();
		try {
			
			{
				Term dest = new VarTerm("X");
				an.execute(ts, new Unifier(), new Term[]{dest});
			}
			
			//percent mode
			for(float i = 0; i <= 1.0; i+=0.1)
			{
				Unifier u = new Unifier();
				NumberTerm percent = new NumberTermImpl(i);
				VarTerm dest = new VarTerm("X");
				an.execute(ts, u, new Term[]{dest, percent});
				Term value = u.get(dest);
				ListTerm lt = (ListTerm)value;
				assertEquals(lt.size(), Math.max(((int)(i*10)),1));
			}
			
			//regx mode
			for(String x: ARRAY_NAME_AG)
			{
				String s = String.format(".*%s.*", x);
				VarTerm dest = new VarTerm("X");
				Unifier u = new Unifier();
				an.execute(ts, u, new Term[]{dest, new StringTermImpl(s)});
				Term value = u.get(dest);
				String compare = "";
				if(value.isList())
				{
					value = ((ListTerm)value).get(0);
				}
				
				if(value.isAtom())
				{
					compare = value.toString();
				}else if(value.isString())
				{
					compare = ((StringTerm)value).getString();
				}else
				{
					fail();
				}
				
				assertEquals(compare, x);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
}
