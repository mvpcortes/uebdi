/**
	occfoo agent
	O agente emocional baseado no modelo OCC-Foo
*/

//like not like
like(Ag):- msuccess(_, Ag, Value) & Value > 0.5.

~like(Ag):-msuccess(_, Ag, Value) & Value < 0.5.


//seleciona o servidor
+!selectServ(COSTS, SELECTED_COST):
	br.uff.ic.uebdi.internalaction.getMaxIntensity(MEI)
	<-
	if(MEI >0)
	{
		+max_emo_int(MEI);
	}
	else
	{
		+max_emo_int(1);
	}

	!rep_of_agents(COSTS);	
	!findEmotions(COSTS, EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING);
	.log.log(severe, "result: ", emotions(EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING));
	.length(EMO_PITY, N_PITY);
	if(N_PITY > 0)
	{
		.log.log(info, "select pity set");
		!selectEmotion(EMO_PITY, SELECTED_COST, -1);
	}else
	{
		.length(EMO_HAPPY_NOT, N_HAPPY_NOT);
		if(N_HAPPY_NOT> 0)
		{
			.log.log(info, "select happy or not set");
			!selectEmotion(EMO_HAPPY_NOT, SELECTED_COST, -1);
		}else
		{
			.length(EMO_GLOATING, N_GLOATING);
			if(N_GLOATING> 0)
			{
				.log.log(info, "select gloating set");
				!selectEmotion(EMO_GLOATING, SELECTED_COST, 1);
			}else
			{
				.log.log(info, "select nothing set");
				false;
			}
		}
	}		
	.

//point(Serv, Value, Int)
@selectPity
+!selectEmotion([point(A1, I1, V1, E1), point(A2, I2, V2, E2)|L], SELECTED_COST, EmoWeight):
			mfail		(MSI1, A1, F1) 		&
		 	mfail		(MSI2, A2, F2)		& 
			mrfail		(MRI1, A1, R1)		&
			mrfail		(MRI2, A2, R2) 				
			<-
	if(
		(
			V1*(F1+R1+(EmoWeight*R1))/3 <=
			V2*(F2+R2+(EmoWeight*R2))/3 
			
		)
	)
 	{
 		!selectEmotion([point(A1, I1, V1, E1)|L], SELECTED_COST, EmoWeight);
 	}else
 	{
 		!selectEmotion([point(A2, I2, V2, E2)|L], SELECTED_COST, EmoWeight);
 	}
.

@selectEmotionBase
+!selectEmotion([point(A1, I1, V1, E1)], SELECTED_COST, _):true<-
	SELECTED_COST = cost(A1, I1, V1).
	
@findEmoPity
+!findEmotions([cost(Serv, I, C) | COST], EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING):
	emotion(pity, Serv, Int) &
	max_emo_int(MEI)
	<-
	!findEmotions(COST, EE, EMO_HAPPY_NOT, EMO_GLOATING);
	M = (Int/MEI);
	EMO_PITY = [point(Serv, I, C, M) | EE];
	.
@findEmoHappy	
+!findEmotions([cost(Serv, I, Value) | COST], EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING):
	emotion(happy_for, Serv, Int)
	& max_emo_int(MEI)
	<-
	!findEmotions(COST, EMO_PITY, EE, EMO_GLOATING);
	M = (Int/MEI);
	EMO_HAPPY_NOT = [point(Serv, I, Value, M) | EE];
	.

@findEmoNotEmotion
+!findEmotions([cost(Serv, I, Value) | COST], EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING):
	not emotion(_, Serv, _)
	& max_emo_int(MEI)
	<-
	!findEmotions(COST, EMO_PITY, EE, EMO_GLOATING);
	EMO_HAPPY_NOT = [point(Serv, I, Value, 0) | EE];
	.
@findEmoGloating		
+!findEmotions([cost(Serv, I, Value) | COST], EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING):
	  emotion(gloating, Serv, Int) 
	& max_emo_int(MEI)
	<-
	!findEmotions(COST, EMO_PITY, EMO_HAPPY_NOT, EE);
	M = (Int/MEI);
	EMO_GLOATING = [point( Serv, I, Value, M) | EE];
	.

@findEmoOther
+!findEmotions([cost(Serv, I, Value) | COST], EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING):emotion(_, Serv, Int) <-
	!findEmotions(COST, EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING);
	.
	
@findEmoNotBase
+!findEmotions([], EMO_PITY, EMO_HAPPY_NOT, EMO_GLOATING):true<-
	EMO_PITY = [];
	EMO_HAPPY_NOT = [];
	EMO_GLOATING = [];
	.

 @answerInternalDataNull_emotion[atomic]
+!kqml_received(MANAGER, askOne, internalData(ID), MsgId):.my_name(MY_NAME) & br.uff.ic.uebdi.internalaction.getEmotions(EMO)
<-
	I = internalData(ID, MY_NAME, -1, -1, -1, EMO);
	.log.log(info, "answer internalDataNull: ", I);
	.send(MANAGER, tell, I, MsgId);
	.
	
		
 {include("clientReputation.asl")}