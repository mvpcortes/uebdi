// Agent Manager in project TaskAlloc

/* Initial beliefs and rules */
/* Initial goals */
//!check.
/* Plans */


+!alloc_task_list(LIST):true
<-
	.print("init alloc_task_list");
	for(.member(A, LIST))
	{
		!A;
	};
	!tryEndMAS.


@alloc_general
+!alloc_a(BEGIN, END):.all_names(LIST_CLIENT, "client.*") & .all_names(LIST_SERV, "serv.*")
<-
	.log.log(info, "alloc_general: lists: ", LIST_CLIENT,", ", LIST_SERV);
	!alloc_a(LIST_CLIENT, LIST_SERV, BEGIN, END);
	.
	  
@alloc_a_list
+!alloc_a(LIST_CLIENT, CANDIDATES, BEGIN, END):.list(LIST_CLIENT)
<-
	for ( .range(I,BEGIN,END) ) 
	{	
		for(.member(CLIENT, LIST_CLIENT))
		{
			!alloc(I, CLIENT, CANDIDATES);
		}
		.send(LIST_CLIENT, askOne, updateReputation, LIST_ANSWER);//<fazendo assim para for�ar sincronismo
	}
	.
	
@alloc_a_not_list
+!alloc_a(CLIENT, CANDIDATES, BEGIN, END):true
<-
	for ( .range(I,BEGIN,END) ) 
	{	
		!alloc(I, CLIENT, CANDIDATES);
		.log.log(info, "alloc ", task(I), "to ", CLIENT); 
	}. 
	
+!alloc(CLIENT, CANDIDATES):true
<-
	!alloc(ID, CLIENT, CANDIDATES);
	.
	
+!alloc(ID, CLIENT, CANDIDATES):br.uff.ic.taskalloc.get_other_clients(CLIENTS)
<-
	.log.log(info, "alloc ", task(ID), " to ", CLIENT, " with candidates ", CANDIDATES);
	register(ID, CLIENT);
	.send(CLIENT, achieve, tryAlloc(ID, CANDIDATES));
	.wait({+finishTask(ID, SERV, MS, MR, SUCCESS)});
	.concat(CANDIDATES, CLIENTS, AGENTS);
	.send(AGENTS, askOne, internalData(ID), INT_DATA);
	unregister(ID, CLIENT, SERV, SUCCESS, MS, MR, CANDIDATES, INT_DATA);
	.

/**
	Trata caso haja uma falha na sele��o do Servidor
*/
@cannotAlloc_a
-!alloc_a(CLIENT, CANDIDATES, COUNT):true<-
	.print("failed alloc_a");
.	

/**
	Trata caso haja uma falha na sele��o do Servidor
*/
@cannotAlloc
-!alloc(ID, CLIENT, CANDIDATES):true<-
	.print("failed alloc");
.

@answerProb
+!kqml_received(AG, askOne, my_prob(AG, ID), MsgId):true
<-
	!getProb(AG, ID, VALUE);
	.send(AG, tell, prob(AG, ID, VALUE), MsgId).
	
	
@answerCost
+!kqml_received(AG, askOne, my_cost(AG, ID), MsgId):true
<-
	!getCost(AG, ID, VALUE);
	.send(AG, tell, cost(AG, ID, VALUE), MsgId).
	
+!tryEndMAS: not .desire(alloc(_,_,_)) & not .intend(alloc(_,_,_))<-
	.stopMAS.
	
+!tryEndMAS:true
<-true.//faz nada pq ainda existe desire
{include("cost_prob_func.asl")}
	