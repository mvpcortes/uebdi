// Agent cost_prob_func in project TaskAlloc

/* Initial beliefs and rules */

/* Initial goals */

@getProb[atomic]
+!getProb(AG, ID, VALUE):prob(AG, ID, P)
<-
	VALUE = P.
	
@getProbRange[atomic]
+!getProb(AG, ID, VALUE):probRange(B, E, P) & ID >= B & ID <= E
<-
	VALUE = P.
	

@getProbBase
+!getProb(AG, ID, VALUE):.my_name(NAME)
<-
	if(NAME == manager)
	{
		.random(X);
		VALUE = X;
		+prob(AG, ID, VALUE);
	}else
	{
		.send(manager, askOne, my_prob(AG, ID), P[source(_)]);
		prob(_, _, P_VALUE) = P;		
		+prob(AG, ID, P_VALUE);
		VALUE = P_VALUE;
	}
	.
	
@getCost[atomic]
+!getCost(AG, ID, VALUE):cost(AG, ID, C)
<-
	VALUE = C;
	.

@getCostRange[atomic]
+!getCost(AG, ID, VALUE):costRange(IDBEGIN, IDEND, C) &  ID >=IDBEGIN & ID <=IDEND
<-
	VALUE = C;
	.
		
@getCostBase
+!getCost(AG, ID, VALUE):.my_name(NAME)
<-
	if(NAME == manager)
	{
		.random(X);
		VALUE = math.ceil(1+X*10);
		+cost(AG, ID, VALUE);
	}else
	{
		.send(manager, askOne, my_cost(AG, ID), COST[source(_)]);
		cost(_,_, C_VALUE) = COST;
		+cost(AG, ID, C_VALUE);
		VALUE = C_VALUE;
	}
	.
	