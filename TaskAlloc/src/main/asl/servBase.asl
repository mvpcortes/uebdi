// Agent ServBase in project TaskAlloc

/* Initial beliefs and rules */

//probabilidade de sucesso

/* Initial goals */



@answerCostBelief[atomic]
+!kqml_received(Client, askOne, request(ID), MsgId):.my_name(NAME)
<-
	!getCost(NAME, ID, COST);
	.send(Client, tell, cost(NAME, ID, COST), MsgId).


@answerInternalData[atomic]
+!kqml_received(MANAGER, askOne, internalData(ID), MsgId):.my_name(MY_NAME) & cost(MY_NAME, ID, C) & prob(ID, P)
<-
	I = internalData(ID, MY_NAME, P, C, -1, []);
	.log.log(fine, "answer internalData: ", I);
	.send(MANAGER, tell, I, MsgId);
	.	
	
@answerInternalDataNull[atomic]
+!kqml_received(MANAGER, askOne, internalData(ID), MsgId):.my_name(MY_NAME)
<-
	!getCost(MY_NAME, ID, C);
	!getProb(MY_NAME, ID, P);
	I = internalData(ID, MY_NAME, P, C, -1, [], []);
	//I = ~internalData;
	.log.log(fine, "answer internalDataNull: ", I);
	.send(MANAGER, tell, I, MsgId);
	.	
	
+!do(cost(SERV, I, _))[source(Client)]:.my_name(MY_NAME) & MY_NAME == SERV
<-
	.log.log(info, "doing ", task(I));
 	!execute(I, RESULT);
 	.send(Client, tell, success(I, SERV, RESULT));
 	.
 	
 	

 @executeMethod[atomic]
 +!execute(I, RESULT):.random(X) & .my_name(NAME)
 <-
 	!getProb(NAME, I, P);
 	if(X <= P)
 	{
 		RESULT = 1;
 	}else
 	{
 		RESULT = 0;
 	}
 	 .log.log(info, "execute method: ", p(P), ", ", x(X), ", ", result(RESULT));
 	.
 {include("cost_prob_func.asl")}