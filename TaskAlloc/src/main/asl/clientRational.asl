// Agent clientRational in project TaskAlloc

/* Initial beliefs and rules */

/**
	Sele��o do servidor ao modo rand�mico.
*/
@selectServRational
+!selectServ(VALID_COSTS, SELECTED_COST):true<-
	!min_point(VALID_COSTS, SELECTED_COST);
.

/**
	Acha dentro dos custos cobrados qual que oferece menor custo 
	e maior chance de execu��o (probabilidade inferida por mediaSucess)
 */
 @minpoint[atomic]
 +!min_point([cost(A1, I1, C1), cost(A2, I2, C2)|L], SELECTED_COST):
 	mfail(IF1, A1, F1) &
 	mfail(IF2, A2, F2) 
 	<-
 	.log.log(fine, "=======================");
 	.log.log(fine, mfail(IF1, A1, F1), ", ", point(F1*C1));
 	.log.log(fine, mfail(IF2, A2, F2), ", ", point(F2*C2));
 	.log.log(fine, "=======================");
 	if(F1*C1 <= F2*C2)
 	{
 		!min_point([cost(A1, I1, C1)|L], SELECTED_COST);
 	}else
 	{
 		!min_point([cost(A2, I2, C2)|L], SELECTED_COST);
 	}.
 	
 @minpoint_base[atomic]	
 +!min_point([C], SELECTED_COST):true<-
 	SELECTED_COST = C;
 	.log.log(fine, "min_point base: ", select(SELECTED_COST));
 	.
/* Initial goals */

{include("clientBase.asl")}
