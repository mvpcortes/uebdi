// Agent ClientBase in project TaskAlloc

/* Initial beliefs and rules */
msuccess(0, _,0.5).

mfail(I, AG, V):-
	msuccess(I, AG, S) & (V = 1-S).


//mfail(0, AG, 0).

/* Initial goals */

/* Plans */

@tryAlloc
+!tryAlloc(ID, CANDIDATES):
	(msuccess(ACTUAL, SERV, _) & ACTUAL < ID) | (not msuccess(_,SERV, _))
<-
	.send(CANDIDATES, askOne, request(ID), COSTS);
	br.uff.ic.uebdi.internalaction.randomize(COSTS, RANDOM_COSTS);
	.log.log(info, "random_costs =", RANDOM_COSTS);
	!preSelectServ(ID, RANDOM_COSTS); 
	.
	
@preSelectServ
+!preSelectServ(ID, COSTS):.my_name(MY)
<-
	!selectServ(COSTS, SELECTED_COST);
	.log.log(severe, "Selected Cost: ", SELECTED_COST);
	+SELECTED_COST;
	!getCost(MY, ID, FAIR);
	+fairCost(ID, FAIR);
    if(fairCost(ID, _))
	{
		?fairCost(A, B);
		.log.log(fine, fairCost(A, B)); 
	}else
	{
		.log.log(fine, not fairCost(ID, _));
	}
	
	
	!allocTask(SELECTED_COST);
.

+!allocTask(cost(SERV, ID, COST)):true
<- 
	.log.log(info, "Alloc ", task(ID), " to: ", SERV);
	.send(SERV, achieve, do(cost(SERV, ID, COST)));
	.
	
@success_client
+success(ID, SERV, RESULT)[source(SERV)]:cost(SERV, ID, VALUE)
<-
	!updateMetric(success(ID, SERV, RESULT));
	
	if(not msuccess(_, SERV, _))
	{
		MS = -1; 
	}else
	{
		?msuccess(_, SERV, MS);
	}
	
	if(not mediaRep(_, SERV, _))
	{
		MR = -1; 
	}else
	{
		?mediaRep(_, SERV, MR);
	}
	
	if(.findall(msuccess(F_ID, F_AGG, F_MS), msuccess(F_ID, F_AGG , F_MS) & .ground(F_AGG), LIST_MS))
	{
		.log.log(info, listMS(LIST_MS)); 	
	}
	.send(manager, tell, finishTask(ID, SERV, MS, MR, RESULT));
. 

/**
	Sele��o do servidor ao modo rand�mico.
*/
@selectServRandom
+!selectServ(VALID_COSTS, SELECTED_COST):true<-
	.member(SELECTED_COST, VALID_COSTS);
	.print(selected(SELECTED_COST));
.

+!updateMetric(success(ID, SERV, SUCCESS)):msuccess(H, SERV, MS)
<-
	-msuccess(H, SERV, _);
	NEW_VALUE = (ID*SUCCESS + H*MS)/(ID + H); 
	+msuccess(ID, SERV, NEW_VALUE);
	.log.log(info, "change ", msuccess(H, SERV, MS), " to ", msuccess(ID, SERV, NEW_VALUE));
	.
	
+!updateMetric(success(ID, SERV, RESULT)):true
<-
	+msuccess(ID, SERV, RESULT);
	.log.log(info, "new msuccess is ", msuccess(ID, SERV, RESULT));
	.

//respondendo a uma solicita��o de reputa��o
/*
	rep(CANDIDATE, SOURCE, LAST_INSTANT, VALUE, EVENT)
	CANDIDATE 	= O agente que est� sendo consultado a reputa��o
	SOURCE		= A fonte da reputa��o (quem reputa)
	LAST_INSTANT= O �ltimo instante que houve uma interea��o entre SOURCE (como client) e CANDIDATE (como serv)
	VALUE		= Valor da reputa��o (consideramos mediaSuccess)
	EVENT		= o evento (event)
	===========================
	event(H, S, C): 
	H = instante
	S = sucesso ou n�o (1 ou 0)
	C = custo que o candidate deu para este evento)
*/
@kqmlReceivedAskOne1Rep
+!kqml_received(Client, askOne, rep_of(CANDIDATE), MsgId):
	.my_name(MYNAME) & 
	toRep(H, CANDIDATE, VALUE) & 
	success(H, CANDIDATE, S) &
	cost(CANDIDATE, H, C)<-
      .send(Client, tell, rep(CANDIDATE,MYNAME,H, VALUE, event(CANDIDATE, H, S, C)) , MsgId).


@kqmlReceivedAskOne1RepBase
+!kqml_received(Client, askOne, rep_of(CANDIDATE), MsgId):.my_name(MYNAME)<-
      .send(Client, tell, ~rep(CANDIDATE,MYNAME), MsgId).
 
@answerInternalDataNull[atomic]
+!kqml_received(MANAGER, askOne, internalData(ID), MsgId):.my_name(MY_NAME) 
<-	
	I = internalData(ID, MY_NAME, -1, -1, -1, [], []);
	.send(MANAGER, tell, I, MsgId);
	.
	
@answerUpdateReputation[atomic]
+!kqml_received(MANAGER, askOne, updateReputation, MsgId):.my_name(MY_NAME) 
<-	
	!updateReputation;
	.send(MANAGER, tell, updateReputationOK(MY_NAME), MsgId);
	.
@updateReputation[atomic]
+!updateReputation:true 
<-
	.log.log(info, "updateReputation now");
	for(msuccess(ID_OLD, SERV, VALUE_OLD) & .ground(SERV))
	{
		-toRep(_,SERV,_);
		+toRep(ID_OLD, SERV, VALUE_OLD);
	}
	.
	
 {include("cost_prob_func.asl")}