// Agent clientReputation in project TaskAlloc

// Agent clientRational in project TaskAlloc

/* Initial beliefs and rules */

//Qual a m�dia de reputa��o falha deste agente, inferida pela m�dia de sucesso 
mrfail(H, Ag, ValueFail):-
	mediaRep(H, Ag, Value) & ValueFail = 1-Value.

//Qual a m�dia de reputa��o falha deste agente, inferida pela m�dia de sucesso (base	
mrfail(0,A,0.5).//<<<< ver o valor. se colocar 0 vai dar prefer�ncia a um cara sem reptua��o!

//faz a inser��o de um evento caso ele n�o seja uma nega��o
//n�o pode ser atomic para poder gerar o evento de E
@insertEventnotE
+!insertEvent(~event):true<- true.

//n�o pode ser atomic para poder gerar o evento de E
@insertEventE
+!insertEvent(E):true<-
	.print("insert E = ", E);
	+E.

//seleciona o servidor
@selectServRep
+!selectServ(COSTS, SELECTED_COST)<-
	!rep_of_agents(COSTS);//acha a reputa��o dos agentes
	!min_point_rep(COSTS, SELECTED_COST);//acha o menor custo com maior prob. de execu��o (mediaSucess) e melhor reptua��o
	.
	
	
 @minpointrep
 +!min_point_rep([cost(A1, I1, C1), cost(A2, I2, C2)|L], SELECTED_COST):
 	mfail		(F_ID1, A1, F1) 	&
 	mfail		(F_ID2, A2, F2)		& 
	mrfail		(R_ID1, A1, R1)		&
	mrfail		(R_ID2, A2, R2)
 	<-
 	AAA = C1*(F1 + R1);
 	BBB = C2*(F2 + R2);
 	.log.log(fine, "=========================================");
 	.log.log(fine, "costA = ", C1, ", costB = ", C2);
 	.log.log(fine, mfail		(F_ID1, A1, F1) );
 	.log.log(fine, mfail		(F_ID2, A2, F2)	); 
	.log.log(fine, mrfail		(R_ID1, A1, R1)	);
	.log.log(fine, mrfail		(R_ID2, A2, R2)	);
	.log.log(fine, "resul:");
	.log.log(fine, AAA);
	.log.log(fine, "<=");
	.log.log(fine, BBB);
 	.log.log(fine, "=========================================");
 	if(
 		AAA 
 		<= 
 	   	BBB
		)
 	{
 		!min_point_rep([cost(A1, I1, C1)|L], SELECTED_COST);
 	}else
 	{
 		!min_point_rep([cost(A2, I2, C2)|L], SELECTED_COST);
 	}.
 	
 
 
 @minpointrepbase
 +!min_point_rep([C], SELECTED_COST):true<-
 	SELECTED_COST = C;
 	.log.log(fine, "min_point_rep base: ", C);
 	.
 	
@rep_of_agents
+!rep_of_agents(LIST):br.uff.ic.taskalloc.get_other_clients(NEIG_T) & .my_name(MY_NAME)<-
	.log.log(info, rep_of_agents(LIST));
	for(.member(cost(CANDIDATE,ID,_), LIST))
	{
		.difference(NEIG_T, [CANDIDATE, MY_NAME], NEIG); //neig sem o candidate
		.length(NEIG, NEIG_SIZE);
		if(NEIG_SIZE > 0)
		{		
			.send(NEIG, askOne, rep_of(CANDIDATE), LIST_REP);
			!findMediaRep(LIST_REP, T, H);
			.length(LIST_REP, COUNT);
			if(H == 0)
			{
				!changeID(ID, CANDIDATE, 0.5);
			}else
			{
				!changeID(ID, CANDIDATE, T/H);
			}
		}
	}
 	.
 	
@changeID[atomic]
+!changeID(ID, CANDIDATE, VALUE):true<-
	if(mediaRep(OLD_ID, CANDIDATE, OLD_VALUE))
	{
		-mediaRep(OLD_ID, CANDIDATE, _);	
	}
	.log.log(info, "change mediaRep:", old(OLD_ID, CANDIDATE, OLD_VALUE),
		 new( mediaRep(ID ,CANDIDATE, VALUE)
		 	)
		 );
	+mediaRep(ID ,CANDIDATE, VALUE);
 	.
 	
@findMediaRep
+!findMediaRep([rep( A, B, H, M, E)[_]|LIST], RM, RH):true
<-
	!findMediaRep(LIST, SM, SH);
	
	!insertEvent(E); //adiciona o evento na belief base para ser processado pelo agente emocional
	
	RH = H + SH;
	RM = M*H + SM;
	.log.log(fine, "sumrep = ", sumrep(RH, RM, RM/RH));
	.

/**
Sem reputa��o para este agente
*/
@findMediaNotRep
+!findMediaRep([~rep( A, B)[_]|LIST], RM, RH):true
<-
	!findMediaRep(LIST, SM, SH);
	
	RH = SH;
	RM = SM;
	if(RH == 0)
	{
		DIV = 0;
	}else
	{
		DIV = RM/RH;
	}
	.log.log(fine, "sumnotrep =", sumrep(RH, RM, DIV));
	.
	
@findMediaRepBase	
+!findMediaRep([], SM, SH):true<-
	SH = 0;
	SM = 0;
	.log.log(fine, "sumrep base=", sumrep(SH, SM, 0));
	
	.
 	
 {include("clientBase.asl")}