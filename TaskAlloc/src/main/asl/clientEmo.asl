// Agent new_agent in project TaskAlloc

like(Ag):-emotion(like, Ag, VALUE) & VALUE > 6.
unlike(Ag):-emotion(like, Ag, VALUE) & VALUE < 4.
neutral(Ag):-emotion(like, Ag, VALUE)& VALUE <= 6 & VALUE >=4.




//like(Ag, VALUE):-not emotion(like, Ag, _) & VALUE = (0.0).
/* Plans */


//seleciona o servidor
+!selectServ(COSTS, SELECTED_COST):true
	<-
	!rep_of_agents(COSTS);//acha a reputa��o dos agentes
	!min_cost_like(COSTS, SELECTED_COST);
	.
	
+!min_cost_like(COSTS, SEL_COST):true			
 	<-
 	!groupLike(COSTS, COSTS_LIKE, COSTS_NEUTRAL, COSTS_UNLIKE);
 	.length(COSTS_LIKE, NLIKE);
 	if(NLIKE > 0)
 	{
 		.log.log(info, "sel like=", COSTS_LIKE);
 		!min_point_rep(COSTS_LIKE, SEL_COST);
 	}else
 	{
 		.length(COSTS_NEUTRAL, NNEUTRAL);
 		if(NNEUTRAL > 0)
 		{
 			.log.log(info, "sel neutral=", COSTS_NEUTRAL);
 			!min_point_rep(COSTS_NEUTRAL, SEL_COST);
 		}else{
 			.log.log(info, "sel unlike=", COSTS_UNLIKE);
 			!min_point_rep(COSTS_UNLIKE, SEL_COST);
 		}
 	}
	.

+!groupLike([cost(A1, I1, C1) | L], CL, CN, CU):br.uff.ic.uebdi.internalaction.getEmotions(EMOTIONS)<-
	.log.log(info, "my emotions: ", EMOTIONS);
	//seta o valor randomico inicial
	if(not emotion(_, A1, _))
	{
		.log.log(info, "init emotion");
		if(initRandomEmotion(_))
		{
			.random(RANDOM_VALUE);
			.log.log(severe, "init emotion with random value");
		}else
		{
			RANDOM_VALUE = 5;
			.log.log(severe, "init emotion with neutral value");
		}
		+emotion(like, A1, RANDOM_VALUE);
	}
	if(like(A1))
	{
		.log.log(info, "groupLike like");
		!groupLike(L, O_CL, CN, CU);
		CL = [cost(A1, I1, C1)|O_CL];
	}else
	{
		if(unlike(A1))
		{
			.log.log(info, "groupLike unlike");
			!groupLike(L, CL, CN, O_CU);
			CU = [cost(A1, I1, C1)|O_CU];			
		}else
		{
			.log.log(info, "groupLike neutral");
			!groupLike(L, CL, O_CN, CU);
			CN = [cost(A1, I1, C1)|O_CN];			
		}
	}
	.
		

 @answerInternalDataNull_emotion[atomic]
+!kqml_received(MANAGER, askOne, internalData(ID), MsgId):.my_name(MY_NAME) &
 br.uff.ic.uebdi.internalaction.getEmotions(EMO) 
<-
	I = internalData(ID, MY_NAME, -1, -1, -1, EMO, []);
	.log.log(info, "answer internalDataNull: ", I);
	.send(MANAGER, tell, I, MsgId);
	.
	
	
+!groupLike([], CL, CN, CU):true<-
	.log.log(info, "groupLike base");
	CL = [];
	CN = [];
	CU = [];
	.
	
	 {include("clientReputation.asl")}