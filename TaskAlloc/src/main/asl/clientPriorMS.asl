
//seleciona o servidor
@selectServRepPrioMS
+!selectServ(COSTS, SELECTED_COST)<-
	!rep_of_agents(COSTS);//acha a reputa��o dos agentes
	!getSets(COSTS, HP, MP, LP);
	.length(HP, HPN);
	if(HPN > 0)
	{
		.log.log(info, selectedSet(HP));
		!min_point_rep(HP, SELECTED_COST);//acha o menor custo com maior prob. de execu��o (mediaSucess) e melhor reptua��o
	}else
	{
		.length(MP, MPN);
		if(MPN > 0)
		{
			.log.log(info, selectedSet(MP));
			!min_point_rep(MP, SELECTED_COST);//acha o menor custo com maior prob. de execu��o (mediaSucess) e melhor reptua��o
		}else
		{
			.length(LP, LPN);
			if(LPN > 0)
			{
				.log.log(info, selectedSet(LP));
				!min_point_rep(LP, SELECTED_COST);//acha o menor custo com maior prob. de execu��o (mediaSucess) e melhor reptua��o
			}else
			{
				.log.log(info, selectedSet([fail]));			
				.fail;
			}
		}
	}
	.
	
+!getSets([cost(A, I , C) |COSTS], HP_COSTS, MP_COSTS, LP_COSTS):
	msuccess(_,A, MS)
<-
	if(MS > 0.6)
	{
		!getSets(COSTS, HP, MP_COSTS, LP_COSTS);
		HP_COSTS = [cost(A, I, C) | HP];
	}else
	{
		if(MS <=0.6 & MS >= 0.4)
		{
			!getSets(COSTS, HP_COSTS, MP, LP_COSTS);
			MP_COSTS = [cost(A, I, C) | MP];
		}else
		{
			!getSets(COSTS, HP_COSTS, MP_COSTS, LP);
			LP_COSTS = [cost(A, I, C) | LP];
		}
	}
	.
	
+!getSets([], HP_COSTS, MP_COSTS, LP_COSTS):true
<-
	HP_COSTS = [];
	MP_COSTS = [];
	LP_COSTS = [];
.	
	
 	
 {include("clientReputation.asl")}