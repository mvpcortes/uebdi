package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.Task;

public class MSDataSet implements XYDataset {

	private Map<String, List<Task>> map = new HashMap<String, List<Task>>();
	private List<String> listClient = null;
	public MSDataSet(List<Task> tasks) {
		for(Task t: tasks)
		{
			List<Task> list = map.get(t.getAgClient());
			if(list == null)
			{
				list = new ArrayList<Task>(tasks.size()/2);
				map.put(t.getAgClient(), list);				
			}
			list.add(t);
		}
		
		listClient = new ArrayList<String>();
		for(Map.Entry<String, List<Task>> e: map.entrySet())
		{
			listClient.add(e.getKey());
		}
	}
	
	List<Task> get(int pos)
	{
		String s = listClient.get(pos);
		return map.get(s);
	}

	@Override
	public double getYValue(int serie, int pos)
	{
		List<Task> list = get(serie); 
		if(list!=null)
		{
			return list.get(pos).getMediaSuccess();
		}
		return -1;
	}

	@Override
	public int getSeriesCount() {
		return listClient.size();
	}

	@Override
	public Comparable getSeriesKey(int pos) {
		return listClient.get(pos);
	}

	@Override
	public int indexOf(Comparable arg0) {
		int i = 0;
		for(String s: listClient)
		{
			if(s.equals(arg0))
				return i;
			i++;
		}
		
		return -1;
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {

	}

	private DatasetGroup dg = new DatasetGroup();
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
		
	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		dg = arg0;
	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int serie) {
		List<Task> list = get(serie);
		return list.size();
	}

	@Override
	public Number getX(int s, int pos) {

		return getXValue(s, pos);
	}
	@Override
	public double getXValue(int s, int pos) {
		List<Task> list = get(s);
		return list.get(pos).getIdTask();
	}

	@Override
	public Number getY(int s, int pos) {
		List<Task> list = get(s);
		return list.get(pos).getMediaSuccess();
	}
}
