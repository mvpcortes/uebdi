package br.uff.ic.taskalloc;


public class TaskManagerException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TaskManagerException(String msg, Throwable t)
	{
		super(msg, t);
	}

	public TaskManagerException(Throwable parent, String format, Object ... args)
	{
		super(String.format(format, args), parent);
	}
}
