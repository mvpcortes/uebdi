package br.uff.ic.taskalloc.exp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Runner implements Runnable {

	private File file;
	public Runner(File f)
	{
		file = f;
	}
	private void execute(File file)
	{ 
	   System.out.format("executing file: %s\n", file.toString());
	   String[] args = new String[]{"java", "-jar", "./taskalloc.jar", file.toString()};
	   Runtime runtime = Runtime.getRuntime();
       
	   try
	   {
		   Process process = runtime.exec(args);
	   
	
	       InputStream is = process.getInputStream();
	       InputStreamReader isr = new InputStreamReader(is);
	       BufferedReader br = new BufferedReader(isr);
	       String line;
	
	       System.out.printf("Output of running %s is:\n", 
	           Arrays.toString(args));

	       while ((line = br.readLine()) != null) {
	         System.out.println(line);
	       }
	       System.out.println();
	   } catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		
		execute(file);
	}

}
