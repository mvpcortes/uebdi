package br.uff.ic.taskalloc.probfunction;

import jason.asSyntax.Term;

import java.util.Iterator;

import br.uff.ic.util.TermHelper;

/**
 * Define um valor fixo da probabilidade dos agentes executarem uma tarefa
 * @author Marcos
 *
 */
public class FixProbFunction implements IProbFunction
{
	double value = 0;
	
	@Override
	public double run(String agent, int idTask) {
		return value;
	}

	@Override
	public void init(Iterable<Term> params) throws InitProbFunctionException {
		
		Iterator<Term> it = params.iterator();
		if(it.hasNext())
		{
			Term t = it.next();
			if(t.isNumeric())
			{
				double d = TermHelper.toDouble(t);
				setValue(d);
			}else
			{
				double d = TermHelper.toDouble(t);
				if(d > 0)
				{
					setValue(d);
				}else
				{
					setValue(Math.random());
				}
			}
		}
	}
	
	public String toString()
	{
		return String.format("const(%f)", this.value);
	}
	
	private void setValue(double d)
	{
		this.value = Math.min(Math.max(d, 0), 1);
	}
}
