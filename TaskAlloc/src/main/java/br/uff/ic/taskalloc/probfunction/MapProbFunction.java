package br.uff.ic.taskalloc.probfunction;

import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.uff.ic.util.TermHelper;

public class MapProbFunction implements IProbFunction {

	private Map<String, IProbFunction> mapProbFunction =  new HashMap<String, IProbFunction>();
	
	
	@Override
	public void init(Iterable<Term> params) throws InitProbFunctionException {
		for(Term l: params)
		{
			if(l.isLiteral())
			{
				proccessLiteral((Literal)l);
			}
		}
	}

	private void proccessLiteral(Literal l) {
		List<String> listAgents = getAgents(l);
		IProbFunction prob = null;
		if(listAgents != null && listAgents.size() > 0)
		{
			prob = ProbFunctionFactory.createFunc(l.getFunctor());
			
			if(prob != null)
			{	
				try {
					prob.init(getParams(l));
				} catch (InitProbFunctionException e) {
					return;
				}
				
				for(String s: listAgents)
				{
					mapProbFunction.put(s, prob);
				}
			}
		}

	}

	private List<String> getAgents(Literal l) {
		if(l.getTerms().size() <= 0)
			return null;
		
		Term t = l.getTerm(0);
		List<String> listRet = new LinkedList<String>();
		if(t.isList())
		{
			ListTerm lt = (ListTerm)t;
			Iterator<ListTerm> lti = lt.listTermIterator();
			while(lti.hasNext())
			{
				ListTerm intT =lti.next();
				if(intT.getTerm() != null)
				{
					String s = TermHelper.toString(intT.getTerm());
					listRet.add(s);
				}
			}
		}else 
		{
			listRet.add(TermHelper.toString(t));
		}
		
		if(listRet.size() == 0)
			return null;
		else
			return listRet;
	}

	private Iterable<Term> getParams(Literal l) {
		if(l.getTerms().size() <=1)
			return Collections.<Term>emptyList();
		
		final int size = l.getTerms().size()-1;
		ArrayList<Term> litRet = new ArrayList<Term>(size);
		
		for(int i = 0; i < size; i++)
		{
			litRet.add(l.getTerm(i+1));
		}
		
		return litRet;
	}

	@Override
	public double run(String agent, int idTask) {
		IProbFunction p = mapProbFunction.get(agent);
		if(p!=null)
		{
			return p.run(agent, idTask);
		}else
		{
			return -1;
		}
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("map(");
		
		for(Map.Entry<String, IProbFunction> entry: mapProbFunction.entrySet())
		{
			sb.append(entry.getKey());
			sb.append("=>");
			sb.append(entry.getValue());
			sb.append(", ");
		}
		sb.append(")");
		
		return sb.toString();
	}
}
