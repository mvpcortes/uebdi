package br.uff.ic.taskalloc.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JPanel;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.urls.StandardXYURLGenerator;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.FileHelper;
import br.uff.ic.taskalloc.Task;
import br.uff.ic.taskalloc.gui.datasets.CostDataSet;
import br.uff.ic.taskalloc.gui.datasets.DelayDataSet;
import br.uff.ic.taskalloc.gui.datasets.DevDataSet;
import br.uff.ic.taskalloc.gui.datasets.EmotionsDataset;
import br.uff.ic.taskalloc.gui.datasets.InstantPointClientDataSet;
import br.uff.ic.taskalloc.gui.datasets.MSByClientDataset;
import br.uff.ic.taskalloc.gui.datasets.MSDataSet;
import br.uff.ic.taskalloc.gui.datasets.MainDataSet;
import br.uff.ic.taskalloc.gui.datasets.MainProbDataSet;
import br.uff.ic.taskalloc.gui.datasets.MediaSucessByClient;
import br.uff.ic.taskalloc.gui.datasets.SuccessServDataSet;
import br.uff.ic.taskalloc.gui.datasets.OptimalPointDataSet;
import br.uff.ic.taskalloc.gui.datasets.PointDataSet;
import br.uff.ic.taskalloc.gui.datasets.ServDataset;
import br.uff.ic.taskalloc.gui.datasets.SuccessByClient;
import br.uff.ic.taskalloc.xml.InputXML;
public class ControllerGUI {
	private Map<String, List<Task>> loadedData = new HashMap<String, List<Task>>();
			

	private List<Task> currentList = null;
	private String    strCurrentList = "";
	
	private class ListenerManager implements ControllerGUIListener
	{
		private List<ControllerGUIListener> listListener = new LinkedList<ControllerGUIListener>();
		
		@Override
		public void fileAdded(String name) {
			for(ControllerGUIListener c: listListener)
			{
				c.fileAdded(name);
			}
		}
		
		public void add(ControllerGUIListener lis)
		{
			listListener.add(lis);
		}
	}
	
	ListenerManager listener = new ListenerManager();
	
	private List<Task> getList(String key)
	{
		List<Task> list = loadedData.get(key);
		if(list == null)
		{
			list = new LinkedList<Task>();
			loadedData.put(key, list);
			if(strCurrentList  == null || strCurrentList == "")
			{
				strCurrentList = key;
				currentList = list;
			}
		}
		return list;
	}
	
	public void addListener(ControllerGUIListener listener)
	{
		
		this.listener.add(listener);
	}
	
	
	public void setCurrentSource(String name)
	{
		strCurrentList = name;
		currentList = getList(name);
	}
	
	private String getCurrentListName()
	{
		return strCurrentList;
	}
	
	private ControllerGUI()
	{
		
	}
	
	private static ControllerGUI control = new ControllerGUI();
	
	public static ControllerGUI get()
	{
		return control;
	}
	
	public void loadFiles(File[] files)
	{
		for(File file: files)
		{
			FileInputStream input;
			
			try {
				
				final String name = file.getName();
				List<Task> list = getList(name);
				input = new FileInputStream(file);
				
				new InputXML(list, input);
				sort(list);
			
				listener.fileAdded(name);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}	
		
	}
			
	
	public void loadFile(File file)
	{
		FileInputStream input;
		try {
			
			final String name = file.getName();
			List<Task> list = getList(name);
			input = new FileInputStream(file);
			
			new InputXML(list, input);
			sort(list);
		
			listener.fileAdded(name);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	private void sort(List<Task> list) {
		Comparator<Task> comp = new Comparator<Task>() {
			@Override
			public int compare(Task o1, Task o2) {
				return o1.getIdTask() - o2.getIdTask();
			}
		};
		
		Collections.sort(list, comp);
		for(Task t: list)
		{
			t.sortAgStates();
		}
	}

	private static JFreeChart createDotChart(String title, String xLabel, String yLabel, XYDataset dataset)
	{
		 NumberAxis xAxis = new NumberAxis(xLabel);
	 
	             xAxis.setAutoRangeIncludesZero(false);
	             NumberAxis yAxis = new NumberAxis(yLabel);
	             XYItemRenderer renderer = new XYDotRenderer();
	             XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
	             plot.setOrientation(PlotOrientation.VERTICAL);
	             renderer.setBaseToolTipGenerator(new StandardXYToolTipGenerator());
	             
	            renderer.setURLGenerator(new StandardXYURLGenerator());
	     
	             JFreeChart chart = new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT,
	                     plot, true);
	             ChartFactory.getChartTheme().apply(chart);
	             return chart;
	 }
	
	public MyChartFrame createDotChartFrame(String nameFrame, String nameY, XYDataset dataset)
	{
		String nameJanela = String.format("%s(at %s)", nameY, nameFrame);
		JFreeChart xyc = createDotChart(
					nameJanela,
	                "task(i)",
	                nameY,
	                dataset // dataset, 
	              	); // urls
		
		MyChartFrame frame = new MyChartFrame(nameJanela, xyc, dataset);
	    frame.setSize(800,500);
	    frame.setVisible(true);
	    return frame;
		
		 
	}
	public MyChartFrame createDefaultFrame(String nameFrame, String nameY, XYDataset dataset)
	{
		return createDefaultFrame(nameFrame, nameY, dataset, (JPanel)null);
	}
	public MyChartFrame createDefaultFrame(String nameFrame, String nameY, XYDataset dataset, JPanel opPanel)
	{
	
		String nameJanela = String.format("%s(at %s)", nameY, nameFrame);
		JFreeChart xyc = ChartFactory.createXYLineChart(
					nameJanela,
	                "task(i)",
	                nameY,
	                dataset, // dataset, 
	                PlotOrientation.VERTICAL, // orientation, 
	                true, // legend, 
	                true, // tooltips, 
	                true); // urls
		
		MyChartFrame frame = null;
		if(opPanel!=null)
		{
			frame = new MyChartFrame(nameJanela, xyc, dataset,opPanel);
		}else
		{
			frame = new MyChartFrame(nameJanela, xyc, dataset);
		}
	    frame.setSize(800,500);
	    frame.setVisible(true);
	    return frame;
	}
	
	private MyChartFrame createEmoDefaultDrame(String nameFrame, String nameY, EmotionsDataset dataset) {
		ConfigGraph cg = new ConfigGraph(dataset);
		
		String nameJanela = String.format("%s(at %s)", nameY, nameFrame);
		JFreeChart xyc = ChartFactory.createXYLineChart(
					nameJanela,
	                "task(i)",
	                nameY,
	                dataset, // dataset, 
	                PlotOrientation.VERTICAL, // orientation, 
	                true, // legend, 
	                true, // tooltips, 
	                true); // urls
		MyChartFrame frame = new MyChartFrame(nameJanela, xyc, dataset, cg);
	    frame.setSize(800,500);
	    frame.setVisible(true);
	    return frame;
	}
	
	public MyChartFrame ShowMainDataSet()
	{
		XYDataset dataSet = new MainDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(), "Money", dataSet);
	}

	
	public MyChartFrame ShowMainProbDataSet()
	{
		XYDataset dataSet = new MainProbDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(),  "Probability", dataSet);
	}
	
	public MyChartFrame ShowCostDataSet()
	{
		XYDataset dataSet = new CostDataSet(currentList);
		//XYDataset dataSet = new SelectedServDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(), "Success", dataSet);
	}
	
	
	public MyChartFrame ShowMainDevDataSet()
	{
		XYDataset dataSet = new DevDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(), "Dev", dataSet);
	}
	public MyChartFrame ShowOptimalDataSet() {
		XYDataset dataSet = new OptimalPointDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(), "Optimal", dataSet);
	}
	
	public MyChartFrame ShowServDataset()
	{
		ServDataset dataSet = new ServDataset(currentList);
		
		return createDefaultFrame( getCurrentListName(), "Serv", dataSet, dataSet.getSymbolAxis());
	}
	
	public MyChartFrame ShowMediaSuccessByClient()
	{
		MediaSucessByClient dataSet = new MediaSucessByClient(currentList);
		
		return createDefaultFrame( getCurrentListName(), "MS by Client", dataSet);
	}
	
	private MyChartFrame createDefaultFrame(
			String nameFrame, 
			String nameY, 
			XYDataset dataset,
			SymbolAxis symbolAxis) {
		String nameJanela = String.format("%s(at %s)", nameY, nameFrame);
		JFreeChart xyc = ChartFactory.createXYLineChart(
					nameJanela,
	                "task(i)",
	                nameY,
	                dataset, // dataset, 
	                PlotOrientation.VERTICAL, // orientation, 
	                true, // legend, 
	                true, // tooltips, 
	                true); // urls
		
		Plot plot = xyc.getPlot();
		if(plot instanceof XYPlot)
		{
			XYPlot xyplot = (XYPlot) plot;
			xyplot.setRangeAxis(symbolAxis);
		}
		MyChartFrame frame = new MyChartFrame(nameJanela, xyc, dataset);
	    frame.setSize(800,500);
	    frame.setVisible(true);
	    return frame;
	}

	public MyChartFrame ShowTimeDataSet() {

		XYDataset dataSet = new DelayDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(), "Delay", dataSet);
	}
	public MyChartFrame ShowInstantOptimalDataSet() {
		XYDataset dataSet = new InstantPointClientDataSet(currentList);
		return createDefaultFrame( getCurrentListName(), "Real Optimal", dataSet);
		
	}
	public MyChartFrame ShowMediaSuccessDataSet() {
		XYDataset dataSet = new MSDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(), "MediaSuccess", dataSet);
	}
	
	public MyChartFrame ShowEmotionDataSet()
	{
		EmotionsDataset eds = new EmotionsDataset(currentList);
		return createEmoDefaultDrame(getCurrentListName(), "Emotion", eds);
	}
	


	public MyChartFrame ShowSuccessServDataset() 
	{
		SuccessServDataSet dataSet = new SuccessServDataSet(currentList);
		
		MyChartFrame m = createDefaultFrame( getCurrentListName(), "Serv Success", dataSet, dataSet.createConfig());
		ValueAxis va = m.getChart().getXYPlot().getRangeAxis(); 
		va.setAutoRange(false);
		va.setRange(-0.1, +1.1);
		
		return m;
	}
	
	public MyChartFrame showMSSelectingClient()
	{
		MSByClientDataset dataSet = new MSByClientDataset(currentList);
		
		MyChartFrame m = createDefaultFrame( getCurrentListName(), "MS by ClientxServ", dataSet, dataSet.createConfig());
		ValueAxis va = m.getChart().getXYPlot().getRangeAxis(); 
		va.setAutoRange(false);
		va.setRange(-0.1, +1.1);
		return m;
	}
	
	public MyChartFrame ShowPointDataSet() {
		XYDataset dataSet = new PointDataSet(currentList);
		
		return createDefaultFrame( getCurrentListName(), "Point", dataSet);
		
	}
	
	public MyChartFrame ShowSuccessByClientDataSet() {
		XYDataset dataSet = new SuccessByClient(currentList);
		
		return createDefaultFrame( getCurrentListName(), "SuccessByClient", dataSet);
		
	}
	
	public void exportGraphics()
	{
		
		String oldName = this.getCurrentListName();
		for(Map.Entry<String, List<Task>> entry: loadedData.entrySet())
		{
			String newName = FileHelper.getNameWithoutExtension(entry.getKey());
			this.setCurrentSource(entry.getKey());
			MyChartFrame frame = this.ShowOptimalDataSet();
			writeImage(newName+ "_optimal", frame.getChart());
			frame.setVisible(false);
			
			frame = this.ShowInstantOptimalDataSet();
			writeImage(newName+ "_point_optimal", frame.getChart());
			frame.setVisible(false);
			
			frame = this.ShowServDataset();
			writeImage(newName+ "_serv", frame.getChart());
			frame.setVisible(false);
			
			frame = this.showMSSelectingClient();
			writeImage(newName+ "_ms", frame.getChart());
			frame.setVisible(false);
		}
		
		setCurrentSource(oldName);
	}
	private void writeImage(String nameFile, JFreeChart chart) {
		File parent = new File(System.getProperty("user.dir"));
		File file = new File(parent, nameFile + ".png");
		
		try {
			ChartUtilities.saveChartAsPNG(file,chart, 800, 300);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public static OutputStream generateData(XYDataset data, OutputStream out, SymbolAxis sa)
	{
		final int serieCount = data.getSeriesCount();
		PrintStream ps = new PrintStream(out);
		
		int[] seriesCount = new int[serieCount];
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		
		/**
		 * pega o valor m�ximo
		 */
		ps.format("x\t");
		for(int i = 0; i < serieCount; i++)
		{

			seriesCount[i] = data.getItemCount(i);
			max = Math.max(max, seriesCount[i]);
			min = Math.min(min, seriesCount[i]);
			
			ps.format("%s\t", data.getSeriesKey(i));
		}		 
		ps.format("\n");
		
		for(int j = 0; j < min; j++)
		{
			final int x = (int) data.getXValue(0,j);
			ps.format("%d\t", x);
			for(int i = 0; i < serieCount; i++)
			{
				final double y = data.getYValue(i,j);
				if(sa != null)
				{
					String name = sa.getSymbols()[(int)y];
					ps.format("%s\t", name);
				}else
				{
					ps.format("%f\t", y);
				}
			}
			ps.format("\n");
		}
		
		for(int j = min; j < max; j++)
		{
			final int x;
			int pos=0;
			while(j >= seriesCount[pos])
			{
				pos++;
			}
			
			x = (int)data.getXValue(pos,j);
			ps.format("%d\t", x);
			
			for(int i = 0; i < serieCount; i++)
			{
			
				double y;
				if(j < seriesCount[i])
				{
					y = data.getYValue(i,j);
				}
				else
				{
					y = data.getYValue(i, seriesCount[i]-1);
				}
				
				
				if(sa != null)
				{
					String name = sa.getSymbols()[(int)y];
					ps.format("%s\t", name);
				}else
				{
					ps.format("%f\t", y);
				}
			}
			ps.format("\n");
		}
		
		return out;
	
	}
	
	public static OutputStream generateDataSimple(XYDataset data, OutputStream out, SymbolAxis sa)
	{
		final int serieCount = data.getSeriesCount();
		PrintStream ps = new PrintStream(out);
		
		for(int i = 0; i < serieCount; i++)
		{
			ps.append("x\t");
			ps.append(data.getSeriesKey(i).toString());
			ps.append("\n");
			for(int j = 0; j < data.getItemCount(i); j++)
			{
				ps.format("%s\t", data.getX(i, j));
				ps.format("%s\n", data.getY(i, j));
			}
			ps.append("\n");
		}
		return out;
	}

	public int getLoadedFiles() {
		return this.loadedData.size();
	}
	
	public int getLoadedFilePos(String name)
	{
		int pos = 0;
		for(Entry<String, List<Task>> e: loadedData.entrySet())
		{
			if(name.equals(e.getKey()))
				return pos;
			pos++;
		}
		
		return -1;
	}
	public String getLoadedFile(int id)
	{
		for(Entry<String, List<Task>> e: loadedData.entrySet())
		{
			if(id <= 0)
				return e.getKey();
			id--;
		}
		
		return "";
	}

	
}
