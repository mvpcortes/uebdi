package br.uff.ic.taskalloc.exp;

class ValueMediaDev 
{
	private double mediaSum = 0.0;
	
	private int itemCount = 0;
	
	private double devSum;
	
	private int idTask = -1;
	
	public ValueMediaDev(int id)
	{
		idTask = id;
	}
	
	public void addItemMedia(double value)
	{
		mediaSum +=value;
		itemCount++;
	}
	
	public void addItemDev(double value)
	{
		devSum += Math.pow(value-getMedia(), 2.0);
	}
	
	public double getMedia()
	{
		if(itemCount <= 0)
			return 0;
		
		return mediaSum/itemCount;
	}
	
	public double getDev()
	{
		return Math.sqrt(devSum/((double)(itemCount-1)));
	}
	
	public int getId()
	{
		return idTask;
	}
	
}