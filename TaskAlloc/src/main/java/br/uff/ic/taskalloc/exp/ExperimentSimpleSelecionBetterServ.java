package br.uff.ic.taskalloc.exp;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


import br.uff.ic.taskalloc.Task;
import br.uff.ic.taskalloc.xml.InputXML;
import br.uff.ic.util.Pair;

public class ExperimentSimpleSelecionBetterServ {
	
	public static void main(String[] args) {
		
		String strCurrent = System.getProperty("user.dir");
		File fileCurrent = new File(strCurrent);
		System.out.format("user.dir: %s\n", strCurrent);
		final int count = 30; 
		File pathDir = new File(fileCurrent, "mas2j");
		pathDir.mkdir();
		List<File> fileMas = new LinkedList<File>();
		for(int i = 0; i < count; i++)
		{
			File f;
			f = createMAS2J(pathDir, 5, 5, "clientEmo", generateNeutralEmotions(5), i); 		
			fileMas.add(f);
			
			//
			f = createMAS2J(pathDir, 5, 5, "clientEmo", generateAscEmotions(5), i, "asc");
			fileMas.add(f);
			
			//
			f = createMAS2J(pathDir, 5, 5, "clientEmo", generateDecEmotions(5), i, "dec");
			fileMas.add(f);
			
			//
			
			f = createMAS2J(pathDir, 5, 5, "clientEmo", "initRandomEmotion(2)", i, "random");
			fileMas.add(f);
			
			//
			f = createMAS2J(pathDir, 5, 5, "clientReputation", "", i);						
			fileMas.add(f);
			
			//
			f = createMAS2J(pathDir, 5, 5, "clientRational", "", i);							
			fileMas.add(f);
			
		}
		
		execute(fileMas);
		
		File[] vecEmo 	 = getXMLFiles(fileCurrent, new String[]{"Emo", ".xml"}, new String[]{"dec", "asc", "random"});
		File[] vecEmoAsc = getXMLFiles(fileCurrent, new String[]{"Emo", ".xml", "asc"	}, new String[]{"dec", "random"});
		File[] vecEmoDec = getXMLFiles(fileCurrent, new String[]{"Emo", ".xml", "dec"	}, new String[]{"asc", "random"});
		File[] vecEmoRan = getXMLFiles(fileCurrent, new String[]{"Emo", ".xml", "random"}, new String[]{"dec", "asc"});
		File[] vecRep	 = getXMLFiles(fileCurrent, new String[]{"Reputation", ".xml"}, new String[] {});
		File[] vecRat	 = getXMLFiles(fileCurrent, new String[]{"Rational", ".xml"}, new String[] {});
		
		calcMedia(vecEmo	, "emoMedia.txt");
		calcMedia(vecEmoAsc	, "emoAscMedia.txt");
		calcMedia(vecEmoDec , "emoDecMedia.txt");
		calcMedia(vecEmoRan , "emoRanMedia.txt");
		calcMedia(vecRat	, "ratMedia.txt");
		calcMedia(vecRep	, "repMedia.txt");
		
	}
	
	private static void calcMedia(File[] arrayFile, String pathOut) 
	{
		System.out.format("creating mediaFile %s\n", pathOut);
		List<Task> tempListTask = new ArrayList<Task>();
		HashMap<Integer, ValueMediaDev> mapValues = new HashMap<Integer, ValueMediaDev>();
		List<Pair<Integer, Double>> tempListPoint = new LinkedList<Pair<Integer, Double>>();
		for(File f: arrayFile)
		{
			try {
				tempListTask.clear();
			
				InputXML ix = new InputXML(tempListTask, new FileInputStream(f), false);
				for(Task t: tempListTask)
				{
					final int id = t.getIdTask();
					final double point = t.getOptimalPointSelServ();
					ValueMediaDev vmd = mapValues.get(id);
					if(vmd == null)
					{
						vmd = new ValueMediaDev(id);
						mapValues.put(id, vmd);
					}
					tempListPoint.add(new Pair<Integer, Double>(id, point));
					
					vmd.addItemMedia(point);
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
			
		for(Pair<Integer, Double> pair: tempListPoint)
		{
			final int id = pair.getX();
			final double point = pair.getY(); 
			ValueMediaDev vmd = mapValues.get(id);
			
			if(vmd == null)
				System.out.println("problema na hora de fazer o desvio");
			else
				vmd.addItemDev(point);
		}
		
		
		//agora gera a tabela
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(pathOut);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		PrintStream ps = new PrintStream(os);
		ps.println();
		
		//System.out.println()
	
		List<ValueMediaDev> temp = new ArrayList<ValueMediaDev>(mapValues.entrySet().size());
		for(Map.Entry<Integer, ValueMediaDev> e: mapValues.entrySet())
		{
			temp.add(e.getValue());
		}
		
		java.util.Collections.sort(temp,
					new Comparator<ValueMediaDev>()
					{	
						@Override
						public int compare(
								ValueMediaDev arg0, 
								ValueMediaDev arg1) 
						{
							return arg0.getId() - arg1.getId();
						}
					}
				);
		
		for(ValueMediaDev vmd: temp)
		{			
			ps.format("%f\t%f\n", vmd.getMedia(), vmd.getDev());
		}
		ps.println();
		ps.close();
	}





	private static File[] getXMLFiles(File source, final String[] contain, final String[] notContain) {
		return source.listFiles(new FileFilter(){

			@Override
			public boolean accept(File arg0) {
				String name = arg0.getName();
				
				for(String c: contain)
				{
					if(name.indexOf(c) < 0)
						return false;
				}
				
				for(String c: notContain)
				{
					if(name.indexOf(c) >= 0)
						return false;
				}
				
				return true;
			}
			
		}
		);

	}
		
	private static File[] getXMLFiles(File source, final String pattern) {
		return source.listFiles(new FileFilter(){

			@Override
			public boolean accept(File arg0) {
				return arg0.getName().matches(pattern);
				
			}
			
		}
		);
	}

	private static void execute(List<File> fileMas) {
		System.out.format("executing files\n");
		int processors = Runtime.getRuntime().availableProcessors();
		int threadPoolSize = (int)Math.round(processors*1.5);
	    
	    ExecutorService tpes =
	            Executors.newFixedThreadPool(threadPoolSize);
	        
		for(File f: fileMas)
		{
			Runner r = new Runner(f);
			tpes.execute(r);
		}
		
		tpes.shutdown();
		try {
			tpes.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String generateAscEmotions(final int servCount) {
		List<String> listServ = generateServList(servCount);
		List<String> listBeliefs = new LinkedList<String>();
		int i = 1;
		for(String s: listServ)
		{
			double d = ((double)i-1)/((double)(servCount-1));
			int value = (int) Math.round(d*10.00);
			String strEmo = String.format(Locale.US, "emotion(like, %s, %d)", s, value);
			listBeliefs.add(strEmo);
			i++;
		}
		
		return listToString(listBeliefs);
	}
	
	private static String generateDecEmotions(final int servCount) {
		List<String> listServ = generateServList(servCount);
		List<String> listBeliefs = new LinkedList<String>();
		int i = 1;
		for(String s: listServ)
		{
			double d = 1.0-((double)i-1)/((double)(servCount-1));
			int value = (int) Math.round(d*10.00);
			String strEmo = String.format(Locale.US, "emotion(like, %s, %d)", s, value);
			listBeliefs.add(strEmo);
			i++;
		}
		
		return listToString(listBeliefs);
	}

	private static String generateNeutralEmotions(int servCount)
	{
		List<String> listServ = generateServList(servCount);
		List<String> listBeliefs = new LinkedList<String>();
		for(String s: listServ)
		{
			listBeliefs.add(String.format(Locale.US, "emotion(like, %s, 5)", s));
		}
		return listToString(listBeliefs);
		
	}
	
	private static <T> String listToString(List<T> list)
	{
		if(list.size() == 1)
		{
			return list.get(0).toString();
		}else if(list.size() > 1)
		{
			StringBuilder sb = new StringBuilder();
			Iterator<T> it = list.iterator();
			T s = it.next();
			sb.append(s);
			while(it.hasNext())
			{
				sb.append(", ");
				sb.append(it.next().toString());	
			}
			return sb.toString();
		}else
		{
			return "";
		}
	}
	private static List<String> generateServList(int servCount)
	{
		List<String> list = new LinkedList<String>();
		if(servCount == 0)
		{
			list.add("serv");
		}else if(servCount > 0)
		{
			for(int i = 1; i <=servCount; i++)
			{
				list.add(String.format("serv%d", i));
			}
		}
		
		return list;
	}
	
	private static File createMAS2J(File pathDir, int clientCount, int servCount, String nameClient, String beliefs, int id) {
		return createMAS2J(pathDir, clientCount, servCount, nameClient, beliefs, id, "") ;
	}
	private static File createMAS2J(File pathDir, int clientCount, int servCount, String nameClient, String beliefs, int id, String mod) {
		final String sname;
		if(mod !=  null && !mod.equals(""))
			sname = String.format("%s_%dx%d_%s_%02d", nameClient, clientCount, servCount, mod, id);
		else
			sname = String.format("%s_%dx%d_%02d", nameClient, clientCount, servCount, id);
		
		final int taskCount = 50;
		try {
			File f = null;
			PrintStream ps = new PrintStream(
							new FileOutputStream(
									f = new File(pathDir, sname + ".mas2j")
							)
					);
			ps.format("MAS %s{\n", sname);
			ps.println("infrastructure: Centralised");
			ps.println("environment: br.uff.ic.taskalloc.EnvironmentTaskAlloc");
			ps.println("(");
			ps.format("out(\"./%s.xml\"),\n", sname);
			ps.format("log(\"./%s.log\", warning)\n", sname);
			ps.println(")");
			ps.println();
			
			ps.println("agents:");
			if(nameClient.indexOf("Emo") > 0)
			{
				ps.format("client\t%s.asl	[verbose=1, emotionalDyn=\"br.uff.ic.taskalloc.EmotionDynEmo\", beliefs=\"%s\"] #%d agentClass br.uff.ic.uebdi.WrapperJasonUEBDI;\n", nameClient, beliefs, clientCount);
			}else
			{	
				ps.format("client\t%s.asl	[verbose=1] #%d;\n", nameClient, clientCount);
			}
			ps.format("manager\tmanager.asl\t[verbose=1, goals=\"alloc_task_list([alloc_a(1, %d)])\" ];", taskCount-1);
			ps.println();
			for(int i = 1; i <=servCount; i++)
			{
				double p = ((double)i-1)/(servCount-1);
				ps.format(Locale.US, "serv%d\tservBase.asl\t[verbose=1, beliefs=\"costRange(1,%d, 1), probRange(1,%d,%f)\"];", i, taskCount, taskCount,  p);
				ps.println();
			}
			
			ps.println();
			ps.println("aslSourcePath: \"src/main/asl\";");
			ps.println("}");
			ps.close();
			return f;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	
	}

}
