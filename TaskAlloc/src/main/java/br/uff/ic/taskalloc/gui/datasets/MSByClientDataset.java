package br.uff.ic.taskalloc.gui.datasets;

import java.util.List;

import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.Task;

public class MSByClientDataset extends MappedDataset {
	
	public MSByClientDataset(List<Task> list)
	{
		super();
		for(Task t: list)
		{
			String c= t.getAgClient();
			MSByClientOnlyDataset d = (MSByClientOnlyDataset) get(c);
			if(d == null)
			{
				d = new MSByClientOnlyDataset(c);
				this.put(c, d);
			}
			d.addTask(t);
		}
		
		for(XYDataset m: this.getDataSets())
		{
			MSByClientOnlyDataset mm = (MSByClientOnlyDataset)m;
			mm.sortData();
		}
	}
}
