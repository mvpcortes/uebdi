package br.uff.ic.taskalloc;


import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;
import br.uff.ic.util.TermHelper;

@SuppressWarnings("serial")
public class minCost extends DefaultInternalAction {

    @Override
    public int getMinArgs(){return 2;}
    
    @Override
    public int getMaxArgs(){return getMinArgs();}
    
    @Override
    public void checkArguments(Term[] terms)throws JasonException 
    {
	if(!terms[0].isList()) 
	    throw JasonException.createWrongArgument(this,"first term is not a list");
	
	if(!(terms[1].isVar() && ((VarTerm)terms[1]).getValue()!= null))
	    throw JasonException.createWrongArgument(this,"second term is not a unnamedvar");
    }
    
    
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

	// execute the internal action
        ts.getAg().getLogger().fine("executing internal action 'br.uff.ic.taskalloc.validCosts'");
        
        ListTerm listCost = (ListTerm)args[0];
        
        
      double minValue = Double.MAX_VALUE;
        Term minTerm = null;
        for(Term t: listCost)
        {
            if(t.isLiteral())
            {
        	Literal lcost = (Literal)t;
        	if("cost".equals(lcost.getFunctor()) & lcost.getTerms().size() == 3)
        	{
        	    double costValue = TermHelper.getDouble(lcost, 2);
        	    if(costValue <= minValue)
        	    {
        		minTerm = lcost;
        		minValue = costValue;
        	    }        		
        	}
            }
        }
        
        if(minTerm !=null)
            return un.unifies(args[2], minTerm);
        else
            return false;
            
    }
}