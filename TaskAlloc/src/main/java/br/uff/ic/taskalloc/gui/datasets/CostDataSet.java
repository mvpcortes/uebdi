package br.uff.ic.taskalloc.gui.datasets;

import java.util.List;

import br.uff.ic.taskalloc.AgState;
import br.uff.ic.taskalloc.Task;

public class CostDataSet extends MainDataSet {

	public CostDataSet(List<Task> tasks) {
		super(tasks);
	}

	@Override
	public double getYValue(int serie, int pos)
	{
		Task t = getTasks().get(pos);
		AgState ag = t.getAg(serie);
		return ag.getCost();
	}
}
