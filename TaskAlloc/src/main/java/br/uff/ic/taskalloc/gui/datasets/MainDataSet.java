package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.AgState;
import br.uff.ic.taskalloc.Task;

public class MainDataSet implements XYDataset {

	private List<Task> coll;
	
	private Map<String, Integer> agNames;
	private List<String> agListNames;
	
	protected List<Task> getTasks()
	{
		return coll;
	}
	public MainDataSet(List<Task> tasks) {
		coll = tasks;
		if(tasks.size() > 0)
		{
			Task t = tasks.iterator().next();
			agNames= new HashMap<String, Integer>();
			agListNames = new ArrayList<String>(t.getAgStatesCount());
			Iterator<AgState> it = t.getAgStates().iterator();
			int i = 0;
			while(it.hasNext())
			{
				String name = it.next().getAgent(); 
				agNames.put(name, i);
				agListNames.add(name);
				i++;
			}
		}
	}

	@Override
	public int getSeriesCount() {
		return agListNames.size();
	}

	@Override
	public Comparable getSeriesKey(int arg0) {

		return agListNames.get(arg0);
	}

	@Override
	public int indexOf(Comparable arg0) {	
		Integer i = agNames.get(arg0);
		if(i!= null)
			return i;
		else
			return -1;
	}

	
	private DatasetGroup dg = new DatasetGroup();
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	

	@Override
	public void setGroup(DatasetGroup arg0) {
		dg = arg0;
	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int arg0) {
		return getTasks().size();
	}

	@Override
	public Number getX(int serie, int item) {
		return getXValue(serie, item);
	}

	@Override
	public double getXValue(int serie, int item)
	{
		Task t = getTasks().get(item);
		return t.getIdTask();
	}

	@Override
	public Number getY(int serie, int item)
	{
		return getYValue(serie, item);
	}

	@Override
	public double getYValue(int serie, int item)
	{
		Task t = getTasks().get(item);
		AgState ag = t.getAg(serie);
		return ag.getMoney();
	}
	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		
	}
	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
		
	}

}
