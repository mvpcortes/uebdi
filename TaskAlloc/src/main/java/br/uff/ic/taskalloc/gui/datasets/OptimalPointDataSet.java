package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;


import br.uff.ic.taskalloc.Task;
import br.uff.ic.util.Pair;

public class OptimalPointDataSet implements XYDataset {

	class Serie
	{
		private String  agName = "";
		private int		id  = 0;
		private List<Pair<Integer, Double>> listPoints = new ArrayList<Pair<Integer, Double>>();
		
		public Serie(String a, int _id)
		{
			agName = a;
			id = _id;
			
		}
		
		public void addPoint(int x, double y)
		{
			listPoints.add(new Pair<Integer, Double>(x,y));
		}
		
		public final Pair<Integer, Double> getPoint(int pos)
		{
			return listPoints.get(pos);
		}
		
		public int size()
		{
			return listPoints.size();
		}
		
		public final String getAgName()
		{
			return agName;
		}

		public void setId(int i) {
			id = i;
		}
		public int getId()
		{
			return id;
		}
	}
	
	private Map<String, Serie> mapSerie  = new TreeMap<String, Serie>();
	
	private List<String>       listName  = new ArrayList<String>();
	
	
	public OptimalPointDataSet(Collection<Task> coll)
	{
		Map<String, Pair<Integer, Double>> mapSuccess 	= new HashMap<String, Pair<Integer, Double>>();
		
		for(Task t: coll)
		{
			if(t.getSuccess() !=null)
			{
				final String agClient = t.getAgClient();
				Pair<Integer, Double> pair = mapSuccess.get(agClient);
				if(pair == null)
				{
					pair = new Pair<Integer, Double>(0,0.0);
				}
				
				int count = pair.getX() + 1;
				double value = pair.getY() + t.getOptimalPointSelServ();
				double insertValue = value/count;
				mapSuccess.put(agClient, new Pair<Integer, Double>(count, value));
				
				Serie serie = this.getSerie(agClient);
				serie.addPoint(t.getIdTask(), insertValue);
			}
				
				
		}
	}
	protected void addSerie(Serie s)
	{
		mapSerie.put(s.getAgName(), s);
		listName.add(s.getAgName());
		s.setId(listName.size()-1);
	}
	
	protected Serie getSerie(String ag)
	{
		Serie s = mapSerie.get(ag);
		if(s == null)
		{
			s = new Serie(ag, 0);
			addSerie(s);
		}
		return s;
		
	}
	
	@Override
	public int getSeriesCount() {
		return listName.size();
	}

	@Override
	public Comparable getSeriesKey(int pos) 
	{
		return listName.get(pos);
	}

	@Override
	public int indexOf(Comparable arg0) {
		Serie s = mapSerie.get(arg0);
		if(s!= null)
		{
			return s.getId();
		}
		return -1;
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {

	}

	DatasetGroup dg = new DatasetGroup();
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {

	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		dg = arg0;

	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int arg0) {
		String s = listName.get(arg0);
		if(s==null)
			return 0;
		
		Serie serie = mapSerie.get(s);
		if(serie == null)
			return 0;
		
		return serie.size();
	}

	@Override
	public Number getX(int serie, int pos) {
		return getXValue(serie, pos);
	}

	@Override
	public double getXValue(int iserie, int pos) {
		String s = listName.get(iserie);
		if(s==null)
			return -1;
		
		Serie serie = mapSerie.get(s);
		if(serie == null)
			return -1;
		
		return serie.getPoint(pos).getX();
		
	}

	@Override
	public Number getY(int serie, int pos) {
		return getYValue(serie, pos);
	}

	@Override
	public double getYValue(int iserie, int pos) {
		String s = listName.get(iserie);
		if(s==null)
			return -1;
		
		Serie serie = mapSerie.get(s);
		if(serie == null)
			return -1;
		
		return serie.getPoint(pos).getY();
	}

}
