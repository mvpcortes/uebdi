package br.uff.ic.taskalloc.gui.datasets;

import java.util.List;
import br.uff.ic.taskalloc.Task;

public class DelayDataSet extends DevDataSet {
	
	public DelayDataSet(List<Task> tasks) {
		super(tasks);
	}

	@Override
	public double getYValue(int serie, int pos)
	{
		return getTasks().get(pos).getLifeTime();
	}
	
}
