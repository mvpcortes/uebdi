package br.uff.ic.taskalloc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.uff.ic.taskalloc.gui.ControllerGUI;
import br.uff.ic.taskalloc.xml.InputXML;
import br.uff.ic.util.Pair;

public class TiraMedia {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Map<File, List<Task>> map = new HashMap<File, List<Task>>();
		String strCurrent = System.getProperty("user.dir");
		File fileCurrent  = new File(strCurrent);
		if(fileCurrent.exists() && fileCurrent.isDirectory())
		{
			File[] children = fileCurrent.listFiles();
			for(File f: children)
			{
				if(f.isFile())
				{
					String ext = FileHelper.getExtension(f.getName());
					if("xml".equals(ext))
					{
						FileInputStream input;
						try {
							input = new FileInputStream(f);
						
							List<Task> list = new ArrayList<Task>(500);
							new InputXML(list, input);
							sort(list);
							printPoints(f.getName(), list, System.out);
							//map.put(f, new ArrayList<Task>(50));
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
			}
			/*
			for(Map.Entry<File, List<Task>> e: map.entrySet())
			{
				FileInputStream input;
				try {
					File file = e.getKey();
					input = new FileInputStream(file);
					List<Task> list = e.getValue();
					new InputXML(list, input);
					sort(list);
					//printMediaByTask(file.getName(), list, System.out);
					//printPoints(file.getName(), list, System.out);
					
				} catch (FileNotFoundException ee) {
					ee.printStackTrace();
				}
			}*/
		}
	}
	
	private static void printPoints(String name, List<Task> list, OutputStream out)
	{
		PrintStream ps = new PrintStream(out);
		ps.format("\n%s\n", name);
		int actual = 0;
		//primeiro loop para imprimmir nomes
		ps.format("x\t");
		for(Task t: list)
		{
			if(t.getIdTask() >1)
			{
				break;
			}
			ps.format("%s\t", t.getAgClient());			
		}
		for(Task t: list)
		{
			if(actual < t.getIdTask())
			{
				ps.format("\n%d\t", t.getIdTask());
				actual = t.getIdTask();
			}
			
			if(t.getIdTask() > 10)
			{
				break;
			}
			ps.format("%f\t", t.getOptimalPointSelServ());
		}
		ps.print("\n");
	}
	private static void printMediaByTask(String name, List<Task> list, OutputStream out) {
		Map<String, Pair<Double, Integer>> mapData = new HashMap<String, Pair<Double, Integer>>();
		PrintStream ps = new PrintStream(out);
		ps.format("\n%s\n", name);
		Map<Integer, Pair<Integer, Double>> map = new HashMap<Integer, Pair<Integer,Double>>(); 
		for(Task t: list)
		{
			Pair<Integer, Double> pair = map.get(t.getIdTask());
			if(pair == null)
			{
				pair = new Pair<Integer, Double>(1, t.getOptimalPointSelServ());
			}else
			{
				pair = new Pair<Integer, Double>(pair.getX() +1, pair.getY() + t.getOptimalPointSelServ());
			}
			map.put(t.getIdTask(), pair);
		}
		
		for(Map.Entry<Integer, Pair<Integer, Double>>e : map.entrySet())
		{
			Pair<Integer, Double> pair = e.getValue();
			ps.format("%d\t%f\n", e.getKey(), pair.getY()/pair.getX().doubleValue());
		}
		
	}

		private static void sort(List<Task> list) {
			Comparator<Task> comp = new Comparator<Task>() {
				@Override
				public int compare(Task o1, Task o2) {
					int dif1 =  o1.getIdTask() - o2.getIdTask();
					int dif2 =  o1.getAgClient().compareTo(o2.getAgClient());
					if(dif1 > 0)
					{
						return +1;
					}else if(dif1 < 0)
					{
						return -1;
					}else
					{
						if(dif2 > 0)
						{
							return +1;
						}else
						{
							if(dif2 < 0)
							{
								return -1;
							}else
							{
								return 0;
							}
						}
					}
				}				
			};
			
			Collections.sort(list, comp);
			for(Task t: list)
			{
				t.sortAgStates();
			}
		}
}
