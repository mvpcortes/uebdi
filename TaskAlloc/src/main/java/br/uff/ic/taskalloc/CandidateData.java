package br.uff.ic.taskalloc;

public class CandidateData {
	
	private AgState ags;
	public CandidateData(AgState _ags)
	{
		ags = _ags;
	}
	public String getName()
	{
		return ags.getAgent();
	}
	
	public long getCost()
	{
		return ags.getCost();
	}
	
	public double getProb()
	{
		return ags.getP();
	}
}
