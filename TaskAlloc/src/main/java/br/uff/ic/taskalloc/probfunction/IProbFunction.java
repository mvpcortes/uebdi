package br.uff.ic.taskalloc.probfunction;


import jason.asSyntax.Term;

/**
 * Define uma fun��o p(agent, task)
 * @author Marcos
 *
 */
public interface IProbFunction {

	/**
	 * Inicia a fun��o com dados passados pelo usu�rio
	 * @param params
	 */
	void init(Iterable<Term> params) throws InitProbFunctionException;
	/**
	 * Executa a fun��o
	 * @param agent ID do agente
	 * @param idTask instante da tarefa
	 * @return o valor da probabilidade
	 */
	
	double run(String agent, int idTask);
}
