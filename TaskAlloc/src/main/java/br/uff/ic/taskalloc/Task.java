package br.uff.ic.taskalloc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Representa uma tarefa no processo de aloca��o
 * @author Marcos
 *
 */
public class Task {
	
	/**
	 * Determina os estados da tarefa. isto impedir� que o usu�rio do servi�o execute uma etapa antes da outra
	 * 1) create
	 * 2) register to client
	 * 3) alloc to serv
	 * 4) execute
	 * 5) finish
	 * 6) unregister
	 */
	public enum Stage
	{
		created,
		registered,
		allocated,
		executed,
		finished,
		unregisted;
	}
	/**
	 * Instante da tarefa (ou ID)
	 */
	private int idTask =-1;//1 in create
	
	/**Se houve sucesso ou n�o (ou ainda n�o foi executada) na execu��o da tarefa
	 * 
	 */
	private Boolean success = null;//4 in execute

	/**
	 * Lista contendo os agentes que foram candidatos desta tarefa.
	 */
	private List<String> listAgCandidate = new LinkedList<String>();
	
	/**
	 * Estado da tarefa
	 * 
	 */
	private Stage stage = Stage.created;
	/**
	 * Nome do cliente
	 */
	private String client = "-";//2 in register
	
	/**
	 * Nome do serv
	 */
	private String serv   = "-";//3 in alloc
		
	/**
	 * Lista que indica o estado do agente ap�s sua execu��o
	 */
	private List<AgState> listAgState = new ArrayList<AgState>();//6 unregister
	
	private Map<String, AgState> mapAgState = new HashMap<String, AgState>();
	
	/**
	 * M�dia de sucesso do agente cliente ap�s a execu��o 
	 */
	private double mediaSuccess = -1; //5 in finish
	
	/**
	 * M�dia de sucesso do agente cliente ap�s a execu��o
	 * 
	 */
	private double mediaRep		= -1;//5 in finish
	
	/**
	 * Instante de constru��o do objeto
	 */
	private long createTime		= 0;
	
	/**
	 * Instante de devolu��o (unregister) do objeto
	 */
	private long destroyTime    = 0;
	
	/**
	 * Pontua��o acumulada
	 */
	private double accumPoint   = 0;
	
	
	/**
	 * Construtor
	 * @param _idTask o id da tarefa
	 */
	public Task(int _idTask)
	{
		idTask = _idTask;
		createTime = System.nanoTime();
	}
	
	public Task(int _id, boolean unregisted)
	{
		this(_id);
		if(unregisted)
			setStage(Stage.unregisted);
	}
	
	void setStage(Stage s)
	{
		stage = s;
		switch(s)
		{
			case unregisted:
				destroyTime = System.nanoTime()-createTime;
			break;
		}
	}
	
	Stage getStage()
	{
		return stage;
	}
	
	public String getAgClient() {
		return client;
	}
	
	
	public String getAgServ() {
		return serv;
	}
	
	private static Stack<Long> stackTempMoney = new Stack<Long>();
	
	public double getDesvioMonetario()
	{
		
		
		synchronized(stackTempMoney)
		{
			stackTempMoney.ensureCapacity(listAgState.size());
			double media = 0;
			for(AgState as: listAgState)
			{
				media += as.getMoney();
				stackTempMoney.push(as.getMoney());
			}
			media /=  listAgState.size();
			
			double dev = 0;
			long size = stackTempMoney.size();
			while(stackTempMoney.size() > 0)
			{
				Long temp = stackTempMoney.pop();
				dev += Math.pow(temp - media, 2);
			}
			
			dev/=size-1;
			dev = Math.sqrt(dev);
			return dev;
		}
	}
	
	public int getIdTask() {
		return idTask;
	}

	
	public Boolean getSuccess() {
		return success;
	}
	
	public void setAccumPoint(double d)
	{
		accumPoint = d;
	}
	
	public double getAccumPoint()
	{
		return accumPoint;
	}
	
	public String getStrSuccess()
	{
		if(success == null)
			return "null";
		else 
			return success?"1":"0";
			
		
	}
	
	public void addAgCandidate(String name)
	{
		listAgCandidate.add(name);
	}
	
	public final Collection<String> getAgCandidate()
	{
		return listAgCandidate;
	}
	public  void setAgClient(String agClient) {
		client = agClient;
	}
	
	
	public final Iterable<AgState> getAgStates()
	{
		return listAgState;		
	}
	
	
	public  void setAgServ(String agServ) {
		serv = agServ;
	}

	public  void setSuccess(Boolean s) {
		this.success = s;
	}
	
	@Override
	public String toString()
	{
		return String.format("task(%d), client(%s), serv(%s), success(%s), mediaSuccess(%f), mediaRep(%f), agState(%s) ",
				getIdTask(),
				getAgClient(),
				getAgServ(),
				getStrSuccess(),
				getMediaSuccess(),
				getMediaRep(),
				getAgStates().toString()
				);
	}

	public final int getAgStateCount() {
		return listAgState.size();
	}

	public  void addAgState(AgState agState) {
		this.listAgState.add(agState);
		this.mapAgState.put(agState.getAgent(), agState);
	}
	
	public  void clearAgState()
	{
		this.listAgState.clear();
		this.mapAgState.clear();
	}

	public AgState getClientState() {
		AgState ag = mapAgState.get(getAgClient());
		
		return ag;
	}

	public double getMediaSuccess() {
		return mediaSuccess;
	}

	public void setMediaSuccess(double mediaSuccess) {
		this.mediaSuccess = mediaSuccess;
	}

	public double getMediaRep() {
		return mediaRep;
	}

	public void setMediaRep(double mediaRep) {
		this.mediaRep = mediaRep;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof Integer)
		{
			Integer i = (Integer)o;
			
			return i.equals(this.getIdTask());
		}else if(o instanceof Task)
		{
			return ((Task)o).getIdTask() == getIdTask();
		}else
			return false;
	}
	
	public int hashCode()
	{
		return getIdTask();
	}



	public int getAgStatesCount() {
		return listAgState.size();
	}



	public final AgState getServState() {
		return mapAgState.get(getAgServ());
	}
	
	public long getLifeTime()
	{
		if(destroyTime > 0)
			return destroyTime;
		else
			return System.nanoTime()-createTime;
	}

	public AgState getAg(int item) {
		return this.listAgState.get(item);
	}

	public void setIdTask(int id) {
		idTask = id;		
	}

	public void sortAgStates() {
		Collections.sort(listAgState, new Comparator<AgState>()
				{

					@Override
					public int compare(AgState o1, AgState o2) {
						return o1.getAgent().compareTo(o2.getAgent());
					}
			
				}
		);
	}
	
	
	/**
	 * 
	 * @return
	 */
	public synchronized double getOptimalPointSelServ()
	{
		ArrayList<AgState> list = new ArrayList<AgState>(this.getAgCandidate().size());
		for(String name: getAgCandidate())
		{	
			AgState ag = getAgState(name);
			if(ag!=null)
			list.add(ag);
		}
		
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		
		
		for(AgState ag: list)
		{
			double point = ag.getPoint();
				min = Math.min(point, min);
			max = Math.max(point, max);
		}
		AgState serv = getServState();
		
		double value = (serv.getPoint() - min)/(max-min);
		return 1-value;
	}
	
	
	private AgState getAgState(String name) {
		return mapAgState.get(name);
	}	
	
}
