package br.uff.ic.taskalloc.probfunction;

import jason.asSyntax.Term;

import java.util.Iterator;

import br.uff.ic.util.TermHelper;

/**
 * Implementa uma fun��o p semiconstante baseada numa fun��o qualquer com dom�nio [0..1].
 * @author Marcos
 *
 */
abstract class SemiConstantProbFunction implements IProbFunction {

	private int base = 10;
	
	private int delta= 100;
	
	
	
	/**
	 * [0] = o valor de delta
	 * [1] = o valor de base
	 */
	@Override
	public void init(Iterable<Term> params) throws InitProbFunctionException {
		Iterator<Term> it = params.iterator();
		
		if(!it.hasNext())
			return;
		
		Term t = it.next();
		
		double d = TermHelper.toDouble(t);
		if(d > 0)
			setDelta((int)Math.floor(d));
		
		//ver a base agora
		if(!it.hasNext())
			return;
		
		t = it.next();
		d = TermHelper.toDouble(t);
		if(d > 0)
			setBase((int)Math.floor(d));
	}

	@Override
	public double run(String agent, int idTask) {
		long ee = (idTask) % (getBase()*getDelta());
		return fFunc(agent, ee);
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getDelta() {
		return delta;
	}

	public void setDelta(int delta) {
		this.delta = delta;
	}
	
	
	protected abstract double realFunc(String agent, double t);
	
	protected double fFunc(String agent, long e)
	{
		double temp = Math.floor((double)e/(double)getDelta())/getBase();
		return realFunc(agent, temp);
		
	}
	
	@Override
	public String toString()
	{
		return String.format("semiconst(%d, %d)", base, delta);
	}
}
