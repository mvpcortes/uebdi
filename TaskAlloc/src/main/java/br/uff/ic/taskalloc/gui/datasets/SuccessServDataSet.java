package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.Task;
import br.uff.ic.taskalloc.gui.GenericConfig;
import br.uff.ic.util.Pair;

public class SuccessServDataSet extends MappedDataset 
{
	private class SuccessServByClientDataset implements XYDataset{

		String clientName = "";
		Map<String, List<Pair<Integer, Boolean>>> map = new HashMap<String, List<Pair<Integer, Boolean>>>();
		List<String> listNames = new ArrayList<String>();
		
		public String getClientName()
		{
			return clientName;
		}
		public SuccessServByClientDataset(String cn)
		{
			clientName = cn;
		}
		private void put(String nameServ, int task, boolean success)
		{
			List<Pair<Integer, Boolean>> list = getList(nameServ);
			list.add(new Pair<Integer, Boolean>(task, success));
		}
		
		private List<Pair<Integer, Boolean>> getList(int pos)
		{
			String name = listNames.get(pos);
			return getList(name);
		}
		
		private List<Pair<Integer, Boolean>> getList(String nameServ)
		{
			List<Pair<Integer, Boolean>> list = map.get(nameServ);
			if(list == null)
			{
				listNames.add(nameServ);
				list = new ArrayList<Pair<Integer,Boolean>>();
				map.put(nameServ, list);
			}
			return list;
		}
		@Override
		public int getSeriesCount() {
			return listNames.size();
		}
		@Override
		public Comparable getSeriesKey(int arg0) {
			return listNames.get(arg0);
		}

		@Override
		public int indexOf(Comparable arg0) {
			int i = 0;
			for(String s: listNames)
			{
				if(s.equals(arg0))
					return i;
				i++;
			}
			return -1;
		}

		@Override
		public void addChangeListener(DatasetChangeListener arg0) {
			// TODO Auto-generated method stub
			
		}

		private DatasetGroup dg = new DatasetGroup();
		@Override
		public DatasetGroup getGroup() {
			return dg;
		}

		@Override
		public void removeChangeListener(DatasetChangeListener arg0) {
			
		}

		@Override
		public void setGroup(DatasetGroup arg0) {
			dg = arg0;
			
		}

		@Override
		public DomainOrder getDomainOrder() {
			return DomainOrder.ASCENDING;
		}

		@Override
		public int getItemCount(int arg0) {
			return getList(arg0).size();
		}

		@Override
		public Number getX(int s, int p) {
			List<Pair<Integer, Boolean>> list = getList(s);
			return list.get(p).getX();
		}

		@Override
		public double getXValue(int s, int p) {
			List<Pair<Integer, Boolean>> list = getList(s);
			return list.get(p).getX().doubleValue();
		}

		@Override
		public Number getY(int s, int p) {
			List<Pair<Integer, Boolean>> list = getList(s);
			return toNumber(list.get(p).getY());
		}

		private Number toNumber(Boolean b)
		{
			if(b==null)
				return new Integer(0);
			else
			{
				if(b==true)
				{
					return new Integer(1);
				}else
				{
					return new Integer(0);
				}
			}
		}
		@Override
		public double getYValue(int s, int p) {
			List<Pair<Integer, Boolean>> list = getList(s);
			return toNumber(list.get(p).getY()).doubleValue();
		}
		public void sortLists() {
			for(Entry<String, List<Pair<Integer, Boolean>>> e: map.entrySet())
			{
				List<Pair<Integer, Boolean>> list = e.getValue();
				Collections.sort(list, new Comparator<Pair<Integer, Boolean>>(){

					@Override
					public int compare(Pair<Integer, Boolean> o1,
							Pair<Integer, Boolean> o2) {
						return o1.getX()- o2.getX();
					}
					
				});
			}
			
		}

	}

	public SuccessServDataSet(List<Task> list)
	{
		for(Task t: list)
		{
			String serv  = t.getAgServ();
			String client= t.getAgClient();
			SuccessServByClientDataset xyd = (SuccessServByClientDataset )this.get(client);
			if(xyd == null)
			{
				xyd = new SuccessServByClientDataset(client);
				this.put(client, xyd);
			}
			
			xyd.put(serv, t.getIdTask(), t.getSuccess());
		}
		
		this.sortLists();
	}
	
	private void sortLists()
	{
		for(XYDataset xyd: getDataSets())
		{
			if(xyd instanceof SuccessServByClientDataset)
			{
				SuccessServByClientDataset sss = (SuccessServByClientDataset)xyd;
				sss.sortLists();
			}
		}
	}

}
