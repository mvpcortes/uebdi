package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

public class EmoDataset implements XYDataset{
	
	private String agSource;
	
	private String agTarget;
	
	private HashMap<String, EmotionSerie> mapEmotionSerie = new HashMap<String, EmotionSerie>();
	
	private List<EmotionSerie> listEmotionSerie = new ArrayList<EmotionSerie>();
	
	
	private EmotionSerie getEmotionSerie(String nameEmotion)
	{
		EmotionSerie s = mapEmotionSerie.get(nameEmotion);
		if(s == null)
		{
			mapEmotionSerie.put(nameEmotion, s=new EmotionSerie(nameEmotion));
			listEmotionSerie.add(s);
			s.setPos(listEmotionSerie.size()-1);
		}
		return s;
	}
	
	public void addPointInSerie(String nameEmotion, int x, double y)
	{
		EmotionSerie s = getEmotionSerie(nameEmotion);
		s.add(x, y);
	}
	
	public EmoDataset()
	{
		
	}
	
	public String getSource()
	{
		return agSource;
	}
	
	public String getTarget()
	{
		return agTarget;
	}
	
	public EmoDataset(String _s, String _t)
	{
		agSource = _s;
		agTarget = _t;
	}

	@Override
	public int getSeriesCount() {
		return mapEmotionSerie.size();
	}

	@Override
	public Comparable getSeriesKey(int pos) {
		return listEmotionSerie.get(pos).getName();
	}

	@Override
	public int indexOf(Comparable n) {
		EmotionSerie es = getEmotionSerie(n.toString());
		if(es != null)
			return es.getPos();
		else
			return -1;
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DatasetGroup getGroup() {
		return new DatasetGroup();
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
		
		
	}

	@Override
	public void setGroup(DatasetGroup arg0) {
	
		
	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int posSerie) {
		EmotionSerie e = listEmotionSerie.get(posSerie);
		if(e!=null)
			return e.size();
		else
			return 0;
	}

	@Override
	public Number getX(int serie, int pos) {
		EmotionSerie es = listEmotionSerie.get(serie);
		return es.get(pos).getX();
	}

	@Override
	public double getXValue(int serie, int pos) 
	{
		EmotionSerie es = listEmotionSerie.get(serie);
		return es.get(pos).getX(); 
		
	}


	@Override
	public double getYValue(int serie, int pos) {
		EmotionSerie es = listEmotionSerie.get(serie);
		return es.get(pos).getY();
	}
	

	@Override
	public Number getY(int serie, int pos) 
	{
		EmotionSerie es = listEmotionSerie.get(serie);
		return es.get(pos).getY();
	}

	public void fixRange(int min, int max) {
		for(EmotionSerie es: listEmotionSerie)
		{
			es.fixRange(min, max);
		}
	}

}
