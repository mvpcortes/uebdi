package br.uff.ic.taskalloc.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import br.uff.ic.taskalloc.gui.datasets.EmotionsDataset;

public class ConfigGraph extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	EmotionsDataset emotionsDataset;
	
	
	public static void main(String [] ags)
	{
		JFrame frame = new JFrame();
		ConfigGraph cg = new ConfigGraph(null);
		frame.setContentPane(cg);
		frame.setVisible(true);
	
	}
	
	@SuppressWarnings("rawtypes")
	private JList listCases;
	
	private class PairStringToList 
	
	{
		private EmotionsDataset.PairString ps;
		
		public PairStringToList(EmotionsDataset.PairString _ps)
		{
			ps = _ps;
		}
		
		public String toString()
		{
			return String.format("%s->%s", ps.getX(), ps.getY());
		}
		
		public String getX()
		{
			return ps.getX();
		}
		
		public String getY()
		{
			return ps.getY();
		}
				
	}
	/**
	 * Create the panel.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ConfigGraph(EmotionsDataset _eds) {
		emotionsDataset = _eds;
		DefaultListModel lmCases = new DefaultListModel();
				
		{
			Collection<EmotionsDataset.PairString> setTarget = _eds.getCases();
			for(EmotionsDataset.PairString s: setTarget)
			{
				lmCases.addElement(new PairStringToList(s));
			}
		}
		
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		
		JPanel panelSource = new JPanel();
		this.add(panelSource);
		panelSource.setLayout(new BoxLayout(panelSource, BoxLayout.PAGE_AXIS));
		
		JLabel lblSource = new JLabel("Cases");
		lblSource.setAlignmentX(LEFT_ALIGNMENT);
		panelSource.add(lblSource);
		
		{
			listCases = new JList(lmCases);
			listCases.addListSelectionListener(new ListSelectionListener()
			{

				@Override
				public void valueChanged(ListSelectionEvent e) {
					updateCurrentDataSet();
				}
				
			}
			);
			JScrollPane jspSource  = putScrollPane(listCases, 100, 200);
			panelSource.add(jspSource);
		}
		
		////////////////////////////////////
		this.add(Box.createVerticalStrut(10));
		JButton btnUpdate = new JButton("Update");
		btnUpdate.setAlignmentY(Component.CENTER_ALIGNMENT);
		btnUpdate.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnUpdate.setPreferredSize(new Dimension(100, 20));
		btnUpdate.addActionListener( new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				updateCurrentDataSet();
			}

			
		});
		
		this.add(btnUpdate);
	}
	
	private void updateCurrentDataSet() {
		PairStringToList ps = (PairStringToList)listCases.getSelectedValue();
		
		emotionsDataset.changeEmoDataset(ps.getX(), ps.getY());
		
	}

	private JScrollPane putScrollPane(@SuppressWarnings("rawtypes") JList list, int w, int h) {
		JScrollPane sp = new JScrollPane(list);
		list.setFixedCellWidth(50);
		list.setFixedCellHeight(15);
		sp.setPreferredSize(new Dimension(w,h));
		return sp;
	}

}
