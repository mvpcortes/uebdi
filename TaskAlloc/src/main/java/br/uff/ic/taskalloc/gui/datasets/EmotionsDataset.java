package br.uff.ic.taskalloc.gui.datasets;

import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import com.sun.corba.se.spi.orb.StringPair;

import br.uff.ic.taskalloc.AgState;
import br.uff.ic.taskalloc.Task;
import br.uff.ic.util.Pair;
import br.uff.ic.util.TermHelper;

public class EmotionsDataset implements XYDataset{

	public class PairString extends Pair<String, String>
	{
		public PairString(String a, String b) {
			super(a, b);
		}
		String getSource()
		{
			return getX();
		}
		String getTarget()
		{
			return getY();
		}
	}
	
	Logger log = Logger.getLogger(this.getClass().getName());
	
	public Logger getLogger()
	{
		return log;
	}
	private Map<PairString, EmoDataset> map = new HashMap<PairString, EmoDataset>();
	
	private PairString currentPair = new PairString("", "");
	
	private EmoDataset currentEmoDataset = null;
	
	private List<DatasetChangeListener> listListener = new LinkedList<DatasetChangeListener>(); 
	
	private DatasetGroup dg = new DatasetGroup();
	
	
	///////////////////////////////////////////
	public EmotionsDataset(List<Task> listTask)
	{
		int minTask = Integer.MAX_VALUE;
		int maxTask = Integer.MIN_VALUE;
		for(Task t: listTask)
		{
			getLogger().info(String.format("task(%d)", t.getIdTask()));
			minTask = Math.min(minTask, t.getIdTask());
			maxTask = Math.max(maxTask, t.getIdTask());
			for(AgState ag: t.getAgStates())
			{
				String source = ag.getAgent();
				for(Term term: ag.getEmotions())
				{
					if(term.isLiteral())
					{
						Literal l = (Literal)term;
						String name 	= TermHelper.getString(l, 0);
						String target	= TermHelper.getString(l, 1);
						Double intensity= TermHelper.getDouble(l, 2);

						putValue(source, target, name, t.getIdTask(), intensity);
					}
				}
			}
		}
		
		for(Map.Entry<PairString, EmoDataset> e: map.entrySet())
		{
			e.getValue().fixRange(minTask, maxTask);
		}
		
		
		//
		getCurrentEmoDataset(); //for�a selecionar o primeiro
	}
	
	private EmoDataset getEmoDataSet(String source, String target)
	{
		PairString sp = new PairString(source, target);
		EmoDataset edt = map.get(sp);
		if(edt == null)
		{
			edt = new EmoDataset(source, target);
			map.put(sp, edt);
		}
		
		return edt;
			
			
	}
	
	private void putValue(String source, String target, String nameEmotion, int x, double y)
	{
		getLogger().info(String.format("source = %s, target = %s, name = %s, x = %d, y = %f", source, target, nameEmotion, x, y));
		EmoDataset eds = getEmoDataSet(source, target);
		eds.addPointInSerie(nameEmotion, x, y);
	}
	///////////////////////////////////////////
	private EmoDataset getCurrentEmoDataset()
	{
		if(currentEmoDataset == null)
		{
			if(map.size() > 0)
			{
				Iterator<Map.Entry<PairString, EmoDataset>> it = map.entrySet().iterator();
				
				Map.Entry<PairString, EmoDataset> e = it.next();
				EmoDataset ed = e.getValue();
				for(DatasetChangeListener d: listListener)
				{
					ed.addChangeListener(d);
				}
				
				ed.setGroup(dg);
				currentEmoDataset = ed;
			}
		}
		
		return currentEmoDataset;
	}
	
	public void changeEmoDataset(String source, String target)
	{
		EmoDataset eds = getEmoDataSet(source, target);
		
		setCurrentEmoDataset(eds);
	}
	private void setCurrentEmoDataset(EmoDataset newCurrent)
	{
		if(newCurrent == null)
			return;
		
		
		if(newCurrent == currentEmoDataset)
			return;
		
		if(	
				newCurrent.getSource().equals(currentEmoDataset.getSource()) &
				newCurrent.getTarget().equals(currentEmoDataset.getTarget())
			)
		{
			return;
		}
		EmoDataset oldCurrent = currentEmoDataset;
		if(oldCurrent != null)
		{
			for(DatasetChangeListener d: listListener)
			{
				oldCurrent.removeChangeListener(d);
			}
			
		}
		
		for(DatasetChangeListener d: listListener)
		{
			newCurrent.addChangeListener(d);
		}
		
		newCurrent.setGroup(dg);	
		
		currentEmoDataset = newCurrent;//deve-se atribuir este valor aqui antes de atualizar os outros
		
		//atualiza listeners
		for(DatasetChangeListener d: listListener)
		{
			d.datasetChanged( new DatasetChangeEvent(this, this));
		}
	}
	@Override
	public int getSeriesCount() {
		EmoDataset eds = getCurrentEmoDataset();
		
		if(eds != null)
			return eds.getSeriesCount();
		else
			return 0;
	}

	@Override
	public Comparable getSeriesKey(int pos) {
		EmoDataset eds = getCurrentEmoDataset();
		
		if(eds != null)
			return eds.getSeriesKey(pos);
		else
			return "";
	}

	@Override
	public int indexOf(Comparable comp) {
		EmoDataset eds = getCurrentEmoDataset();
		
		if(eds != null)
			return eds.indexOf(comp);
		else
			return -1;	
		}

	@Override
	public void addChangeListener(DatasetChangeListener d) {
		listListener.add(d);
		EmoDataset eds = getCurrentEmoDataset();
		
		if(eds != null)
			eds.addChangeListener(d);
		
	}

	
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener d) {
		listListener.remove(d);
		
		
		EmoDataset eds = getCurrentEmoDataset();
		
		if(eds != null)
			eds.removeChangeListener(d);
	}

	@Override
	public void setGroup(DatasetGroup _dg) {
		dg = _dg;
		EmoDataset eds = getCurrentEmoDataset();
		if(eds!=null)
			eds.setGroup(dg);
	}

	@Override
	public DomainOrder getDomainOrder() {
		EmoDataset eds = getCurrentEmoDataset();
		if(eds!=null)
			return eds.getDomainOrder();
		else
			return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int arg0) {
		EmoDataset eds = getCurrentEmoDataset();
		if(eds!=null)
			return eds.getItemCount(arg0);
		else
			return 0;
	}

	@Override
	public Number getX(int a, int b) {
		EmoDataset eds = getCurrentEmoDataset();
		if(eds!=null)
			return eds.getX(a, b);
		else
			return 0;
	}

	@Override
	public double getXValue(int a, int b) {
		EmoDataset eds = getCurrentEmoDataset();
		if(eds!=null)
			return eds.getXValue(a, b);
		else
			return 0;
	}

	@Override
	public Number getY(int a, int b) {
		EmoDataset eds = getCurrentEmoDataset();
		if(eds!=null)
			return eds.getY(a, b);
		else
			return 0;
	}

	@Override
	public double getYValue(int a, int b) {
		EmoDataset eds = getCurrentEmoDataset();
		if(eds!=null)
			return eds.getYValue(a, b);
		else
			return 0;
	}
	
	public Collection<PairString> getCases()
	{
		Set<PairString> set = new HashSet<PairString>();
		
		for(Map.Entry<PairString, EmoDataset> e: map.entrySet())
		{
			set.add(e.getKey());
		}
		return set;
	}
}
