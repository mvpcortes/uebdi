package br.uff.ic.taskalloc;

import java.util.ArrayList;
import java.util.List;

class ListenerSend implements TaskManagerListener
{
	private List<TaskManagerListener> listListener = new ArrayList<TaskManagerListener>();
	
	public void add(TaskManagerListener tm)
	{
		listListener.add(tm);
	}

	@Override
	public void onCreate(int id) {
		for(TaskManagerListener tm: listListener)
		{
			tm.onCreate(id);
		}
		
	}

	@Override
	public void onRegister(int id, String strClient) {
		for(TaskManagerListener tm: listListener)
		{
			tm.onRegister(id, strClient);
		}
		
	}

	@Override
	public void onAalloc(int id, String strServ) {

		for(TaskManagerListener tm: listListener)
		{
			tm.onAalloc(id, strServ);
		}
	}

	@Override
	public void onExecute(int id, boolean success) {
		for(TaskManagerListener tm: listListener)
		{
			tm.onExecute(id, success);
		}
	}

	@Override
	public void onFinish(int id, double mediaSuccess, double mediaRep) {
		for(TaskManagerListener tm: listListener)
		{
			tm.onFinish(id, mediaSuccess, mediaRep);
		}
	}

	@Override
	public void onUnregister(Task task) {
		for(TaskManagerListener tm: listListener)
		{
			tm.onUnregister(task);
		}			
	}
}
