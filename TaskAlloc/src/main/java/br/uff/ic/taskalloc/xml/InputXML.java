package br.uff.ic.taskalloc.xml;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Term;

import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Logger;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import br.uff.ic.taskalloc.AgState;
import br.uff.ic.taskalloc.Task;
import br.uff.ic.util.TermHelper;

public class InputXML {

	private Collection<Task> collTask = null;
	
	private InputStream inputStream = null;
	
	private boolean readExtraData = false;
	
	private class Handler extends DefaultHandler
	{
		InputXML inputXML = null;
		private Task currentTask = new Task(0, true);
		
		private AgState currentAgState = new AgState("", 0, 0, 0.0, new ArrayList<Term>(), new ArrayList<Term>());
		
		private String [] currentEmotionData  = new String[3];
		
		private String [] currentMSData		  = new String[3];
		
		private Stack<String> stackElement = new Stack<String>();
		
		private List<Term> listEmotions = new LinkedList<Term>();
		
		private Task getTask()
		{
			return currentTask;
		}
		
		private AgState getAgState()
		{
			return currentAgState;
		}
		
		private void releaseAgState(String str)
		{
			AgState oldAgState = getAgState();
			currentAgState = new AgState(str, 0, 0, 0.0, new ArrayList<Term>(), new ArrayList<Term>());
			getTask().addAgState(oldAgState);
		}
		
		private String [] getEmotionData()
		{
			return currentEmotionData;
		}
		
		private String[] getMSData()
		{
			return currentMSData;
		}
		
		private Term releaseEmotion()
		{
			try
			{
				Term temp = ASSyntax.createLiteral("emotion", ASSyntax.parseNumber(currentEmotionData[2]), ASSyntax.parseLiteral(currentEmotionData[0]), ASSyntax.parseLiteral(currentEmotionData[1]));
				for(int i = 0; i < currentEmotionData.length; i++)
				{
					currentEmotionData[i] = "";
				}
				
				return temp;
			}catch(Exception e)
			{
				return null;
			}
		}
		
		private Term releaseMS()
		{
			Term temp = TermHelper.generateLiteral("msuccess", currentMSData[0], currentMSData[1], currentMSData[2]);
			for(int i = 0; i < currentMSData.length; i++)
			{
				currentMSData[i] = "";
			}
			return temp;
		}
		
		private void releaseTask(int id)
		{
			Task t = getTask();
			currentTask = new Task(id, true);
			inputXML.addTask(t);
		}
		
		Handler(InputXML i)
		{
			inputXML = i;
		}
		
		@Override
		public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException
		{
			stackElement.add(qName);
		}

		@Override
		public void endElement (String uri, String localName, String qName) throws SAXException
		{
			if(CONSTANTS_XML.STR_TASK.equals(qName))
			{
				releaseTask(0);
			}else if(CONSTANTS_XML.STR_AGENT.equals(qName))
			{
				releaseAgState("");
			}else if(readExtraData && CONSTANTS_XML.STR_EMOTION.equals(qName))
			{
				Term t = releaseEmotion();
				listEmotions.add(t);
			}
			else if(readExtraData && CONSTANTS_XML.STR_MS_DATA.equals(qName))
			{
				Term t = releaseMS();
				getAgState().addMS(t);
			}
			else if(CONSTANTS_XML.STR_EMOTIONS.equals(qName))
			{
				
				Collection<Term> coll = new ArrayList<Term>(listEmotions);
				getAgState().addEmotions(coll);
				listEmotions.clear();
			}
			stackElement.pop();
		}
		
		private double toDouble(String s)
		{
			NumberFormat fmt = NumberFormat.getInstance(CONSTANTS_XML.LOCALE);
			Number number;
			try {
				number = fmt.parse(s);
			return number.doubleValue();
			} catch (ParseException e) {
				return Double.NaN;
			}
		}
		
		@Override
		public void characters(char[] vec, int init, int length)
		{
			String s = new String(vec, init, length);
			String top = stackElement.peek();
			String parent= "";
			if(stackElement.size() >= 2)
				parent = stackElement.get(stackElement.size()-2);
			
			if(CONSTANTS_XML.STR_ID.equals(top))
			{
				int i = Integer.parseInt(s);
				getTask().setIdTask(i);
			}else if(CONSTANTS_XML.STR_CLIENT.equals(top))
			{
				getTask().setAgClient(s);
			}
			else if(CONSTANTS_XML.STR_SERV.equals(top))
			{
				getTask().setAgServ(s);
			}else if(CONSTANTS_XML.STR_MS.equals(top))
			{
				getTask().setMediaSuccess(toDouble(s));
			}else if(CONSTANTS_XML.STR_MS_DATA.equals(top))
			{
				//getTask().setMediaSuccess(toDouble(s));
			}
			else if (readExtraData && CONSTANTS_XML.STR_MS_OTHER.equals(top))
			{
				getMSData()[1] = s;
			}
			else if (readExtraData && CONSTANTS_XML.STR_MS_VALUE.equals(top))
			{
				getMSData()[2] = s;
			}
			else if (readExtraData && CONSTANTS_XML.STR_MS_ID.equals(top))
			{
				getMSData()[0] = s;
			}
			else if(CONSTANTS_XML.STR_SUCCESS.equals(top))
			{
				getTask().setSuccess(toBoolean(s));
			}
			else if(CONSTANTS_XML.STR_MR.equals(top))
			{
				getTask().setMediaRep(toDouble(s));
			}else if(CONSTANTS_XML.STR_NAME.equals(top))
			{
				if(CONSTANTS_XML.STR_AGENT.equals(parent))
				{
					getAgState().setAgent(s);
				}else if(readExtraData && CONSTANTS_XML.STR_EMOTION.equals(parent))
				{
					getEmotionData()[0] = s;
				}else if(CONSTANTS_XML.STR_CANDIDATE.equals(parent))
				{
					getTask().addAgCandidate(s);
				}
			}else if (CONSTANTS_XML.STR_MONEY.equals(top))
			{
				getAgState().setMoney(Long.parseLong(s));
			}else if (CONSTANTS_XML.STR_COST.equals(top))
			{
				getAgState().setCost(Long.parseLong(s));
			}
			else if (CONSTANTS_XML.STR_PROB.equals(top))
			{
				getAgState().setP(toDouble(s));
			}
			else if (readExtraData && CONSTANTS_XML.STR_AG_NAME.equals(top))
			{
				getEmotionData()[1] = s;
			}else if (readExtraData && CONSTANTS_XML.STR_INTENSITY.equals(top))
			{
				getEmotionData()[2] = s;
			}else if(CONSTANTS_XML.STR_NAME.equals(top))
			{
				if(CONSTANTS_XML.STR_MS.equals(parent))
				{
					
				}
			}
		}

		private Boolean toBoolean(String s) {
			s = s.toLowerCase();
			if(s.equals("1"))
				return true;
			else if(s.equals("0"))
				return false;
			else if(s.equals("null"))
				return null;
			else if(s.equals("nil"))
				return null;
			else
				return Boolean.parseBoolean(s);
				
				
		}
	}
	
	public InputXML(Collection<Task> _collTask, InputStream input)
	{
		this(_collTask, input, true);
	}
	public InputXML(Collection<Task> _collTask, InputStream input, boolean _readExtraData)
	{			
		collTask = _collTask;
		inputStream = input;
		
		{
			  SAXParserFactory factory = SAXParserFactory.newInstance();
			  try {

			        SAXParser saxParser = factory.newSAXParser();
			        saxParser.parse(inputStream, new Handler(this));
			        
			  }catch (Exception e) {
				  getLogger().severe("Problem in parser: " + e.toString());
			}
		}
	}

	private Logger logger = Logger.getLogger(this.getClass().getName());
	private Logger getLogger() {
		return logger;
	}

	public void addTask(Task t) {
		collTask.add(t);
	}
}
