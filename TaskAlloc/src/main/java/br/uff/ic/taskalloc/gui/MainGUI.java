package br.uff.ic.taskalloc.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import br.uff.ic.taskalloc.FileHelper;

public class MainGUI {

	private JFrame frame;


		/**
		 * Launch the application.
		 */
		public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						MainGUI window = new MainGUI();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	 
	@SuppressWarnings("rawtypes")
	JList listFiles = null;
	/**
	 * Create the application.
	 */
	public MainGUI() {
		initialize();
		if(autoOpenFiles().size() > 0)
		{
		//	listFiles.
		}
		
	}
	
	
	private File[] openFile()
	{
		//Create a file chooser
		String strCurrent = System.getProperty("user.dir");
		File fileCurrent  = new File(strCurrent);
		final JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(fileCurrent);
		fc.setMultiSelectionEnabled(true);
		fc.addChoosableFileFilter(new FileFilter()
		{

			@Override
			public boolean accept(File f) {
				if(f.isFile())
				{
					String ext = FileHelper.getExtension(f.getName());
					if("xml".equals(ext) )
						return true;
					else
						return false;
				}else
				{
					return true;
				}
			}

			@Override
			public String getDescription() {
				return "*.xml";
			}
			
		});
		
		//In response to a button click:
		
		int returnVal = fc.showSaveDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			File[] files = fc.getSelectedFiles();
			return files;
		}else
			return null;
	}
	
	private Collection<File> autoOpenFiles()
	{
		List<File> xmlFiles = new LinkedList<File>();
		String strCurrent = System.getProperty("user.dir");
		File fileCurrent  = new File(strCurrent);
		if(fileCurrent.exists() && fileCurrent.isDirectory())
		{
			File[] children = fileCurrent.listFiles();
			for(File f: children)
			{
				if(f.isFile())
				{
					String ext = FileHelper.getExtension(f.getName());
					if("xml".equals(ext))
					{
						xmlFiles.add(f);
					}
				}
			}
			
			for(File f: xmlFiles)
			{
				ControllerGUI.get().loadFile(f);
			}
			return xmlFiles;
		}
		return Collections.<File>emptyList();
	}
	
	
	private JPanel createColumn(JComponent ...comp)
	{
		JPanel panelMiddle = new JPanel();
		BoxLayout layoutMiddle = new BoxLayout(panelMiddle, BoxLayout.PAGE_AXIS);
		panelMiddle.setLayout(layoutMiddle);
		for(int i = 0; i < comp.length-1; i++)
		{
			if(comp[i] instanceof JComponent)
			{
				JComponent j = (JComponent)comp[i];
				j.setAlignmentX(Component.CENTER_ALIGNMENT);
			}
			panelMiddle.add(comp[i]);
			panelMiddle.add(Box.createRigidArea(new Dimension(5,0)));		
		}
		
		if(comp.length>0)
		{
			Component c = comp[comp.length-1];
			if(c instanceof JComponent)
			{
				JComponent j = (JComponent)c;
				j.setAlignmentX(Component.CENTER_ALIGNMENT);
			}
			panelMiddle.add(comp[comp.length-1]);
		}
		return panelMiddle;
	}
	
	private JPanel createRow(Component ...comp)
	{
		JPanel panelMiddle = new JPanel();
		BoxLayout layoutMiddle = new BoxLayout(panelMiddle, BoxLayout.LINE_AXIS);
		panelMiddle.setLayout(layoutMiddle);
		for(int i = 0; i < comp.length-1; i++)
		{
			if(comp[i] instanceof JComponent)
			{
				JComponent j = (JComponent)comp[i];
				j.setAlignmentY(Component.CENTER_ALIGNMENT);
			}
			panelMiddle.add(comp[i]);
			panelMiddle.add(Box.createRigidArea(new Dimension(5,0)));		
		}
		
		if(comp.length>0)
		{
			Component c = comp[comp.length-1];
			if(c instanceof JComponent)
			{
				JComponent j = (JComponent)c;
				j.setAlignmentY(Component.CENTER_ALIGNMENT);
			}
			panelMiddle.add(comp[comp.length-1]);
		}
		return panelMiddle;		
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		ListModelSource lms = new ListModelSource();
		ControllerGUI.get().addListener(lms);
		
		listFiles = new JList();
		listFiles.setModel(lms);
		listFiles.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e)
			{
				JList l = (JList)e.getSource();
				ControllerGUI.get().setCurrentSource((String)l.getSelectedValue());	
			}
		});
	
		JScrollPane listScroller = new JScrollPane(listFiles );
		listScroller.setPreferredSize(new Dimension(100, 100));
		listScroller.setAlignmentX(JScrollPane.LEFT_ALIGNMENT);
		
		listFiles.setFixedCellWidth(50);
		listFiles.setFixedCellHeight(15);
	    
		JButton btnServ = new JButton("selected serv");
		btnServ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowServDataset();
				
			}});
		
		JButton btnLoad = new JButton("load file");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File[] files = openFile();
				ControllerGUI.get().loadFiles(files);
				
			}});
		
		JButton btnMoney = new JButton("money");
		btnMoney .addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowMainDataSet();
			}
		});
		
		JButton btnProb = new JButton("prob");
		btnProb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowMainProbDataSet();
			}
		});
		

		JButton btnMediaSuccess = new JButton("MediaSuccess");
		btnMediaSuccess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowMediaSuccessDataSet();
			}
		});
		
		
		JButton btnRealMediaSuccess = new JButton("Serv Success");
		btnRealMediaSuccess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowSuccessServDataset();
			}
		});
		
		JButton btnPoint = new JButton("point");
		btnPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowPointDataSet();
			}
		});
		
		JButton btnSuccessByClient = new JButton("Success By Client");
		btnSuccessByClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowSuccessByClientDataSet();
			}
		});
		
		JButton btnMediaSuccessByClient = new JButton("MS by Client");
		btnMediaSuccessByClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowMediaSuccessByClient();
			}
		});
		
		JButton btnByClientServ = new JButton("MS by CxS");
		btnByClientServ.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().showMSSelectingClient();
			}
		});
		
		
		JButton btnCost = new JButton("cost");
		btnCost.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowCostDataSet();
			}
		});
		
		JButton btnDev = new JButton("dev");
		btnDev.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowMainDevDataSet();
			}
		});
		
		JButton btnOptimal = new JButton("Optimal");
		btnOptimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowOptimalDataSet();
			}
		});
		
		JButton btnDelay = new JButton("Delay");
		btnDelay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowTimeDataSet();
			}
		});
		
		JButton btnRealOptimal = new JButton("Instant Optimal");
		btnRealOptimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowInstantOptimalDataSet();
			}
		});
		
		JButton btnExportCharts = new JButton("Export Charts");
		btnExportCharts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().exportGraphics();
			}
		});
		
		JButton btnEmotion = new JButton("Emotion");
		btnEmotion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ControllerGUI.get().ShowEmotionDataSet();
			}
		});
		
		

		Container panel = (JPanel) frame.getContentPane();
		
		//
		{
			/*BoxLayout layoutTop = new BoxLayout(panelTop, BoxLayout.LINE_AXIS);
			
			panelTop.add(listScroller);
			panelTop.add(Box.createHorizontalGlue());
			panelTop.add(btnLoad);*/
			JPanel p = createRow(listScroller, Box.createHorizontalGlue(), btnLoad);
			panel.add(p, BorderLayout.NORTH);
		}
		{
			JPanel p1 = createColumn(btnMoney, btnDev, btnProb, btnCost, btnPoint, btnSuccessByClient, btnServ);
			JPanel p2 = createColumn(btnDelay, btnRealOptimal, btnRealMediaSuccess, btnMediaSuccessByClient, btnByClientServ , btnEmotion);
			JPanel p3 = createColumn(btnMediaSuccess, btnOptimal, btnRealOptimal, btnExportCharts);
			JPanel button = createRow(p1, Box.createHorizontalGlue(), p2, Box.createHorizontalGlue(), p3);
			panel.add(button, BorderLayout.CENTER);
			
		}
		
		frame.pack();
	
	}
	
	@SuppressWarnings("rawtypes")
	private class ListModelSource implements ListModel, ControllerGUIListener
	{
		ListDataListener listener = null;
		
		@Override
		public int getSize() {
			return ControllerGUI.get().getLoadedFiles();
		}

		@Override
		public Object getElementAt(int index) {
			return ControllerGUI.get().getLoadedFile(index);
		}

		@Override
		public void addListDataListener(ListDataListener l) {
			listener  = l;
			
		}

		@Override
		public void removeListDataListener(ListDataListener l) {
			if(listener.equals(l))
			{
				listener = null;
			}
		}

		@Override
		public void fileAdded(String name) {
			int pos = ControllerGUI.get().getLoadedFilePos(name);
			if(listener!=null)
				listener.intervalAdded(
							new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, pos, pos)
						);
		}
	}
}
