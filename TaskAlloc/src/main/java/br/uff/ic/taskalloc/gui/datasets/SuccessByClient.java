package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.Task;

public class SuccessByClient implements XYDataset {

	List<String> listClient = new ArrayList<String>();
	Map<String, List<Task>> mapTasks = new HashMap<String, List<Task>>();
	public SuccessByClient(List<Task> list)
	{
		for(Task t: list)
		{
			String ag = t.getAgClient();
			List<Task> listTask = mapTasks.get(ag);
			if(listTask == null)
			{
				listClient.add(ag);
				listTask = new ArrayList<Task>();
				mapTasks.put(ag, listTask);
			}
			listTask.add(t);
		}
	}
	@Override
	public int getSeriesCount() {
		return listClient.size();
	}

	@Override
	public Comparable getSeriesKey(int i) {
		return listClient.get(i);
	}

	@Override
	public int indexOf(Comparable c) {
		for(int i = 0; i < listClient.size(); i++)
		{
			if(c.equals(listClient.get(i)))
				return i;
		}
		return -1;
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		// TODO Auto-generated method stub

	}

	DatasetGroup dg = new DatasetGroup();
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {

	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		dg = arg0;

	}

	@Override
	public DomainOrder getDomainOrder() {
		// TODO Auto-generated method stub
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int serie) {
		String name = listClient.get(serie);
		return mapTasks.get(name).size();
	}

	@Override
	public Number getX(int serie, int pos) {
		String name = (String) this.getSeriesKey(serie);
		List<Task> list = mapTasks.get(name);
		return list.get(pos).getIdTask();
	}

	@Override
	public double getXValue(int serie, int pos) {
		return getX(serie, pos).doubleValue();
	}

	@Override
	public Number getY(int serie, int pos) {
		String name = (String) this.getSeriesKey(serie);
		List<Task> list = mapTasks.get(name);
		return list.get(pos).getSuccess()?1:0;
	}

	@Override
	public double getYValue(int serie, int pos) {
		return getY(serie, pos).doubleValue();
	}

}
