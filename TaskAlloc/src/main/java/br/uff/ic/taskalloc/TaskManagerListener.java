package br.uff.ic.taskalloc;

public interface TaskManagerListener {
	
	
	public  void onCreate(int id);
	
	public void onRegister(int id, final String strClient);
	
	public void onAalloc(int id, final String strServ);
	
	public void onExecute(int id, boolean success);
	
	public void onFinish(int id, double mediaSuccess, double mediaRep);	
	
	public void onUnregister(final Task task);
}
