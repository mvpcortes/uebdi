package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.chart.axis.SymbolAxis;
import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.Task;
import br.uff.ic.util.Pair;

public class ServDataset implements XYDataset {

	List<String> listServ = new ArrayList<String>();
	List<String>	listSerie	= new ArrayList<String>();
	Map<String, List<Pair<Integer, Integer> > > mapSerie = new HashMap<String, List<Pair<Integer, Integer>>>();
	public ServDataset(List<Task> listTask)
	{
		Map<String, Integer> mapServ = new HashMap<String, Integer>();
		for(Task t: listTask)
		{
			
			final String serv = t.getAgServ();
			Integer posServ = mapServ.get(serv);
			if(posServ == null)
			{
				mapServ.put(t.getAgServ(), listServ.size());
				posServ = listServ.size();
				listServ.add(serv);
			}
			
			final String client = t.getAgClient();
			List<Pair<Integer, Integer>> list = mapSerie.get(client);
			if(list == null)
			{
				list = new ArrayList<Pair<Integer, Integer>>();
				mapSerie.put(client, list);
				listSerie.add(client);
			}
			
			list.add(new Pair<Integer, Integer>(t.getIdTask(), posServ));
		}
	}
	
	@Override
	public int getSeriesCount() {
		return listSerie.size();
	}

	@Override
	public Comparable getSeriesKey(int a) {
		return listSerie.get(a);
	}

	@Override
	public int indexOf(Comparable arg0) {
		int i = 0;
		for(String s: listSerie)
		{
			if(s.equals(arg0))
			{
				return i;
			}
			i++;
		}
		return -1;
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		// TODO Auto-generated method stub

	}

	DatasetGroup dg = new DatasetGroup();
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		dg = arg0;

	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int i) {
		String serie = listSerie.get(i);
		List<Pair<Integer, Integer>> list = mapSerie.get(serie);
		if(list != null)
			return list.size();
		
		return 0;
	}

	@Override
	public Number getX(int i, int pos) {
		String serie = listSerie.get(i);
		List<Pair<Integer, Integer>> list = mapSerie.get(serie);
		if(list != null)
		{
			return list.get(pos).getX();
		}
		
		return -1;
	}

	@Override
	public double getXValue(int i, int pos) {
		return getX(i, pos).doubleValue();
	}

	@Override
	public Number getY(int i, int pos) {
		String serie = listSerie.get(i);
		List<Pair<Integer, Integer>> list = mapSerie.get(serie);
		if(list != null)
		{
			return list.get(pos).getY();
		}
		
		return -1;
	}

	@Override
	public double getYValue(int i, int pos) {
		return getY(i, pos).doubleValue();
	}

	public SymbolAxis getSymbolAxis()
	{
		String[] str = new String[listServ.size()];
		str = listServ.toArray(str);
		
		return new SymbolAxis("serv", str);
	}
}
