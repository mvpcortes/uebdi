package br.uff.ic.taskalloc;

import jason.asSyntax.Term;

import java.util.ArrayList;
import java.util.Collection;

public class AgState {
	private String agent;

	private long money;
	
	/**real probability to execute the current task*/
	private double realP;
	
	
	private long cost;
	
	private Collection<Term> emotions = new ArrayList<Term>();
	private Collection<Term> cms = new ArrayList<Term>();
	
	public AgState(String ag, long c, long m, double p, Collection<Term> _emos, Collection<Term> _cms)
	{
		agent = ag;
		cost  = c;
		money = m;
		realP = p;
		emotions.addAll(_emos);
		cms.addAll(_cms);
	}

	public void addEmotions(Collection<Term> _emotions)
	{
		emotions.addAll(_emotions);
	}

	public long getCost()
	{
		return cost;
	}
	
	public double getP()
	{
		return realP;
	}
	
	public void setP(double p)
	{
		realP = Math.max(0, Math.min(1, p));
	}
	
	public String getAgent() {
		return agent;
	}
	
	
	public long getMoney() {
		return money;
	}

	public Object[] getRowData() {
		return 
				new Object[]
						{
							getAgent(),
							new Long(getMoney())
						};
	}

	public void setAgent(String str) {
		agent = str;
	}

	public void setMoney(long value) {
		money = value;		
	}
	
	public final Collection<Term> getEmotions()
	{
		return emotions;
	}
	
	@Override
	public String toString()
	{
		/*
		 * 	agent = ag;
		money = m;
		realP = p;
		mediaSuccess = mediaS;
		mediaRep = mediaR;
		emotions.addAll(emos);
		 */
		return String.format("(name: %s, money: %d, realP: %f, emotions: %s, ms: %s)", 
				getAgent(),
				getMoney(),
				getP(),
				getEmotions().toString(),
				cms.toString()
				);
	}

	public void setCost(long c) {
		cost = c;
	}

	public double getPoint() {
		return (1-getP())*getCost();
	}
	
	public Collection<Term> getMS()
	{
		return cms;
	}

	public void addMS(Term t) {
		cms.add(t);
	}
}
