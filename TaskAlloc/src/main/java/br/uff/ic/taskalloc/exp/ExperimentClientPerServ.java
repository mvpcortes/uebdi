package br.uff.ic.taskalloc.exp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;




public class ExperimentClientPerServ {

	
	public static void main(String[] args) {
		String strCurrent = System.getProperty("user.dir");
		File fileCurrent = new File(strCurrent);
		
		
		File pathDir = new File(fileCurrent, "mas2j");
		pathDir.mkdir();
		List<File> fileMas = new LinkedList<File>();
		for(Integer client: new Integer[]{2, 5, 10, 15})
		{
			for(Integer serv: new Integer[]{2, 5, 10, 15})
			{
				fileMas.add(createMAS2J(pathDir, client, serv, "clientRational"));
				fileMas.add(createMAS2J(pathDir, client, serv, "clientReputation"));
			}
		}
		
		File fileBat = new File(fileCurrent, "test.bat");
			try {
				if(!fileBat.exists())
					fileBat.createNewFile();
				
				PrintStream ps = new PrintStream(fileBat);
				for(File f: fileMas)
				{
					ps.format("java -jar TaskAlloc.jar %s\n", f.getAbsoluteFile());		
				}
				ps.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	}

	private static File createMAS2J(File pathDir, Integer client, Integer serv, String sAg) {
		String sname = String.format("%s_%d_%d.mas2j", sAg, client, serv);
		final int taskCount = 100;
		try {
			File f = null;
			PrintStream ps = new PrintStream(
							new FileOutputStream(
									f = new File(pathDir, sname)
							)
					);
			ps.format("MAS %s_%d_%d {\n", sAg, client, serv);
			ps.println("infrastructure: Centralised");
			ps.println("environment: br.uff.ic.taskalloc.EnvironmentTaskAlloc");
			ps.println("(");
			ps.format("out(\"./%s_%d_%d.xml\"),\n", sAg, client, serv);
			ps.format("log(\"./%s_%d_%d.log\")\n", sAg, client, serv);
			ps.println(")");
			ps.println();
			
			ps.println("agents:");
			ps.format("client\t%s.asl	[verbose=1] #%d;\n", sAg, client);
			ps.format("manager\tmanager.asl\t[verbose=1, goals=\"alloc_task_list([alloc_a(%s, %s, 1, %d)])\" ];",generateClientList(client), generateServList(serv), taskCount);
			ps.println();
			for(int i = 1; i <=serv; i++)
			{
				double p = ((double)i-1)/(serv-1);
				ps.format(Locale.US, "serv%d\tservBase.asl\t[verbose=1, beliefs=\"costRange(1,%d, 1), probRange(1,%d,%f)\"];", i, taskCount, taskCount,  p);
				ps.println();
			}
			
			ps.println();
			ps.println("aslSourcePath: \"src/main/asl\";");
			ps.println("}");
			ps.close();
			return f;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static String generateClientList(Integer clientCount) {
		return generateNameList("client", clientCount);
	}

	private static String generateServList(Integer servCount) {
		return generateNameList("serv", servCount);
	}

	private static String generateNameList(String string, Integer count) {
		if(count == 0)
			return "[]";
		
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for(int i = 1; i < count; i++)
		{
			sb.append(string);
			sb.append(i);
			sb.append(", ");
		}
		
		sb.append(string);
		sb.append(count);
		sb.append(']');
		return sb.toString();
	}
	

}
