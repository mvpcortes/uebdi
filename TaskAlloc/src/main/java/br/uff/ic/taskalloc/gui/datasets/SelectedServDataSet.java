package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.uff.ic.taskalloc.Task;
import br.uff.ic.taskalloc.gui.datasets.OptimalPointDataSet.Serie;
import br.uff.ic.util.Pair;

public class SelectedServDataSet extends OptimalPointDataSet{
	public SelectedServDataSet(List<Task> list)
	{
		super(new ArrayList<Task>());
		Map<String, Pair<Integer, Double>> mapSuccess 	= new HashMap<String, Pair<Integer, Double>>();
		
		for(Task t: list)
		{
			if(t.getSuccess() !=null)
			{
				final String ag = t.getAgServ();
				Pair<Integer, Double> pair = mapSuccess.get(ag);
				double value = 0;
				int qtd = 0;
				if(pair != null)
				{
					qtd = pair.getX();
					value = pair.getY();
				}
				
				qtd++;
				value ++;
				Serie serie = this.getSerie(ag);
				serie.addPoint(qtd, value);
				mapSuccess.put(ag, new Pair<Integer, Double>(qtd, value));
			
			}
		}
	}
}
