package br.uff.ic.taskalloc.gui;



import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.gui.datasets.ServDataset;
import br.uff.ic.util.StringOutputStream;

public class MyChartFrame extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The chart panel. */
    //private ChartPanel chartPanel;
	private JFreeChart chart = null;
    
    private JButton btnExportImage;
    
    private JButton btnExportData;
    
    
    private JPanel  optionalPanel;
       
    private XYDataset dataSet = null;
    /**
     * Constructs a frame for a chart.
     *
     * @param title  the frame title.
     * @param chart  the chart.
     */
    public MyChartFrame(String title, JFreeChart _chart, XYDataset ds) {
    	this(title, _chart, ds, null);
    }
    
    public MyChartFrame(String title, JFreeChart _chart, XYDataset ds, JPanel op) {
    	super(title);
    	chart = _chart;
    	dataSet = ds;
    	optionalPanel = op;
        init(chart);
    }
    
    public void init(JFreeChart chart)
    {
    	setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        
    	//
    	ChartPanel chartPanel = new ChartPanel(chart);
    	
    	this.btnExportData = new JButton("Export Data");
    	this.btnExportData.setAlignmentX(CENTER_ALIGNMENT);
    	this.btnExportData.setAlignmentY(TOP_ALIGNMENT);
    	this.btnExportData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onExportData(e.getSource());
			}});

    	this.btnExportImage = new JButton("Export Image");
    	this.btnExportImage.setAlignmentX(CENTER_ALIGNMENT);
    	this.btnExportImage.setAlignmentY(TOP_ALIGNMENT);
    	this.btnExportImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onExportImage(e.getSource());
			}
			});
    	
    	if(optionalPanel != null)
    	{
    		
    	}
    	
    	//

    	JPanel mainPanel = new JPanel();
        BoxLayout layoutMain = new BoxLayout(mainPanel, BoxLayout.LINE_AXIS);//cima para baixo.
        
        chartPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        mainPanel.setLayout(layoutMain);
        
        this.setContentPane(mainPanel);
        
        
        mainPanel.add(chartPanel);
       
        JPanel tempPanel = new JPanel();
        tempPanel.setAlignmentY(TOP_ALIGNMENT);
        tempPanel.setLayout(new BoxLayout(tempPanel, BoxLayout.PAGE_AXIS));
        tempPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        tempPanel.add(btnExportData);
        tempPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        
        tempPanel.add(btnExportImage);
        tempPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        if(optionalPanel != null)
        {
        	optionalPanel.setMaximumSize(new Dimension(1500, 1000000));
        	tempPanel.add(optionalPanel);
            tempPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        }
        
        mainPanel.add(tempPanel);
        mainPanel.add(Box.createRigidArea(new Dimension(5,0)));
    }

    public void onExportData(Object source)
    {
    	if(dataSet!= null)
    	{
    		StringOutputStream os = new StringOutputStream();
    		SymbolAxis sa = null;
    		if(dataSet instanceof ServDataset)
    		{
    			ServDataset ss = (ServDataset)(dataSet);
    			sa = ss.getSymbolAxis();
    		}
    		ControllerGUI.generateData(dataSet, os, sa);
    		//ControllerGUI.generateDataSimple(dataSet, os, sa);
    		TextClipboard.get().setClipboardContents(os.toString());
    	}
    }
    

	private void onExportImage(Object source) {
		BufferedImage bi = chart.createBufferedImage(800, 300, null);
		setClipboard(bi);
	}
	
	// This method writes a image to the system clipboard.
	// otherwise it returns null.
	public static void setClipboard(Image image) {
	    ImageSelection imgSel = new ImageSelection(image);
	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
	}

	public JFreeChart getChart()
	{
		return chart;
	}
	// This class is used to hold an image while on the clipboard.
	public static class ImageSelection implements Transferable {
	    private Image image;

	    public ImageSelection(Image image) {
	        this.image = image;
	    }

	    // Returns supported flavors
	    public DataFlavor[] getTransferDataFlavors() {
	        return new DataFlavor[]{DataFlavor.imageFlavor};
	    }

	    // Returns true if flavor is supported
	    public boolean isDataFlavorSupported(DataFlavor flavor) {
	        return DataFlavor.imageFlavor.equals(flavor);
	    }

	    // Returns image
	    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
	        if (!DataFlavor.imageFlavor.equals(flavor)) {
	            throw new UnsupportedFlavorException(flavor);
	        }
	        return image;
	    }
	}
}
