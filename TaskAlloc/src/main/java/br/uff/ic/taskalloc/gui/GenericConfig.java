package br.uff.ic.taskalloc.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import br.uff.ic.taskalloc.gui.datasets.MappedDataset;

public class GenericConfig extends JPanel{

	private JList list = null;
	
	private MappedDataset mapDataset = null;
	
	public GenericConfig(List<Object> items, MappedDataset map)
	{
		mapDataset = map;
		init(items);
	}
	
	public GenericConfig(Object[] items)
	{
		List<Object> coll = null;
		Collections.addAll(new ArrayList<Object>(), items);
		init(coll);
	}
	
	private class ObjectModel implements ListModel
	{
		private List<Object> collObj;
		
		public ObjectModel(List<Object> coll)
		{
			collObj = coll;
		}
		
		@Override
		public int getSize() {
			return collObj.size();
		}

		@Override
		public Object getElementAt(int index) {
			return collObj.get(index);
		}

		@Override
		public void addListDataListener(ListDataListener l) {
			
		}

		@Override
		public void removeListDataListener(ListDataListener l) {
			
		}
		
	}
	private void init(List<Object> _list)
	{
		ListModel lm = new ObjectModel(_list);
		this.list = new JList(lm);
		
		this.list.addListSelectionListener(new ListSelectionListener()
		{

			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateCurrentDataSet(e);
			}	
		});
		
		JScrollPane pp = new JScrollPane(list);
		add(pp);
	}
	
	private void updateCurrentDataSet(ListSelectionEvent e) {
		String s = list.getSelectedValue().toString();
		mapDataset.setCurrent(s);		
	}
}
