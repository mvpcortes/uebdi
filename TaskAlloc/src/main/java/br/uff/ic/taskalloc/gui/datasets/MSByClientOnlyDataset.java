package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.Task;

public class MSByClientOnlyDataset implements XYDataset {
	
	private List<Task> listTask = new ArrayList<Task>();
	private String strClient = "";
	
	private List<String> listSerie = new ArrayList<String>();
	
	public MSByClientOnlyDataset(String name)
	{
		strClient = name;
	}
	
	void addTask(Task t)
	{
		String serv = t.getAgServ();
		if(!listSerie.contains(serv))
		{
			listSerie.add(serv);
		}
		listTask.add(t);
	}
	
	void sortData()
	{
		Collections.sort(listTask, 
				new Comparator<Task>()
				{

					@Override
					public int compare(Task o1, Task o2) {
						return o1.getIdTask() - o2.getIdTask();
					}
			
				}
		);
		Collections.sort(listSerie);
	}
	
	@Override
	public int getSeriesCount() {
		return listSerie.size();
	}

	@Override
	public Comparable getSeriesKey(int i) {
		return listSerie.get(i);
	}

	@Override
	public int indexOf(Comparable compa) {
		for(int  i = 0; i < listSerie.size(); i++)
		{
			if(listSerie.get(i).equals(compa))
				return i;
		}
		return -1;
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {

	}

	DatasetGroup dg = new DatasetGroup();
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {

	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		dg = arg0;
	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}
	
	@Override
	public int getItemCount(int arg0) {
		return listTask.size();
	}

	@Override
	public Number getX(int serie, int p) 
	{
		String serv = listSerie.get(serie);
		Task t = listTask.get(p);
		return t.getIdTask();
	}

	@Override
	public double getXValue(int arg0, int arg1) {
		return getX(arg0, arg1).doubleValue();
	}

	@Override
	public Number getY(int serie, int p) {
		String serv = listSerie.get(serie);
		Task t = null;
		
		ListIterator<Task> it = listTask.listIterator(p);
		
		
		while(it.hasPrevious() && t == null)
		{
			Task actualTask = it.previous();
			if(actualTask.getAgServ().equals(serv))
			{
				t = actualTask;
			}
		}
		if(t == null)
		{
			return 0.5; 
		}else
		{
			double ms = t.getMediaSuccess();
			if(ms > 1.0 || ms < 0.0)
			{
				int i = 0;
			}
			return ms;
		}
	}

	@Override
	public double getYValue(int serie, int p) {
		return getY(serie, p).doubleValue();
	}

}
