package br.uff.ic.taskalloc;

public class OrganizationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrganizationException(String msg)
	{
		super(msg, null);
	}
	
	public OrganizationException(String msg, Exception base)
	{
		super(msg, base);
	}
	
	@Override
	public String toString()
	{
		return String.format("OrganizationException(msg(%s), parent(%s))", getMessage(), getCause()!=null?getCause().toString():"no cause");
	}
}
