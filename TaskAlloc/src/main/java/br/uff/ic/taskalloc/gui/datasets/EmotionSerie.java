package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.uff.ic.util.Pair;

class EmotionSerie {
	
	String nameEmotion;
	
	int pos = 0;
	
	ArrayList<Pair<Integer, Double>> listData;
	
	public EmotionSerie(String name)
	{
		nameEmotion = name;
		listData = new ArrayList<Pair<Integer, Double>>();
	}
	
	public int getPos()
	{
		return pos;
	}
	
	public void setPos(int p)
	{
		pos = p;
	}
	
	public String getName()
	{
		return nameEmotion;
	}
	public int size()
	{
		return listData.size();
	}
	
	
	public Double getY(int pos)
	{
		return listData.get(pos).getY();
	}
	
	public Integer getX(int pos)
	{
		return listData.get(pos).getX();
	}
	
	public Pair<Integer, Double> get(int pos)
	{
		return listData.get(pos);
	}

	public void add(int x, double y) {
		if(listData.size() > 0)
		{
			Pair<Integer, Double> pair = listData.get(listData.size()-1);
			if(pair.getX() >= x)
			{
				return;//n�o insere para n�o duplicar
			}
		}
		listData.add(new Pair<Integer, Double>(x, y));
	}

	public void fixRange(int min, int max) {
				
			Pair<Integer, Double> first = listData.get(0);
			Pair<Integer, Double> last  = listData.get(listData.size()-1);
			
			ArrayList<Pair<Integer, Double>> newList = new ArrayList<Pair<Integer, Double>>(listData.size()+2);
			if(first.getX() > min)
			{
				Pair<Integer, Double> preFirst = new Pair<Integer, Double>(first.getX(), 0.0);
				first = new Pair<Integer, Double>( min, 0.0);
				newList.add(first);
				newList.add(preFirst);
			}
			
			newList.addAll(listData);
			
			if(last.getX() < max)
			{
				Pair<Integer, Double> preLast = new Pair<Integer, Double>(last.getX(), 0.0);
				last = new Pair<Integer, Double>( max, 0.0);
				newList.add(preLast);
				newList.add(last);
			}
			listData = newList;
		};
	
}
