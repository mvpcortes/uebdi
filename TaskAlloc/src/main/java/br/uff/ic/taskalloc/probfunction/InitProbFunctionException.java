package br.uff.ic.taskalloc.probfunction;

public class InitProbFunctionException extends Exception {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InitProbFunctionException(String msg, Exception parent)
	{
		super(msg, parent);
	}
}
