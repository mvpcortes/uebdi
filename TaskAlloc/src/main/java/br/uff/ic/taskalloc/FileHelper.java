package br.uff.ic.taskalloc;

import java.io.File;
import java.io.FileFilter;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public class FileHelper {

	public static String getExtension(String name)
	{
		int pos = name.indexOf('.');
		if(pos >=0)
		{
			return name.substring(pos+1, name.length());
		}
		return "";
	}
	
	public static Collection<File> findFiles(File path, Collection<File> collDest, final FileFilter fileFilter)
	{		
		Queue<File> queuePath = new LinkedList<File>();
		queuePath.offer(path);

		while(queuePath.size() >0)
		{

			path = queuePath.remove();
			File[] childs = path.listFiles(new FileFilter() {
				
				@Override
				public boolean accept(File pathname) {

					return (pathname.isDirectory() || fileFilter.accept(pathname));
				}
			});
			for(File fileChild: childs)
			{
				if(fileChild.isDirectory())
				{
					queuePath.offer(fileChild);
				}else
				{
					collDest.add(fileChild);
				}
			}
		}
		return collDest;	
	}

	public static String getNameWithoutExtension(String name) {
		int pos = name.indexOf('.');
		if(pos >=0)
		{
			return name.substring(0, pos);
		}
		return "";
	}

}
