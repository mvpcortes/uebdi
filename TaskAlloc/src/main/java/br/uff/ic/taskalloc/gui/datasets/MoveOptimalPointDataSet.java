package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.uff.ic.taskalloc.Task;
import br.uff.ic.util.Pair;


public class MoveOptimalPointDataSet extends OptimalPointDataSet{

	public MoveOptimalPointDataSet(List<Task> list)
	{
		super(new ArrayList<Task>());
		Map<String, Pair<Integer, Double>> mapSuccess 	= new HashMap<String, Pair<Integer, Double>>();
		
		final double alpha = 1;
		for(Task t: list)
		{
			if(t.getSuccess() !=null)
			{
				final String agClient = t.getAgClient();
				Pair<Integer, Double> pair = mapSuccess.get(agClient);
				double prevValue = 0;
				int prevId = 0;
				
				if(pair != null)
				{
					prevValue = pair.getY();
					prevId    = pair.getX();
				}
				
				double value = t.getOptimalPointSelServ();
				int id       = t.getIdTask();
				double insertValue;
				if(prevId <= 0)
				{
					insertValue = value;
				}else
				{
					insertValue = ((double)(alpha*value + (1-alpha)*prevValue));
				}
				
				Serie serie = this.getSerie(agClient);
				serie.addPoint(t.getIdTask(), insertValue);
				mapSuccess.put(agClient, new Pair<Integer, Double>(id, insertValue));
			}
				
				
		}
	}
	
}
