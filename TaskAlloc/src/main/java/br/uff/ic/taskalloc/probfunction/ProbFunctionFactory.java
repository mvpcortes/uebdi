package br.uff.ic.taskalloc.probfunction;

import jason.asSemantics.Agent;
import jason.asSyntax.Term;

import java.util.Collections;
import java.util.logging.Logger;

public class ProbFunctionFactory {
	
	private static Logger logger = Logger.getLogger(Agent.class.getName());
	
	private static Logger getLogger()
	{
		return logger;
	}
	   
	public static IProbFunction createFunc(String name)
	{
		IProbFunction ipf = createByAlias(name); 
			
		if(ipf == null)
		{
			try
			{
				@SuppressWarnings("unchecked")
				Class<IProbFunction> c= (Class<IProbFunction>) Class.forName(name);
				
				ipf = c.newInstance();
				
			} catch (ClassNotFoundException e) {
				getLogger().warning("Cannot create instance of " + name + " because ClassNotFoundException");
				ipf = null;
			} catch (InstantiationException e) {
				getLogger().warning("Cannot create instance of " + name + " because InstantiationException");
				ipf = null;
			} catch (IllegalAccessException e) {
				getLogger().warning("Cannot create instance of " + name + " because IllegalAccessException");
				ipf = null;
			}
		}
		if(ipf == null)
		{
			ipf = defaultFunc();
		}

		return ipf;		
	}
	
	
	static private IProbFunction createByAlias(String name) {
		if("map".equals(name))
		{
			return new MapProbFunction();
		}else if("const".equals(name))
		{
			return new FixProbFunction();
		}else if("random".equals(name))
		{
			
			return new RandomProbFunction();
		}else if("semiconst".equals(name))
		{
			return new SemiConstRandomProbFunction();
		}else
		{
			return null;
		}
	}


	static IProbFunction defaultIPF = null;
	static{
		defaultIPF = new RandomProbFunction();
		try {
			defaultIPF.init(Collections.<Term> emptyList());
		} catch (InitProbFunctionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static IProbFunction defaultFunc()
	{
		return defaultIPF;
	}
}
