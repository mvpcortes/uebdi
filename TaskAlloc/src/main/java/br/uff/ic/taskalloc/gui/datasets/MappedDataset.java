package br.uff.ic.taskalloc.gui.datasets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.gui.GenericConfig;

/**
 * � um dataset que cont�m outros datasets. O que � exibido � selecionado por "setDataset". O tipo da chave � sempre string
 * @author Marcos
 *
 * @param <T>
 */
public class MappedDataset implements XYDataset {
	
	private Map<String, XYDataset> mapDataset = null;
	
	private XYDataset currentDataset = null;
	
	private List<DatasetChangeListener> listChangeListener = new ArrayList<DatasetChangeListener>();
	private XYDataset getCurrent()
	{
		if(currentDataset == null)
		{
			if(mapDataset == null || mapDataset.size() <= 0)
			{
				return new XYDataset() {
					
					@Override
					public void setGroup(DatasetGroup arg0) {
						
					}
					
					@Override
					public void removeChangeListener(DatasetChangeListener arg0) {
						
					}
					
					@Override
					public DatasetGroup getGroup() {
						return new DatasetGroup();
					}
					
					@Override
					public void addChangeListener(DatasetChangeListener arg0) {
						
					}
					
					@Override
					public int indexOf(Comparable arg0) {
						return 0;
					}
					
					@Override
					public Comparable getSeriesKey(int arg0) {
						return "";
					}
					
					@Override
					public int getSeriesCount() {
						return 0;
					}
					
					@Override
					public double getYValue(int arg0, int arg1) {
						return 0;
					}
					
					@Override
					public Number getY(int arg0, int arg1) {
						return 0;
					}
					
					@Override
					public double getXValue(int arg0, int arg1) {
						return 0;
					}
					
					@Override
					public Number getX(int arg0, int arg1) {
						return 0;
					}
					
					@Override
					public int getItemCount(int arg0) {
						return 0;
					}
					
					@Override
					public DomainOrder getDomainOrder() {
						return DomainOrder.ASCENDING;
					}
				};
			}else
			{
				Iterator<Map.Entry<String, XYDataset>> it = mapDataset.entrySet().iterator();
				currentDataset = it.next().getValue();
			}
		}
		return currentDataset;
	}

	public void put(String name, XYDataset xyd)
	{
		if(mapDataset == null)
		{
			mapDataset = new HashMap<String, XYDataset>();
		}
		
		mapDataset.put(name, xyd);
		if(currentDataset == null)
		{
			currentDataset =xyd;
		}
	}
	
	public Iterable<XYDataset> getDataSets()
	{
		return new Iterable<XYDataset>() {
			
			@Override
			public Iterator<XYDataset> iterator() {
				return new Iterator<XYDataset>() 
				{
					Iterator<Map.Entry<String, XYDataset>> it = mapDataset.entrySet().iterator();

					@Override
					public boolean hasNext() {
						return it.hasNext();
					}

					@Override
					public XYDataset next() {
						Map.Entry<String, XYDataset> e = it.next();
						return e.getValue();
					}

					@Override
					public void remove() {
						it.remove();
					}
					
				};
			}
		};
	}
	public void setCurrent(String name)
	{
		XYDataset oldDataset = currentDataset;
		XYDataset newDataset = mapDataset.get(name); 
		if(newDataset != null)
		{
			currentDataset = newDataset;
			updateDataset(oldDataset, newDataset);
		}
		
	}
	
	private void updateDataset(XYDataset oldDataset, XYDataset newDataset)
	{
		for(DatasetChangeListener lc: listChangeListener)
		{
			oldDataset.removeChangeListener(lc);
			newDataset.addChangeListener(lc);
			lc.datasetChanged( new DatasetChangeEvent(this, this));
		}
	}
	
	public XYDataset get(String name)
	{
		if(mapDataset == null)
			return null;
		else
			return mapDataset.get(name);
	}
	
	@Override
	public int getSeriesCount() {
		return getCurrent().getSeriesCount();
	}

	@Override
	public Comparable getSeriesKey(int arg0) {
		return getCurrent().getSeriesKey(arg0);
	}

	@Override
	public int indexOf(Comparable arg0) {
		return getCurrent().indexOf(arg0);
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		listChangeListener.add(arg0);
		getCurrent().addChangeListener(arg0);
		
	}

	@Override
	public DatasetGroup getGroup() {
		return getCurrent().getGroup();
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
		listChangeListener.remove(arg0);
		getCurrent().removeChangeListener(arg0);
	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		getCurrent().setGroup(arg0);
		
	}

	@Override
	public DomainOrder getDomainOrder() {
		return getCurrent().getDomainOrder();
	}

	@Override
	public int getItemCount(int arg0) {
		return getCurrent().getItemCount(arg0);
	}

	@Override
	public Number getX(int arg0, int arg1) {
		return getCurrent().getX(arg0, arg1);
	}

	@Override
	public double getXValue(int arg0, int arg1) {
		return getCurrent().getXValue(arg0, arg1);
	}

	@Override
	public Number getY(int arg0, int arg1) {
		return getCurrent().getY(arg0, arg1);
	}

	@Override
	public double getYValue(int arg0, int arg1) {
		return getCurrent().getYValue(arg0, arg1);
	}
	
	public GenericConfig createConfig()
	{
		List<Object> names = new ArrayList<Object>();
		for(Map.Entry<String, XYDataset> e: mapDataset.entrySet())
		{
			names.add(e.getKey());
		}
		return new GenericConfig(names, this);
	}
	
}
