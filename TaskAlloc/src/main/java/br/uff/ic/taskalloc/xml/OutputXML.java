package br.uff.ic.taskalloc.xml;

import jason.asSyntax.Literal;
import jason.asSyntax.Term;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.text.NumberFormat;
import java.util.Locale;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import br.uff.ic.taskalloc.AgState;
import br.uff.ic.taskalloc.Task;
import br.uff.ic.util.TermHelper;

import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;


public class OutputXML {

	private XMLStreamWriter xmlWriter;
	
	URI uri = null;
	
	public OutputXML(OutputStream _o, URI _uri)
	{
		try {
			xmlWriter = new
						IndentingXMLStreamWriter(
								XMLOutputFactory.newInstance().createXMLStreamWriter(
										new OutputStreamWriter(_o, "utf-8")
								)
						);
			uri = _uri;
			xmlWriter.writeStartDocument();
		} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		}
		
		try {
			xmlWriter.writeProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"table.xsl\"");
			xmlWriter.writeStartElement(CONSTANTS_XML.STR_TASKALLOC);
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}
	
	public final URI getURI()
	{
		return uri;
	}
	
	public synchronized void close()
	{
		if(xmlWriter == null) return;
		
		try {
			xmlWriter.writeEndElement();
			xmlWriter.writeEndDocument();
			xmlWriter.close();
			xmlWriter = null;
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	
	public synchronized void write (Task tal)
	{
		try {
			xmlWriter.writeStartElement(CONSTANTS_XML.STR_TASK);
				
				writeTag(CONSTANTS_XML.STR_ID		, tal.getIdTask());	
				writeTag(CONSTANTS_XML.STR_CLIENT	, tal.getAgClient());				
				writeTag(CONSTANTS_XML.STR_SERV		, tal.getAgServ());
				writeTag(CONSTANTS_XML.STR_DEV		, tal.getDesvioMonetario());
								
				writeTag(CONSTANTS_XML.STR_SUCCESS	, tal.getStrSuccess());
				
				writeTag(CONSTANTS_XML.STR_MS		, tal.getMediaSuccess());
				
				writeTag(CONSTANTS_XML.STR_MR		, tal.getMediaRep());
				writeTag(CONSTANTS_XML.STR_OP_POINT	, tal.getOptimalPointSelServ());
				
				writeTag(CONSTANTS_XML.STR_LIFE_TIME	, (((double)tal.getLifeTime())*(1.0e-9)));
				
				xmlWriter.writeStartElement(CONSTANTS_XML.STR_CANDIDATE);
					
					writeTag(CONSTANTS_XML.STR_CANDIDATE_COUNT, tal.getAgCandidate().size());
					for(String s: tal.getAgCandidate())
					{
						writeTag(CONSTANTS_XML.STR_NAME, s);
					}
						
				xmlWriter.writeEndElement();
				AgState servState = tal.getServState();
				if(servState != null)
				{
					writeTag(CONSTANTS_XML.STR_SERV_COST, servState.getCost());
					writeTag(CONSTANTS_XML.STR_SERV_PROB, servState.getP());
				}
				xmlWriter.writeStartElement(CONSTANTS_XML.STR_AGENTS);
					for(AgState am: tal.getAgStates())
					{						
						xmlWriter.writeStartElement(CONSTANTS_XML.STR_AGENT);
							writeTag(CONSTANTS_XML.STR_NAME, am.getAgent());
							if(am.getMoney() >= 0)
								writeTag(CONSTANTS_XML.STR_MONEY, am.getMoney());
							if(am.getP() >= 0)
								writeTag(CONSTANTS_XML.STR_PROB, am.getP());
							if(am.getCost() >=0)
								writeTag(CONSTANTS_XML.STR_COST, am.getCost());
							if(am.getPoint() >= 0)
								writeTag(CONSTANTS_XML.STR_POINT, am.getPoint());
							if(am.getEmotions()!= null && am.getEmotions().size()>0)
								writeEmotions(am.getEmotions());
							
							for(Term t: am.getMS())
							{
								if(t.isLiteral())
								{
									writeMS((Literal)t);
								}
							}
						xmlWriter.writeEndElement();
					}
				xmlWriter.writeEndElement();		
			xmlWriter.writeEndElement();
			xmlWriter.flush();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		

	}
	

	private void writeMS(Literal t) throws XMLStreamException {
		int id = TermHelper.getInteger(t, 0);
		String ag = TermHelper.getString(t, 1);
		double d = TermHelper.getDouble(t, 2);
		xmlWriter.writeStartElement(CONSTANTS_XML.STR_MS_DATA);
			writeTag(CONSTANTS_XML.STR_MS_ID, id);
			writeTag(CONSTANTS_XML.STR_MS_OTHER, ag);
			writeTag(CONSTANTS_XML.STR_MS_VALUE, d);
		xmlWriter.writeEndElement();
	}

	private synchronized  void writeEmotions(Iterable<Term> emotions) throws XMLStreamException {
		xmlWriter.writeStartElement(CONSTANTS_XML.STR_EMOTIONS);
		for(Term t: emotions)
		{
			if(t.isLiteral())
			{
				xmlWriter.writeStartElement(CONSTANTS_XML.STR_EMOTION);
					Literal l = (Literal)t;
					final String name 		= TermHelper.getString(l,0);
					final String agName		= TermHelper.getString(l,1);
					final double intensity 	= TermHelper.getDouble(l,2);
					
					writeTag(CONSTANTS_XML.STR_NAME, name);
					writeTag(CONSTANTS_XML.STR_AG_NAME, agName);
					writeTag(CONSTANTS_XML.STR_INTENSITY, intensity);
				xmlWriter.writeEndElement();
			}			
		}
		xmlWriter.writeEndElement();
	}

	private synchronized void writeTag(String strTag, String data) throws XMLStreamException {
		xmlWriter.writeStartElement(strTag);
			xmlWriter.writeCharacters(data);
		xmlWriter.writeEndElement();
	}
	
	private synchronized void writeTag(String strTag, double data) throws XMLStreamException {
		xmlWriter.writeStartElement(strTag);
		String strD = NumberFormat.getInstance(CONSTANTS_XML.LOCALE).format(data);
			xmlWriter.writeCharacters(strD);
		xmlWriter.writeEndElement();
		
	}
	
	private synchronized void writeTag(String strTag, long data) throws XMLStreamException {
		xmlWriter.writeStartElement(strTag);
			xmlWriter.writeCharacters(String.format("%d", data));
		xmlWriter.writeEndElement();
		
	}

	public void writeStatistics(long delay)
	{
		try {
		xmlWriter.writeStartElement(CONSTANTS_XML.STR_STATISTICS);
			xmlWriter.writeStartElement(CONSTANTS_XML.STR_CPU_TIME);
				xmlWriter.writeAttribute(CONSTANTS_XML.STR_UNIT, "s");
				xmlWriter.writeCharacters(String.format("%.2f", (((double)delay)*(1.0E-9))));
			xmlWriter.writeEndElement();
		xmlWriter.writeEndElement();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}catch(Exception ee)
		{
		    ee.printStackTrace();
		}
	}
	
	
}
