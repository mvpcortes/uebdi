package br.uff.ic.taskalloc.probfunction;

import jason.asSyntax.Term;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import br.uff.ic.taskalloc.Task;
import br.uff.ic.taskalloc.TaskManagerListener;
import br.uff.ic.util.Pair;
import br.uff.ic.util.TermHelper;

/**
 * Uma fun��o totalmente randomica
 * @author Marcos
 */
public class RandomProbFunction implements IProbFunction, TaskManagerListener {
		
	private Logger logger = Logger.getLogger(this.getClass().getName());
	private double min = 0;
	
	private double max = 1;
	

	
	private class PairParam extends Pair<Integer, String>
	{

		public PairParam(Integer a, String b) {
			super(a, b);
		}

		public PairParam(Task task) {
			super(task.getIdTask(), task.getAgClient());
		}
		
	}
	
	private Map<PairParam, Double> mapProb = new HashMap<PairParam, Double>();
	
	private Double getRandom()
	{
		return 	new Double(Math.random()*(max-min) + min);
	}
	@Override
	public double run(String agent, int idTask) {
		
		PairParam key = new PairParam(idTask, agent);
		Double p = mapProb.get(key);
		if(p == null)
		{
			getLogger().severe("A random prob query not had generated. Generate now.");
		
			p = getRandom();
			mapProb.put(key, p);
		}
		return p;
	}
	
	private Logger getLogger() {
		return logger;
	}
		

	/**
	 * random(MIN_PROB, MAX_PROB)
	 */
	@Override
	public void init(Iterable<Term> params) throws InitProbFunctionException {
		
		Iterator<Term> it = params.iterator();
		if(it.hasNext() )
		{
			double min =  TermHelper.toDouble(it.next());

			if(it.hasNext())
			{
				double max =  TermHelper.toDouble(it.next());
				setMinMax(min, max);
			}
		}
	}

	private void setMinMax(double _min, double _max) {
		min = _min;
		max = _max;
	}

	@Override
	public void onCreate(int id) {}

	@Override
	public void onRegister(int id, String strClient) {
		PairParam key = new PairParam(id, strClient);
		Double d = mapProb.put(key, getRandom());
		if(d!= null)
		{
			getLogger().warning("Registing a already key.");
		}
	}

	@Override
	public void onAalloc(int id, String strServ) {}

	@Override
	public void onExecute(int id, boolean success) {}

	@Override
	public void onFinish(int id, double mediaSuccess, double mediaRep) {}

	@Override
	public void onUnregister(Task task) {
		PairParam key = new PairParam(task);
		
		Double value = mapProb.remove(key);
		if(value == null)
		{
			getLogger().warning("unregister a not registed key!");
		}
		
	}
	
	@Override
	public String toString()
	{
		return String.format("random(%.3f, %.3f)", min, max);
	}
}
