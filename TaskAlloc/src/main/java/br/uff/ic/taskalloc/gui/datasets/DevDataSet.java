package br.uff.ic.taskalloc.gui.datasets;

import java.util.List;


import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

import br.uff.ic.taskalloc.Task;


public class DevDataSet implements XYDataset {

	private List<Task> coll;

	protected List<Task> getTasks()
	{
		return coll;
	}
	
	@Override
	public int getSeriesCount() {
		return 1;
	}

	public DevDataSet(List<Task> list)
	{
		coll = list;
	}
	@Override
	public Comparable getSeriesKey(int arg0) {
		return "dev";
	}

	@Override
	public int indexOf(Comparable arg0) {
		if("dev".equals(arg0))
		{
			return 0;
		}else
		{
			return -1;
		}
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		// TODO Auto-generated method stub

	}

	private DatasetGroup dg = new DatasetGroup();
	@Override
	public DatasetGroup getGroup() {
		return dg;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		dg = arg0;

	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int arg0) {
		return getTasks().size();
	}

	@Override
	public Number getX(int arg0, int arg1) {
		return getXValue(arg0, arg1);
	}

	@Override
	public double getXValue(int arg0, int arg1) {
		return getTasks().get(arg1).getIdTask();
	}

	@Override
	public Number getY(int arg0, int arg1) {
		return getYValue(arg0, arg1);
	}

	@Override
	public double getYValue(int arg0, int arg1) {
		return getTasks().get(arg1).getDesvioMonetario();
	}

}
