// Internal action code for project TaskAlloc

package br.uff.ic.taskalloc;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.LogicalFormula;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

import java.util.Iterator;

import br.uff.ic.util.TermHelper;

/**
 * Internal Action to calc media success of a agent. It use the success of a agent in the past.<br/>
 * format: calcMediaSuccess(agent, VAR)
 * params:<br/>
 * <li>
 * 		<lu>agent: the  agent to calc the media success</lu>
 * 		<lu>VAR: the var that receive the media success</lu> 
 * </li>
 * @author Marcos
 */
public class calcMediaSuccess extends DefaultInternalAction {

	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	    public int getMinArgs(){return 2;}
	    
	    @Override
	    public int getMaxArgs(){return getMinArgs();}
	    
	    @Override
	    public void checkArguments(Term[] terms)throws JasonException 
	    {
	    	if(!terms[0].isAtom())
	    		throw JasonException.createWrongArgument(this,"first term is not a atom");
	    	
	    	if(terms[1].isVar() && !terms[1].isGround()) 
	    		throw JasonException.createWrongArgument(this,"second term is not a var not grounded");
	    }
	    
	    
	    @Override
	    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

			// execute the internal action
		    ts.getAg().getLogger().fine("executing internal action 'br.uff.ic.taskalloc.calcMediaSuccess'");
		       
		    double sumValue = 0;
		    double sumWeight=0;
		    
		    Literal literalQuery = Literal.parseLiteral(String.format("success(task(XXX), %s, ZZZ)", args[0].toString())); 
		    Term var = literalQuery.clone();
		    LogicalFormula logExpr = (LogicalFormula)literalQuery.clone();
		 //   ListTerm all = new ListTermImpl();
		    {
		       // ListTerm tail = all;
		        Iterator<Unifier> iu = logExpr.logicalConsequence(ts.getAg(), un);
		        while (iu.hasNext()) {
		            Unifier nu = iu.next();
		            Term vl = var.clone();//sucess(task(I), A, Value);
		            vl.apply(nu);
		            Double d = getSuccessValue(vl);
		            Double i = getSuccessTaskid(vl);
		            
		            if(d != null && i != null)
		            {
		            	sumValue += d*i;
		            	sumWeight+= i;
		            }
		        }
		    }
	    
	    	return un.unifies(args[1], new NumberTermImpl(sumValue/sumWeight));
	    }

		private Double getSuccessTaskid(Term vl) {
			if(vl.isLiteral())
            {
            	Literal l = (Literal)vl;
            	if(l.getTerms().size() >= 1)
            	{
            		Term tn = ((Structure)vl).getTerm(0);
            		Term tTask = (Literal)((Structure)tn).getTerm(0);
            		return new Double(TermHelper.toDouble(tTask));
            	}
            }
			return null;
		}

		private Double getSuccessValue(Term vl) 
		{
			if(vl.isLiteral())
            {
            	Literal l = (Literal)vl;
            	if(l.getTerms().size() >= 3)
            	{
            		Term tn = ((Structure)vl).getTerm(2);
            		return new Double(TermHelper.toDouble(tn));
            	}
            }
			return null;
		}
	        
}
