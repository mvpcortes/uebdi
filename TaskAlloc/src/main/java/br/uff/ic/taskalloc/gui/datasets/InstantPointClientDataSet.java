package br.uff.ic.taskalloc.gui.datasets;

import java.util.List;


import br.uff.ic.taskalloc.Task;

public class InstantPointClientDataSet extends SuccessByClient {

	public InstantPointClientDataSet(List<Task> list) {
		super(list);
	}
	
	@Override
	public Number getY(int serie, int pos)
	{
		String name = (String) this.getSeriesKey(serie);
		List<Task> list = mapTasks.get(name);
		return list.get(pos).getOptimalPointSelServ();
	}
}
