package br.uff.ic.taskalloc.emotionmodel;

/**
 * Classe de custos (se um custo � considerado barato, justo ou caro por um agente.
 * @author Marcos
 *
 */
enum CostClass
{
	cheap		(-1),
	fair 		(0),
	expensive	(+1);
	
	/**
	 * A classe, seu ID num�rico
	 */
	private int classValue = 0;
	/**
	 * Transforma um dado valor e um valor considerado justo em CostClass
	 * @param cost O custo a ser transformado
	 * @param fairCost O custo Justo
	 * @return o CostClass
	 */
	public static CostClass getCostClass(int cost, int fairCost)
	{
		int delta = cost - fairCost;
		
		final int cheap_cost = -(fairCost/3);
		final int expensive_cost = +(fairCost/3);
		if(delta < cheap_cost)
		{
			return cheap;
		}
		
		if(delta > expensive_cost)
		{
			return expensive;
		}
		
		return fair;
	}
	
	private CostClass(int value)
	{
		classValue = value;
	}
	
	/**
	 * Retorna o valor da classse
	 * @return
	 */
	public final int getClassValue()
	{
		return classValue;
	}
	
}


