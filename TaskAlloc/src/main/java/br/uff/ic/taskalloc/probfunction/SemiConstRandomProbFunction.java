package br.uff.ic.taskalloc.probfunction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementa uma semiconstante randomica
 * @author Marcos
 *
 */
public final class SemiConstRandomProbFunction extends SemiConstantProbFunction {

	private class Cache
	{
		private double[] data;
		
		
		public Cache(int size)
		{
			resetCache(size);
		}
		public void resetCache(int newSize)
		{
			data = new double[newSize];
			Arrays.fill(data, -1);
		}
		
		@SuppressWarnings("unused")
		public int size() 
		{
			return data.length;
		}
		
		public double getValue(int pos)
		{
			if(pos >= 0 && pos < data.length)
			{
				if(data[pos] < 0)
				{
					data[pos] = Math.random();
				}
				
				return data[pos];
			}
			
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	
	private Map<String, Cache> mapAgentData = new HashMap<String, Cache>();
	
	public SemiConstRandomProbFunction()
	{
		super();
	
	}

	@Override
	protected double realFunc(String agent, double t) {
		
		Cache c = mapAgentData.get(agent);
		if(c == null)
		{
			c = new Cache(getBase());
			mapAgentData.put(agent, c);
		}
		return c.getValue((int)(t*getBase()));
	}
	
	@Override
	public void setBase(int _base)
	{
		super.setBase(_base);
		mapAgentData.clear();
	}

}
