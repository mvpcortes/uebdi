package br.uff.ic.taskalloc;

import jason.asSemantics.Agent;
import jason.asSemantics.Event;
import jason.asSyntax.Literal;
import jason.asSyntax.Trigger;
import jason.asSyntax.Trigger.TEOperator;
import jason.bb.BeliefBase;

import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import br.uff.ic.uebdi.DynHelper;
import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.IEmotionDyn;
import br.uff.ic.uebdi.IIntentionBase;
import br.uff.ic.uebdi.ISetAgent;
import br.uff.ic.util.TermHelper;

public class EmotionDynEmo implements IEmotionDyn, ISetAgent {

	private enum Confiance {
		LIKE(), UNLIKE(), NEUTRAL();

		@SuppressWarnings("unused")
		public static Confiance getConfiance(int value) {
			if (value < 4)
				return UNLIKE;
			else if (value > 6)
				return LIKE;
			else
				return NEUTRAL;
		}

		public static Confiance getConfiance(Literal l) {
			double value = TermHelper.getDouble(l, 2);
			if (value < 4)
				return UNLIKE;
			else if (value > 6)
				return LIKE;
			else
				return NEUTRAL;
		}
	}

	private Logger logger = null;

	private Agent ag = null;

	private Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(this.getClass().getName() + ":"
					+ this.getAgName());
		}
		return logger;
	}

	private String getAgName() {
		Agent ag = getAgent();
		if (ag != null) {
			return ag.getTS().getUserAgArch().getAgName();
		} else {
			return "noname";
		}
	}

	@Override
	public void setAgent(Agent _ag) {
		ag = _ag;
	}

	@Override
	public void firstReviewEmotion(List<Literal> listPerc, IEmotionBase eb,
			IIntentionBase intentionBase) {
		// none
	}

	public Agent getAgent() {

		return ag;
	}

	@Override
	public void secondReviewEmotion(BeliefBase bb, Queue<Event> events,
			IEmotionBase eb, IIntentionBase intentionBase) {

		Iterator<Event> it = DynHelper.getItEventQuery(events.iterator(),
				TEOperator.add, "success");

		while (it.hasNext()) {
			final Trigger tit = it.next().getTrigger();
			if (tit != null) {
				final Literal lit = tit.getLiteral();

				String myName = this.getAgent().getTS().getUserAgArch()
						.getAgName();
				// int H = (int) TermHelper.getDouble(lit, 0);
				String ag = TermHelper.getString(lit, 1);
				boolean S = TermHelper.getDouble(lit, 2) > 0;

				if (!ag.equals(myName))
					elicitEmotion(S, ag, eb);
			}
		}
	}

	private void elicitEmotion(boolean s, String ag, IEmotionBase eb) {
		Iterator<Literal> it = eb.getAll("like", ag);
		double incValue = 0;
		Confiance c = null;
		if (it.hasNext() == false) {
			c = Confiance.NEUTRAL;
		} else {
			Literal l = it.next();
			c = Confiance.getConfiance(l);
		}

		getLogger().info(
				String.format("elicitEmotion(%s, %s, %s)", Boolean.toString(s),
						ag, eb.toString()));
		switch (c) {
		case LIKE:
			if (s)
				incValue = +2;
			else
				incValue = -1;
			break;
		case UNLIKE:
			if (s)
				incValue = +1;
			else
				incValue = -2;
			break;
		case NEUTRAL:
			if (s)
				incValue = +1;
			else
				incValue = -1;
			break;
		}

		double intensity = eb.getIntensity("like", ag);
		double newIntensity = incValue + intensity;
		if (newIntensity > 10) {
			incValue = 10 - intensity;
		} else if (newIntensity < 0) {
			incValue = -intensity;
		}
		eb.incIntensity(incValue, "like", ag);
	}

	@Override
	public boolean criticalContext(IEmotionBase eb, BeliefBase bb) {
		return true;
	}
}
