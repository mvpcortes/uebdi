// Internal action code for project TaskAlloc

package br.uff.ic.taskalloc;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;
import br.uff.ic.util.TermHelper;

@SuppressWarnings("serial")
public class validCosts extends DefaultInternalAction {

    @Override
    public int getMinArgs(){return 3;}
    
    @Override
    public int getMaxArgs(){return getMinArgs();}
    
    @Override
    public void checkArguments(Term[] terms)throws JasonException 
    {
	if(!terms[0].isList()) 
	    throw JasonException.createWrongArgument(this,"first term is not a list");
	
	if(!terms[1].isNumeric())
	    throw JasonException.createWrongArgument(this,"second term is not a number");
	
	if(!(terms[2].isVar() && ((VarTerm)terms[2]).getValue()!= null))
	    throw JasonException.createWrongArgument(this,"third term is not a unnamedvar");
    }
    
    
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

	// execute the internal action
        ts.getAg().getLogger().fine("executing internal action 'br.uff.ic.taskalloc.validCosts'");
        
        ListTerm listCost = (ListTerm)args[0];
        
        final double money = TermHelper.toDouble(args[1]);
        
        ListTerm all = new ListTermImpl();
        ListTerm tail = all;
        for(Term t: listCost)
        {
            if(t.isLiteral())
            {
	        	Literal lcost = (Literal)t;
	        	if("cost".equals(lcost.getFunctor()) & lcost.getTerms().size() == 3)
	        	{
	        	    double costValue = TermHelper.getDouble(lcost, 2);
	        	    if(costValue <= money)
	        	    {
	        		tail = tail.append(lcost.clone());
	        	    }        		
	        	}
            }
        }
        
        return un.unifies(args[2], all);
    }
}
