package br.uff.ic.taskalloc;

/**
 * Represents a agent action in Task Alloc Scenario.
 * @author Marcos
 *
 */
public enum Action {
	/**none action*/
	NONE("none"),
	/**the agent executes a transference*/
	TRANSFER("transfer"),	
	/** se the candidates of task*/
	SET_CANDIDATES("setCandidates"),
	/**execute a action*/
	EXECUTE("execute"), 		
	/**enter in the market/organization*/
	ENTER("enter"), 			
	/**finish the task informing the execution result*/
	FINISH_TASK("finishTask"),
	/**inform their internal state*/
	INFORM_STATE("informState"), 
	REGISTER("register"),
	UNREGISTER("unregister");
	
	private String name = "";
	
	Action(String _name)
	{
		name = _name;
	}

	public Object getName() {
		return name;
	}
}
