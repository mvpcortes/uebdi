package br.uff.ic.taskalloc;

// Environment code for project TaskAlloc

import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.environment.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;


import br.uff.ic.taskalloc.xml.OutputXML;
import br.uff.ic.util.TermHelper;

public class EnvironmentTaskAlloc extends Environment {

	/**
	 * String constants to parse parameters
	 */
	private static final String STR_LOG 				= "log";
	private static final String STR_OUT_FILE 			= "out";
	
	private Map<Integer, Task> mapTask = new HashMap<Integer, Task>();
	/**
	 * determina se o ambiente j� foi iniciado
	 */
	private boolean initied = false;
	
	/**
	 * Momento que iniciou a organiza��o
	 */
	private long timeInit = -1;

	
	/**
	 * Fluxo de sa�da dos dados da simula��o
	 */
	OutputXML outXML = null;
	
	    
	/**
	 * ListernerEngine
	 */
	ListenerSend ls = new ListenerSend();
	
	//================================
	/**
	 * parse a structure (term collection) and return it id.<br/>
	 * For example: a structure task(I) and I is the same id task I (in primitive int).
	 * @param action
	 * @param i
	 * @return
	 */
	private static int getIdTask(Structure action, int i) {
		Term testTerm = null;
		if(i >= 0)
		{
			testTerm = action.getTerm(i);			
		}else
		{
			testTerm = action;
		}
		
		if(testTerm.isLiteral())
		{
			Literal temp = (Literal)testTerm;
			if(temp.getTerms().size() > 0)
				testTerm = temp.getTerm(0);
			else
				return -1;
		}
		
		return (int)TermHelper.toDouble(testTerm);
	}
	
	//================================================================================================
	//
	//instance methods
	//================================================================================================
	
	/**
	 * Set the output path file.
	 * @param strFile
	 * @return true if set succeeded.
	 */
	private boolean setOutput(String strFile)
	{
        try 
        {
        	if(strFile.equals(""))
    		{
        		strFile = "java:System.out";
    			outXML = new OutputXML(System.out, new URI(strFile));	
    		}
        	else
        	{
	        	File f = new File(strFile);
	        	outXML = new OutputXML(new FileOutputStream(f), f.toURI());
        	}
        	return true;
		} catch (FileNotFoundException e) {
			getLogger().severe("Cannot open xml output file: " + e.toString());
		} catch (URISyntaxException e) {
			getLogger().severe("Cannot create URI: " + e.toString());
		}
        setOutput("");
        return false;
	}
	
	/**
	 * Return the init time of simulation
	 */
	private long getTimeInit()
	{
		return timeInit;
	}
	
	/**
	 * set the time init
	 * @param time the initial time
	 */
	private void setTimeInit(long time)
	{
		this.timeInit = time;
	}
	
	/**
	 * Return the outputXML
	 */
	private OutputXML getOutXML() 
	{
		if(outXML == null)
		{
			setOutput("");
		}
		return outXML;
	}	
	
	/**
	 * This method receive a literal and parse it to set the log output.<br/>
	 * The literal is the form {@value #STR_LOG}(file, limit, count)
	 * @param param the literal
	 */
	private void setLoggerOutput(Literal param) {
		String fileName = TermHelper.getString(param, 0);
	
		String strLevel = TermHelper.getString(param, 1);
		strLevel = strLevel.toUpperCase();
		Level level = Level.INFO;
		if(!strLevel.equals(""))
		{
			try
			{
				level = Level.parse(strLevel);
			}catch(Exception e)
			{
				level = Level.INFO;
			}
		}
		
		int limit		= (int)TermHelper.getDouble(param, 2);
		if(limit<= 0)
			limit = 1000000000;
		
		int count		= (int)TermHelper.getDouble(param, 3);
		if(count <= 0)
			count = 1;
		
		
		try {
			FileHandler fh = new FileHandler(fileName, limit, count, false);
			fh.setLevel(level);
			
			//=============================
			Handler[] hs = Logger.getLogger("").getHandlers(); 
	        for (int i = 0; i < hs.length; i++) 
	        { 
	           	Logger.getLogger("").removeHandler(hs[i]); 
	        }
	        Logger.getLogger("").addHandler(fh);
		        
		} catch (SecurityException e) {
			getLogger().severe(String.format("Cannot add new FileHandler (\"%s\") because the Secutity Exeption (%s)", fileName, e.toString()));
		} catch (IOException e) {
			getLogger().severe(String.format("Cannot add new FileHandler (\"%s\") because the IOException (%s)", fileName, e.toString()));
		}
	}
	
	public EnvironmentTaskAlloc()
	{
	}
	
	
	/**
	 * transform a literal functor to the correspondence {@link Action} value. 
	 * @param functor the literal functor
	 * @return the Action
	 */
	private Action parse(String functor) {
		if(functor == null)
			return Action.NONE;
		for(Action act: Action.values())
		{
			if(act.getName().equals(functor))
				return act;
		}
		
		return Action.NONE;
	}

/**
   * executa a tarefa
   * @param agName O agnete que executar� a tarefa (servi)
   * @param action O conjunto de terms passados por par�metro
   * @return retorn se conseguiu o un�o realizar com sucesso
   * @throws OrganizationException 
  */
	private synchronized boolean register(String agName, Structure action) throws OrganizationException 
	{
		int id 				= getIdTask(action, 0);
		String strClient 	= TermHelper.toString(action.getTerm(1));
		ls.onRegister(id, strClient);
		mapTask.put(id, new Task(id));
		return true;
	}
	
/**
   * executa a tarefa
   * @param agName O agnete que executar� a tarefa (servi)
   * @param action O conjunto de terms passados por par�metro
   * @return retorn se conseguiu o un�o realizar com sucesso
   * @throws OrganizationException 
  */
	private synchronized boolean unregister(String agName, Structure action) throws OrganizationException 
	{
		//unregister(ID, CLIENT, SERV, SUCCESS, MS, MR, CANDIDATES, INT_DATA);
		int id 			= getIdTask(action, 0);
		Task t= mapTask.get(id);
		if(t==null)
		{
			getLogger().severe(String.format("Cannot found task %d", id));
			t = new Task(id);
		}
		String client 	= TermHelper.toString(action.getTerm(1));
		String serv		= TermHelper.toString(action.getTerm(2));
		int    success	= (int) TermHelper.toDouble(action.getTerm(3));
		double ms		= TermHelper.toDouble(action.getTerm(4));
		double mr		= TermHelper.toDouble(action.getTerm(5));
		Collection<Term> collCand = TermHelper.toCollTerm(action, 6);
		Collection<Term> collInfo = TermHelper.toCollTerm(action, 7);
		t.setAgClient(client);
		t.setAgServ(serv);
		t.setMediaSuccess(ms);
		t.setMediaRep(mr);
		t.setSuccess(success!=0);
		for(Term item: collCand)
			t.addAgCandidate(item.toString());
			
				
		for(Term info: collInfo)
		{
			if(info.isLiteral())
			{
				Literal l = (Literal)info;
				if(!l.negated())
				{
					String ag   = TermHelper.getString(l, 1);
					double prob = TermHelper.getDouble(l, 2);
					long cost 	= (long)TermHelper.getDouble(l, 3);
					long money  = (long)TermHelper.getDouble(l, 4);
					Collection<Term> cemotions = TermHelper.toCollTerm((Structure) l, 5);
					Collection<Term> cms       = TermHelper.toCollTerm((Structure) l, 6);
					t.addAgState(new AgState(ag, cost, money, prob, cemotions, cms));
				}
			}else
			{
				getLogger().severe("A internalData not valid");
			}
		}
		
		t.setStage(Task.Stage.unregisted);//para for�ar gerar o tempo
		ls.onUnregister(t);
		mapTask.remove(id);
		getOutXML().write(t);
		return true;
	}
/**
   * executa a tarefa
   * @param agName O agnete que executar� a tarefa (servi)
   * @param action O conjunto de terms passados por par�metro
   * @return retorn se conseguiu o un�o realizar com sucesso
   * @throws OrganizationException 
  */
	private synchronized boolean execute(String agName, Structure action) throws OrganizationException {
		if(action.getTerms().size() != 1)
			throw new OrganizationException("execute action: Invalid number of args  " + action.toString());
	
		int id = (int)Math.round(TermHelper.getDouble(action, 0));
		
		double p = 0;//realProb(agName, id);
		double dado = Math.random();
		int result = (dado < p)?1:0;
		getLogger().info(String.format("Agent %s execute task %d with prob %d and success %d",
				agName,
				id,
				(int)(p*100),
				result
			));
		/*try
		{
			getTaskManager().alloc(id, agName);
			getTaskManager().execute(id, result!=0);
		}catch(TaskManagerException tme)
		{
			throw new OrganizationException("Problem in alloc task", tme);
		}
		
		addPercept(agName, Literal.parseLiteral(String.format("success(task(%s), %s ,%d)",id, agName, result)));
		*/
			return true;
	}
	
	//=============================================================
    
	
	/** Called before the MAS execution with the args informed in .mas2j 
     * 
     * 
     * */
    @Override
    public void init(String[] args) {
        super.init(args);
        
    //associa listener
    //taskManager.addListener(new CostTMListener(this));
        
    
    setTimeInit(System.nanoTime());//armazena tempo inicial
    
    
    parseInitParam(args);
    
    
    //======================
	//getLogger().info(String.format("Execute experiment for %d tasks and outfile = %s", getTaskCount(), getOutXML().getURI()));
	//getLogger().info("The prob function is: " + probFunction.toString());
	initied = true;
    }
    
  
    /**
	 * Interprets the params of organization.
	 * <ul>
	 * <li><code>taskCount(count)</code>: the number of count in a execution. Default value
	 * is 10;</li>
	 * <li><code>parallelTaks(n)</code>: Number max of proccess of allocation task. Default
	 * is 10;</li>
	 * <li><code>outFile(strFile)</code>: String with the path and name of a output text
	 * file. The organization will put out the results the simulation. If this
	 * param not is defined, the Organization use the System.out OutputStream;</li>
	 * <li><code>visionCost(n)</code>: n is the percent of agents in the system wich a agent
	 * client should send a request to alloc a task. this value is between
	 * [0..1] and the default is 0.7;</li>
	 * <li><code>visionRep(n)</code>: n is the percent of agents in the system wich a client
	 * agent should ask the reputation of a candidate agent. This value is
	 * between [0..1] and the default is 0.2;</li>
	 * <li><code>pFunc(pFunc(...))</code> Define the agent probability function of a agent 
	 * <i><code>a</i></code> to a <code><i>task(i)</i></code>. A probability function implements
	 *  the interface {@link br.uff.ic.taskalloc.probfunction.IProbFunction}<br/>
	 *  the basic use is specify a list of terms in this form:
	 *  	<ul>
	 *  		<li><code>map(x([agents_x],...), y([agents_y], ...), ...)</code>: maps the agents <code>agents_x</code> to the function <code>x</code>. 
	 *  See that the next params of them map are the params of x function</li> 
	 *  		<li><code>random([agents])</code>: use the random function to the <code>agents.</code></li>
	 *  		<li><code>const([agents], value)</code>: use a const <code>value</code> to the <code>agents</code>. </li>
	 *  	</ul>
	 *  	For Example:<br/>
	 *  	pFunc(<br/>
	 *  			const([a, b], "0.5"),<br/>
	 *  			random([c,d, e]),<br/>
	 *  			map(const([f, g], "0.2"), const([l,m], "1"))<br/>  	
	 *  		)
	 *  </li>
	 *  <li><code>defaultMoney(n)</code>: the default initial money to all agents.
	 *  Without it all agents receive a random value between [{@link MIN_COST}, {@link MAX_COST}];</li>
	 *  <li><code>log(strFile, limit, count)</code> Define the name of output file and the limit size and the count files. It follow de java getLogger() syntax.</li>
	 * </ul>
	 * <br/>
	 * </p>
	 * 
	 * @param args
	 *            the params.
	 *            
	 */
    
	private void parseInitParam(final String[] args) {
		//interpreta todos os argumentos
        for(String arg: args)
        {
        	Literal param = Literal.parseLiteral(arg);
	    	if(param != null)
	    	{
	    		final String functor = param.getFunctor();
	    		if(EnvironmentTaskAlloc.STR_OUT_FILE.equals(functor))
	    		{
	    			String temp = TermHelper.toString(param.getTerm(0));
	    			setOutput(temp);
	    		}else if(EnvironmentTaskAlloc.STR_LOG.equals(functor))
	    		{
	    			setLoggerOutput(param); 
	    		}
	    		
	    	/*	final String FUNCTOR = param.getFunctor();
	    		if(Organization.STR_TASK_COUNT.equals(FUNCTOR))
	    		{
	    			setTaskCount((int)TermHelper.getDouble(param, 0));
	    		}
	    		else if(STR_PARALEL_TASK.equals(FUNCTOR))
	    		{
	    			setMaxParallelTasks((int)TermHelper.getDouble(param, 0));
	    		}        		
	    		else if(STR_OUT_FILE.equals(FUNCTOR))
	    		{
	    			String temp = TermHelper.toString(param.getTerm(0));
	    			setOutput(temp);	    			
	    			
	    		}else if(STR_VISION_COST.equals(FUNCTOR))
	    		{
	    			setVisionCost(TermHelper.getDouble(param, 0));
	    		}else if(STR_VISION_REP.equals(FUNCTOR))
	    		{
	    			setVisionRep(TermHelper.getDouble(param, 0));
	    		}else if(STR_P_FUNC.equals(FUNCTOR))
	    		{
	    			processPFunction(param);
	    		}else if(STR_DEFAULT_MONEY.equals(FUNCTOR))
	    		{
	    			setDefaultMoney((int)TermHelper.getDouble(param, 0));
	    		}
	    		else if(STR_LOG.equals(FUNCTOR))
	    		{
	    			setLoggerOutput(param); 
	    		}
	    		else
	    		{
	    			getLogger().warning(String.format("init: Cannot parse {%s} param of Organization", param.toString()));
	    		}*/
	    	}else
	    	{
	    		getLogger().warning(String.format("init: \"%s\" param is not a Literal", arg.toString()));
	    	}
	    }
	}



	@Override
    public boolean executeAction(String agName, Structure action) {
        try
        {
        	getLogger().info(String.format("executing action \"%s\" by agent %s", action.getFunctor(), agName));
	        switch(parse(action.getFunctor()))
	        {
	        	case REGISTER:
	        		register(agName, action);
	        		return true;
	        	case UNREGISTER:
	        		unregister(agName, action);
	        		return true;
	        	case EXECUTE:
	        		return execute(agName, action);
	        	case NONE:
	        	default:
	        		return false;
	        }
        }catch(OrganizationException e)
        {
        	getLogger().severe(String.format("Problem executing \"%s\" action by agent %s", action.getFunctor(), agName));
        }
        
        return false;
    }

	
	
	
    /** Called before the end of MAS execution */
    @Override
    public void stop() {
    	getLogger().info("Stop Organization");
    	getOutXML().close();
        super.stop();
    }
	
	public void tryFinishSMA() {
		try 
    	{
    		long timeEnd = System.nanoTime();
	    	getOutXML().writeStatistics(timeEnd - getTimeInit());
	    	getLogger().info("stoping MAS");
	    	getEnvironmentInfraTier().getRuntimeServices().stopMAS();
    	} catch (Exception e) {
    		getLogger().severe("Cannot Finish MAS! Force finish application now.");
    			System.exit(0);
    	}
	}

	public synchronized boolean initied() {
		return initied;
	}
}
