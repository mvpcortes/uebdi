// Internal action code for project TaskAlloc

package br.uff.ic.taskalloc;

import java.util.Set;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class get_other_clients extends DefaultInternalAction {

	  /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		    public int getMinArgs(){return 1;}
		    
		    @Override
		    public int getMaxArgs(){return getMinArgs();}
		    
		    @Override
		    public void checkArguments(Term[] terms)throws JasonException 
		    {
		    	
		    	if(terms[0].isVar() && !terms[0].isGround()) 
		    		throw JasonException.createWrongArgument(this,"second term is not a var not grounded");
		    }
		    
		    
		    @Override
		    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

				// execute the internal action
			    ts.getAg().getLogger().fine("executing internal action 'br.uff.ic.taskalloc.get_other_clients'");
			       
			    Set<String> coll = ts.getUserAgArch().getArchInfraTier().getRuntimeServices().getAgentsNames();
			    
			    ListTerm all = new ListTermImpl();
		        ListTerm tail = all;
			    
			    for(String s: coll)
			    {
			    	if(s.indexOf("client") >= 0)
			    	{
			    		tail = tail.append(Literal.parseLiteral(s));
			    	}
			    }
			  
		    	return un.unifies(args[0], all);
		    }

		    /*
			private Double getSuccessTaskid(Term vl) {
				if(vl.isLiteral())
	            {
	            	Literal l = (Literal)vl;
	            	if(l.getTerms().size() >= 1)
	            	{
	            		Term tn = ((Structure)vl).getTerm(0);
	            		Term tTask = (Literal)((Structure)tn).getTerm(0);
	            		return new Double(TermHelper.toDouble(tTask));
	            	}
	            }
				return null;
			}

			private Double getSuccessValue(Term vl) 
			{
				if(vl.isLiteral())
	            {
	            	Literal l = (Literal)vl;
	            	if(l.getTerms().size() >= 3)
	            	{
	            		Term tn = ((Structure)vl).getTerm(2);
	            		return new Double(TermHelper.toDouble(tn));
	            	}
	            }
				return null;
			}*/
		        
}
