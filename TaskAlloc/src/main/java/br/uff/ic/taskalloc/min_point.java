// Internal action code for project TaskAlloc

package br.uff.ic.taskalloc;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.VarTerm;

import java.util.Iterator;
import java.util.List;

import br.uff.ic.util.TermHelper;

/**
 * Procura um item de valor m�nimo usando um "visitor" de compara��o
 * formato: 	min_point(list, return);
 * @author Marcos
 *@deprecated funciona mas � o mesmo custo de fazer em jason
 */
public class min_point extends DefaultInternalAction {

	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	@Override
	  public int getMinArgs(){return 4;}
	    
	  @Override
	  public int getMaxArgs(){return getMinArgs();}
	    
	    @Override
	    public void checkArguments(Term[] terms)throws JasonException 
	    {
		if(!terms[0].isList()) 
		    throw JasonException.createWrongArgument(this,"first term is not a list");
		
	    if(!(terms[1].isVar() && ((VarTerm)terms[1]).getValue()!= null))
		    throw JasonException.createWrongArgument(this,"second term is not a unnamedvar");
	    }
	    
	    
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        // execute the internal action
        ts.getAg().getLogger().info("executing internal action 'br.uff.ic.taskalloc.min_point'");
        
        ListTerm lt = (ListTerm)args[0];
        List<Term> list = lt.getAsList();
        
        //encontra o m�nimo
        Iterator<Term> it = list.iterator();
        if(it.hasNext())
        {
        	Term minTerm = it.next();
        	Params minParams = new Params((Literal)minTerm, ts, un);
	        while(it.hasNext())
	        {
	        	Term term = it.next();
	        	Params params = new Params((Literal)term, ts, un);
	        	if(minParams.compareTo(params) > 0)
	        	{
	        		minTerm = term;
	        		minParams = params;
	        	}
	        }
	        
	        return un.unifies(args[1], minTerm.clone()); 
        }

        return un.unifies(args[1], Literal.parseLiteral("none"));
    }

  
    
    private class Params implements Comparable<Params>
    {
    	private Term agent;
    	public Term getAgent() {
			return agent;
		}

		public void setAgent(Term agent) {
			this.agent = agent;
		}

		public long getCost() {
			return cost;
		}

		public void setCost(long cost) {
			this.cost = cost;
		}

		public double getMediaFail() {
			return mediaFail;
		}

		public void setMediaFail(double mediaFail) {
			this.mediaFail = mediaFail;
		}

		public double getMediaRep() {
			return mediaRep;
		}

		public void setMediaRep(double mediaRep) {
			this.mediaRep = mediaRep;
		}

		public double getEmoInf() {
			return emoInf;
		}

		public void setEmoInf(double emoInf) {
			this.emoInf = emoInf;
		}

		private long cost;
    	private double mediaFail;
    	private double mediaRep;
    	private double emoInf;
    	
    	public Params(Literal lit, TransitionSystem ts, Unifier un)
    	{
    		setAgent(lit.getTerm(0));
    		
    		
    		setCost((long) TermHelper.getDouble(lit, 2));
    		
    		setEmoInf(TermHelper.getDouble(lit, 3));
    		
    		Unifier un2 = un.clone();
    		setMediaFail
    		(
    			getValue(String.format("mediaFail(_, %s, VALUE)", getAgent()), ts, un2)
    		);
    		
    		setMediaRep
    		(
    			getValue(String.format("mediaFail(_, %s, VALUE)", getAgent()), ts, un2)
    		);
    	}

		private double getValue(String format, TransitionSystem ts, Unifier un) {
			
			Literal lit = null;
			{
				Literal findIt = Literal.parseLiteral(format);
				Iterator<Unifier> itFindUN = findIt.logicalConsequence(ts.getAg(), un);
				Unifier itUn = itFindUN.next();
				lit = (Literal) findIt.clone();
				lit.apply(itUn);
			}
    		
    		return TermHelper.getDouble(lit, lit.getTerms().size()-1);
		}

		@Override
		public int compareTo(Params b) {
			
			double fa = this.getEmoInf() 	+ this.getMediaFail() 	+ this.getMediaRep();
			double fb = b.getEmoInf() 		+ b.getMediaFail() 		+ b.getMediaRep();
			
			double ca = this.getCost();
			double cb = b.getCost();
			
			double dif = (fa*(1.0/3.0)*ca) - (fb*(1.0/3.0)*cb);
			
			if(Math.abs(dif) < 1)
			{
				return dif>0?1:0;
			}else
			{
				return (int) dif;
			}
		}
    }
 
}
