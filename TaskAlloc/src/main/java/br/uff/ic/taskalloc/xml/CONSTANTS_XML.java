package br.uff.ic.taskalloc.xml;

import java.util.Locale;

public class CONSTANTS_XML {

	static final String STR_RELATION = "relation";
	static final String STR_ESTIMATIVE = "estimative";
	static final String STR_SUPPORT = "support";
	static final String STR_UNIT = "unit";
	static final String STR_CPU_TIME = "cpu_time";
	static final String STR_STATISTICS = "statistics";
	static final String STR_INTENSITY = "intensity";
	static final String STR_AG_NAME = "agName";
	static final String STR_EMOTION = "emotion";
	static final String STR_EMOTIONS = "emotions";
	static final String STR_COST = "cost";
	static final String STR_PROB = "prob";
	static final String STR_MONEY = "money";
	static final String STR_NAME = "name";
	static final String STR_AGENT = "agent";
	static final String STR_AGENTS = "agents";
	static final String STR_SERV_PROB = "servProb";
	static final String STR_SERV_COST = "servCost";
	static final String STR_LIFE_TIME = "lifeTime";
	static final String STR_MR = "mr";
	static final String STR_MS = "ms";
	static final String STR_MS_DATA = "ms_data";
	static final String STR_SUCCESS = "success";
	static final String STR_DEV = "dev";
	static final String STR_SERV = "serv";
	static final String STR_CLIENT = "client";
	static final String STR_ID = "id";
	static final String STR_TASK = "task";
	static final String STR_TASKALLOC = "taskalloc";
	static final String STR_OP_POINT = "optimalPoint";
	static final String STR_ACCUM_POINT = "accumPoint";
	static final String STR_POINT = "point";
	static final String STR_CANDIDATE = "candidates";
	static final String STR_CANDIDATE_COUNT = "count";
	static final String STR_MS_OTHER = "ms_other";
	static final String STR_MS_ID = "ms_other_id";
	static final String STR_MS_VALUE = "ms_other_value";
	static final Locale LOCALE = new Locale("pt", "BR");
}
