package br.uff.ic.taskalloc.emotionmodel;

import jason.asSemantics.Agent;
import jason.asSemantics.Event;
import jason.asSemantics.Unifier;
import jason.asSyntax.Literal;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Trigger;
import jason.asSyntax.Trigger.TEOperator;
import jason.bb.BeliefBase;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import br.uff.ic.uebdi.DynHelper;
import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.IEmotionDyn;
import br.uff.ic.uebdi.IIntentionBase;
import br.uff.ic.uebdi.ISetAgent;
import br.uff.ic.util.TermHelper;

/**
 * Din�mica emocional do OCC-Foo
 * 
 * @author Marcos
 * 
 */
public class EmotionDynOccFoo implements IEmotionDyn, ISetAgent {

	/**
	 * Classe que repressenta uma Classe de Emo��o do modelo OccFoo
	 * 
	 * @author Marcos
	 * 
	 */
	private enum EmotionClass {
		happy_for("happy_for"), pity("pity"), ressentment("ressentment"), gloating(
				"gloating"), not_emotion("not_emotion");

		/**
		 * Nome da classe
		 * 
		 */
		private String name;

		/**
		 * Construtor
		 * 
		 * @param _name
		 *            O nome da Classe
		 */
		EmotionClass(String _name) {
			name = _name;
		}

		/**
		 * Transforma a classe em string
		 */
		@Override
		public String toString() {
			return name;
		}
	}

	/**
	 * Refer�ncia ao agente dono desta din�mica
	 */
	private Agent ag;

	/**
	 * Refer�ncia ao Logger
	 */
	private Logger logger = null;

	/**
	 * +---+-------------------------------------------+ | | event |agent |
	 * +---+---------------+---------------------------+ |s |class(∆cost) |
	 * clienti |candidatei | +---+---------------+---------------+-----------+
	 * |0 |cheap | desire |~desire | | |fair | ~desire |~desire | | |expensive |
	 * desire |~desire | +---+---------------+---------------+-----------+ |1
	 * |cheap | ~desire |desire | | |fair | desire |desire | | |expensive |
	 * ~desire |desire | +---+-------------------------------------------+
	 * 
	 * @param cc
	 * @param s
	 */
	private EmotionClass elicitDesire(final CostClass cc, final boolean s,
			final boolean like, final boolean notLike) {
		getLogger().info(
				String.format("elicitDesire(CC:%s, S:%s, like:%s, unlike:%s)",
						cc, s, like, notLike));
		if (!s) {
			switch (cc) {
			case cheap:
			case expensive:// desire(client) & ~desire(candidate)
				if (notLike) {
					return EmotionClass.gloating;
				}
				break;
			case fair:// ~desire & ~desire
				if (like) {
					return EmotionClass.pity;
				}
				break;
			}
		} else {
			switch (cc) {
			case cheap:
			case expensive:// ~desire(client) & desire(candidate)
				if (notLike) {
					return EmotionClass.ressentment;
				}
				break;
			case fair:// desire & desire
				if (like) {
					return EmotionClass.happy_for;
				}
				break;
			}
		}

		return EmotionClass.not_emotion;
	}

	private void elicitEmotion(EmotionClass ec, String ag, IEmotionBase eb) {
		getLogger().info(String.format("elicitEmotion(%s, %s, base)", ec, ag));
		switch (ec) {
		case not_emotion:
			break;
		default:

			try {
				Literal old = null;
				Iterator<Literal> lit = eb.getAll(ec.toString(), ag);
				if(lit != null && lit.hasNext())
					old = lit.next();
				
				if (old == null) {
					getLogger().info(String.format("add emotion emotion(%s, %s)",ec.toString(), ag));
				} else {
					getLogger().info("update emotion " + old);
				}

				eb.incIntensity(getIncValue(), getStartValue(), ec.toString(), ag);
			} catch (Exception e) {
				getLogger().severe("Problem in change emotion " + e.toString());
			}

			break;
		}
	}

	@Override
	public void firstReviewEmotion(List<Literal> listPerc, IEmotionBase eb,
			IIntentionBase intentionBase) {
		getLogger().fine("first Review Emotion Function");
	}

	private Agent getAgent() {
		return ag;
	}

	private String getAgName() {
		Agent ag = getAgent();
		if (ag != null) {
			return ag.getTS().getUserAgArch().getAgName();
		} else {
			return "noname";
		}
	}

	private double getDecValue() {
		return 0.01;
	}

	static Unifier unifierVoid = new Unifier();

	private int getFairCost(int instant, BeliefBase bb) {
		int fairCost = 0;
		{
			Literal l = TermHelper.generateLiteral("fairCost", "_", "_");
			Iterator<Literal> it = bb.getCandidateBeliefs(l, unifierVoid);
			if (it != null) {
				while (it.hasNext()) {
					Literal t = it.next();

					int id = (int) TermHelper.getDouble(t, 0);
					int value = (int) TermHelper.getDouble(t, 1);
					if (id == instant && value > 0) {
						fairCost = value;
						break;
					}
				}
			}

			return fairCost;
		}
	}

	private double getIncValue() {
		return 2;
	}

	private Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(this.getClass().getName() + ":"
					+ this.getAgName());
		}
		return logger;
	}

	private double getStartValue() {
		return 3;
	}

	private boolean iLikeIt(String agName) {

		Literal lit = Literal.parseLiteral(String.format("like(%s)", agName));
		boolean test = getAgent().believes(lit, new Unifier());
		getAgent().getLogger()
				.info("test iLikeIt " + (test ? "true" : "false"));
		return test;
	}

	private boolean iNotLikeIt(String agName) {
		Literal litNot = Literal.parseLiteral(String
				.format("~like(%s)", agName));
		boolean test = getAgent().believes(litNot, new Unifier());
		getAgent().getLogger().info(
				"test iNotLikeIt " + (test ? "true" : "false"));
		return test;
	}

	@Override
	public void secondReviewEmotion(BeliefBase bb, Queue<Event> events,
			IEmotionBase eb, IIntentionBase intentionBase) {
		getLogger().fine("second Review Emotion Function");

		// processa evento
		/*
		 * H = instante S = sucesso (1) ou fracasso (0) C = valor do custo (int)
		 */
		// +event(H, S, C)
		Iterator<Event> it = DynHelper.getItEventQuery(events.iterator(),
				TEOperator.add, "event");

		while (it.hasNext()) {
			final Trigger tit = it.next().getTrigger();
			if (tit != null) {
				final Literal lit = tit.getLiteral();
				getLogger().info("event: " + lit.toString());

				@SuppressWarnings("unused")
				String myName = this.getAgent().getTS().getUserAgArch()
						.getAgName();
				String ag = TermHelper.getString(lit, 0);
				int H = (int) TermHelper.getDouble(lit, 1);
				boolean S = TermHelper.getDouble(lit, 2) > 0;
				int C = (int) TermHelper.getDouble(lit, 3);

				int fairCost = getFairCost(H, bb);

				CostClass cc = CostClass.getCostClass(C, fairCost);
				getLogger().info("CostClass: " + cc.toString());
				boolean like = iLikeIt(ag);
				boolean notLike = iNotLikeIt(ag);

				EmotionClass ec = elicitDesire(cc, S, like, notLike);

				elicitEmotion(ec, ag, eb);
			}
		}

		Iterator<Literal> itEmotions = eb.getAllEmotions();
		List<Literal> listRemove = new LinkedList<Literal>();
		List<Literal> listRemoveChange = new LinkedList<Literal>();
		List<Literal> listChange = new LinkedList<Literal>();
		while (itEmotions.hasNext()) {
			Literal lemotion = itEmotions.next();

			double intensity = TermHelper.getDouble(lemotion, 2);
			intensity -= getDecValue();
			if (intensity > 0) {
				Literal newL = (Literal) lemotion.clone();
				newL.setTerm(2, new NumberTermImpl(intensity));
				listChange.add(newL);
				listRemoveChange.add(lemotion);
			} else {
				listRemove.add(lemotion);
			}
		}

		for (Literal l : listRemove) {
			eb.delete(l);
			getLogger().info("remove emotion: " + l);
		}

		for (Literal l : listRemoveChange) {
			eb.delete(l);
		}

		for (Literal l : listChange) {
			eb.add(l);
			getLogger().fine("change emotion: " + l);
		}
	}

	@Override
	public void setAgent(Agent _ag) {
		ag = _ag;
	}

	@Override
	public boolean criticalContext(IEmotionBase eb, BeliefBase bb) {
		return true;
	}

}
