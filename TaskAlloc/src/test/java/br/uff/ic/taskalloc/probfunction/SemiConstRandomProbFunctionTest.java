package br.uff.ic.taskalloc.probfunction;

import jason.asSyntax.Literal;
import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class SemiConstRandomProbFunctionTest {

	static SemiConstRandomProbFunction ipf = new SemiConstRandomProbFunction();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ipf.init(Literal.parseLiteral("semiconst").getTerms());
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() {
		final int base = ipf.getBase();
		final int delta= ipf.getDelta();
		
		for(int i = 0; i < base; i++)
		{
			int initPos =  (i*delta);
			double prevValue = ipf.run("", initPos);
			for(int j = 1; j < delta; j++)
			{
				int pos = initPos + j;
				double value = ipf.run("", pos);
				Assert.assertEquals(prevValue, value, 1e-5);
			}
		}
	}

}
