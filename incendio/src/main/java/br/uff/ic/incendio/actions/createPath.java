// Internal action code for project FoodSimulation

package br.uff.ic.incendio.actions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.logging.Logger;

import br.uff.ic.uebdi.mapmodel.MapModel;
import br.uff.ic.util.TermHelper;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class createPath extends DefaultInternalAction {
	
	private static final Point [] VEC_POINT_MOVE =  MapModel.generateRoundPoints(1);
	
	private Logger logger = Logger.getLogger("createPath");
	@Override
	public int getMinArgs() { return 3; }
	@Override
    public int getMaxArgs() { return 3; }
    
	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
       Literal source 	= (Literal)args[0];
       Literal dest		= (Literal)args[1];
       
       VarTerm v 		= (VarTerm)args[2];
       
       try
       {
	       List<Point> list = createPath(ts, TermHelper.toPoint(source), TermHelper.toPoint(dest));
	       
	       
	       if(list == null)
	    	   return null;
	       
	       ListTerm all = new ListTermImpl();
	    	   
		   ListTerm tail = all;
		   for(Point p: list)
		   {
			   tail = tail.append(Literal.parseLiteral(String.format("pos(%d, %d)", p.x, p.y)));
	       }
	       return un.unifies(v, all);
       }catch(Exception e)
       {
    	   int i = 0;
    	  
       }
       return false;   
    }
    
	
	/**
	*
	* Retorna os vizinhos da posição x, y que são válidos
	*/
	private List<Point> getNeigbour(TransitionSystem ts, int x, int y, List<Point> list)
	{
		list.clear();
	            
		for(Point p:VEC_POINT_MOVE)
		{
			if(validPos(ts, p.x+x, p.y+y))
			{
				list.add(new Point(p.x+x, p.y+y));
			}
		}
		
		return list;
	}
		
	private  long costPos(TransitionSystem ts, int x, int y){
		Literal lit = Literal.parseLiteral(String.format("costPos(%d, %d, COST)", x, y));
		Unifier u = new Unifier();
    	ts.getAg().believes(lit, u);
    	Term t = u.get("COST");
    	
    	return TermHelper.toInteger(t)*10;
	}
    private boolean validPos(TransitionSystem ts, int x, int y)
    {
    	
    	Literal lit = Literal.parseLiteral(String.format("believeValidPos(%d, %d)", x, y));
    	boolean test = ts.getAg().believes(lit, new Unifier());
    
    	return test;
    }
    
	
    private class NodeInformation
    {
    	public Point point;
    	public long gscore = 0;
    	public long hscore = 0;
    	//public long fscore = 0;
    	
    	public Point come_from = null;
    	
    	
    	public boolean closed = false;
    	
    	public long getFScore()
    	{
    		return gscore + hscore;
    	}
    	
    	public NodeInformation(Point p, long g, long h, Point cf)
    	{
    		point = p;
    		gscore = g;
    		hscore = h;
    		come_from = cf;
    	}
    	
    	public String toString(){
    		return String.format("ni(%s, %d, %d, %d)", createPath.toString(point), gscore, hscore, getFScore());
    	}
    }
    
    private class ComparatorPoints implements Comparator<Point>
    {
		private Map<Point, NodeInformation> mapData;
		
		public ComparatorPoints(Map<Point, NodeInformation> m)
		{
			mapData = m;
		}
		
		@Override
		public int compare(Point a, Point b) {
			
			if(a.equals(b))
				return 0;
			
			NodeInformation nia = mapData.get(a);
			NodeInformation nib = mapData.get(b);
			
			if(nia == null)
			{
				if(nib == null)
					return 0;
				else
					return -1;
			}else
			{
				if(nib == null)
					return +1;
			}
			
			return (int) (nia.getFScore() - nib.getFScore());
			
		}
    	
    }
    
	List<Point> createPath(TransitionSystem ts, final Point source, final Point dest) {
		
		Map<Point, NodeInformation> mapNI= new HashMap<Point, NodeInformation>();
		
		//SortedSet<Point> openSet = new TreeSet<Point>(new ComparatorPoints(mapNI));
		PriorityQueue<Point> openQueue = new PriorityQueue<Point>(10, new ComparatorPoints(mapNI));
	
		openQueue.add((Point) source);//Inicialize Q com o nó de busca (S) como única entrada;
		
		long hce =  heuristic_cost_estimate(ts, source, dest);

		NodeInformation niFirst = new NodeInformation(source,  0, hce, null);
		mapNI.put(source, niFirst);
		
		List<Point> listNeir = new ArrayList<Point>();
		while(openQueue.size() > 0)
		{
			Point current = openQueue.poll();
			logger.fine(String.format("openPoint: %s%n", current.toString()));
			if(current.equals(dest))
				return reconstructPath(mapNI, dest, new LinkedList<Point>());

			
			NodeInformation niCurrent = mapNI.get(current); 
			niCurrent.closed = true;
			listNeir = getNeigbour(ts, current.x, current.y, listNeir);
			for(Point neighbor : listNeir)
			{
				NodeInformation niNeighbor = mapNI.get(neighbor);
				
				//está fechado
				if(niNeighbor != null){
					if(niNeighbor.closed){
						//faznada
					}else{
						long pesoCaminhoAtual = niCurrent.gscore + distBetween(current, neighbor) + costPos(ts, neighbor.x, neighbor.y);
						if(pesoCaminhoAtual < niNeighbor.gscore){
							niNeighbor.gscore = pesoCaminhoAtual;
							niNeighbor.come_from = current;
						}
					}
				}else{
					niNeighbor = new NodeInformation(neighbor, niCurrent.gscore + distBetween(neighbor, current) + costPos(ts, neighbor.x, neighbor.y), heuristic_cost_estimate(ts, neighbor, dest),current);
					mapNI.put(neighbor, niNeighbor);
					openQueue.add(neighbor);
				}
			}	
		}
		
		return null;
	}
	private long distBetween(Point current, Point neighbor) {
		if(current.x != neighbor.x)
		{
			if(current.y != neighbor.y)
				return 14;
			else
				return 10;
		}else
		{
			if(current.y != neighbor.y)
				return 10;
			else
				return 0;
		}
	}
	

	private long heuristic_cost_estimate(TransitionSystem ts, Point source, Point dest) {
		long i = (long)(source.distance(dest)*10);//+ costPos(ts, dest.x, dest.y)
	//	long i = Math.abs(source.x - dest.x) + Math.abs(source.y - dest.y)*10;
		logger.fine(String.format("heuristic_cost_estimate: %d for [%s,%s]", i, toString(source), toString(dest)));
		return i;
	}
	
	public static String toString(Point p) {
		return String.format("pos(%d, %d)", p.x, p.y);
	}
	private List<Point> reconstructPath(Map<Point, NodeInformation> mapNI, Point dest, List<Point> list) {
		
		NodeInformation ni = mapNI.get(dest);
		if(ni != null)
		{
			Point comeFrom = ni.come_from;
			if(comeFrom != null)
			{
				list = reconstructPath(mapNI, comeFrom, list);
			}			
			
			list.add(ni.point);
			return list;
		}
		return null;
	}
	
	/**
	 * createPath(SOURCE, DEST, PATH);
	 */
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		
		if(	!args[0].isLiteral())
			throw new JasonException("The first argument should be literal");
		if(	!args[1].isLiteral())
			throw new JasonException("The second argument should be literal");
		
		if(	!(args[2].isVar() && !args[2].isGround()))
			throw new JasonException("The third argument should be Var and not ground");
				
		
    }
}
