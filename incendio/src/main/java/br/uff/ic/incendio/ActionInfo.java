package br.uff.ic.incendio;

import java.util.logging.Logger;

public enum ActionInfo {
	/** (name, time_spend)*/
	MOVE("move", 1),
	PAUSE("pause", 1),
	SHOOT("shoot", 5),
	REGISTER("register", 0),
	NONE("none", 0);
	
	private static final int WAIT_TIME_SCALE = 100;

	/**
	 * name of action
	 */
	private String name;
	
	/**
	 * time spend in action
	 */
	private long timeSpend;
	
	/**
	 * constructor
	 * @param _name
	 * @param _timeSpend
	 */
	ActionInfo(String _name, int _timeSpend)
	{
		name = _name;
		timeSpend = _timeSpend;
	}
	
	public String getName()
	{
		return name;
	}
	
	public long getTimeSpend()
	{
		return timeSpend;
	}
	
	public boolean testAction(String id)
	{
		if(name != null && id != null)
			return name.equals(id);
		else
			return false;
	}

	/**
	 * Como � string o identificador, n�o usei um MAP pq o tempo de HASH seria ruim comparado ao for de poucos itens.
	 * @param actId
	 * @return
	 */
	public static ActionInfo getActionInfo(String actId) {
		if(actId == null)
			return NONE;
		for(ActionInfo ai : ActionInfo.values())
		{
			if(ai.testAction(actId))
			{
				return ai;
			}
		}
		
		return NONE;
	}

	public void simpleWait(Logger logger) {
		try
		{
			Boolean bo = new Boolean(true);
		
			synchronized(bo) 
			{
				bo.wait(WAIT_TIME_SCALE*this.getTimeSpend());
			}
		}catch(InterruptedException e)
		{
			logger.warning("The wait is interrupted. It ignore the exception and run the rest of algorithm");
		}
	}
	
}
