package br.uff.ic.incendio.entity;

import java.awt.Color;
import java.awt.Point;

import br.uff.ic.uebdi.mapmodel.MapModel;
import br.uff.ic.uebdi.mapmodel.entities.Entity;

public class Fire extends Entity {
	

	private static final Color bkgColor = Color.black;
	
	
	public static final int MAX_INTENSITY = 100;
	public static final int MIN_INTENSITY = 0;
	public static final int INITIAL_COMBUSTIVEL = 80;
	private static final double CONST_VELOCITY = 0.3;
	
	double intensity = 0;
	
	double prevIntensity = 0;
	
	double combustivel = INITIAL_COMBUSTIVEL;
	
	public final Point[] AREA_INFLUENCE = MapModel.minusPoints(MapModel.generateRoundPoints(2), new Point[]{new Point(0,0)});
	
	public double getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(double combustivel) {
		this.combustivel = combustivel;
	}

	public double getIntensity() {
		return intensity;
	}

	public void setIntensity(double intensity) {
		prevIntensity = this.intensity;
		this.intensity = Math.min(MAX_INTENSITY, Math.max(MIN_INTENSITY, intensity));
	}
	
	public double getDeltaIntensity(){
		return getIntensity() - prevIntensity;
	}

	public Fire(Point p, double i) {
		super("", p);
		setIntensity(i);
		setColor(Color.red);
	}

	public Fire(Point p) {
		this(p, 0);
	}
	
	public void simulateFire(MapModel mm){
		
		double velocity = 0;
		int count = 0;
		for(int i = 0; i < AREA_INFLUENCE.length; i++){
			Point p = AREA_INFLUENCE[i];
			int realX = this.getPoint().x + p.x;
			int realY = this.getPoint().y + p.y;
			
			if(mm.isValidPos(realX, realY)){
				Fire fire = (Fire)mm.getEntity(realX, realY, Fire.class);
				if(fire != null){
					count++;
					double diagonal = mm.getDiagonalSize();
					velocity += fire.getIntensity()*(1 - Point.distance(getPoint().x, getPoint().y, fire.getPoint().x, fire.getPoint().y)/diagonal);
				}	
			}
				
		}
		
		if(count > 0)
			velocity = CONST_VELOCITY*(velocity/count);
		else
			velocity = 0;
		
		if(velocity > 0)
		{
			if(getCombustivel() > 0){
				
				final double incValue = Math.min((double)getCombustivel(), velocity);
				setIntensity(getIntensity()+incValue);			
				setCombustivel(getCombustivel()-incValue);
			}else{
				final double newIntensity = Math.max(0.0, getIntensity()-velocity);
				setIntensity(newIntensity);
			}
		}
		
		
		final int baseRed = bkgColor.getRed();
		final int sizeRed = Color.red.getRed() - baseRed;
		final int incRed  = (int)Math.min(Color.red.getRed(), Math.round((getIntensity()/100.0)*sizeRed));
		setColor(new Color(baseRed+incRed, bkgColor.getGreen(), bkgColor.getBlue()));
		setText(String.format("%d", (int)getIntensity()));
	}
	
	public boolean showText(){return true;}
	
	public boolean visible(){return true;}
	
}
