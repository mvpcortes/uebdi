package br.uff.ic.incendio;

import jason.asSemantics.Agent;
import jason.asSemantics.Unifier;
import jason.asSyntax.ASSyntax;
import jason.asSyntax.Atom;
import jason.asSyntax.Literal;
import jason.asSyntax.Term;
import jason.asSyntax.UnnamedVar;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.uff.ic.uebdi.UnifierIterator;
import br.uff.ic.util.TermHelper;

public class Cell {

	private static final String CELL_FUNCTOR = "cell";
	private static final int LITERAL_INDEX_X = 0;
	private static final int LITERAL_INDEX_Y = 1;
	private static final int LITERAL_INDEX_AGENT = 2;
	private static final int LITERAL_INDEX_FIRE = 3;
	private static final int LITERAL_INDEX_DELTA_FIRE = 4;
	private static final int LITERAL_INDEX_WALL = 5;
	private  static final int LITERAL_INDEX_EXIT = 6;
	private  static final int LITERAL_INDEX_COUNT = 7;
	
	public static final Atom ATOM_NONE = ASSyntax.createAtom("none");
	public static final Atom ATOM_WALL = ASSyntax.createAtom("wall");
	public static final Atom ATOM_EXIT = ASSyntax.createAtom(IncendioEnvironment.STR_FUNCTOR_EXIT);
	
	
	private Point point = new Point(0,0);
	private double fire;
	private double deltaFire; //determina a variância do fogo
	
	private boolean wall;
	private boolean exit;
	private Term agent;
	
	public Cell(Literal litCell){
		point.x 	= TermHelper.getInteger(litCell, LITERAL_INDEX_X);
		point.y 	= TermHelper.getInteger(litCell, LITERAL_INDEX_Y);
		agent	 	= litCell.getTerm(LITERAL_INDEX_AGENT);
		fire		= TermHelper.getDouble(litCell, LITERAL_INDEX_FIRE);
		deltaFire	= TermHelper.getDouble(litCell, LITERAL_INDEX_DELTA_FIRE);
		wall 		= TermHelper.getBoolean(litCell, LITERAL_INDEX_WALL);
		exit 		= TermHelper.getBoolean(litCell, LITERAL_INDEX_EXIT, IncendioEnvironment.STR_FUNCTOR_EXIT);
	}
	
	public Cell(int x, int y, Term _agent, double _fire, double _deltaFire, boolean _wall, boolean _exit){
		point.x = x;
		point.y = y;
		setAgent(_agent);
		deltaFire = _deltaFire;
		fire= _fire;
		wall = _wall;
		exit = _exit;
	}
	
	public Cell(){
		point = new Point(0,0);
		fire = 0;
		wall = false;
		exit = false;
		agent = ATOM_NONE;
	}
	
	public static boolean isCell(Literal l){
		return l.getTerms().size() == LITERAL_INDEX_COUNT && CELL_FUNCTOR.equals(l.getFunctor());
	}
	public Literal toLiteral(){
		return ASSyntax.createLiteral("cell", 
				ASSyntax.createNumber(point.x),
				ASSyntax.createNumber(point.y),
				agent.clone(),
				ASSyntax.createNumber(fire),
				ASSyntax.createNumber(deltaFire),
				wall ? ATOM_WALL : ATOM_NONE,
				exit ? ATOM_EXIT : ATOM_NONE
						);
	}
	
	public Cell fromLiteral(Literal l){
		return new Cell(l);
	}
	
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point point) {
		this.point = point;
	}
	public double getFire() {
		return fire;
	}
	public void setFire(double fire) {
		this.fire = fire;
	}
	public boolean isWall() {
		return wall;
	}
	public void setWall(boolean wall) {
		this.wall = wall;
	}
	public boolean isExit() {
		return exit;
	}
	public void setExit(boolean exit) {
		this.exit = exit;
	}
	public Term getAgent() {
		return agent;
	}
	public void setAgent(Term agent) {
		this.agent = agent!=null?agent:ATOM_NONE;
	}
	
	public double getDeltaFire() {
		return deltaFire;
	}

	public void setDeltaFire(double deltaFire) {
		this.deltaFire = deltaFire;
	}
	
	private static Literal litQueryExit = null;
	public static List<Cell> getExits(final Agent agent) {
		List<Cell> cellList = new ArrayList<Cell>(3);
		if(litQueryExit == null){
			litQueryExit = ASSyntax.createLiteral(CELL_FUNCTOR,
					new UnnamedVar(),
					new UnnamedVar(),
					new UnnamedVar(),
					new UnnamedVar(),
					new UnnamedVar(),
					new UnnamedVar(),
					ATOM_EXIT
					);
		}
		//Iterator<Literal> it = bb.getCandidateBeliefs(new PredicateIndicator(CELL_FUNCTOR, LITERAL_INDEX_COUNT));
		
		Unifier un = new Unifier();
		Iterator<Unifier> itUnifier = litQueryExit.logicalConsequence(agent, un);
		Iterator<Literal> it =new UnifierIterator(itUnifier, litQueryExit);
		
		while(it.hasNext()){
			Cell cell = new Cell(it.next());
			cellList.add(cell);
		}
		
		return cellList;
		
	}
	
	
	
}
