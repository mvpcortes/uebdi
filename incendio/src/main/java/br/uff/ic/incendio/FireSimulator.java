package br.uff.ic.incendio;

import jason.stdlib.list;

import java.util.LinkedList;
import java.util.List;

import br.uff.ic.incendio.entity.Fire;
import br.uff.ic.uebdi.mapmodel.entities.Agent;

public class FireSimulator implements Runnable {

	IncendioEnvironment env;
	
	private int waitTime = 250; //em milisegundos


	public FireSimulator(IncendioEnvironment e) {
		env = e;
	}

	@Override
	public void run() {
		List<String> listAgToKill = new LinkedList<String>();
		while (env.isRunning()) {
			final IncendioModel model = (IncendioModel) env.getMapModel();
			synchronized (model) {

				for (int i = 0; i < model.getWidth(); i++)
					for (int j = 0; j < model.getHeight(); j++) {
						Fire f = (Fire) model.getEntity(i, j, Fire.class);
						if (f != null) {
							f.simulateFire(model);
							
							//mata quem esteja aqui
							model.doBurn(i,j);
							Agent a = model.getAgent(i, j);
							if(a != null && a.isDied())
								listAgToKill.add(a.getName());
								
						}
					}
			}

			env.killAgentList(listAgToKill);
			env.testAndStopMasByTimeOver();
			env.testAndStopMasByKillAgent();
			env.updatePercepts();
			env.updateView();
			synchronized(this){
				try{
					wait(waitTime);
				}catch(InterruptedException ie){}
			}
		}
	}

}
