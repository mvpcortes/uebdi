package br.uff.ic.incendio;

import jason.asSemantics.Event;
import jason.asSyntax.Literal;
import jason.bb.BeliefBase;

import java.awt.Point;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;

import br.uff.ic.incendio.entity.Fire;
import br.uff.ic.uebdi.IEmotionBase;
import br.uff.ic.uebdi.IIntentionBase;
import br.uff.ic.uebdi.MainWithConfigParams;
import br.uff.ic.uebdi.UEBDIImpl;
import br.uff.ic.uebdi.exceptions.EmotionalDynException;
import br.uff.ic.util.InvertComparator;
import br.uff.ic.util.TermHelper;

public class AgentEmoIncendio extends UEBDIImpl {
	
	private static final double DEC_INT_EMOTION = 0.02;
	private static final Literal LITERAL_QUERY_MY_POS = Literal.parseLiteral(IncendioEnvironment.STR_FUNCTOR_MY_POS + "(_,_)");
	
	private static final String STR_EMO_ESTRESSE 	= "estresse";
	private static final int MAX_ESTRESSE  = +1;
	private static final int MIN_ESTRESSE  = -1;
		
	private String agName = null;
	
	private double diagonalSize = -1.0;
	
	private int idExperiment = -1;
	
	@Override public void setLoggerData(String name, Level level){
		agName = name;
		super.setLoggerData(name, level);
	}
	
	private static double between(final double init, final double end, double value){
		if(value > end)
			return end;
		else if(value < init)
			return init;
		else
			return value;
	}
	  	
	private double getEstresse(IEmotionBase eb){
		return eb.getIntensity(STR_EMO_ESTRESSE);
	}
	
	
	private double incEstresse(IEmotionBase eb, double inc){
		double prevInt = eb.getIntensity(STR_EMO_ESTRESSE);
		double newInt = between(MIN_ESTRESSE, MAX_ESTRESSE, prevInt+inc);
		eb.changeIntensity(newInt, STR_EMO_ESTRESSE);
		return newInt;
	}
	
	
	@Override
	public void firstReviewEmotion(List<Literal> listPerc,
			IEmotionBase emotionBase, IIntentionBase intentionBase)  throws EmotionalDynException{
		
		testAndInsertInitialEmotions(emotionBase);
		
		if(listPerc == null || listPerc.isEmpty())
			return;
		
		getLogger().fine("AgentEmoIncendio:firstReviewFunction");
		Point myPos;
		{
			Literal l = listPerc.get(0);
			if(IncendioEnvironment.STR_FUNCTOR_MY_POS.equals(l.getFunctor())){
				myPos = TermHelper.toPoint(l);
			}else{ 
				l = TermHelper.contain(listPerc, Literal.parseLiteral(IncendioEnvironment.STR_FUNCTOR_MY_POS + "(_, _)"));
				if(l != null){
					myPos = TermHelper.toPoint(l);
				}else{
					myPos = new Point((int)-diagonalSize*2, (int)-diagonalSize*2);
				}
			}
					
		}
	
		double incFelicidade = 0;
		double incAngustia = 0;
		//pega o maior valor de inc
		Iterator<Literal> itLit = listPerc.iterator();
		while(itLit.hasNext()){
			Literal l = itLit.next();
			final double newIncFelicidade = avalFelicidadeEvent(emotionBase, myPos, l); 
			final double newIncAngustia = avalAngustiaEvent(emotionBase, myPos, l);
			incAngustia  = Math.max(incAngustia, newIncAngustia);
			incFelicidade = Math.max(incFelicidade, newIncFelicidade);
		}
		
		double incValueStress = incFelicidade - incAngustia;
		
		double prevEstresse = getEstresse(emotionBase);
		
		final double divValue = 1.0;
		double newEstresse = incEstresse(emotionBase, incValueStress/divValue);
		
		getLogger().info(String.format("EmotionalState:\t%.3f\t%.3f\t%.3f", incValueStress/divValue, newEstresse, prevEstresse));
		//System.out.printf("[%s]:\t%.3f\t%.3f\t%.3f%n", agName, incValueStress/10, newEstresse, prevEstresse);
	}
	

	@Override
	public void secondReviewEmotion(BeliefBase beliefBase,
			 Queue<Event> events, IEmotionBase emotionBase,
			IIntentionBase intentionBase)  throws EmotionalDynException {
		super.secondReviewEmotion(beliefBase, events, emotionBase, intentionBase);
		
		final double estresse = getEstresse(emotionBase);
		double incValue;
		if(estresse > 0){
			incValue = Math.min(-DEC_INT_EMOTION, estresse);
		}else{
			incValue = Math.max(+DEC_INT_EMOTION, estresse);
		}
		incEstresse(emotionBase, incValue);
	}
	
	private double avalAngustiaEvent(IEmotionBase emotionBase, final Point myPos, Literal l) {
		if(Cell.isCell(l)){
			Cell cell = new Cell(l);
			if(cell.getDeltaFire() > 0){
			
				double distance = cell.getPoint().distance(myPos);
				return (cell.getFire()/Fire.MAX_INTENSITY)*(1 - distance/diagonalSize);
			}
		}
		
		return 0.0;
		
	}

	private double avalFelicidadeEvent(IEmotionBase emotionBase, final Point myPos, Literal l) {
		if(Cell.isCell(l)){
			Cell cell = new Cell(l);
			if(cell.getDeltaFire() < 0){
			
				double distance = cell.getPoint().distance(myPos);
				return (cell.getFire()/Fire.MAX_INTENSITY)*(1 - distance/diagonalSize);
			}
		}
		
		return 0.0;
		
	}

	@Override
	public void perceptionEmotionFilter(IEmotionBase emotionBase, List<Literal> listSensing) {
		
		sortByImportance(listSensing);
		
		final int L = (int)Math.floor(getLValue(emotionBase)*((double)listSensing.size()));		
		
		
		//getLogger().info(String.format("L value:\t%d\t%d\t%.3f", L, listSensing.size(), getEstresse(emotionBase)));
		System.out.printf("[%d, %s] L:\t%d\t%.3f%n", idExperiment, agName, L, ((double)L)/((double)listSensing.size()));
		
		for(int i = listSensing.size()-1; i >= L; i--){
			listSensing.remove(listSensing.size()-1);
		}
	}

	private double getLValue(IEmotionBase emotionBase) {
		

		testAndInsertInitialEmotions(emotionBase);
		
		double estresse = getEstresse(emotionBase);//valor entre -1 e 1.
		
		final double sigma = 2;
		final double mi = 0;
		
		double exp = Math.exp(-sigma*Math.pow(estresse-mi,2));
		return exp;
	}

	private void testAndInsertInitialEmotions(IEmotionBase emotionBase) {
		if(!emotionBase.getAll(STR_EMO_ESTRESSE).hasNext()){ //esta operação é rápida pq o functor emotion é tratado pelo hashset do beliefbase
			emotionBase.add(0.0, STR_EMO_ESTRESSE);
			idExperiment = (int)MainWithConfigParams.queryDouble("idExperiment(_)");
		}
	}

	private void sortByImportance(List<Literal> listSensing) {
		//critérios usados para ordenação:
		/** 
		 * 	* Saída (máximo)
		 *  * Intensidade do Fogo 
		 * 	* Distância
		 * 
		 * Literal litCell = ASSyntax.createLiteral("cell", 
							ASSyntax.createNumber(i),
							ASSyntax.createNumber(j),
							agent != null ? agent.toLiteralPerception() : ASSyntax.createAtom("none"),
							ASSyntax.createNumber(fire), 
							ASSyntax.createAtom(wall ? "wall" : "none"),
							ASSyntax.createAtom(exit ? "exit" : "none"));
		 */
		
	if(diagonalSize <= 0){
		Literal literalEnv = TermHelper.contain(listSensing, Literal.parseLiteral("size(_,_)"));
		int sizeW = TermHelper.getInteger(literalEnv, 0);
		int sizeH = TermHelper.getInteger(literalEnv, 1);
		diagonalSize = Math.sqrt(sizeW*sizeW + sizeH*sizeH);
	}
	
	Literal literalMyPos = TermHelper.contain(listSensing, LITERAL_QUERY_MY_POS);
	
	final Point myPos;
	if(literalMyPos != null)
		myPos = TermHelper.toPoint(literalMyPos);
	else
		myPos = new Point(0,0);
	
		Comparator<Literal> comparator = new Comparator<Literal>() {
			
			@Override
			public int compare(Literal a, Literal b) {
				
				if(IncendioEnvironment.STR_FUNCTOR_MY_POS.equals(a.getFunctor())){
					if(IncendioEnvironment.STR_FUNCTOR_MY_POS.equals(b.getFunctor())){
						return 0;
					}else{
						return +1;
					}
				}else if(IncendioEnvironment.STR_FUNCTOR_MY_POS.equals(b.getFunctor())){
					return -1;
				}
				
				final boolean isCellA =Cell.isCell(a);
				final boolean isCellB =Cell.isCell(b);
				
				if(!isCellA){
					if(!isCellB){
						return 0;
					}else{
						return +1;
					}
				}else if(!isCellB){
					return -1;
				}
				
				Cell cellA = new Cell(a);
				Cell cellB = new Cell(b);
				
				
				if(cellA.isExit()){
					if(cellB.isExit()){
						return 0;
					}else{
						return +1;
					}
				}else if(cellB.isExit()){
					return -1;
				}
				
				final double distanceA = cellA.getPoint().distance(myPos);
				final double distanceB = cellB.getPoint().distance(myPos);
				final double termA = distanceA*cellA.getDeltaFire();
				final double termB = distanceB*cellB.getDeltaFire();

				return Double.compare(termA, termB);
				
			}
			
		};
		
		Collections.sort(listSensing, new InvertComparator<Literal>(comparator));
	}

}
