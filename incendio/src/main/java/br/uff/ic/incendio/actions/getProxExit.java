// Internal action code for project Incendio

package br.uff.ic.incendio.actions;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import br.uff.ic.incendio.Cell;
import br.uff.ic.incendio.IncendioEnvironment;
import br.uff.ic.uebdi.WrapperBeliefBase;
import br.uff.ic.util.TermHelper;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class getProxExit extends DefaultInternalAction {

	private Logger logger = Logger.getLogger("getProxExit");

	@Override
	public int getMinArgs() {
		return 2;
	}

	@Override
	public int getMaxArgs() {
		return 2;
	}

	private static Literal cellExitQuery;
	private static List<Point> listExits = null;
	static {
		Cell cell = new Cell(-1, -1, null, 0, 0, false, true);
		cellExitQuery = cell.toLiteral();
		listExits = new ArrayList<Point>(4);
	}

	@Override
	public Object execute(TransitionSystem ts, Unifier un, Term[] args)
			throws Exception {

		synchronized(listExits){
			if (listExits.isEmpty()) {
				List<Cell> listCell = Cell.getExits(ts.getAg());
				for (Cell cell : listCell) {
					listExits.add(cell.getPoint());
				}
			}
		}

		if (listExits == null || listExits.size() == 0)
			return null;

		Iterator<Literal> itMyPos = ts
				.getAg()
				.getBB()
				.getCandidateBeliefs(
						new PredicateIndicator(
								IncendioEnvironment.STR_FUNCTOR_MY_POS, 2));

		if (itMyPos == null || !itMyPos.hasNext()) {
			return null;
		}

		Literal litMyPos = itMyPos.next();
		Point myPos = TermHelper.toPoint(litMyPos);

		Point nearPoint = listExits.get(0);
		double minDistance = myPos.distance(nearPoint);
		for (Point p : listExits) {
			double distance = myPos.distance(p);
			if (distance < minDistance) {
				minDistance = distance;
				nearPoint = p;
			}
		}

		return un.unifies(args[0], ASSyntax.createNumber(nearPoint.x))
				&& un.unifies(args[1], ASSyntax.createNumber(nearPoint.y));
	}
}
