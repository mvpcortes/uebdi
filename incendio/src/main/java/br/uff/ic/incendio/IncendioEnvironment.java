package br.uff.ic.incendio;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;
import jason.environment.Environment;

import java.awt.Point;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.uff.ic.incendio.entity.AgentIcendio;
import br.uff.ic.incendio.entity.Fire;
import br.uff.ic.uebdi.mapmodel.Direction;
import br.uff.ic.uebdi.mapmodel.MapModel;
import br.uff.ic.uebdi.mapmodel.Viewer;
import br.uff.ic.uebdi.mapmodel.entities.Agent;
import br.uff.ic.uebdi.mapmodel.entities.Entity;
import br.uff.ic.uebdi.mapmodel.entities.Exit;
import br.uff.ic.uebdi.mapmodel.entities.Wall;
import br.uff.ic.util.TermHelper;

/**
 * Tempo total de simula��o e seu desvio padr�o; N�mero total de passos de
 * simula��o e seu desvio padr�o; N�mero total de agentes mortos na simula��o e
 * seu desvio padr�o;
 * 
 * @author Marcos
 * 
 */
public class IncendioEnvironment extends Environment {

	public static final String STR_FUNCTOR_MY_POS = "myPos";

	private static final String STR_LOG = "log";

	public static final String STR_POS_OF_AGENTS = "posOfAgents";

	public static final String STR_SET_FIRE = "setFire";
	public static final String STR_FUNCTOR_EXIT = "exit";
	public static final String STR_ADD_FIRE = "addExit";

	private static final Object STR_MAX_SIM_TIME = "maxSimulationTime";

	public static final String LITERAL_SIZE_FUNCTOR = "size";

	public static final String STR_FUNCTOR_STEP_FIRE = "stepFire";
	

	// =====================================================

	// Modelo
	MapModel mapModel;

	Class<MapModel> mapModelClazz = MapModel.class;

	// em milisegundos
	private long initTimeEnvironment = 0;
	// em milisegundos
	private long maxSimulationTime = Long.MAX_VALUE;

	// view
	private Viewer view;

	private Literal literalEnvSize = null;

	private FireSimulator fireSimulator;
	

	// ==============================================================
	public void setMapModel(MapModel mapModel) {
		this.mapModel = (IncendioModel) mapModel;
	}

	public MapModel getMapModel() {
		return mapModel;
	}

	// ==============================================================

	// ==============================================================
	/** Called before the MAS execution with the args informed in .mas2j */
	@SuppressWarnings("unchecked")
	@Override
	public void init(String[] args) {
		String[] temp = { "2" };
		super.init(temp);
		List<Literal> listProp = parseProp(args);

		List<Fire> listIntensityFire = new LinkedList<Fire>();
		List<Point> listExit = new LinkedList<Point>();

		int size = 10;
		boolean showGUI = false;
		for (Literal l : listProp) {
			final String functor = l.getFunctor();
			if ("size".equals(functor)) {
				size = TermHelper.getInteger(l, 0);
			} else if ("mapModel".equals(functor)) {
				try {
					mapModelClazz = (Class<MapModel>) Class.forName(TermHelper
							.getString(l, 0));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			} else if ("showGUI".equals(functor)) {
				showGUI = true;
			} else if (STR_LOG.equals(functor)) {
				setLoggerOutput(l);
			} else if (STR_MAX_SIM_TIME.equals(functor)) {
				maxSimulationTime = Math
						.round(TermHelper.getDouble(l, 0) * 1000);// em segundos
				getLogger().info(
						"Setting maxSimulationTime = " + maxSimulationTime
								+ "ms");
			} else if (STR_SET_FIRE.equals(functor)) {
				int x = TermHelper.getInteger(l, 0);
				int y = TermHelper.getInteger(l, 1);
				int intensity = TermHelper.getInteger(l, 2);

				Fire f = new Fire(new Point(x, y));
				f.setIntensity(intensity);
				listIntensityFire.add(f);

			} else if (STR_ADD_FIRE.equals(functor)) {
				int x = TermHelper.getInteger(l, 0);
				int y = TermHelper.getInteger(l, 1);

				listExit.add(new Point(x, y));

			}

		}

		try {
			setMapModel(mapModelClazz.getConstructor(Integer.TYPE).newInstance(
					size));

		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}

		for (Point p : listExit) {
			((IncendioModel) getMapModel()).putExit(p);
		}

		for (Fire f : listIntensityFire) {
			Fire destFile = (Fire) getMapModel().getEntity(f.getPoint().x,
					f.getPoint().y, Fire.class);
			destFile.setIntensity(f.getIntensity());
		}

		literalEnvSize = Literal.parseLiteral(String.format("%s(%d, %d)",
				LITERAL_SIZE_FUNCTOR, getMapModel().getWidth(), getMapModel()
						.getHeight()));

		fireSimulator = new FireSimulator(this);
		Thread t = new Thread(fireSimulator);
		t.start();
		initTimeEnvironment = System.currentTimeMillis();
		// show view
		if (showGUI) {
			view = new Viewer(getMapModel(), "Viewer", 360);
			view.setVisible(true);
		}
		
	}

	private List<Literal> parseProp(String[] args) {
		List<Literal> array = new ArrayList<Literal>(args.length);
		for (String s : args) {
			Literal l = Literal.parseLiteral(s);
			if (l != null) {
				array.add(l);
			}
		}

		return array;
	}

	/**
	 * This method receive a literal and parse it to set the log output.<br/>
	 * The literal is the form {@value #STR_LOG}(file, limit, count)
	 * 
	 * @param param
	 *            the literal
	 */
	private void setLoggerOutput(Literal param) {
		String fileName = TermHelper.getString(param, 0);

		String strLevel = TermHelper.getString(param, 1);
		strLevel = strLevel.toUpperCase();
		Level level = Level.INFO;
		if (!strLevel.equals("")) {
			try {
				level = Level.parse(strLevel);
			} catch (Exception e) {
				level = Level.INFO;
			}
		}

		int limit = (int) TermHelper.getDouble(param, 2);
		if (limit <= 0)
			limit = 1000000000;

		int count = (int) TermHelper.getDouble(param, 3);
		if (count <= 0)
			count = 1;

		try {
			FileHandler fh = new FileHandler(fileName, limit, count, false);
			fh.setLevel(level);

			// =============================
			Handler[] hs = Logger.getLogger("").getHandlers();
			for (int i = 0; i < hs.length; i++) {
				Logger.getLogger("").removeHandler(hs[i]);
			}
			Logger.getLogger("").addHandler(fh);

		} catch (SecurityException e) {
			getLogger()
					.severe(String
							.format("Cannot add new FileHandler (\"%s\") because the Secutity Exeption (%s)",
									fileName, e.toString()));
		} catch (IOException e) {
			getLogger()
					.severe(String
							.format("Cannot add new FileHandler (\"%s\") because the IOException (%s)",
									fileName, e.toString()));
		}
	}

	/**
	 * Retorna o nome dos agentes
	 * 
	 * @return
	 */
	protected Set<String> getAgNames() {
		return getEnvironmentInfraTier().getRuntimeServices().getAgentsNames();
	}

	@Override
	public boolean executeAction(String agName, Structure action) {
		// verifica se está parando

		if (!isRunning()) {
			getLogger()
					.info(String
							.format("Cannot execute action %s for agent %s because the MAS is stoping",
									action, agName));
			return false;
		}

		AgentIcendio agent = (AgentIcendio) mapModel.getAgent(agName);
		if (agent != null) {
			if (agent.isDied()) {
				getLogger()
						.info(String
								.format("Cannot execute action %s for agent %s because the agent is died.",
										action, agName));
				killAgent(agName);
				return false;
			}

			if (agent.isSave()) {
				getLogger()
						.info(String
								.format("Cannot execute action %s for agent %s because the agent is saved.",
										action, agName));
				killAgent(agName);
				return false;
			}
		}

		getLogger().info(String.format("executing: %s to %s", action, agName));
		String actId = action.getFunctor();
		ActionInfo ai = ActionInfo.getActionInfo(actId);
		boolean ok = false;
		try {
			switch (ai) {
			case MOVE: {
				if (agent != null) {
					if (action.getTerms().size() > 1) {
						final double mx = TermHelper.getDouble(action, 0);
						final double my = TermHelper.getDouble(action, 1);
						ok = getMapModel().move(agName, (int) Math.round(mx),
								(int) Math.round(my));
					} else {
						Direction dir = Direction.fromTerm(action.getTerm(0));
						ok = getMapModel().move(agName, dir);

					}
					if (ok) {
						ai.simpleWait(getLogger());
						if (agent != null && (agent.isDied() || agent.isSave())) {
							killAgent(agent.getName());
						}
					} else {
						getLogger()
								.warning(
										String.format(
												"the model reject the action %s of agent %s",
												actId, agName));
					}
				} else {
					getLogger().warning(
							String.format("Agent %s not registed", agName));
					ok = false;
				}

			}
				break;
			case REGISTER:
				if (agent == null) {
					mapModel.setInit(true);
					agent = new AgentIcendio(agName, new Point());
					getLogger().info(
							String.format("Registering agent %s", agName));
					if (action.getTerms().size() > 0) {
						Point p = TermHelper.getPoint(action, 0);
						agent.setPoint(p);
						getMapModel().registerAgent(agent);
					} else {
						getMapModel().registerAgentRandomPos(agent);
					}
					ok = true;
				} else {
					getLogger().info(
							String.format("Agent %s already registed", agName));
					ok = false;
				}
				break;
			default:
				getLogger().severe("Invalid action: " + ai.getName());
				ok = false;
			}
		} catch (Exception e) {
			getLogger().severe(
					"unknow exception in TiroteioEnvironment.action: "
							+ e.toString());
			ok = false;
		}

//		if (ok) {
//			testAndStopMasByTimeOver();
//
//			testAndStopMasByKillAgent();
//
//			updatePercepts();
//
//			updateView();
//		}

		return ok;
	}

	public List<Literal> getPercepts(String agName){
		synchronized (getMapModel()) {
			return super.getPercepts(agName);			
		}
	}
	protected void updatePercepts() {
		synchronized (getMapModel()) {
			clearPercepts();
			getLogger().fine("init updatePercepts");

			addPercept(literalEnvSize);

			for (int i = 0; i < getMapModel().getWidth(); i++)
				for (int j = 0; j < getMapModel().getHeight(); j++) {
					Iterator<Entity> it = getMapModel().getEntities(i, j);
					double fire = 0;
					double deltaFire = 0;
					Agent agent = null;
					boolean wall = false;
					boolean exit = false;

					while (it != null && it.hasNext()) {
						Entity e = it.next();
						if (e instanceof Agent) {
							agent = (Agent) e;

						} else if (e instanceof Wall) {
							wall = true;
						} else if (e instanceof Fire) {
							Fire _fire = (Fire) e;
							fire = _fire.getIntensity();
							deltaFire = _fire.getDeltaIntensity();
						} else if (e instanceof Exit) {
							exit = true;
						}
					}

					if (agent != null) {
						// myPos
						Point pos = agent.getPoint();
						Literal lpos = ASSyntax.createLiteral(
								STR_FUNCTOR_MY_POS,
								ASSyntax.createNumber(pos.x),
								ASSyntax.createNumber(pos.y));
						addPercept(agent.getName(), lpos);
					}

					final Term termAgent = ((agent != null) ? agent
							.toLiteralPerception() : Cell.ATOM_NONE);

					Cell litCell = new Cell(i, j, termAgent, fire, deltaFire,
							wall, exit);
					addPercept(litCell.toLiteral());
				}
		}
	}

	public void updateView() {
		if (view != null) {
			view.update();
		}

	}

	private long getMaxSimulationTime() {
		return maxSimulationTime;
	}

	/**
	 * Retorna quanto tempo passou desde o inicio do ambiente
	 * 
	 * @return
	 */
	private long getTimeCount() {
		return System.currentTimeMillis() - initTimeEnvironment;
	}

	private int printStopCount = 0;

	public void testAndStopMasByKillAgent() {
		synchronized (getMapModel()) {
			if (!isRunning())
				return;
			
				Iterable<Agent> coll = getMapModel().getAgents();
				int saved = 0;
				int died = 0;
				int totalActions = 0;
				double timeCount;
				for (Agent a : coll) {
					AgentIcendio ai = (AgentIcendio) a;
					if (ai.isSave())
						saved++;
					if (ai.isDied())
						died++;

					totalActions += ai.getActionCount();
				}
				
				final int registeredAgents = getMapModel().getRegisteredAgentCount();

				if(registeredAgents > 0 && (saved + died >= registeredAgents)){
					// timeCount
					{
						long tt = getTimeCount();
						timeCount = ((double) tt) / 100;
					}
	
					printStopCount++;
					getLogger().info("exit because all agent killed.");
					System.out.println(
					// String.format("%d: saved(%d)\tdied(%d)\ttimeCount(%.3f, seconds), totalActions(%d)",
					// printStopCount, saved, died, timeCount, totalActions)
							String.format("%d:\t%d\t%d\t%.3f\t%d", printStopCount,
									saved, died, timeCount, totalActions));
					getMapModel().setTimeOver(true);
					stopMAS();
				}
		}
	}

	public void testAndStopMasByTimeOver() {

		synchronized (getMapModel()) {
			if (!isRunning())
				return;
			if ((getTimeCount() > getMaxSimulationTime())) {
				getLogger().info("exit because time over.");
				getMapModel().setTimeOver(true);
				stopMAS();
			}
		}
	}

	private void stopMAS() {
		if (!isRunning())
			return;
		synchronized (getMapModel()) {

			try {
				getMapModel().finishSimulation();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
			try {
				this.getEnvironmentInfraTier().getRuntimeServices().stopMAS();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void killAgent(String name) {
		getEnvironmentInfraTier().getRuntimeServices().killAgent(name);
	}

	public void killAgentList(List<String> listAgToKill) {
		for(String agName: listAgToKill){
			getEnvironmentInfraTier().getRuntimeServices().killAgent(agName);
		}
		
	}


}
