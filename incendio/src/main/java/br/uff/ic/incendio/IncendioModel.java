package br.uff.ic.incendio;

import java.awt.Point;
import java.util.Iterator;

import br.uff.ic.incendio.entity.AgentIcendio;
import br.uff.ic.incendio.entity.Fire;
import br.uff.ic.uebdi.mapmodel.Direction;
import br.uff.ic.uebdi.mapmodel.MapModel;
import br.uff.ic.uebdi.mapmodel.entities.Entity;
import br.uff.ic.uebdi.mapmodel.entities.Exit;

public class IncendioModel extends MapModel {

	public IncendioModel(int size) {
		super(size);
		for (int x = 0; x < this.getWidth(); x++) {
			for (int y = 0; y < this.getHeight(); y++) {
				this.addEntity(new Fire(new Point(x, y)), x, y);
			}
		}
	}

	@Override
	public synchronized boolean move(String strAg, Direction dir) {
		if (super.move(strAg, dir)) {
			AgentIcendio a = (AgentIcendio)this.getAgent(strAg);
			Point pos = a.getPoint();
			doBurn(pos.x, pos.y);
			doExit(pos);
			a.incActionCount(1);
			return true;
		}

		return false;

	}

	private void doExit(Point pos) {
		Exit exit = (Exit)this.getEntity(pos.x, pos.y, Exit.class);
		if(exit!=null){
			Iterator<Entity> it = this.getEntities(pos.x, pos.y);
			while (it.hasNext()) {
				Entity e = it.next();
				if (e instanceof AgentIcendio) {
					AgentIcendio a = (AgentIcendio)e;
					a.setSave(true);
				}
			}
		}
	}

	public Fire getFire(int x, int y) {
		return (Fire) this.getEntity(x, y, Fire.class);
	}

	public void doBurn(int i, int j) {
		Iterator<Entity> it = this.getEntities(i, j);
		while (it.hasNext()) {
			Entity e = it.next();
			if (e instanceof AgentIcendio) {
				AgentIcendio a = (AgentIcendio)e;
				if (a != null && !a.isSave() && !a.isDied()) {
					Fire fire = getFire(i, j);

					if (fire.getIntensity() > 50) {
						a.setDied(true);
					}
				}
			}
		}
	}

	public void putExit(Point point) {
		Exit e = (Exit) this.getEntity(point.x, point.y, Exit.class);
		if (e == null)
			this.addEntity(new Exit(point), point.x, point.y);
	}

}
