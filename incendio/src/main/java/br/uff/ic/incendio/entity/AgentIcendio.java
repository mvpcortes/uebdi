package br.uff.ic.incendio.entity;

import java.awt.Color;
import java.awt.Point;
import java.util.logging.Logger;

import  br.uff.ic.uebdi.mapmodel.entities.Agent;



/**
 * Agent model class
 * @author mcortes
 *
 */
public class AgentIcendio extends Agent {
	
	private boolean save = false;
	private int actionCount = 0;
	
	private Logger logger;
	
	public AgentIcendio(String n, Point p) {
		super(n, p, true);
		setText(" ");
		setColor(Color.white);
		logger = Logger.getLogger(getName() + getClass().getSimpleName());
	}
	
	@Override
	public void setDied(boolean yes){
		if(save){
			logger.warning("Cannot set died a saved agent!");
		}else{
			setColor(Color.black);
			super.setDied(yes);
		}
		
	}
	
	@Override 
	public boolean isColised(){
		return false;
	}

	public void setSave(boolean b) {
		setColor(Color.yellow);
		save = true;
	}
	
	public boolean isSave(){
		return save;
	}

	public int getActionCount() {
		return actionCount;
	}
	public void incActionCount(int actions) {
		this.actionCount += actions;
	}
}
