package br.uff.ic.incendio.actions;
//Internal action code for project FoodSimulation

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import br.uff.ic.util.TermHelper;
import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class createLookPath extends DefaultInternalAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6694274690106808751L;

	@Override
	public int getMinArgs() { return 3; }
	@Override
    public int getMaxArgs() { return 3; }
    
	/**
	 * createLookPath(SOURCE, PATH);
	 */
	@Override
    protected void checkArguments(Term[] args) throws JasonException {
		super.checkArguments(args);
		
		if(	!args[0].isLiteral())
			throw new JasonException("The first argument should be literal");
		
		if(	!args[1].isNumeric())
			throw new JasonException("The second argument should be numeric");
		
		if(	!args[2].isNumeric())
			throw new JasonException("The third argument should be numeric");
		
		if(	!args[3].isNumeric())
			throw new JasonException("The fourth argument should be numeric");

		if(	!(args[4].isVar() && !args[4].isGround()))
			throw new JasonException("The fifth argument should be Var and not ground");
				
		
    }
	
	
	public enum MovePathState
	{
		START(0,0),
		TOP(0,-1),
		LEFT(-1,0),
		RIGHT(+1,0),
		BOTTOM(0,+1),
		END(Integer.MAX_VALUE, Integer.MAX_VALUE);
		
		int x;
		int y;
		
		MovePathState(int _x, int _y)
		{
			x = _x;
			y = _y;
		}
		
		Point change(Point p)
		{
			p.x+= x;
			p.y+=y;
			return p;
		}
		
		Point unchange(Point p)
		{
			p.x-=x;
			p.y-=y;
			return p;			
		}
	}
	
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
	  Literal source 	= (Literal)args[0];
	  int windowSize    = TermHelper.toInteger(args[1]);
	  int envWidth		= TermHelper.toInteger(args[2]);
	  int envHeight     = TermHelper.toInteger(args[3]);
      Literal v			= (VarTerm)args[4];
      
      
      int wCount = envWidth/windowSize;
      
      int hCount  = envHeight/windowSize;
      
      //Point[] vecP = (Point[])Array.newInstance(Point.class, (wCount*hCount)-1);
      List<Point> listP = new LinkedList<Point>();
     
      int initX = TermHelper.getInteger(source, 0)/windowSize;
      int initY = TermHelper.getInteger(source, 1)/windowSize;
      
      Point actual = new Point(initX, initY);
      
      MovePathState state = MovePathState.START;
      
      boolean afterMeioX = initX > (wCount/2);
      
      while(state != MovePathState.END)
      {
    	  switch(state)
    	  {
    	  case START:
    		  if(afterMeioX)
    		  {
    			  state = testTopLeft(wCount, hCount, actual);
    		  }else
    		  {
    			  state = testRightBottom(wCount, hCount, actual);
    		  }
    		  break;
    	  case TOP:
    		  	MovePathState.TOP.change(actual);
    		  	listP.add((Point) actual.clone());
    		  	state = testTopLeft(wCount, hCount, actual);
    		  break;
    	  case LEFT:
			  	MovePathState.LEFT.change(actual);
			  	listP.add((Point) actual.clone());
			  	state = testTopLeft(wCount, hCount, actual);
    		  break;
    	  case RIGHT:
			  	MovePathState.RIGHT.change(actual);
		  	 	listP.add((Point) actual.clone());
				state = testRightBottom(wCount, hCount, actual);
    		  break;
    	  case BOTTOM:
	    		MovePathState.BOTTOM.change(actual);
	  		  	listP.add((Point) actual.clone());
				state = testRightBottom(wCount, hCount, actual);
    		  break;
    	  case END:
    		  
    		  break;
    	  }
      }
    
      //
      StringBuilder sb = new StringBuilder();
      sb.append("map: \n");
      for(int i = 0; i < envWidth; i++)
      {
    	 String line = "";
    	  for(int j = 0; j < envHeight; j++)
    	  {
    		  Point pTemp = new Point(i,j);
    		  int pos = -1;
    		  if((pos = find(listP, pTemp, windowSize)) > 0)
    		  {
    			  line += Integer.toString(pos );
    		  }else
    		  {
    			  line += "_";
    		  }
    	  }
    	  sb.append(line);
    	  sb.append("\n");
      }
      
      ts.getLogger().info(sb.toString());
      ListTerm all = new ListTermImpl();
	   
	   ListTerm tail = all;
	   for(Point p: listP)
	   {
		   tail = tail.append(Literal.parseLiteral(String.format("pos(%d, %d)", p.x*windowSize + (windowSize/2), p.y*windowSize + (windowSize/2))));
       }
       return un.unifies(v, all);
    }
	private MovePathState testRightBottom(int wCount, int hCount, Point actual) {
		MovePathState state;
		if(canMoveTo(actual, MovePathState.BOTTOM, wCount, hCount))
			state = MovePathState.BOTTOM;
		else
		{
			if(canMoveTo(actual, MovePathState.LEFT, wCount, hCount))
		  		state = MovePathState.LEFT;
			else
				state = MovePathState.END;
		}
		return state;
	}
	
	private MovePathState testTopLeft(int wCount, int hCount, Point actual) {
		MovePathState state;
		if(canMoveTo(actual, MovePathState.TOP, wCount, hCount))
			state = MovePathState.TOP;
		else
		{
			if(canMoveTo(actual, MovePathState.RIGHT, wCount, hCount))
		  		state = MovePathState.RIGHT;
			else
				state = MovePathState.END;
		}
		return state;
	}
    
	private static boolean canMoveTo(Point actual, MovePathState dir, int wCount, int hCount) {
		int x = actual.x + dir.x;
		int y = actual.y + dir.y;
		return (x >= 0) && (x < wCount) && (y >= 0) && (y < hCount);
	}
	
	private int find(List<Point> vecP, Point pTemp, int ws) {
		for(int i = 0; i < vecP.size(); i++)
		{
			Point p = vecP.get(i);
			Point pp = new Point(p.x*ws + (ws/2), p.y*ws + (ws/2));
			if(pTemp.equals(pp))
				return i;
		}
		return -1;
	}
}
