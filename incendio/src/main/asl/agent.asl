// Agent agent in project Incendio

/* Initial beliefs and rules */
believeValidPos(X, Y):-cell(X, Y, none, _,_,_,_).
believeValidPos(X, Y):- size(W, H) & X >= 0 & Y >= 0 & X < W & Y < H.

costPos(X, Y, COST):-cell(X, Y, _, COST,_, _, _).
costPos(X, Y, 0):-size(W, H) & X >= 0 & Y >= 0 & X < W & Y < H.
costPos(X, Y, 100000000000).
/* Initial goals */

!start.

/* Plans */

+!start : initialPos(X, Y) <- 
	register(pos(X, Y));
	!!scape;
.

+!start:true<-
	register;
	!!scape;
	.

+!scape:myPos(MX, MY) & br.uff.ic.incendio.actions.getProxExit(EX, EY)<-
	.log.log(info, "Try scape");
	if(br.uff.ic.incendio.actions.createPath(pos(MX, MY), pos(EX, EY), PATH)){
		PATH = [X|PATH_TAIL];
		.log.log(info, "PATH_TAIL: ", PATH_TAIL);
		!varrer(PATH_TAIL);
	}else{
		.log.log(info, "fail to create path");
		!!scape;
	}
. 

-!scape:true<-
	!!scape;
	.
	
@varrer_chegou
+!varrer(_):myPos(X, Y) & cell(X, Y, _, _,_, _, exit)<-
	.log.log(info, "chegou na saida" , pos(X , Y));
	!scape;
.

@varrer_mesmo_lugar
+!varrer([pos(X, Y)|PATH_TAIL]):myPos(X, Y)<-
	.log.log(info, "Já está nesta está na posicao, pular movimento" , pos(X , Y), ". lista sobrando: ", PATH_TAIL);
	!varrer(PATH_TAIL);
.
@varrer_main
+!varrer([pos(X, Y)|PATH_TAIL]):true<- 
	for(.member(pos(NX, NY), PATH_TAIL)){
		if(cell(X, Y, _, FIRE,_, _, _) & FIRE >= 40){
			.log.log(info, "reconsiderando path pq achou fogo alto");
			!!scape; //reconsidera
			.fail;
		}
	}
	
	if(cell(X, Y, _, FIRE,_, _, _) & FIRE >= 50){
		.log.log(info, "reconsiderando path pq achou fogo alto");
		!!scape; //reconsidera
	}else{
		move(X, Y);
		!!varrer(PATH_TAIL);
	}
	.	
	
@varrer_acabou_path
+!varrer([])<-
	.log.log(info, "finish path");
	!!scape;
	.
	
@varrer_fail
-!varrer(_)<-
	.log.log(info, "fail varrer");
	!!scape;
	.
	