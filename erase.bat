@ECHO OFF

@setlocal

REM Edit this to indicate what the root directory is for both eclipse and eclipseThirdParty.
REM If you have some other arrangement, edit the four SET commands.
SET WORK_ROOT=D:\Softwares

SET ECLIPSE_HOME=c:\Arquivos de Programas\basic_eclipse
SET CONFIG_HOME=D:\eclipseData\configuration
SET WRK_SPACE_HOME=D:\UFF-Mestrado\project\workspace
SET RUN_WRK_SPACE_HOME=D:\UFF-Mestrado\project\workspace\runtime-workspace-dashboard

SET DIR_DEL_CMD=rmdir /s /q
SET FILE_DEL_CMD=del /f /s /q

%FILE_DEL_CMD% %ECLIPSE_HOME%\velocity.log

%DIR_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\.manager
%DIR_DEL_CMD% %CONFIG_HOME%\.settings
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\.contributions*.*
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\.extradata*.*
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\.maindata*.*
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\.orphans*.*
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\.registry*.*
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\.table*.*
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.core.runtime\registry*.new
%DIR_DEL_CMD% %CONFIG_HOME%\org.eclipse.help.base
%DIR_DEL_CMD% %CONFIG_HOME%\org.eclipse.osgi
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.update\*.lock
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.update\*.log
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.update\last.config.stamp
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.update\platform.xml
%FILE_DEL_CMD% %CONFIG_HOME%\org.eclipse.update\registry
%FILE_DEL_CMD% %CONFIG_HOME%\*.log
%DIR_DEL_CMD% %CONFIG_HOME%\org.eclipse.update\history

%FILE_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.lock
%FILE_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.log
%FILE_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.bak_*.log
%FILE_DEL_CMD% %WRK_SPACE_HOME%\.metadata\version.ini
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\com.ibm.sample.zip.creation
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.ajdt.core
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.core.resources\.history
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.core.resources\.root\.indexes
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.debug.core
%FILE_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.debug.ui\launchConfigurationHistory.xml
%FILE_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.jdt.debug.ui\stackTraceConsole.txt
%FILE_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.jdt.ui\TypeInfoHistory.xml
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.jst.jsp.core
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.help.base
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.pde.core
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.target
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.team.core
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.team.cvs.core
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.tomcat
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.update.core
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.wst.server.core
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.wst.validation
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.wst.ws.explorer

%FILE_DEL_CMD% %RUN_WRK_SPACE_HOME%\.metadata\.lock
%FILE_DEL_CMD% %RUN_WRK_SPACE_HOME%\.metadata\.log
%DIR_DEL_CMD% %RUN_WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.core.resources

REM loop through all the project directories and delete certain files
REM DO NOT DELETE .markers - since that file holds info abt the project's build path, etc
for /R %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.core.resources\.projects %%c in (.syncinfo .syncinfo.snap *.tree) do %FILE_DEL_CMD% %%c
for /R %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.core.resources\.projects %%c in (org.eclipse.jdt.core) do %DIR_DEL_CMD% %%c

REM loop through all the plugin folders and delete the dialog size preference files
REM This has been commented out since the synch info is being stored in one of these files
REM for /R %WRK_SPACE_HOME%\.metadata\.plugins %%c in (dialog_settings.xml) do %FILE_DEL_CMD% %%c

REM THIS MAKES YOUR STARTUP TIME GO UP IF YOU HAVE ANY OPEN PROJECTS
%DIR_DEL_CMD% %WRK_SPACE_HOME%\.metadata\.plugins\org.eclipse.jdt.core
